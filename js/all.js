(function() {
  var a, b, c, d, e, f = function(a, b) {
      return function() {
        return a.apply(b, arguments)
      }
    },
    g = [].indexOf || function(a) {
      for (var b = 0, c = this.length; c > b; b++)
        if (b in this && this[b] === a) return b;
      return -1
    };
  b = function() {
    function a() {}
    return a.prototype.extend = function(a, b) {
      var c, d;
      for (c in b) d = b[c], null == a[c] && (a[c] = d);
      return a
    }, a.prototype.isMobile = function(a) {
      return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(a)
    }, a.prototype.createEvent = function(a, b, c, d) {
      var e;
      return null == b && (b = !1), null == c && (c = !1), null == d && (d = null), null != document.createEvent ? (e = document.createEvent("CustomEvent"), e.initCustomEvent(a, b, c, d)) : null != document.createEventObject ? (e = document.createEventObject(), e.eventType = a) : e.eventName = a, e
    }, a.prototype.emitEvent = function(a, b) {
      return null != a.dispatchEvent ? a.dispatchEvent(b) : b in (null != a) ? a[b]() : "on" + b in (null != a) ? a["on" + b]() : void 0
    }, a.prototype.addEvent = function(a, b, c) {
      return null != a.addEventListener ? a.addEventListener(b, c, !1) : null != a.attachEvent ? a.attachEvent("on" + b, c) : a[b] = c
    }, a.prototype.removeEvent = function(a, b, c) {
      return null != a.removeEventListener ? a.removeEventListener(b, c, !1) : null != a.detachEvent ? a.detachEvent("on" + b, c) : delete a[b]
    }, a.prototype.innerHeight = function() {
      return "innerHeight" in window ? window.innerHeight : document.documentElement.clientHeight
    }, a
  }(), c = this.WeakMap || this.MozWeakMap || (c = function() {
    function a() {
      this.keys = [], this.values = []
    }
    return a.prototype.get = function(a) {
      var b, c, d, e, f;
      for (f = this.keys, b = d = 0, e = f.length; e > d; b = ++d)
        if (c = f[b], c === a) return this.values[b]
    }, a.prototype.set = function(a, b) {
      var c, d, e, f, g;
      for (g = this.keys, c = e = 0, f = g.length; f > e; c = ++e)
        if (d = g[c], d === a) return void(this.values[c] = b);
      return this.keys.push(a), this.values.push(b)
    }, a
  }()), a = this.MutationObserver || this.WebkitMutationObserver || this.MozMutationObserver || (a = function() {
    function a() {
      "undefined" != typeof console && null !== console && console.warn("MutationObserver is not supported by your browser."), "undefined" != typeof console && null !== console && console.warn("WOW.js cannot detect dom mutations, please call .sync() after loading new content.")
    }
    return a.notSupported = !0, a.prototype.observe = function() {}, a
  }()), d = this.getComputedStyle || function(a, b) {
    return this.getPropertyValue = function(b) {
      var c;
      return "float" === b && (b = "styleFloat"), e.test(b) && b.replace(e, function(a, b) {
        return b.toUpperCase()
      }), (null != (c = a.currentStyle) ? c[b] : void 0) || null
    }, this
  }, e = /(\-([a-z]){1})/g, this.WOW = function() {
    function e(a) {
      null == a && (a = {}), this.scrollCallback = f(this.scrollCallback, this), this.scrollHandler = f(this.scrollHandler, this), this.resetAnimation = f(this.resetAnimation, this), this.start = f(this.start, this), this.scrolled = !0, this.config = this.util().extend(a, this.defaults), null != a.scrollContainer && (this.config.scrollContainer = document.querySelector(a.scrollContainer)), this.animationNameCache = new c, this.wowEvent = this.util().createEvent(this.config.boxClass)
    }
    return e.prototype.defaults = {
      boxClass: "wow",
      animateClass: "animated",
      offset: 0,
      mobile: !0,
      live: !0,
      callback: null,
      scrollContainer: null
    }, e.prototype.init = function() {
      var a;
      return this.element = window.document.documentElement, "interactive" === (a = document.readyState) || "complete" === a ? this.start() : this.util().addEvent(document, "DOMContentLoaded", this.start), this.finished = []
    }, e.prototype.start = function() {
      var b, c, d, e;
      if (this.stopped = !1, this.boxes = function() {
          var a, c, d, e;
          for (d = this.element.querySelectorAll("." + this.config.boxClass), e = [], a = 0, c = d.length; c > a; a++) b = d[a], e.push(b);
          return e
        }.call(this), this.all = function() {
          var a, c, d, e;
          for (d = this.boxes, e = [], a = 0, c = d.length; c > a; a++) b = d[a], e.push(b);
          return e
        }.call(this), this.boxes.length)
        if (this.disabled()) this.resetStyle();
        else
          for (e = this.boxes, c = 0, d = e.length; d > c; c++) b = e[c], this.applyStyle(b, !0);
      return this.disabled() || (this.util().addEvent(this.config.scrollContainer || window, "scroll", this.scrollHandler), this.util().addEvent(window, "resize", this.scrollHandler), this.interval = setInterval(this.scrollCallback, 50)), this.config.live ? new a(function(a) {
        return function(b) {
          var c, d, e, f, g;
          for (g = [], c = 0, d = b.length; d > c; c++) f = b[c], g.push(function() {
            var a, b, c, d;
            for (c = f.addedNodes || [], d = [], a = 0, b = c.length; b > a; a++) e = c[a], d.push(this.doSync(e));
            return d
          }.call(a));
          return g
        }
      }(this)).observe(document.body, {
        childList: !0,
        subtree: !0
      }) : void 0
    }, e.prototype.stop = function() {
      return this.stopped = !0, this.util().removeEvent(this.config.scrollContainer || window, "scroll", this.scrollHandler), this.util().removeEvent(window, "resize", this.scrollHandler), null != this.interval ? clearInterval(this.interval) : void 0
    }, e.prototype.sync = function(b) {
      return a.notSupported ? this.doSync(this.element) : void 0
    }, e.prototype.doSync = function(a) {
      var b, c, d, e, f;
      if (null == a && (a = this.element), 1 === a.nodeType) {
        for (a = a.parentNode || a, e = a.querySelectorAll("." + this.config.boxClass), f = [], c = 0, d = e.length; d > c; c++) b = e[c], g.call(this.all, b) < 0 ? (this.boxes.push(b), this.all.push(b), this.stopped || this.disabled() ? this.resetStyle() : this.applyStyle(b, !0), f.push(this.scrolled = !0)) : f.push(void 0);
        return f
      }
    }, e.prototype.show = function(a) {
      return this.applyStyle(a), a.className = a.className + " " + this.config.animateClass, null != this.config.callback && this.config.callback(a), this.util().emitEvent(a, this.wowEvent), this.util().addEvent(a, "animationend", this.resetAnimation), this.util().addEvent(a, "oanimationend", this.resetAnimation), this.util().addEvent(a, "webkitAnimationEnd", this.resetAnimation), this.util().addEvent(a, "MSAnimationEnd", this.resetAnimation), a
    }, e.prototype.applyStyle = function(a, b) {
      var c, d, e;
      return d = a.getAttribute("data-wow-duration"), c = a.getAttribute("data-wow-delay"), e = a.getAttribute("data-wow-iteration"), this.animate(function(f) {
        return function() {
          return f.customStyle(a, b, d, c, e)
        }
      }(this))
    }, e.prototype.animate = function() {
      return "requestAnimationFrame" in window ? function(a) {
        return window.requestAnimationFrame(a)
      } : function(a) {
        return a()
      }
    }(), e.prototype.resetStyle = function() {
      var a, b, c, d, e;
      for (d = this.boxes, e = [], b = 0, c = d.length; c > b; b++) a = d[b], e.push(a.style.visibility = "visible");
      return e
    }, e.prototype.resetAnimation = function(a) {
      var b;
      return a.type.toLowerCase().indexOf("animationend") >= 0 ? (b = a.target || a.srcElement, b.className = b.className.replace(this.config.animateClass, "").trim()) : void 0
    }, e.prototype.customStyle = function(a, b, c, d, e) {
      return b && this.cacheAnimationName(a), a.style.visibility = b ? "hidden" : "visible", c && this.vendorSet(a.style, {
        animationDuration: c
      }), d && this.vendorSet(a.style, {
        animationDelay: d
      }), e && this.vendorSet(a.style, {
        animationIterationCount: e
      }), this.vendorSet(a.style, {
        animationName: b ? "none" : this.cachedAnimationName(a)
      }), a
    }, e.prototype.vendors = ["moz", "webkit"], e.prototype.vendorSet = function(a, b) {
      var c, d, e, f;
      d = [];
      for (c in b) e = b[c], a["" + c] = e, d.push(function() {
        var b, d, g, h;
        for (g = this.vendors, h = [], b = 0, d = g.length; d > b; b++) f = g[b], h.push(a["" + f + c.charAt(0).toUpperCase() + c.substr(1)] = e);
        return h
      }.call(this));
      return d
    }, e.prototype.vendorCSS = function(a, b) {
      var c, e, f, g, h, i;
      for (h = d(a), g = h.getPropertyCSSValue(b), f = this.vendors, c = 0, e = f.length; e > c; c++) i = f[c], g = g || h.getPropertyCSSValue("-" + i + "-" + b);
      return g
    }, e.prototype.animationName = function(a) {
      var b;
      try {
        b = this.vendorCSS(a, "animation-name").cssText
      } catch (c) {
        b = d(a).getPropertyValue("animation-name")
      }
      return "none" === b ? "" : b
    }, e.prototype.cacheAnimationName = function(a) {
      return this.animationNameCache.set(a, this.animationName(a))
    }, e.prototype.cachedAnimationName = function(a) {
      return this.animationNameCache.get(a)
    }, e.prototype.scrollHandler = function() {
      return this.scrolled = !0
    }, e.prototype.scrollCallback = function() {
      var a;
      return !this.scrolled || (this.scrolled = !1, this.boxes = function() {
        var b, c, d, e;
        for (d = this.boxes, e = [], b = 0, c = d.length; c > b; b++) a = d[b], a && (this.isVisible(a) ? this.show(a) : e.push(a));
        return e
      }.call(this), this.boxes.length || this.config.live) ? void 0 : this.stop()
    }, e.prototype.offsetTop = function(a) {
      for (var b; void 0 === a.offsetTop;) a = a.parentNode;
      for (b = a.offsetTop; a = a.offsetParent;) b += a.offsetTop;
      return b
    }, e.prototype.isVisible = function(a) {
      var b, c, d, e, f;
      return c = a.getAttribute("data-wow-offset") || this.config.offset, f = this.config.scrollContainer && this.config.scrollContainer.scrollTop || window.pageYOffset, e = f + Math.min(this.element.clientHeight, this.util().innerHeight()) - c, d = this.offsetTop(a), b = d + a.clientHeight, e >= d && b >= f
    }, e.prototype.util = function() {
      return null != this._util ? this._util : this._util = new b
    }, e.prototype.disabled = function() {
      return !this.config.mobile && this.util().isMobile(navigator.userAgent)
    }, e
  }()
}).call(this);;;
(function(factory) {
  'use strict';
  if (typeof define === 'function' && define.amd) {
    define(['jquery'], factory);
  } else if (typeof exports !== 'undefined') {
    module.exports = factory(require('jquery'));
  } else {
    factory(jQuery);
  }
}(function($) {
  'use strict';
  var Slick = window.Slick || {};
  Slick = (function() {
    var instanceUid = 0;

    function Slick(element, settings) {
      var self = this,
        dataSettings;
      self.defaults = {
        accessibility: true,
        adaptiveHeight: false,
        appendArrows: $(element),
        appendDots: $(element),
        arrows: true,
        asNavFor: null,
        prevArrow: '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
        nextArrow: '<button class="slick-next" aria-label="Next" type="button">Next</button>',
        autoplay: false,
        autoplaySpeed: 3000,
        centerMode: false,
        centerPadding: '50px',
        cssEase: 'ease',
        customPaging: function(slider, i) {
          return $('<button type="button" />').text(i + 1);
        },
        dots: false,
        dotsClass: 'slick-dots',
        draggable: true,
        easing: 'linear',
        edgeFriction: 0.35,
        fade: false,
        focusOnSelect: false,
        focusOnChange: false,
        infinite: true,
        initialSlide: 0,
        lazyLoad: 'ondemand',
        mobileFirst: false,
        pauseOnHover: true,
        pauseOnFocus: true,
        pauseOnDotsHover: false,
        respondTo: 'window',
        responsive: null,
        rows: 1,
        rtl: false,
        slide: '',
        slidesPerRow: 1,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 500,
        swipe: true,
        swipeToSlide: false,
        touchMove: true,
        touchThreshold: 5,
        useCSS: true,
        useTransform: true,
        variableWidth: false,
        vertical: false,
        verticalSwiping: false,
        waitForAnimate: true,
        zIndex: 1000
      };
      self.initials = {
        animating: false,
        dragging: false,
        autoPlayTimer: null,
        currentDirection: 0,
        currentLeft: null,
        currentSlide: 0,
        direction: 1,
        $dots: null,
        listWidth: null,
        listHeight: null,
        loadIndex: 0,
        $nextArrow: null,
        $prevArrow: null,
        scrolling: false,
        slideCount: null,
        slideWidth: null,
        $slideTrack: null,
        $slides: null,
        sliding: false,
        slideOffset: 0,
        swipeLeft: null,
        swiping: false,
        $list: null,
        touchObject: {},
        transformsEnabled: false,
        unslicked: false
      };
      $.extend(self, self.initials);
      self.activeBreakpoint = null;
      self.animType = null;
      self.animProp = null;
      self.breakpoints = [];
      self.breakpointSettings = [];
      self.cssTransitions = false;
      self.focussed = false;
      self.interrupted = false;
      self.hidden = 'hidden';
      self.paused = true;
      self.positionProp = null;
      self.respondTo = null;
      self.rowCount = 1;
      self.shouldClick = true;
      self.$slider = $(element);
      self.$slidesCache = null;
      self.transformType = null;
      self.transitionType = null;
      self.visibilityChange = 'visibilitychange';
      self.windowWidth = 0;
      self.windowTimer = null;
      dataSettings = $(element).data('slick') || {};
      self.options = $.extend({}, self.defaults, settings, dataSettings);
      self.currentSlide = self.options.initialSlide;
      self.originalSettings = self.options;
      if (typeof document.mozHidden !== 'undefined') {
        self.hidden = 'mozHidden';
        self.visibilityChange = 'mozvisibilitychange';
      } else if (typeof document.webkitHidden !== 'undefined') {
        self.hidden = 'webkitHidden';
        self.visibilityChange = 'webkitvisibilitychange';
      }
      self.autoPlay = $.proxy(self.autoPlay, self);
      self.autoPlayClear = $.proxy(self.autoPlayClear, self);
      self.autoPlayIterator = $.proxy(self.autoPlayIterator, self);
      self.changeSlide = $.proxy(self.changeSlide, self);
      self.clickHandler = $.proxy(self.clickHandler, self);
      self.selectHandler = $.proxy(self.selectHandler, self);
      self.setPosition = $.proxy(self.setPosition, self);
      self.swipeHandler = $.proxy(self.swipeHandler, self);
      self.dragHandler = $.proxy(self.dragHandler, self);
      self.keyHandler = $.proxy(self.keyHandler, self);
      self.instanceUid = instanceUid++;
      self.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/;
      self.registerBreakpoints();
      self.init(true);
    }
    return Slick;
  }());
  Slick.prototype.activateADA = function() {
    var self = this;
    self.$slideTrack.find('.slick-active').attr({
      'aria-hidden': 'false'
    }).find('a, input, button, select').attr({
      'tabindex': '0'
    });
  };
  Slick.prototype.addSlide = Slick.prototype.slickAdd = function(markup, index, addBefore) {
    var self = this;
    if (typeof(index) === 'boolean') {
      addBefore = index;
      index = null;
    } else if (index < 0 || (index >= self.slideCount)) {
      return false;
    }
    self.unload();
    if (typeof(index) === 'number') {
      if (index === 0 && self.$slides.length === 0) {
        $(markup).appendTo(self.$slideTrack);
      } else if (addBefore) {
        $(markup).insertBefore(self.$slides.eq(index));
      } else {
        $(markup).insertAfter(self.$slides.eq(index));
      }
    } else {
      if (addBefore === true) {
        $(markup).prependTo(self.$slideTrack);
      } else {
        $(markup).appendTo(self.$slideTrack);
      }
    }
    self.$slides = self.$slideTrack.children(this.options.slide);
    self.$slideTrack.children(this.options.slide).detach();
    self.$slideTrack.append(self.$slides);
    self.$slides.each(function(index, element) {
      $(element).attr('data-slick-index', index);
    });
    self.$slidesCache = self.$slides;
    self.reinit();
  };
  Slick.prototype.animateHeight = function() {
    var self = this;
    if (self.options.slidesToShow === 1 && self.options.adaptiveHeight === true && self.options.vertical === false) {
      var targetHeight = self.$slides.eq(self.currentSlide).outerHeight(true);
      self.$list.animate({
        height: targetHeight
      }, self.options.speed);
    }
  };
  Slick.prototype.animateSlide = function(targetLeft, callback) {
    var animProps = {},
      self = this;
    self.animateHeight();
    if (self.options.rtl === true && self.options.vertical === false) {
      targetLeft = -targetLeft;
    }
    if (self.transformsEnabled === false) {
      if (self.options.vertical === false) {
        self.$slideTrack.animate({
          left: targetLeft
        }, self.options.speed, self.options.easing, callback);
      } else {
        self.$slideTrack.animate({
          top: targetLeft
        }, self.options.speed, self.options.easing, callback);
      }
    } else {
      if (self.cssTransitions === false) {
        if (self.options.rtl === true) {
          self.currentLeft = -(self.currentLeft);
        }
        $({
          animStart: self.currentLeft
        }).animate({
          animStart: targetLeft
        }, {
          duration: self.options.speed,
          easing: self.options.easing,
          step: function(now) {
            now = Math.ceil(now);
            if (self.options.vertical === false) {
              animProps[self.animType] = 'translate(' +
                now + 'px, 0px)';
              self.$slideTrack.css(animProps);
            } else {
              animProps[self.animType] = 'translate(0px,' +
                now + 'px)';
              self.$slideTrack.css(animProps);
            }
          },
          complete: function() {
            if (callback) {
              callback.call();
            }
          }
        });
      } else {
        self.applyTransition();
        targetLeft = Math.ceil(targetLeft);
        if (self.options.vertical === false) {
          animProps[self.animType] = 'translate3d(' + targetLeft + 'px, 0px, 0px)';
        } else {
          animProps[self.animType] = 'translate3d(0px,' + targetLeft + 'px, 0px)';
        }
        self.$slideTrack.css(animProps);
        if (callback) {
          setTimeout(function() {
            self.disableTransition();
            callback.call();
          }, self.options.speed);
        }
      }
    }
  };
  Slick.prototype.getNavTarget = function() {
    var self = this,
      asNavFor = self.options.asNavFor;
    if (asNavFor && asNavFor !== null) {
      asNavFor = $(asNavFor).not(self.$slider);
    }
    return asNavFor;
  };
  Slick.prototype.asNavFor = function(index) {
    var self = this,
      asNavFor = self.getNavTarget();
    if (asNavFor !== null && typeof asNavFor === 'object') {
      asNavFor.each(function() {
        var target = $(this).slick('getSlick');
        if (!target.unslicked) {
          target.slideHandler(index, true);
        }
      });
    }
  };
  Slick.prototype.applyTransition = function(slide) {
    var self = this,
      transition = {};
    if (self.options.fade === false) {
      transition[self.transitionType] = self.transformType + ' ' + self.options.speed + 'ms ' + self.options.cssEase;
    } else {
      transition[self.transitionType] = 'opacity ' + self.options.speed + 'ms ' + self.options.cssEase;
    }
    if (self.options.fade === false) {
      self.$slideTrack.css(transition);
    } else {
      self.$slides.eq(slide).css(transition);
    }
  };
  Slick.prototype.autoPlay = function() {
    var self = this;
    self.autoPlayClear();
    if (self.slideCount > self.options.slidesToShow) {
      self.autoPlayTimer = setInterval(self.autoPlayIterator, self.options.autoplaySpeed);
    }
  };
  Slick.prototype.autoPlayClear = function() {
    var self = this;
    if (self.autoPlayTimer) {
      clearInterval(self.autoPlayTimer);
    }
  };
  Slick.prototype.autoPlayIterator = function() {
    var self = this,
      slideTo = self.currentSlide + self.options.slidesToScroll;
    if (!self.paused && !self.interrupted && !self.focussed) {
      if (self.options.infinite === false) {
        if (self.direction === 1 && (self.currentSlide + 1) === (self.slideCount - 1)) {
          self.direction = 0;
        } else if (self.direction === 0) {
          slideTo = self.currentSlide - self.options.slidesToScroll;
          if (self.currentSlide - 1 === 0) {
            self.direction = 1;
          }
        }
      }
      self.slideHandler(slideTo);
    }
  };
  Slick.prototype.buildArrows = function() {
    var self = this;
    if (self.options.arrows === true) {
      self.$prevArrow = $(self.options.prevArrow).addClass('slick-arrow');
      self.$nextArrow = $(self.options.nextArrow).addClass('slick-arrow');
      if (self.slideCount > self.options.slidesToShow) {
        self.$prevArrow.removeClass('slick-hidden').removeAttr('aria-hidden tabindex');
        self.$nextArrow.removeClass('slick-hidden').removeAttr('aria-hidden tabindex');
        if (self.htmlExpr.test(self.options.prevArrow)) {
          self.$prevArrow.prependTo(self.options.appendArrows);
        }
        if (self.htmlExpr.test(self.options.nextArrow)) {
          self.$nextArrow.appendTo(self.options.appendArrows);
        }
        if (self.options.infinite !== true) {
          self.$prevArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
        }
      } else {
        self.$prevArrow.add(self.$nextArrow).addClass('slick-hidden').attr({
          'aria-disabled': 'true',
          'tabindex': '-1'
        });
      }
    }
  };
  Slick.prototype.buildDots = function() {
    var self = this,
      i, dot;
    if (self.options.dots === true && self.slideCount > self.options.slidesToShow) {
      self.$slider.addClass('slick-dotted');
      dot = $('<ul />').addClass(self.options.dotsClass);
      for (i = 0; i <= self.getDotCount(); i += 1) {
        dot.append($('<li />').append(self.options.customPaging.call(this, self, i)));
      }
      self.$dots = dot.appendTo(self.options.appendDots);
      self.$dots.find('li').first().addClass('slick-active');
    }
  };
  Slick.prototype.buildOut = function() {
    var self = this;
    self.$slides = self.$slider.children(self.options.slide + ':not(.slick-cloned)').addClass('slick-slide');
    self.slideCount = self.$slides.length;
    self.$slides.each(function(index, element) {
      $(element).attr('data-slick-index', index).data('originalStyling', $(element).attr('style') || '');
    });
    self.$slider.addClass('slick-slider');
    self.$slideTrack = (self.slideCount === 0) ? $('<div class="slick-track"/>').appendTo(self.$slider) : self.$slides.wrapAll('<div class="slick-track"/>').parent();
    self.$list = self.$slideTrack.wrap('<div class="slick-list"/>').parent();
    self.$slideTrack.css('opacity', 0);
    if (self.options.centerMode === true || self.options.swipeToSlide === true) {
      self.options.slidesToScroll = 1;
    }
    $('img[data-lazy]', self.$slider).not('[src]').addClass('slick-loading');
    self.setupInfinite();
    self.buildArrows();
    self.buildDots();
    self.updateDots();
    self.setSlideClasses(typeof self.currentSlide === 'number' ? self.currentSlide : 0);
    if (self.options.draggable === true) {
      self.$list.addClass('draggable');
    }
  };
  Slick.prototype.buildRows = function() {
    var self = this,
      a, b, c, newSlides, numOfSlides, originalSlides, slidesPerSection;
    newSlides = document.createDocumentFragment();
    originalSlides = self.$slider.children();
    if (self.options.rows > 0) {
      slidesPerSection = self.options.slidesPerRow * self.options.rows;
      numOfSlides = Math.ceil(originalSlides.length / slidesPerSection);
      for (a = 0; a < numOfSlides; a++) {
        var slide = document.createElement('div');
        for (b = 0; b < self.options.rows; b++) {
          var row = document.createElement('div');
          for (c = 0; c < self.options.slidesPerRow; c++) {
            var target = (a * slidesPerSection + ((b * self.options.slidesPerRow) + c));
            if (originalSlides.get(target)) {
              row.appendChild(originalSlides.get(target));
            }
          }
          slide.appendChild(row);
        }
        newSlides.appendChild(slide);
      }
      self.$slider.empty().append(newSlides);
      self.$slider.children().children().children().css({
        'width': (100 / self.options.slidesPerRow) + '%',
        'display': 'inline-block'
      });
    }
  };
  Slick.prototype.checkResponsive = function(initial, forceUpdate) {
    var self = this,
      breakpoint, targetBreakpoint, respondToWidth, triggerBreakpoint = false;
    var sliderWidth = self.$slider.width();
    var windowWidth = window.innerWidth || $(window).width();
    if (self.respondTo === 'window') {
      respondToWidth = windowWidth;
    } else if (self.respondTo === 'slider') {
      respondToWidth = sliderWidth;
    } else if (self.respondTo === 'min') {
      respondToWidth = Math.min(windowWidth, sliderWidth);
    }
    if (self.options.responsive && self.options.responsive.length && self.options.responsive !== null) {
      targetBreakpoint = null;
      for (breakpoint in self.breakpoints) {
        if (self.breakpoints.hasOwnProperty(breakpoint)) {
          if (self.originalSettings.mobileFirst === false) {
            if (respondToWidth < self.breakpoints[breakpoint]) {
              targetBreakpoint = self.breakpoints[breakpoint];
            }
          } else {
            if (respondToWidth > self.breakpoints[breakpoint]) {
              targetBreakpoint = self.breakpoints[breakpoint];
            }
          }
        }
      }
      if (targetBreakpoint !== null) {
        if (self.activeBreakpoint !== null) {
          if (targetBreakpoint !== self.activeBreakpoint || forceUpdate) {
            self.activeBreakpoint = targetBreakpoint;
            if (self.breakpointSettings[targetBreakpoint] === 'unslick') {
              self.unslick(targetBreakpoint);
            } else {
              self.options = $.extend({}, self.originalSettings, self.breakpointSettings[targetBreakpoint]);
              if (initial === true) {
                self.currentSlide = self.options.initialSlide;
              }
              self.refresh(initial);
            }
            triggerBreakpoint = targetBreakpoint;
          }
        } else {
          self.activeBreakpoint = targetBreakpoint;
          if (self.breakpointSettings[targetBreakpoint] === 'unslick') {
            self.unslick(targetBreakpoint);
          } else {
            self.options = $.extend({}, self.originalSettings, self.breakpointSettings[targetBreakpoint]);
            if (initial === true) {
              self.currentSlide = self.options.initialSlide;
            }
            self.refresh(initial);
          }
          triggerBreakpoint = targetBreakpoint;
        }
      } else {
        if (self.activeBreakpoint !== null) {
          self.activeBreakpoint = null;
          self.options = self.originalSettings;
          if (initial === true) {
            self.currentSlide = self.options.initialSlide;
          }
          self.refresh(initial);
          triggerBreakpoint = targetBreakpoint;
        }
      }
      if (!initial && triggerBreakpoint !== false) {
        self.$slider.trigger('breakpoint', [self, triggerBreakpoint]);
      }
    }
  };
  Slick.prototype.changeSlide = function(event, dontAnimate) {
    var self = this,
      $target = $(event.currentTarget),
      indexOffset, slideOffset, unevenOffset;
    if ($target.is('a')) {
      event.preventDefault();
    }
    if (!$target.is('li')) {
      $target = $target.closest('li');
    }
    unevenOffset = (self.slideCount % self.options.slidesToScroll !== 0);
    indexOffset = unevenOffset ? 0 : (self.slideCount - self.currentSlide) % self.options.slidesToScroll;
    switch (event.data.message) {
      case 'previous':
        slideOffset = indexOffset === 0 ? self.options.slidesToScroll : self.options.slidesToShow - indexOffset;
        if (self.slideCount > self.options.slidesToShow) {
          self.slideHandler(self.currentSlide - slideOffset, false, dontAnimate);
        }
        break;
      case 'next':
        slideOffset = indexOffset === 0 ? self.options.slidesToScroll : indexOffset;
        if (self.slideCount > self.options.slidesToShow) {
          self.slideHandler(self.currentSlide + slideOffset, false, dontAnimate);
        }
        break;
      case 'index':
        var index = event.data.index === 0 ? 0 : event.data.index || $target.index() * self.options.slidesToScroll;
        self.slideHandler(self.checkNavigable(index), false, dontAnimate);
        $target.children().trigger('focus');
        break;
      default:
        return;
    }
  };
  Slick.prototype.checkNavigable = function(index) {
    var self = this,
      navigables, prevNavigable;
    navigables = self.getNavigableIndexes();
    prevNavigable = 0;
    if (index > navigables[navigables.length - 1]) {
      index = navigables[navigables.length - 1];
    } else {
      for (var n in navigables) {
        if (index < navigables[n]) {
          index = prevNavigable;
          break;
        }
        prevNavigable = navigables[n];
      }
    }
    return index;
  };
  Slick.prototype.cleanUpEvents = function() {
    var self = this;
    if (self.options.dots && self.$dots !== null) {
      $('li', self.$dots).off('click.slick', self.changeSlide).off('mouseenter.slick', $.proxy(self.interrupt, self, true)).off('mouseleave.slick', $.proxy(self.interrupt, self, false));
      if (self.options.accessibility === true) {
        self.$dots.off('keydown.slick', self.keyHandler);
      }
    }
    self.$slider.off('focus.slick blur.slick');
    if (self.options.arrows === true && self.slideCount > self.options.slidesToShow) {
      self.$prevArrow && self.$prevArrow.off('click.slick', self.changeSlide);
      self.$nextArrow && self.$nextArrow.off('click.slick', self.changeSlide);
      if (self.options.accessibility === true) {
        self.$prevArrow && self.$prevArrow.off('keydown.slick', self.keyHandler);
        self.$nextArrow && self.$nextArrow.off('keydown.slick', self.keyHandler);
      }
    }
    self.$list.off('touchstart.slick mousedown.slick', self.swipeHandler);
    self.$list.off('touchmove.slick mousemove.slick', self.swipeHandler);
    self.$list.off('touchend.slick mouseup.slick', self.swipeHandler);
    self.$list.off('touchcancel.slick mouseleave.slick', self.swipeHandler);
    self.$list.off('click.slick', self.clickHandler);
    $(document).off(self.visibilityChange, self.visibility);
    self.cleanUpSlideEvents();
    if (self.options.accessibility === true) {
      self.$list.off('keydown.slick', self.keyHandler);
    }
    if (self.options.focusOnSelect === true) {
      $(self.$slideTrack).children().off('click.slick', self.selectHandler);
    }
    $(window).off('orientationchange.slick.slick-' + self.instanceUid, self.orientationChange);
    $(window).off('resize.slick.slick-' + self.instanceUid, self.resize);
    $('[draggable!=true]', self.$slideTrack).off('dragstart', self.preventDefault);
    $(window).off('load.slick.slick-' + self.instanceUid, self.setPosition);
  };
  Slick.prototype.cleanUpSlideEvents = function() {
    var self = this;
    self.$list.off('mouseenter.slick', $.proxy(self.interrupt, self, true));
    self.$list.off('mouseleave.slick', $.proxy(self.interrupt, self, false));
  };
  Slick.prototype.cleanUpRows = function() {
    var self = this,
      originalSlides;
    if (self.options.rows > 0) {
      originalSlides = self.$slides.children().children();
      originalSlides.removeAttr('style');
      self.$slider.empty().append(originalSlides);
    }
  };
  Slick.prototype.clickHandler = function(event) {
    var self = this;
    if (self.shouldClick === false) {
      event.stopImmediatePropagation();
      event.stopPropagation();
      event.preventDefault();
    }
  };
  Slick.prototype.destroy = function(refresh) {
    var self = this;
    self.autoPlayClear();
    self.touchObject = {};
    self.cleanUpEvents();
    $('.slick-cloned', self.$slider).detach();
    if (self.$dots) {
      self.$dots.remove();
    }
    if (self.$prevArrow && self.$prevArrow.length) {
      self.$prevArrow.removeClass('slick-disabled slick-arrow slick-hidden').removeAttr('aria-hidden aria-disabled tabindex').css('display', '');
      if (self.htmlExpr.test(self.options.prevArrow)) {
        self.$prevArrow.remove();
      }
    }
    if (self.$nextArrow && self.$nextArrow.length) {
      self.$nextArrow.removeClass('slick-disabled slick-arrow slick-hidden').removeAttr('aria-hidden aria-disabled tabindex').css('display', '');
      if (self.htmlExpr.test(self.options.nextArrow)) {
        self.$nextArrow.remove();
      }
    }
    if (self.$slides) {
      self.$slides.removeClass('slick-slide slick-active slick-center slick-visible slick-current').removeAttr('aria-hidden').removeAttr('data-slick-index').each(function() {
        $(this).attr('style', $(this).data('originalStyling'));
      });
      self.$slideTrack.children(this.options.slide).detach();
      self.$slideTrack.detach();
      self.$list.detach();
      self.$slider.append(self.$slides);
    }
    self.cleanUpRows();
    self.$slider.removeClass('slick-slider');
    self.$slider.removeClass('slick-initialized');
    self.$slider.removeClass('slick-dotted');
    self.unslicked = true;
    if (!refresh) {
      self.$slider.trigger('destroy', [self]);
    }
  };
  Slick.prototype.disableTransition = function(slide) {
    var self = this,
      transition = {};
    transition[self.transitionType] = '';
    if (self.options.fade === false) {
      self.$slideTrack.css(transition);
    } else {
      self.$slides.eq(slide).css(transition);
    }
  };
  Slick.prototype.fadeSlide = function(slideIndex, callback) {
    var self = this;
    if (self.cssTransitions === false) {
      self.$slides.eq(slideIndex).css({
        zIndex: self.options.zIndex
      });
      self.$slides.eq(slideIndex).animate({
        opacity: 1
      }, self.options.speed, self.options.easing, callback);
    } else {
      self.applyTransition(slideIndex);
      self.$slides.eq(slideIndex).css({
        opacity: 1,
        zIndex: self.options.zIndex
      });
      if (callback) {
        setTimeout(function() {
          self.disableTransition(slideIndex);
          callback.call();
        }, self.options.speed);
      }
    }
  };
  Slick.prototype.fadeSlideOut = function(slideIndex) {
    var self = this;
    if (self.cssTransitions === false) {
      self.$slides.eq(slideIndex).animate({
        opacity: 0,
        zIndex: self.options.zIndex - 2
      }, self.options.speed, self.options.easing);
    } else {
      self.applyTransition(slideIndex);
      self.$slides.eq(slideIndex).css({
        opacity: 0,
        zIndex: self.options.zIndex - 2
      });
    }
  };
  Slick.prototype.filterSlides = Slick.prototype.slickFilter = function(filter) {
    var self = this;
    if (filter !== null) {
      self.$slidesCache = self.$slides;
      self.unload();
      self.$slideTrack.children(this.options.slide).detach();
      self.$slidesCache.filter(filter).appendTo(self.$slideTrack);
      self.reinit();
    }
  };
  Slick.prototype.focusHandler = function() {
    var self = this;
    self.$slider.off('focus.slick blur.slick').on('focus.slick blur.slick', '*', function(event) {
      event.stopImmediatePropagation();
      var $sf = $(this);
      setTimeout(function() {
        if (self.options.pauseOnFocus) {
          self.focussed = $sf.is(':focus');
          self.autoPlay();
        }
      }, 0);
    });
  };
  Slick.prototype.getCurrent = Slick.prototype.slickCurrentSlide = function() {
    var self = this;
    return self.currentSlide;
  };
  Slick.prototype.getDotCount = function() {
    var self = this;
    var breakPoint = 0;
    var counter = 0;
    var pagerQty = 0;
    if (self.options.infinite === true) {
      if (self.slideCount <= self.options.slidesToShow) {
        ++pagerQty;
      } else {
        while (breakPoint < self.slideCount) {
          ++pagerQty;
          breakPoint = counter + self.options.slidesToScroll;
          counter += self.options.slidesToScroll <= self.options.slidesToShow ? self.options.slidesToScroll : self.options.slidesToShow;
        }
      }
    } else if (self.options.centerMode === true) {
      pagerQty = self.slideCount;
    } else if (!self.options.asNavFor) {
      pagerQty = 1 + Math.ceil((self.slideCount - self.options.slidesToShow) / self.options.slidesToScroll);
    } else {
      while (breakPoint < self.slideCount) {
        ++pagerQty;
        breakPoint = counter + self.options.slidesToScroll;
        counter += self.options.slidesToScroll <= self.options.slidesToShow ? self.options.slidesToScroll : self.options.slidesToShow;
      }
    }
    return pagerQty - 1;
  };
  Slick.prototype.getLeft = function(slideIndex) {
    var self = this,
      targetLeft, verticalHeight, verticalOffset = 0,
      targetSlide, coef;
    self.slideOffset = 0;
    verticalHeight = self.$slides.first().outerHeight(true);
    if (self.options.infinite === true) {
      if (self.slideCount > self.options.slidesToShow) {
        self.slideOffset = (self.slideWidth * self.options.slidesToShow) * -1;
        coef = -1
        if (self.options.vertical === true && self.options.centerMode === true) {
          if (self.options.slidesToShow === 2) {
            coef = -1.5;
          } else if (self.options.slidesToShow === 1) {
            coef = -2
          }
        }
        verticalOffset = (verticalHeight * self.options.slidesToShow) * coef;
      }
      if (self.slideCount % self.options.slidesToScroll !== 0) {
        if (slideIndex + self.options.slidesToScroll > self.slideCount && self.slideCount > self.options.slidesToShow) {
          if (slideIndex > self.slideCount) {
            self.slideOffset = ((self.options.slidesToShow - (slideIndex - self.slideCount)) * self.slideWidth) * -1;
            verticalOffset = ((self.options.slidesToShow - (slideIndex - self.slideCount)) * verticalHeight) * -1;
          } else {
            self.slideOffset = ((self.slideCount % self.options.slidesToScroll) * self.slideWidth) * -1;
            verticalOffset = ((self.slideCount % self.options.slidesToScroll) * verticalHeight) * -1;
          }
        }
      }
    } else {
      if (slideIndex + self.options.slidesToShow > self.slideCount) {
        self.slideOffset = ((slideIndex + self.options.slidesToShow) - self.slideCount) * self.slideWidth;
        verticalOffset = ((slideIndex + self.options.slidesToShow) - self.slideCount) * verticalHeight;
      }
    }
    if (self.slideCount <= self.options.slidesToShow) {
      self.slideOffset = 0;
      verticalOffset = 0;
    }
    if (self.options.centerMode === true && self.slideCount <= self.options.slidesToShow) {
      self.slideOffset = ((self.slideWidth * Math.floor(self.options.slidesToShow)) / 2) - ((self.slideWidth * self.slideCount) / 2);
    } else if (self.options.centerMode === true && self.options.infinite === true) {
      self.slideOffset += self.slideWidth * Math.floor(self.options.slidesToShow / 2) - self.slideWidth;
    } else if (self.options.centerMode === true) {
      self.slideOffset = 0;
      self.slideOffset += self.slideWidth * Math.floor(self.options.slidesToShow / 2);
    }
    if (self.options.vertical === false) {
      targetLeft = ((slideIndex * self.slideWidth) * -1) + self.slideOffset;
    } else {
      targetLeft = ((slideIndex * verticalHeight) * -1) + verticalOffset;
    }
    if (self.options.variableWidth === true) {
      if (self.slideCount <= self.options.slidesToShow || self.options.infinite === false) {
        targetSlide = self.$slideTrack.children('.slick-slide').eq(slideIndex);
      } else {
        targetSlide = self.$slideTrack.children('.slick-slide').eq(slideIndex + self.options.slidesToShow);
      }
      if (self.options.rtl === true) {
        if (targetSlide[0]) {
          targetLeft = (self.$slideTrack.width() - targetSlide[0].offsetLeft - targetSlide.width()) * -1;
        } else {
          targetLeft = 0;
        }
      } else {
        targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
      }
      if (self.options.centerMode === true) {
        if (self.slideCount <= self.options.slidesToShow || self.options.infinite === false) {
          targetSlide = self.$slideTrack.children('.slick-slide').eq(slideIndex);
        } else {
          targetSlide = self.$slideTrack.children('.slick-slide').eq(slideIndex + self.options.slidesToShow + 1);
        }
        if (self.options.rtl === true) {
          if (targetSlide[0]) {
            targetLeft = (self.$slideTrack.width() - targetSlide[0].offsetLeft - targetSlide.width()) * -1;
          } else {
            targetLeft = 0;
          }
        } else {
          targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
        }
        targetLeft += (self.$list.width() - targetSlide.outerWidth()) / 2;
      }
    }
    return targetLeft;
  };
  Slick.prototype.getOption = Slick.prototype.slickGetOption = function(option) {
    var self = this;
    return self.options[option];
  };
  Slick.prototype.getNavigableIndexes = function() {
    var self = this,
      breakPoint = 0,
      counter = 0,
      indexes = [],
      max;
    if (self.options.infinite === false) {
      max = self.slideCount;
    } else {
      breakPoint = self.options.slidesToScroll * -1;
      counter = self.options.slidesToScroll * -1;
      max = self.slideCount * 2;
    }
    while (breakPoint < max) {
      indexes.push(breakPoint);
      breakPoint = counter + self.options.slidesToScroll;
      counter += self.options.slidesToScroll <= self.options.slidesToShow ? self.options.slidesToScroll : self.options.slidesToShow;
    }
    return indexes;
  };
  Slick.prototype.getSlick = function() {
    return this;
  };
  Slick.prototype.getSlideCount = function() {
    var self = this,
      slidesTraversed, swipedSlide, centerOffset;
    centerOffset = self.options.centerMode === true ? self.slideWidth * Math.floor(self.options.slidesToShow / 2) : 0;
    if (self.options.swipeToSlide === true) {
      self.$slideTrack.find('.slick-slide').each(function(index, slide) {
        if (slide.offsetLeft - centerOffset + ($(slide).outerWidth() / 2) > (self.swipeLeft * -1)) {
          swipedSlide = slide;
          return false;
        }
      });
      slidesTraversed = Math.abs($(swipedSlide).attr('data-slick-index') - self.currentSlide) || 1;
      return slidesTraversed;
    } else {
      return self.options.slidesToScroll;
    }
  };
  Slick.prototype.goTo = Slick.prototype.slickGoTo = function(slide, dontAnimate) {
    var self = this;
    self.changeSlide({
      data: {
        message: 'index',
        index: parseInt(slide)
      }
    }, dontAnimate);
  };
  Slick.prototype.init = function(creation) {
    var self = this;
    if (!$(self.$slider).hasClass('slick-initialized')) {
      $(self.$slider).addClass('slick-initialized');
      self.buildRows();
      self.buildOut();
      self.setProps();
      self.startLoad();
      self.loadSlider();
      self.initializeEvents();
      self.updateArrows();
      self.updateDots();
      self.checkResponsive(true);
      self.focusHandler();
    }
    if (creation) {
      self.$slider.trigger('init', [self]);
    }
    if (self.options.accessibility === true) {
      self.initADA();
    }
    if (self.options.autoplay) {
      self.paused = false;
      self.autoPlay();
    }
  };
  Slick.prototype.initADA = function() {
    var self = this,
      numDotGroups = Math.ceil(self.slideCount / self.options.slidesToShow),
      tabControlIndexes = self.getNavigableIndexes().filter(function(val) {
        return (val >= 0) && (val < self.slideCount);
      });
    self.$slides.add(self.$slideTrack.find('.slick-cloned')).attr({
      'aria-hidden': 'true',
      'tabindex': '-1'
    }).find('a, input, button, select').attr({
      'tabindex': '-1'
    });
    if (self.$dots !== null) {
      self.$slides.not(self.$slideTrack.find('.slick-cloned')).each(function(i) {
        var slideControlIndex = tabControlIndexes.indexOf(i);
        $(this).attr({
          'role': 'tabpanel',
          'id': 'slick-slide' + self.instanceUid + i,
          'tabindex': -1
        });
        if (slideControlIndex !== -1) {
          var ariaButtonControl = 'slick-slide-control' + self.instanceUid + slideControlIndex
          if ($('#' + ariaButtonControl).length) {
            $(this).attr({
              'aria-describedby': ariaButtonControl
            });
          }
        }
      });
      self.$dots.attr('role', 'tablist').find('li').each(function(i) {
        var mappedSlideIndex = tabControlIndexes[i];
        $(this).attr({
          'role': 'presentation'
        });
        $(this).find('button').first().attr({
          'role': 'tab',
          'id': 'slick-slide-control' + self.instanceUid + i,
          'aria-controls': 'slick-slide' + self.instanceUid + mappedSlideIndex,
          'aria-label': (i + 1) + ' of ' + numDotGroups,
          'aria-selected': null,
          'tabindex': '-1'
        });
      }).eq(self.currentSlide).find('button').attr({
        'aria-selected': 'true',
        'tabindex': '0'
      }).end();
    }
    for (var i = self.currentSlide, max = i + self.options.slidesToShow; i < max; i++) {
      if (self.options.focusOnChange) {
        self.$slides.eq(i).attr({
          'tabindex': '0'
        });
      } else {
        self.$slides.eq(i).removeAttr('tabindex');
      }
    }
    self.activateADA();
  };
  Slick.prototype.initArrowEvents = function() {
    var self = this;
    if (self.options.arrows === true && self.slideCount > self.options.slidesToShow) {
      self.$prevArrow.off('click.slick').on('click.slick', {
        message: 'previous'
      }, self.changeSlide);
      self.$nextArrow.off('click.slick').on('click.slick', {
        message: 'next'
      }, self.changeSlide);
      if (self.options.accessibility === true) {
        self.$prevArrow.on('keydown.slick', self.keyHandler);
        self.$nextArrow.on('keydown.slick', self.keyHandler);
      }
    }
  };
  Slick.prototype.initDotEvents = function() {
    var self = this;
    if (self.options.dots === true && self.slideCount > self.options.slidesToShow) {
      $('li', self.$dots).on('click.slick', {
        message: 'index'
      }, self.changeSlide);
      if (self.options.accessibility === true) {
        self.$dots.on('keydown.slick', self.keyHandler);
      }
    }
    if (self.options.dots === true && self.options.pauseOnDotsHover === true && self.slideCount > self.options.slidesToShow) {
      $('li', self.$dots).on('mouseenter.slick', $.proxy(self.interrupt, self, true)).on('mouseleave.slick', $.proxy(self.interrupt, self, false));
    }
  };
  Slick.prototype.initSlideEvents = function() {
    var self = this;
    if (self.options.pauseOnHover) {
      self.$list.on('mouseenter.slick', $.proxy(self.interrupt, self, true));
      self.$list.on('mouseleave.slick', $.proxy(self.interrupt, self, false));
    }
  };
  Slick.prototype.initializeEvents = function() {
    var self = this;
    self.initArrowEvents();
    self.initDotEvents();
    self.initSlideEvents();
    self.$list.on('touchstart.slick mousedown.slick', {
      action: 'start'
    }, self.swipeHandler);
    self.$list.on('touchmove.slick mousemove.slick', {
      action: 'move'
    }, self.swipeHandler);
    self.$list.on('touchend.slick mouseup.slick', {
      action: 'end'
    }, self.swipeHandler);
    self.$list.on('touchcancel.slick mouseleave.slick', {
      action: 'end'
    }, self.swipeHandler);
    self.$list.on('click.slick', self.clickHandler);
    $(document).on(self.visibilityChange, $.proxy(self.visibility, self));
    if (self.options.accessibility === true) {
      self.$list.on('keydown.slick', self.keyHandler);
    }
    if (self.options.focusOnSelect === true) {
      $(self.$slideTrack).children().on('click.slick', self.selectHandler);
    }
    $(window).on('orientationchange.slick.slick-' + self.instanceUid, $.proxy(self.orientationChange, self));
    $(window).on('resize.slick.slick-' + self.instanceUid, $.proxy(self.resize, self));
    $('[draggable!=true]', self.$slideTrack).on('dragstart', self.preventDefault);
    $(window).on('load.slick.slick-' + self.instanceUid, self.setPosition);
    $(self.setPosition);
  };
  Slick.prototype.initUI = function() {
    var self = this;
    if (self.options.arrows === true && self.slideCount > self.options.slidesToShow) {
      self.$prevArrow.show();
      self.$nextArrow.show();
    }
    if (self.options.dots === true && self.slideCount > self.options.slidesToShow) {
      self.$dots.show();
    }
  };
  Slick.prototype.keyHandler = function(event) {
    var self = this;
    if (!event.target.tagName.match('TEXTAREA|INPUT|SELECT')) {
      if (event.keyCode === 37 && self.options.accessibility === true) {
        self.changeSlide({
          data: {
            message: self.options.rtl === true ? 'next' : 'previous'
          }
        });
      } else if (event.keyCode === 39 && self.options.accessibility === true) {
        self.changeSlide({
          data: {
            message: self.options.rtl === true ? 'previous' : 'next'
          }
        });
      }
    }
  };
  Slick.prototype.lazyLoad = function() {
    var self = this,
      loadRange, cloneRange, rangeStart, rangeEnd;

    function loadImages(imagesScope) {
      $('img[data-lazy]', imagesScope).each(function() {
        var image = $(this),
          imageSource = $(this).attr('data-lazy'),
          imageSrcSet = $(this).attr('data-srcset'),
          imageSizes = $(this).attr('data-sizes') || self.$slider.attr('data-sizes'),
          imageToLoad = document.createElement('img');
        imageToLoad.onload = function() {
          image.animate({
            opacity: 0
          }, 100, function() {
            if (imageSrcSet) {
              image.attr('srcset', imageSrcSet);
              if (imageSizes) {
                image.attr('sizes', imageSizes);
              }
            }
            image.attr('src', imageSource).animate({
              opacity: 1
            }, 200, function() {
              image.removeAttr('data-lazy data-srcset data-sizes').removeClass('slick-loading');
            });
            self.$slider.trigger('lazyLoaded', [self, image, imageSource]);
          });
        };
        imageToLoad.onerror = function() {
          image.removeAttr('data-lazy').removeClass('slick-loading').addClass('slick-lazyload-error');
          self.$slider.trigger('lazyLoadError', [self, image, imageSource]);
        };
        imageToLoad.src = imageSource;
      });
    }
    if (self.options.centerMode === true) {
      if (self.options.infinite === true) {
        rangeStart = self.currentSlide + (self.options.slidesToShow / 2 + 1);
        rangeEnd = rangeStart + self.options.slidesToShow + 2;
      } else {
        rangeStart = Math.max(0, self.currentSlide - (self.options.slidesToShow / 2 + 1));
        rangeEnd = 2 + (self.options.slidesToShow / 2 + 1) + self.currentSlide;
      }
    } else {
      rangeStart = self.options.infinite ? self.options.slidesToShow + self.currentSlide : self.currentSlide;
      rangeEnd = Math.ceil(rangeStart + self.options.slidesToShow);
      if (self.options.fade === true) {
        if (rangeStart > 0) rangeStart--;
        if (rangeEnd <= self.slideCount) rangeEnd++;
      }
    }
    loadRange = self.$slider.find('.slick-slide').slice(rangeStart, rangeEnd);
    if (self.options.lazyLoad === 'anticipated') {
      var prevSlide = rangeStart - 1,
        nextSlide = rangeEnd,
        $slides = self.$slider.find('.slick-slide');
      for (var i = 0; i < self.options.slidesToScroll; i++) {
        if (prevSlide < 0) prevSlide = self.slideCount - 1;
        loadRange = loadRange.add($slides.eq(prevSlide));
        loadRange = loadRange.add($slides.eq(nextSlide));
        prevSlide--;
        nextSlide++;
      }
    }
    loadImages(loadRange);
    if (self.slideCount <= self.options.slidesToShow) {
      cloneRange = self.$slider.find('.slick-slide');
      loadImages(cloneRange);
    } else
    if (self.currentSlide >= self.slideCount - self.options.slidesToShow) {
      cloneRange = self.$slider.find('.slick-cloned').slice(0, self.options.slidesToShow);
      loadImages(cloneRange);
    } else if (self.currentSlide === 0) {
      cloneRange = self.$slider.find('.slick-cloned').slice(self.options.slidesToShow * -1);
      loadImages(cloneRange);
    }
  };
  Slick.prototype.loadSlider = function() {
    var self = this;
    self.setPosition();
    self.$slideTrack.css({
      opacity: 1
    });
    self.$slider.removeClass('slick-loading');
    self.initUI();
    if (self.options.lazyLoad === 'progressive') {
      self.progressiveLazyLoad();
    }
  };
  Slick.prototype.next = Slick.prototype.slickNext = function() {
    var self = this;
    self.changeSlide({
      data: {
        message: 'next'
      }
    });
  };
  Slick.prototype.orientationChange = function() {
    var self = this;
    self.checkResponsive();
    self.setPosition();
  };
  Slick.prototype.pause = Slick.prototype.slickPause = function() {
    var self = this;
    self.autoPlayClear();
    self.paused = true;
  };
  Slick.prototype.play = Slick.prototype.slickPlay = function() {
    var self = this;
    self.autoPlay();
    self.options.autoplay = true;
    self.paused = false;
    self.focussed = false;
    self.interrupted = false;
  };
  Slick.prototype.postSlide = function(index) {
    var self = this;
    if (!self.unslicked) {
      self.$slider.trigger('afterChange', [self, index]);
      self.animating = false;
      if (self.slideCount > self.options.slidesToShow) {
        self.setPosition();
      }
      self.swipeLeft = null;
      if (self.options.autoplay) {
        self.autoPlay();
      }
      if (self.options.accessibility === true) {
        self.initADA();
        if (self.options.focusOnChange) {
          var $currentSlide = $(self.$slides.get(self.currentSlide));
          $currentSlide.attr('tabindex', 0).focus();
        }
      }
    }
  };
  Slick.prototype.prev = Slick.prototype.slickPrev = function() {
    var self = this;
    self.changeSlide({
      data: {
        message: 'previous'
      }
    });
  };
  Slick.prototype.preventDefault = function(event) {
    event.preventDefault();
  };
  Slick.prototype.progressiveLazyLoad = function(tryCount) {
    tryCount = tryCount || 1;
    var self = this,
      $imgsToLoad = $('img[data-lazy]', self.$slider),
      image, imageSource, imageSrcSet, imageSizes, imageToLoad;
    if ($imgsToLoad.length) {
      image = $imgsToLoad.first();
      imageSource = image.attr('data-lazy');
      imageSrcSet = image.attr('data-srcset');
      imageSizes = image.attr('data-sizes') || self.$slider.attr('data-sizes');
      imageToLoad = document.createElement('img');
      imageToLoad.onload = function() {
        if (imageSrcSet) {
          image.attr('srcset', imageSrcSet);
          if (imageSizes) {
            image.attr('sizes', imageSizes);
          }
        }
        image.attr('src', imageSource).removeAttr('data-lazy data-srcset data-sizes').removeClass('slick-loading');
        if (self.options.adaptiveHeight === true) {
          self.setPosition();
        }
        self.$slider.trigger('lazyLoaded', [self, image, imageSource]);
        self.progressiveLazyLoad();
      };
      imageToLoad.onerror = function() {
        if (tryCount < 3) {
          setTimeout(function() {
            self.progressiveLazyLoad(tryCount + 1);
          }, 500);
        } else {
          image.removeAttr('data-lazy').removeClass('slick-loading').addClass('slick-lazyload-error');
          self.$slider.trigger('lazyLoadError', [self, image, imageSource]);
          self.progressiveLazyLoad();
        }
      };
      imageToLoad.src = imageSource;
    } else {
      self.$slider.trigger('allImagesLoaded', [self]);
    }
  };
  Slick.prototype.refresh = function(initializing) {
    var self = this,
      currentSlide, lastVisibleIndex;
    lastVisibleIndex = self.slideCount - self.options.slidesToShow;
    if (!self.options.infinite && (self.currentSlide > lastVisibleIndex)) {
      self.currentSlide = lastVisibleIndex;
    }
    if (self.slideCount <= self.options.slidesToShow) {
      self.currentSlide = 0;
    }
    currentSlide = self.currentSlide;
    self.destroy(true);
    $.extend(self, self.initials, {
      currentSlide: currentSlide
    });
    self.init();
    if (!initializing) {
      self.changeSlide({
        data: {
          message: 'index',
          index: currentSlide
        }
      }, false);
    }
  };
  Slick.prototype.registerBreakpoints = function() {
    var self = this,
      breakpoint, currentBreakpoint, l, responsiveSettings = self.options.responsive || null;
    if ($.type(responsiveSettings) === 'array' && responsiveSettings.length) {
      self.respondTo = self.options.respondTo || 'window';
      for (breakpoint in responsiveSettings) {
        l = self.breakpoints.length - 1;
        if (responsiveSettings.hasOwnProperty(breakpoint)) {
          currentBreakpoint = responsiveSettings[breakpoint].breakpoint;
          while (l >= 0) {
            if (self.breakpoints[l] && self.breakpoints[l] === currentBreakpoint) {
              self.breakpoints.splice(l, 1);
            }
            l--;
          }
          self.breakpoints.push(currentBreakpoint);
          self.breakpointSettings[currentBreakpoint] = responsiveSettings[breakpoint].settings;
        }
      }
      self.breakpoints.sort(function(a, b) {
        return (self.options.mobileFirst) ? a - b : b - a;
      });
    }
  };
  Slick.prototype.reinit = function() {
    var self = this;
    self.$slides = self.$slideTrack.children(self.options.slide).addClass('slick-slide');
    self.slideCount = self.$slides.length;
    if (self.currentSlide >= self.slideCount && self.currentSlide !== 0) {
      self.currentSlide = self.currentSlide - self.options.slidesToScroll;
    }
    if (self.slideCount <= self.options.slidesToShow) {
      self.currentSlide = 0;
    }
    self.registerBreakpoints();
    self.setProps();
    self.setupInfinite();
    self.buildArrows();
    self.updateArrows();
    self.initArrowEvents();
    self.buildDots();
    self.updateDots();
    self.initDotEvents();
    self.cleanUpSlideEvents();
    self.initSlideEvents();
    self.checkResponsive(false, true);
    if (self.options.focusOnSelect === true) {
      $(self.$slideTrack).children().on('click.slick', self.selectHandler);
    }
    self.setSlideClasses(typeof self.currentSlide === 'number' ? self.currentSlide : 0);
    self.setPosition();
    self.focusHandler();
    self.paused = !self.options.autoplay;
    self.autoPlay();
    self.$slider.trigger('reInit', [self]);
  };
  Slick.prototype.resize = function() {
    var self = this;
    if ($(window).width() !== self.windowWidth) {
      clearTimeout(self.windowDelay);
      self.windowDelay = window.setTimeout(function() {
        self.windowWidth = $(window).width();
        self.checkResponsive();
        if (!self.unslicked) {
          self.setPosition();
        }
      }, 50);
    }
  };
  Slick.prototype.removeSlide = Slick.prototype.slickRemove = function(index, removeBefore, removeAll) {
    var self = this;
    if (typeof(index) === 'boolean') {
      removeBefore = index;
      index = removeBefore === true ? 0 : self.slideCount - 1;
    } else {
      index = removeBefore === true ? --index : index;
    }
    if (self.slideCount < 1 || index < 0 || index > self.slideCount - 1) {
      return false;
    }
    self.unload();
    if (removeAll === true) {
      self.$slideTrack.children().remove();
    } else {
      self.$slideTrack.children(this.options.slide).eq(index).remove();
    }
    self.$slides = self.$slideTrack.children(this.options.slide);
    self.$slideTrack.children(this.options.slide).detach();
    self.$slideTrack.append(self.$slides);
    self.$slidesCache = self.$slides;
    self.reinit();
  };
  Slick.prototype.setCSS = function(position) {
    var self = this,
      positionProps = {},
      x, y;
    if (self.options.rtl === true) {
      position = -position;
    }
    x = self.positionProp == 'left' ? Math.ceil(position) + 'px' : '0px';
    y = self.positionProp == 'top' ? Math.ceil(position) + 'px' : '0px';
    positionProps[self.positionProp] = position;
    if (self.transformsEnabled === false) {
      self.$slideTrack.css(positionProps);
    } else {
      positionProps = {};
      if (self.cssTransitions === false) {
        positionProps[self.animType] = 'translate(' + x + ', ' + y + ')';
        self.$slideTrack.css(positionProps);
      } else {
        positionProps[self.animType] = 'translate3d(' + x + ', ' + y + ', 0px)';
        self.$slideTrack.css(positionProps);
      }
    }
  };
  Slick.prototype.setDimensions = function() {
    var self = this;
    if (self.options.vertical === false) {
      if (self.options.centerMode === true) {
        self.$list.css({
          padding: ('0px ' + self.options.centerPadding)
        });
      }
    } else {
      self.$list.height(self.$slides.first().outerHeight(true) * self.options.slidesToShow);
      if (self.options.centerMode === true) {
        self.$list.css({
          padding: (self.options.centerPadding + ' 0px')
        });
      }
    }
    self.listWidth = self.$list.width();
    self.listHeight = self.$list.height();
    if (self.options.vertical === false && self.options.variableWidth === false) {
      self.slideWidth = Math.ceil(self.listWidth / self.options.slidesToShow);
      self.$slideTrack.width(Math.ceil((self.slideWidth * self.$slideTrack.children('.slick-slide').length)));
    } else if (self.options.variableWidth === true) {
      self.$slideTrack.width(5000 * self.slideCount);
    } else {
      self.slideWidth = Math.ceil(self.listWidth);
      self.$slideTrack.height(Math.ceil((self.$slides.first().outerHeight(true) * self.$slideTrack.children('.slick-slide').length)));
    }
    var offset = self.$slides.first().outerWidth(true) - self.$slides.first().width();
    if (self.options.variableWidth === false) self.$slideTrack.children('.slick-slide').width(self.slideWidth - offset);
  };
  Slick.prototype.setFade = function() {
    var self = this,
      targetLeft;
    self.$slides.each(function(index, element) {
      targetLeft = (self.slideWidth * index) * -1;
      if (self.options.rtl === true) {
        $(element).css({
          position: 'relative',
          right: targetLeft,
          top: 0,
          zIndex: self.options.zIndex - 2,
          opacity: 0
        });
      } else {
        $(element).css({
          position: 'relative',
          left: targetLeft,
          top: 0,
          zIndex: self.options.zIndex - 2,
          opacity: 0
        });
      }
    });
    self.$slides.eq(self.currentSlide).css({
      zIndex: self.options.zIndex - 1,
      opacity: 1
    });
  };
  Slick.prototype.setHeight = function() {
    var self = this;
    if (self.options.slidesToShow === 1 && self.options.adaptiveHeight === true && self.options.vertical === false) {
      var targetHeight = self.$slides.eq(self.currentSlide).outerHeight(true);
      self.$list.css('height', targetHeight);
    }
  };
  Slick.prototype.setOption = Slick.prototype.slickSetOption = function() {
    var self = this,
      l, item, option, value, refresh = false,
      type;
    if ($.type(arguments[0]) === 'object') {
      option = arguments[0];
      refresh = arguments[1];
      type = 'multiple';
    } else if ($.type(arguments[0]) === 'string') {
      option = arguments[0];
      value = arguments[1];
      refresh = arguments[2];
      if (arguments[0] === 'responsive' && $.type(arguments[1]) === 'array') {
        type = 'responsive';
      } else if (typeof arguments[1] !== 'undefined') {
        type = 'single';
      }
    }
    if (type === 'single') {
      self.options[option] = value;
    } else if (type === 'multiple') {
      $.each(option, function(opt, val) {
        self.options[opt] = val;
      });
    } else if (type === 'responsive') {
      for (item in value) {
        if ($.type(self.options.responsive) !== 'array') {
          self.options.responsive = [value[item]];
        } else {
          l = self.options.responsive.length - 1;
          while (l >= 0) {
            if (self.options.responsive[l].breakpoint === value[item].breakpoint) {
              self.options.responsive.splice(l, 1);
            }
            l--;
          }
          self.options.responsive.push(value[item]);
        }
      }
    }
    if (refresh) {
      self.unload();
      self.reinit();
    }
  };
  Slick.prototype.setPosition = function() {
    var self = this;
    self.setDimensions();
    self.setHeight();
    if (self.options.fade === false) {
      self.setCSS(self.getLeft(self.currentSlide));
    } else {
      self.setFade();
    }
    self.$slider.trigger('setPosition', [self]);
  };
  Slick.prototype.setProps = function() {
    var self = this,
      bodyStyle = document.body.style;
    self.positionProp = self.options.vertical === true ? 'top' : 'left';
    if (self.positionProp === 'top') {
      self.$slider.addClass('slick-vertical');
    } else {
      self.$slider.removeClass('slick-vertical');
    }
    if (bodyStyle.WebkitTransition !== undefined || bodyStyle.MozTransition !== undefined || bodyStyle.msTransition !== undefined) {
      if (self.options.useCSS === true) {
        self.cssTransitions = true;
      }
    }
    if (self.options.fade) {
      if (typeof self.options.zIndex === 'number') {
        if (self.options.zIndex < 3) {
          self.options.zIndex = 3;
        }
      } else {
        self.options.zIndex = self.defaults.zIndex;
      }
    }
    if (bodyStyle.OTransform !== undefined) {
      self.animType = 'OTransform';
      self.transformType = '-o-transform';
      self.transitionType = 'OTransition';
      if (bodyStyle.perspectiveProperty === undefined && bodyStyle.webkitPerspective === undefined) self.animType = false;
    }
    if (bodyStyle.MozTransform !== undefined) {
      self.animType = 'MozTransform';
      self.transformType = '-moz-transform';
      self.transitionType = 'MozTransition';
      if (bodyStyle.perspectiveProperty === undefined && bodyStyle.MozPerspective === undefined) self.animType = false;
    }
    if (bodyStyle.webkitTransform !== undefined) {
      self.animType = 'webkitTransform';
      self.transformType = '-webkit-transform';
      self.transitionType = 'webkitTransition';
      if (bodyStyle.perspectiveProperty === undefined && bodyStyle.webkitPerspective === undefined) self.animType = false;
    }
    if (bodyStyle.msTransform !== undefined) {
      self.animType = 'msTransform';
      self.transformType = '-ms-transform';
      self.transitionType = 'msTransition';
      if (bodyStyle.msTransform === undefined) self.animType = false;
    }
    if (bodyStyle.transform !== undefined && self.animType !== false) {
      self.animType = 'transform';
      self.transformType = 'transform';
      self.transitionType = 'transition';
    }
    self.transformsEnabled = self.options.useTransform && (self.animType !== null && self.animType !== false);
  };
  Slick.prototype.setSlideClasses = function(index) {
    var self = this,
      centerOffset, allSlides, indexOffset, remainder;
    allSlides = self.$slider.find('.slick-slide').removeClass('slick-active slick-center slick-current').attr('aria-hidden', 'true');
    self.$slides.eq(index).addClass('slick-current');
    if (self.options.centerMode === true) {
      var evenCoef = self.options.slidesToShow % 2 === 0 ? 1 : 0;
      centerOffset = Math.floor(self.options.slidesToShow / 2);
      if (self.options.infinite === true) {
        if (index >= centerOffset && index <= (self.slideCount - 1) - centerOffset) {
          self.$slides.slice(index - centerOffset + evenCoef, index + centerOffset + 1).addClass('slick-active').attr('aria-hidden', 'false');
        } else {
          indexOffset = self.options.slidesToShow + index;
          allSlides.slice(indexOffset - centerOffset + 1 + evenCoef, indexOffset + centerOffset + 2).addClass('slick-active').attr('aria-hidden', 'false');
        }
        if (index === 0) {
          allSlides.eq(allSlides.length - 1 - self.options.slidesToShow).addClass('slick-center');
        } else if (index === self.slideCount - 1) {
          allSlides.eq(self.options.slidesToShow).addClass('slick-center');
        }
      }
      self.$slides.eq(index).addClass('slick-center');
    } else {
      if (index >= 0 && index <= (self.slideCount - self.options.slidesToShow)) {
        self.$slides.slice(index, index + self.options.slidesToShow).addClass('slick-active').attr('aria-hidden', 'false');
      } else if (allSlides.length <= self.options.slidesToShow) {
        allSlides.addClass('slick-active').attr('aria-hidden', 'false');
      } else {
        remainder = self.slideCount % self.options.slidesToShow;
        indexOffset = self.options.infinite === true ? self.options.slidesToShow + index : index;
        if (self.options.slidesToShow == self.options.slidesToScroll && (self.slideCount - index) < self.options.slidesToShow) {
          allSlides.slice(indexOffset - (self.options.slidesToShow - remainder), indexOffset + remainder).addClass('slick-active').attr('aria-hidden', 'false');
        } else {
          allSlides.slice(indexOffset, indexOffset + self.options.slidesToShow).addClass('slick-active').attr('aria-hidden', 'false');
        }
      }
    }
    if (self.options.lazyLoad === 'ondemand' || self.options.lazyLoad === 'anticipated') {
      self.lazyLoad();
    }
  };
  Slick.prototype.setupInfinite = function() {
    var self = this,
      i, slideIndex, infiniteCount;
    if (self.options.fade === true) {
      self.options.centerMode = false;
    }
    if (self.options.infinite === true && self.options.fade === false) {
      slideIndex = null;
      if (self.slideCount > self.options.slidesToShow) {
        if (self.options.centerMode === true) {
          infiniteCount = self.options.slidesToShow + 1;
        } else {
          infiniteCount = self.options.slidesToShow;
        }
        for (i = self.slideCount; i > (self.slideCount -
            infiniteCount); i -= 1) {
          slideIndex = i - 1;
          $(self.$slides[slideIndex]).clone(true).attr('id', '').attr('data-slick-index', slideIndex - self.slideCount).prependTo(self.$slideTrack).addClass('slick-cloned');
        }
        for (i = 0; i < infiniteCount + self.slideCount; i += 1) {
          slideIndex = i;
          $(self.$slides[slideIndex]).clone(true).attr('id', '').attr('data-slick-index', slideIndex + self.slideCount).appendTo(self.$slideTrack).addClass('slick-cloned');
        }
        self.$slideTrack.find('.slick-cloned').find('[id]').each(function() {
          $(this).attr('id', '');
        });
      }
    }
  };
  Slick.prototype.interrupt = function(toggle) {
    var self = this;
    if (!toggle) {
      self.autoPlay();
    }
    self.interrupted = toggle;
  };
  Slick.prototype.selectHandler = function(event) {
    var self = this;
    var targetElement = $(event.target).is('.slick-slide') ? $(event.target) : $(event.target).parents('.slick-slide');
    var index = parseInt(targetElement.attr('data-slick-index'));
    if (!index) index = 0;
    if (self.slideCount <= self.options.slidesToShow) {
      self.slideHandler(index, false, true);
      return;
    }
    self.slideHandler(index);
  };
  Slick.prototype.slideHandler = function(index, sync, dontAnimate) {
    var targetSlide, animSlide, oldSlide, slideLeft, targetLeft = null,
      self = this,
      navTarget;
    sync = sync || false;
    if (self.animating === true && self.options.waitForAnimate === true) {
      return;
    }
    if (self.options.fade === true && self.currentSlide === index) {
      return;
    }
    if (sync === false) {
      self.asNavFor(index);
    }
    targetSlide = index;
    targetLeft = self.getLeft(targetSlide);
    slideLeft = self.getLeft(self.currentSlide);
    self.currentLeft = self.swipeLeft === null ? slideLeft : self.swipeLeft;
    if (self.options.infinite === false && self.options.centerMode === false && (index < 0 || index > self.getDotCount() * self.options.slidesToScroll)) {
      if (self.options.fade === false) {
        targetSlide = self.currentSlide;
        if (dontAnimate !== true && self.slideCount > self.options.slidesToShow) {
          self.animateSlide(slideLeft, function() {
            self.postSlide(targetSlide);
          });
        } else {
          self.postSlide(targetSlide);
        }
      }
      return;
    } else if (self.options.infinite === false && self.options.centerMode === true && (index < 0 || index > (self.slideCount - self.options.slidesToScroll))) {
      if (self.options.fade === false) {
        targetSlide = self.currentSlide;
        if (dontAnimate !== true && self.slideCount > self.options.slidesToShow) {
          self.animateSlide(slideLeft, function() {
            self.postSlide(targetSlide);
          });
        } else {
          self.postSlide(targetSlide);
        }
      }
      return;
    }
    if (self.options.autoplay) {
      clearInterval(self.autoPlayTimer);
    }
    if (targetSlide < 0) {
      if (self.slideCount % self.options.slidesToScroll !== 0) {
        animSlide = self.slideCount - (self.slideCount % self.options.slidesToScroll);
      } else {
        animSlide = self.slideCount + targetSlide;
      }
    } else if (targetSlide >= self.slideCount) {
      if (self.slideCount % self.options.slidesToScroll !== 0) {
        animSlide = 0;
      } else {
        animSlide = targetSlide - self.slideCount;
      }
    } else {
      animSlide = targetSlide;
    }
    self.animating = true;
    self.$slider.trigger('beforeChange', [self, self.currentSlide, animSlide]);
    oldSlide = self.currentSlide;
    self.currentSlide = animSlide;
    self.setSlideClasses(self.currentSlide);
    if (self.options.asNavFor) {
      navTarget = self.getNavTarget();
      navTarget = navTarget.slick('getSlick');
      if (navTarget.slideCount <= navTarget.options.slidesToShow) {
        navTarget.setSlideClasses(self.currentSlide);
      }
    }
    self.updateDots();
    self.updateArrows();
    if (self.options.fade === true) {
      if (dontAnimate !== true) {
        self.fadeSlideOut(oldSlide);
        self.fadeSlide(animSlide, function() {
          self.postSlide(animSlide);
        });
      } else {
        self.postSlide(animSlide);
      }
      self.animateHeight();
      return;
    }
    if (dontAnimate !== true && self.slideCount > self.options.slidesToShow) {
      self.animateSlide(targetLeft, function() {
        self.postSlide(animSlide);
      });
    } else {
      self.postSlide(animSlide);
    }
  };
  Slick.prototype.startLoad = function() {
    var self = this;
    if (self.options.arrows === true && self.slideCount > self.options.slidesToShow) {
      self.$prevArrow.hide();
      self.$nextArrow.hide();
    }
    if (self.options.dots === true && self.slideCount > self.options.slidesToShow) {
      self.$dots.hide();
    }
    self.$slider.addClass('slick-loading');
  };
  Slick.prototype.swipeDirection = function() {
    var xDist, yDist, r, swipeAngle, self = this;
    xDist = self.touchObject.startX - self.touchObject.curX;
    yDist = self.touchObject.startY - self.touchObject.curY;
    r = Math.atan2(yDist, xDist);
    swipeAngle = Math.round(r * 180 / Math.PI);
    if (swipeAngle < 0) {
      swipeAngle = 360 - Math.abs(swipeAngle);
    }
    if ((swipeAngle <= 45) && (swipeAngle >= 0)) {
      return (self.options.rtl === false ? 'left' : 'right');
    }
    if ((swipeAngle <= 360) && (swipeAngle >= 315)) {
      return (self.options.rtl === false ? 'left' : 'right');
    }
    if ((swipeAngle >= 135) && (swipeAngle <= 225)) {
      return (self.options.rtl === false ? 'right' : 'left');
    }
    if (self.options.verticalSwiping === true) {
      if ((swipeAngle >= 35) && (swipeAngle <= 135)) {
        return 'down';
      } else {
        return 'up';
      }
    }
    return 'vertical';
  };
  Slick.prototype.swipeEnd = function(event) {
    var self = this,
      slideCount, direction;
    self.dragging = false;
    self.swiping = false;
    if (self.scrolling) {
      self.scrolling = false;
      return false;
    }
    self.interrupted = false;
    self.shouldClick = (self.touchObject.swipeLength > 10) ? false : true;
    if (self.touchObject.curX === undefined) {
      return false;
    }
    if (self.touchObject.edgeHit === true) {
      self.$slider.trigger('edge', [self, self.swipeDirection()]);
    }
    if (self.touchObject.swipeLength >= self.touchObject.minSwipe) {
      direction = self.swipeDirection();
      switch (direction) {
        case 'left':
        case 'down':
          slideCount = self.options.swipeToSlide ? self.checkNavigable(self.currentSlide + self.getSlideCount()) : self.currentSlide + self.getSlideCount();
          self.currentDirection = 0;
          break;
        case 'right':
        case 'up':
          slideCount = self.options.swipeToSlide ? self.checkNavigable(self.currentSlide - self.getSlideCount()) : self.currentSlide - self.getSlideCount();
          self.currentDirection = 1;
          break;
        default:
      }
      if (direction != 'vertical') {
        self.slideHandler(slideCount);
        self.touchObject = {};
        self.$slider.trigger('swipe', [self, direction]);
      }
    } else {
      if (self.touchObject.startX !== self.touchObject.curX) {
        self.slideHandler(self.currentSlide);
        self.touchObject = {};
      }
    }
  };
  Slick.prototype.swipeHandler = function(event) {
    var self = this;
    if ((self.options.swipe === false) || ('ontouchend' in document && self.options.swipe === false)) {
      return;
    } else if (self.options.draggable === false && event.type.indexOf('mouse') !== -1) {
      return;
    }
    self.touchObject.fingerCount = event.originalEvent && event.originalEvent.touches !== undefined ? event.originalEvent.touches.length : 1;
    self.touchObject.minSwipe = self.listWidth / self.options.touchThreshold;
    if (self.options.verticalSwiping === true) {
      self.touchObject.minSwipe = self.listHeight / self.options.touchThreshold;
    }
    switch (event.data.action) {
      case 'start':
        self.swipeStart(event);
        break;
      case 'move':
        self.swipeMove(event);
        break;
      case 'end':
        self.swipeEnd(event);
        break;
    }
  };
  Slick.prototype.swipeMove = function(event) {
    var self = this,
      edgeWasHit = false,
      curLeft, swipeDirection, swipeLength, positionOffset, touches, verticalSwipeLength;
    touches = event.originalEvent !== undefined ? event.originalEvent.touches : null;
    if (!self.dragging || self.scrolling || touches && touches.length !== 1) {
      return false;
    }
    curLeft = self.getLeft(self.currentSlide);
    self.touchObject.curX = touches !== undefined ? touches[0].pageX : event.clientX;
    self.touchObject.curY = touches !== undefined ? touches[0].pageY : event.clientY;
    self.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(self.touchObject.curX - self.touchObject.startX, 2)));
    verticalSwipeLength = Math.round(Math.sqrt(Math.pow(self.touchObject.curY - self.touchObject.startY, 2)));
    if (!self.options.verticalSwiping && !self.swiping && verticalSwipeLength > 4) {
      self.scrolling = true;
      return false;
    }
    if (self.options.verticalSwiping === true) {
      self.touchObject.swipeLength = verticalSwipeLength;
    }
    swipeDirection = self.swipeDirection();
    if (event.originalEvent !== undefined && self.touchObject.swipeLength > 4) {
      self.swiping = true;
      event.preventDefault();
    }
    positionOffset = (self.options.rtl === false ? 1 : -1) * (self.touchObject.curX > self.touchObject.startX ? 1 : -1);
    if (self.options.verticalSwiping === true) {
      positionOffset = self.touchObject.curY > self.touchObject.startY ? 1 : -1;
    }
    swipeLength = self.touchObject.swipeLength;
    self.touchObject.edgeHit = false;
    if (self.options.infinite === false) {
      if ((self.currentSlide === 0 && swipeDirection === 'right') || (self.currentSlide >= self.getDotCount() && swipeDirection === 'left')) {
        swipeLength = self.touchObject.swipeLength * self.options.edgeFriction;
        self.touchObject.edgeHit = true;
      }
    }
    if (self.options.vertical === false) {
      self.swipeLeft = curLeft + swipeLength * positionOffset;
    } else {
      self.swipeLeft = curLeft + (swipeLength * (self.$list.height() / self.listWidth)) * positionOffset;
    }
    if (self.options.verticalSwiping === true) {
      self.swipeLeft = curLeft + swipeLength * positionOffset;
    }
    if (self.options.fade === true || self.options.touchMove === false) {
      return false;
    }
    if (self.animating === true) {
      self.swipeLeft = null;
      return false;
    }
    self.setCSS(self.swipeLeft);
  };
  Slick.prototype.swipeStart = function(event) {
    var self = this,
      touches;
    self.interrupted = true;
    if (self.touchObject.fingerCount !== 1 || self.slideCount <= self.options.slidesToShow) {
      self.touchObject = {};
      return false;
    }
    if (event.originalEvent !== undefined && event.originalEvent.touches !== undefined) {
      touches = event.originalEvent.touches[0];
    }
    self.touchObject.startX = self.touchObject.curX = touches !== undefined ? touches.pageX : event.clientX;
    self.touchObject.startY = self.touchObject.curY = touches !== undefined ? touches.pageY : event.clientY;
    self.dragging = true;
  };
  Slick.prototype.unfilterSlides = Slick.prototype.slickUnfilter = function() {
    var self = this;
    if (self.$slidesCache !== null) {
      self.unload();
      self.$slideTrack.children(this.options.slide).detach();
      self.$slidesCache.appendTo(self.$slideTrack);
      self.reinit();
    }
  };
  Slick.prototype.unload = function() {
    var self = this;
    $('.slick-cloned', self.$slider).remove();
    if (self.$dots) {
      self.$dots.remove();
    }
    if (self.$prevArrow && self.htmlExpr.test(self.options.prevArrow)) {
      self.$prevArrow.remove();
    }
    if (self.$nextArrow && self.htmlExpr.test(self.options.nextArrow)) {
      self.$nextArrow.remove();
    }
    self.$slides.removeClass('slick-slide slick-active slick-visible slick-current').attr('aria-hidden', 'true').css('width', '');
  };
  Slick.prototype.unslick = function(fromBreakpoint) {
    var self = this;
    self.$slider.trigger('unslick', [self, fromBreakpoint]);
    self.destroy();
  };
  Slick.prototype.updateArrows = function() {
    var self = this,
      centerOffset;
    centerOffset = Math.floor(self.options.slidesToShow / 2);
    if (self.options.arrows === true && self.slideCount > self.options.slidesToShow && !self.options.infinite) {
      self.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
      self.$nextArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
      if (self.currentSlide === 0) {
        self.$prevArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
        self.$nextArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
      } else if (self.currentSlide >= self.slideCount - self.options.slidesToShow && self.options.centerMode === false) {
        self.$nextArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
        self.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
      } else if (self.currentSlide >= self.slideCount - 1 && self.options.centerMode === true) {
        self.$nextArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
        self.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
      }
    }
  };
  Slick.prototype.updateDots = function() {
    var self = this;
    if (self.$dots !== null) {
      self.$dots.find('li').removeClass('slick-active').end();
      self.$dots.find('li').eq(Math.floor(self.currentSlide / self.options.slidesToScroll)).addClass('slick-active');
    }
  };
  Slick.prototype.visibility = function() {
    var self = this;
    if (self.options.autoplay) {
      if (document[self.hidden]) {
        self.interrupted = true;
      } else {
        self.interrupted = false;
      }
    }
  };
  $.fn.slick = function() {
    var self = this,
      opt = arguments[0],
      args = Array.prototype.slice.call(arguments, 1),
      l = self.length,
      i, ret;
    for (i = 0; i < l; i++) {
      if (typeof opt == 'object' || typeof opt == 'undefined')
        self[i].slick = new Slick(self[i], opt);
      else
        ret = self[i].slick[opt].apply(self[i].slick, args);
      if (typeof ret != 'undefined') return ret;
    }
    return self;
  };
}));;
Aquarius.module.define('Module/Wow', {
  config: {},
  init: function() {
    var self = this;
    new WOW().init();
  }
});;
Aquarius.component.define('Tools/TransitionFix', function($element, options) {
  var defaults = {
    'class': 'no-transitions'
  };

  function Tool() {}
  Tool.prototype = {
    init: function() {
      var self = this;
      this.$element = $element;
      this.options = $.extend(true, defaults, options);
      this.$element.removeClass(this.options.class);
    }
  };
  return Tool;
});;
Aquarius.component.define('Widgets/Carousel/Small', function($element, options) {
  var defaults = {
    autoplay: false,
    autoplaySpeed: 5000,
    cssEase: 'linear',
    initialSlide: 1,
    infinite: false,
    centerMode: true,
    centerPadding: '0',
    slidesToShow: 3,
    dots: false,
    prevArrow: '<div class="carousel-control left transition-xs">\n' + '        <i class="fa fa-2x fa-chevron-left"></i>\n' + '    </div>',
    nextArrow: '<div class="carousel-control right transition-xs">\n' + '        <i class="fa fa-2x fa-chevron-right"></i>\n' + '    </div>',
    responsive: [{
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 3
      }
    }, {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }]
  };

  function Carousel() {}
  Carousel.prototype = {
    init: function() {
      var self = this;
      this.$element = $element;
      this.options = $.extend(true, defaults, options);
      this.$element.slick(this.options);
    }
  };
  return Carousel;
});;
// Aquarius.component.define('Widgets/Carousel', function($element, options) {
//   var defaults = {
//     interval: 5000,
//     slideSelector: '.carousel-slide',
//     activeSlideClass: 'active',
//     animationDuration: 1000,
//     controlSelector: '.carousel-control',
//     captionSelector: '.carousel-caption',
//     arrowSelector: '.carousel-bottom-arrow'
//   };
//
//   function Carousel() {}
//   Carousel.prototype = {
//     init: function() {
//       var self = this;
//       this.$element = $element;
//       this.options = $.extend(true, defaults, options);
//       this.$slides = $element.find(this.options.slideSelector);
//       this.$activeSlide = this.$slides.filter('.' + this.options.activeSlideClass);
//       this.$element.find(self.options.arrowSelector).on('click', function() {
//         $('html,body').stop().animate({
//           scrollTop: parseFloat(self.$element.offset().top + self.$element.outerHeight() -
//             parseInt($('.main-header .header-content').outerHeight()))
//         }, 1000, 'easeInOutQuint', function() {});
//       });
//       if (this.$slides.length <= 1) {
//         return;
//       }
//       if (!this.$activeSlide.length) {
//         this.$activeSlide = this.$slides.first().addClass(this.options.activeClass);
//       }
//       if (document.documentMode || /Edge/.test(navigator.userAgent)) {
//         this.$activeSlide.hide(10);
//         this.$slides.css('transform', 'rotate(0.01deg)');
//         this.$activeSlide.show(10);
//       }
//       this.cycle();
//       this.$element.find(self.options.controlSelector).on('click', function() {
//         var $newSlide;
//         if ($(this).hasClass('right')) {
//           $newSlide = self.getNextSlide();
//         } else if ($(this).hasClass('left')) {
//           $newSlide = self.getPreviousSlide();
//         } else {
//           return false;
//         }
//         self.stop();
//         self.show($newSlide, self.options.animationDuration / 2);
//         self.cycle();
//       });
//     },
//     stop: function() {
//       clearInterval(this.interval);
//     },
//     cycle: function() {
//       var self = this;
//       this.interval = setInterval(function() {
//         var $nextSlide = self.getNextSlide();
//         self.show($nextSlide, self.options.animationDuration);
//       }, this.options.interval);
//     },
//     getNextSlide: function() {
//       var $nextSlide = this.$activeSlide.next(this.options.slideSelector);
//       if (!$nextSlide.length) {
//         $nextSlide = this.$slides.first();
//       }
//       return $nextSlide;
//     },
//     getPreviousSlide: function() {
//       var $previousSlide = this.$activeSlide.prev(this.options.slideSelector);
//       if (!$previousSlide.length) {
//         $previousSlide = this.$slides.last();
//       }
//       return $previousSlide;
//     },
//     show: function(slide, duration) {
//       var self = this;
//       if (!slide) {
//         return;
//       }
//       if (typeof duration == 'undefined') {
//         duration = self.options.animationDuration;
//       }
//       if (this.$activeSlide.css('position') == 'static') {
//         duration = 0;
//       }
//       var $currentSlide = this.$activeSlide;
//       var $currentCaption = $currentSlide.find(this.options.captionSelector);
//       var $nextSlide = $(slide);
//       var $nextCaption = $nextSlide.find(this.options.captionSelector);
//       if ($currentCaption.length) {
//         $currentCaption.fadeOut(duration / 2, function() {
//           $currentSlide.fadeOut(duration);
//         });
//       } else {
//         $currentSlide.fadeOut(duration);
//       }
//       $nextCaption.hide();
//       $nextSlide.fadeIn(duration, function() {
//         $nextCaption.fadeIn(duration / 2);
//       });
//       this.$activeSlide = $nextSlide;
//       this.$slides.removeClass(this.options.activeClass);
//       this.$activeSlide.addClass(this.options.activeSlideClass);
//     }
//   };
//   return Carousel;
// });;
Aquarius.component.define('Widgets/Navigation', function($element, options) {
  var defaults = {};

  function Navigation() {}
  Navigation.prototype = {
    init: function() {
      var self = this;
      this.$element = $element;
      this.$element.on('click', '.navigation a[href]', function() {
        $(this).toggleClass('active');
      });
      this.$element.on('click', '.navigation-toggle', function() {
        self.$element.toggleClass('open');
      });
      self.initLinks();
    },
    getOffset: function() {
      return Math.max(parseInt($('.main-header .header-content').outerHeight()), 0) + 50;
    },
    initLinks: function() {
      var self = this;
      this.$element.on('click', 'a', function(event) {
        var $link = $(this);
        if (self.$element.hasClass('open') && $link.siblings('.navigation').length) {
          var $button = $link.parent();
          if (!$button.hasClass('open')) {
            event.preventDefault();
            $button.addClass('open').siblings('.open').removeClass('open');
            return false;
          }
        }
        var href = $link.attr('href');
        if (!href || !/^\/?\#/.test(href)) {
          return;
        }
        var pageTop = href == location.pathname;
        if ($link.attr('href').match(/\/?\#/) || pageTop) {
          var scrollTop;
          var name = null;
          var offset;
          var $target;
          if (!pageTop) {
            name = $link.attr('href').replace(/^.*\#/, '');
            offset = self.getOffset();
            $target = $('a[name="' + name + '"]');
            if (!$target.length) {
              $link.closest('li').addClass('active').siblings('li').removeClass('active');
              return;
            }
            scrollTop = (parseInt($target.offset().top) - offset);
          } else {
            scrollTop = 0;
          }
          $('html,body').stop().animate({
            scrollTop: scrollTop
          }, 1000, 'easeInOutQuint', function() {
            if (pageTop) {
              location.hash = '';
              $link.closest('li').addClass('active').siblings('li').removeClass('active');
            }
          });
          event.preventDefault();
          if ($target) {
            $target.attr('name', 'PENDING');
            location.hash = name;
            $target.attr('name', name);
          }
        } else if (pageTop) {
          $link.closest('li').addClass('active').siblings('li').removeClass('active');
        }
      });
    }
  };
  return Navigation;
});;
Aquarius.component.define('Widgets/Swipebox', function($element, options) {
  var defaults = {
    useCSS: true,
    useSVG: true,
    initialIndexOnArray: 0,
    hideCloseButtonOnMobile: false,
    removeBarsOnMobile: true,
    hideBarsDelay: 0,
    videoMaxWidth: 1140,
    beforeOpen: function() {},
    afterOpen: null,
    afterClose: function() {},
    loopAtEnd: false
  };

  function Swipebox() {}
  Swipebox.prototype = {
    init: function() {
      var self = this;
      this.options = Aquarius.extend(defaults, options);
      this.$element = $element;
      this.initElement();
    },
    initElement: function() {
      var self = this;
      var types = [];
      var $button = $('[data-swipeboxbutton]')
      var $items = self.$element.find('[data-swipebox]');
      $items.each(function(i, item) {
        var $item = $(item);
        var type = $item.data('swipebox');
        if ($.inArray(type, types) == -1) {
          types.push(type);
        }
      });
      if (!types.length) {
        $items.swipebox(self.options);
      } else {
        $.each(types, function(i, type) {
          self.$element.find('[data-swipebox="' + type + '"]').swipebox(self.options);
        });
      }
      $button.off('click').on('click', function(e) {
        var swipeArray = [];
        e.preventDefault();
        $items.each(function(i, item) {
          var $item = $(item);
          var type = $item.data('swipebox');
          swipeArray.push({
            href: $item[0].href,
            title: i + 1
          })
        });
        $.swipebox(swipeArray);
      })
    }
  };
  return new Swipebox;
}).dependsOnRemote('/assets/frontend/js/vendor/swipebox/swipebox.js').dependsOnRemote('/assets/frontend/js/vendor/swipebox/swipebox.css');;
Aquarius.component.define('Widgets/Accordion', function($element, options) {
  var defaults = {};

  function Accordion() {}
  Accordion.prototype = {
    init: function() {
      var self = this;
      this.$element = $element;
      this.options = $.extend(true, defaults, options);
      this.$element.find('[data-accordion-item]').each(function() {
        var $item = $(this);
        $item.find('[data-accordion-trigger]').on('click', function() {
          $item.toggleClass('active');
          $item.siblings('.active').removeClass('active');
        });
      });
    }
  };
  return Accordion;
});;
Aquarius.component.define('Frontend/Wheel', function($element, options) {
  var defaults = {
    animated: false
  };

  function Wheel() {}
  Wheel.prototype = {
    init: function() {
      var self = this;
      this.$element = $element;
      this.options = $.extend(true, defaults, options);
      this.rotation = 0;
      this.index = 0;
      this.baseRotation = 0;
      this.$wheel = this.$element.find('.service-wheel');
      this.$contents = this.$element.find('.service-wheel-content');
      this.$slices = this.$wheel.find('.service-wheel-slice');
      this.$fader = this.$wheel.find('.service-wheel-background-fader');
      var order = [0, 1, 3, 2];
      if (this.options.animated) {
        var stepperInterval = setInterval(function() {
          var orderIndex = order.indexOf(self.index);
          var nextIndex;
          if (typeof order[orderIndex + 1] !== 'undefined') {
            nextIndex = order[orderIndex + 1];
          } else {
            nextIndex = order[0];
          }
          self.activateSlide(nextIndex);
        }, 5000);
      }
      this.$slices.on('click', function() {
        var $activeSlice = $(this);
        var activeIndex = self.$slices.index($activeSlice);
        self.activateSlide(activeIndex);
      });
      self.activateSlide(self.$slices.index(self.$slices.filter('.active')));
    },
    activateSlide: function(index) {
      var self = this;
      var $activeSlice = self.$slices.eq(index);
      $activeSlice.addClass('active').siblings('.active').removeClass('active');
      self.rotation = self.getRotation(index);
      self.index = index;
      self.$fader.css('transform', 'rotate(' + self.rotation + 'deg)');
      self.$contents.eq(index).addClass('active').siblings('.active').removeClass('active');
    },
    getRotation: function(newIndex) {
      var oldIndex = this.index;
      var multiplier = 0;
      if (oldIndex === 2 && !newIndex) {
        this.baseRotation += 360;
      } else if (!oldIndex && newIndex === 2) {
        this.baseRotation -= 360;
      }
      if (newIndex === 2) {
        multiplier = 3;
      } else if (newIndex === 3) {
        multiplier = 2;
      } else {
        multiplier = newIndex;
      }
      return this.baseRotation + multiplier * 90;
    }
  };
  return Wheel;
});;
Aquarius.component.define('Frontend/CaseStudies', function($element, options) {
  var defaults = {
    limit: 10,
    url: '',
    offset: 10,
    holder: '.case-studies-holder',
    offsetHolder: '.case-studies-offset-holder',
    buttonSelector: '.btn'
  };

  function Widget() {}
  Widget.prototype = {
    init: function() {
      var self = this;
      this.options = Aquarius.extend(defaults, options);
      this.$element = $element;
      this.offset = this.options.start;
      this.initElement();
    },
    initElement: function() {
      var self = this;
      var $button = this.$element.find(self.options.buttonSelector);
      var $caseStudiesHolder = this.$element.find(self.options.holder);
      var $offsetHolder = this.$element.find(self.options.offsetHolder);
      $button.on('click', function(event) {
        event.preventDefault();
        var originalText = $button.text();
        $button.prop('disabled', true).text(Aquarius.translate('Please wait..'));
        Aquarius.ajax({
          url: self.options.url + (self.options.url.match(/\?/) ? '&' : '?') + 'limit=' + self.options.limit + '&offset=' + self.offset
        }).done(function(response) {
          var $html = $(response.html);
          $html.find('.wow.fadeIn').css('opacity', 0);
          setTimeout(function() {
            $html.find('.wow.fadeIn').css('opacity', 1);
          }, 100);
          $caseStudiesHolder.append($html);
          self.offset += parseInt(response.queryCount);
          $offsetHolder.empty().append(self.offset);
          if (parseInt(response.totalCount) <= self.offset) {
            $button.hide();
          } else {
            $button.prop('disabled', false);
            if ($button.data('translation')) {
              $button.text(Aquarius.translate($button.data('translation'), {
                'count': Math.min(self.options.limit, parseInt(response.totalCount) - self.offset)
              }))
            } else {
              $button.text(originalText);
            }
          }
        });
      });
    }
  };
  return new Widget;
});;
Aquarius.component.define('Frontend/News', function($element, options) {
  var defaults = {
    limit: 10,
    url: '',
    offset: 10,
    holder: '.news-holder',
    offsetHolder: '.news-offset-holder',
    buttonSelector: '.btn'
  };

  function Widget() {}
  Widget.prototype = {
    init: function() {
      var self = this;
      this.options = Aquarius.extend(defaults, options);
      this.$element = $element;
      this.offset = this.options.start;
      this.initElement();
    },
    initElement: function() {
      var self = this;
      var $button = this.$element.find(self.options.buttonSelector);
      var $newsHolder = this.$element.find(self.options.holder);
      var $offsetHolder = this.$element.find(self.options.offsetHolder);
      $button.on('click', function(event) {
        event.preventDefault();
        var originalText = $button.text();
        $button.prop('disabled', true).text(Aquarius.translate('Please wait..'));
        Aquarius.ajax({
          url: self.options.url + (self.options.url.match(/\?/) ? '&' : '?') + 'limit=' + self.options.limit + '&offset=' + self.offset
        }).done(function(response) {
          var $html = $(response.html);
          $html.find('.wow.fadeIn').css('opacity', 0);
          setTimeout(function() {
            $html.find('.wow.fadeIn').css('opacity', 1);
          }, 100);
          $newsHolder.append($html);
          self.offset += parseInt(response.queryCount);
          $offsetHolder.empty().append(self.offset);
          if (parseInt(response.totalCount) <= self.offset) {
            $button.hide();
          } else {
            $button.prop('disabled', false);
            if ($button.data('translation')) {
              $button.text(Aquarius.translate($button.data('translation'), {
                'count': Math.min(self.options.limit, parseInt(response.totalCount) - self.offset)
              }))
            } else {
              $button.text(originalText);
            }
          }
        });
      });
    }
  };
  return new Widget;
});;
Aquarius.component.define('Frontend/Projects', function($element, options) {
  var defaults = {
    limit: 10,
    url: '',
    offset: 10,
    holder: '.projects-holder',
    offsetHolder: '.projects-offset-holder',
    buttonSelector: '.btn'
  };

  function Widget() {}
  Widget.prototype = {
    init: function() {
      var self = this;
      this.options = Aquarius.extend(defaults, options);
      this.$element = $element;
      this.offset = this.options.start;
      this.initElement();
    },
    initElement: function() {
      var self = this;
      var $button = this.$element.find(self.options.buttonSelector);
      var $projectsHolder = this.$element.find(self.options.holder);
      var $offsetHolder = this.$element.find(self.options.offsetHolder);
      $button.on('click', function(event) {
        event.preventDefault();
        var originalText = $button.text();
        $button.prop('disabled', true).text(Aquarius.translate('Please wait..'));
        Aquarius.ajax({
          url: self.options.url + (self.options.url.match(/\?/) ? '&' : '?') + 'limit=' + self.options.limit + '&offset=' + self.offset
        }).done(function(response) {
          var $html = $(response.html);
          $html.find('.wow.fadeIn').css('opacity', 0);
          setTimeout(function() {
            $html.find('.wow.fadeIn').css('opacity', 1);
          }, 100);
          $projectsHolder.append($html);
          self.offset += parseInt(response.queryCount);
          $offsetHolder.empty().append(self.offset);
          if (parseInt(response.totalCount) <= self.offset) {
            $button.hide();
          } else {
            $button.prop('disabled', false);
            if ($button.data('translation')) {
              $button.text(Aquarius.translate($button.data('translation'), {
                'count': Math.min(self.options.limit, parseInt(response.totalCount) - self.offset)
              }))
            } else {
              $button.text(originalText);
            }
          }
        });
      });
    }
  };
  return new Widget;
});;
Aquarius.component.define('Frontend/Anchor', function($element, options) {
  var defaults = {};

  function Anchor() {}
  Anchor.prototype = {
    init: function() {
      var self = this;
      this.options = Aquarius.extend(defaults, options);
      this.$element = $element;
      this.initElement();
    },
    initElement: function() {
      var self = this;
      this.$element.on('click', function(event) {
        var anchor = self.$element.attr('href').replace('#', '');
        var $target = $('a[name="' + anchor + '"]');
        if ($target.length) {
          event.preventDefault();
          $('html,body').stop().animate({
            scrollTop: parseFloat($target.offset().top - parseInt($('.main-header .header-content').outerHeight()))
          }, 1000, 'easeInOutQuint', function() {
            $target.attr('name', 'PENDING');
            location.hash = anchor;
            $target.attr('name', anchor);
          });
          return false;
        }
      });
    }
  };
  return new Anchor;
});;
Aquarius.component.define('Frontend/Form', function($element, options) {
  var defaults = {
    action: null,
    validate: true,
    classes: {
      error: 'error',
      errorList: 'error-list'
    },
    ignoredFields: ['___params']
  };

  function Form() {}
  Form.prototype = {
    init: function() {
      var self = this;
      var promise = this.parent.init();
      this.setOptions(Aquarius.extend(defaults, options));
      var $button = self.$element.find('button');
      var buttonHtml = $button.html();
      this.namespace.listen('submit:after', function(params) {
        self.removeErrors();
        $button.prop('disabled', false).html(buttonHtml);
        if (params.result.isValid) {
          if (params.result.redirectUrl) {
            self.$element.trigger('form.redirect');
            window.location = params.result.redirectUrl;
          } else {
            self.$element.hide().siblings('.success-page').show();
          }
        } else {
          self.addErrors(params.result.messages);
        }
      });
      this.namespace.listen('submit:before', function(params) {
        $button.prop('disabled', true).html(buttonHtml + ' ' + '<i class="fa fa-spinner fa-spin fa-spin-fast"></i>');
      });
      return promise;
    },
    addErrorsToElement: function(element, errors) {
      var self = this;
      var $element = $(element);
      if ($element.next('label').length) {
        $element.addClass('error');
        this.parent.addErrorsToElement($element.next('label'), errors);
      } else {
        this.parent.addErrorsToElement(element, errors);
      }
    }
  };
  return Form;
}).extends('Form');;
Aquarius.component.define('Frontend/Preloader', function($element, options) {
  var defaults = {};

  function Speakers() {}
  Speakers.prototype = {
    init: function() {
      var self = this;
      this.$element = $element;
      this.options = $.extend(true, defaults, options);
      window.onload = function() {
        self.$element.fadeOut(300);
      };
    }
  };
  return Speakers;
});;
Aquarius.component.define('Frontend/Contact', function($element, options) {
  var defaults = {};

  function Contact() {}
  Contact.prototype = {
    init: function() {
      var self = this;
      this.$element = $element;
      this.options = $.extend(true, defaults, options);
      this.$window = $(this.$element.data('window'));
      this.$element.on('click', function() {
        self.$window.show();
        self.$element.hide();
        $(window).scrollTop(parseInt($('body').height()));
      });
      this.$window.on('click', '.closer', function() {
        self.$window.hide();
        self.$element.show();
      });
    }
  };
  return Contact;
});;
Aquarius.component.define('Frontend/Search', function($element, options) {
  var defaults = {};

  function Search() {}
  Search.prototype = {
    init: function() {
      var self = this;
      this.options = Aquarius.extend(defaults, options);
      this.$element = $element;
      this.$overlay = this.$element;
      this.$content = this.$overlay.find('.search-content');
      this.term = null;
      this.scrollTop = null;
      this.$element.on('open', function(event, term) {
        event.preventDefault();
        self.scrollTop = $(window).scrollTop();
        $('html').addClass('noscroll');
        self.$overlay.show();
        self.$form.find('input:first').focus().val(term).trigger('keyup');
        return false;
      });
      this.$overlay.on('click', '.closer', function() {
        self.$overlay.hide();
        self.$form.find('input').blur();
        $('html').removeClass('noscroll');
        $(window).scrollTop(self.scrollTop);
        self.$content.html('');
      });
      this.$form = this.$overlay.find('form').first();
      this.$form.on('keyup', Aquarius.debounce(300, function(event) {
        event.preventDefault();
        var $input = $(event.target);
        if ($input.val() !== $input.data('value')) {
          self.search($input.val());
          $input.data('value', $input.val());
        }
        return false;
      }));
      this.$form.on('submit', function(event) {
        event.preventDefault();
        return false;
      });
    },
    search: function(term) {
      var self = this;
      if (this.xhr) {
        this.xhr.abort();
      }
      if (term === self.term) {
        return;
      }
      self.term = term;
      self.$content.html('');
      if (term.length >= 3) {
        this.$form.addClass('searching');
        this.xhr = Aquarius.ajax({
          url: this.$form.attr('action'),
          type: this.$form.attr('method'),
          data: {
            term: term
          },
          success: function(response) {
            self.$form.removeClass('searching');
            self.$content.html(response);
          }
        });
      }
    }
  };
  return Search;
});;
Aquarius.component.define('Frontend/Search/Opener', function($element, options) {
  var defaults = {
    'term': null
  };

  function Search() {}
  Search.prototype = {
    init: function() {
      var self = this;
      this.options = Aquarius.extend(defaults, options);
      this.$element = $element;
      this.$overlay = $('.search-overlay').first();
      this.$element.on('click', function(event) {
        event.preventDefault();
        self.$overlay.trigger('open', [self.options.term]);
        return false;
      });
    }
  };
  return Search;
});;
Aquarius.component.define('Frontend/Social/Facebook', function($element, options) {
  var defaults = {};

  function Social() {}
  Social.prototype = {
    init: function() {
      var self = this;
      this.options = Aquarius.extend(defaults, options);
      this.$element = $element;
      this.$element.on('click', function(event) {
        event.preventDefault();
        if (typeof FB !== 'undefined') {
          FB.ui({
            method: 'share',
            href: $(this).attr('href')
          }, function(response) {});
        }
        return false;
      });
    }
  };
  return new Social;
});;
Aquarius.component.define('Frontend/Social/Twitter', function($element, options) {
  var defaults = {};

  function Social() {}
  Social.prototype = {
    init: function() {
      var self = this;
      this.options = Aquarius.extend(defaults, options);
      this.$element = $element;
      this.$element.on('click', function(event) {
        event.preventDefault();
        if (typeof FB !== 'undefined') {
          FB.ui({
            method: 'share',
            href: $(this).attr('href')
          }, function(response) {});
        }
        return false;
      });
    }
  };
  return new Social;
});;
Aquarius.component.define('Frontend/Social/Linkedin', function($element, options) {
  var defaults = {};

  function Social() {}
  Social.prototype = {
    init: function() {
      var self = this;
      this.options = Aquarius.extend(defaults, options);
      this.$element = $element;
      this.$element.on('click', function(event) {
        event.preventDefault();
        if (typeof FB !== 'undefined') {
          FB.ui({
            method: 'share',
            href: $(this).attr('href')
          }, function(response) {});
        }
        return false;
      });
    }
  };
  return new Social;
});;
Aquarius.component.define('Frontend/Social/Other', function($element, options) {
  var defaults = {};

  function Social() {}
  Social.prototype = {
    init: function() {
      var self = this;
      this.options = Aquarius.extend(defaults, options);
      this.$element = $element;
      this.$element.on('click', function(event) {
        event.preventDefault();
        if (typeof FB !== 'undefined') {
          FB.ui({
            method: 'share',
            href: $(this).attr('href')
          }, function(response) {});
        }
        return false;
      });
    }
  };
  return new Social;
});;
Aquarius.component.define('Frontend/CookieConsent', function($element, options) {
  var defaults = {};

  function CookieConsent() {}
  CookieConsent.prototype = {
    init: function() {
      var self = this;
      this.$element = $element;
      this.options = $.extend(true, defaults, options);
      this.$element.find('.btn').on('click', function() {
        Aquarius.cookie('cookie-consent-accepted', 1);
        self.$element.slideUp();
      });
    }
  };
  return CookieConsent;
});;
Aquarius.component.define('Frontend/LoadMore', function($element, options) {
  var defaults = {};

  function Widget() {}
  Widget.prototype = {
    init: function() {
      var self = this;
      this.options = Aquarius.extend(defaults, options);
      this.$element = $element;
      this.$element.on('click', function(event) {
        event.preventDefault();
        self.$element.find('i').addClass('fa-spin-fast').prop('disabled', true);
        Aquarius.ajax({
          url: self.$element.data('url'),
          success: function(html) {
            self.$element.after(html);
            self.$element.remove();
          }
        });
        return false;
      });
    }
  };
  return new Widget;
});;
Aquarius.component.define('Frontend/Download', function($element, options) {
  var defaults = {
    target: 'Target/Modal',
    title: Aquarius.translate('Complimentary Download Form')
  };

  function Widget() {}
  Widget.prototype = {
    init: function() {
      var self = this;
      this.options = Aquarius.extend(defaults, options);
      this.$element = $element;
      this.target = Aquarius.registry.get(this.options.target);
      this.target.setOptions({
        title: self.options.title
      });
      this.$element.on('click', function(event) {
        event.preventDefault();
        Aquarius.ajax({
          url: self.$element.attr('href'),
          success: function(html) {
            self.target.open();
            self.target.setContent(html);
            self.target.hideLoader();
          }
        });
        return false;
      });
      this.target.getElement().on('form.redirect', function() {
        self.target.close();
      });
    }
  };
  return new Widget;
});;
Aquarius.register([{
  id: 'loader',
  type: 'Loader',
  config: {
    events: ['content.load:after', 'ajax.load:after'],
    factories: {
      'Factory/Target': ['Target/Modal', 'Target/Side', 'Target/Window'],
      'Factory/Panel': ['Panel/Table', 'Panel/Tree', 'Panel/Login', 'Panel/Chooser', 'Panel/Browser']
    }
  }
}, {
  id: 'module.wow',
  type: 'Module/Wow'
}, {
  id: 'analytics',
  type: 'Analytics'
}]);
$(document).ready(function() {
  Aquarius.startAll();
  Aquarius.hub.get('main').broadcast('content.load:after', {
    element: $('body'),
    url: top.location.href
  });
});;
