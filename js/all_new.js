if (! function(a, b) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = a.document ? b(a, !0) : function(a) {
      if (!a.document) throw new Error("jQuery requires a window with a document");
      return b(a)
    } : b(a)
  }("undefined" != typeof window ? window : this, function(a, b) {
    function s(a) {
      var b = !!a && "length" in a && a.length,
        c = n.type(a);
      return "function" === c || n.isWindow(a) ? !1 : "array" === c || 0 === b || "number" == typeof b && b > 0 && b - 1 in a
    }

    function z(a, b, c) {
      if (n.isFunction(b)) return n.grep(a, function(a, d) {
        return !!b.call(a, d, a) !== c
      });
      if (b.nodeType) return n.grep(a, function(a) {
        return a === b !== c
      });
      if ("string" == typeof b) {
        if (y.test(b)) return n.filter(b, a, c);
        b = n.filter(b, a)
      }
      return n.grep(a, function(a) {
        return n.inArray(a, b) > -1 !== c
      })
    }

    function F(a, b) {
      do a = a[b]; while (a && 1 !== a.nodeType);
      return a
    }

    function H(a) {
      var b = {};
      return n.each(a.match(G) || [], function(a, c) {
        b[c] = !0
      }), b
    }

    function J() {
      d.addEventListener ? (d.removeEventListener("DOMContentLoaded", K), a.removeEventListener("load", K)) : (d.detachEvent("onreadystatechange", K), a.detachEvent("onload", K))
    }

    function K() {
      (d.addEventListener || "load" === a.event.type || "complete" === d.readyState) && (J(), n.ready())
    }

    function P(a, b, c) {
      if (void 0 === c && 1 === a.nodeType) {
        var d = "data-" + b.replace(O, "-$1").toLowerCase();
        if (c = a.getAttribute(d), "string" == typeof c) {
          try {
            c = "true" === c ? !0 : "false" === c ? !1 : "null" === c ? null : +c + "" === c ? +c : N.test(c) ? n.parseJSON(c) : c
          } catch (e) {}
          n.data(a, b, c)
        } else c = void 0
      }
      return c
    }

    function Q(a) {
      var b;
      for (b in a)
        if (("data" !== b || !n.isEmptyObject(a[b])) && "toJSON" !== b) return !1;
      return !0
    }

    function R(a, b, d, e) {
      if (M(a)) {
        var f, g, h = n.expando,
          i = a.nodeType,
          j = i ? n.cache : a,
          k = i ? a[h] : a[h] && h;
        if (k && j[k] && (e || j[k].data) || void 0 !== d || "string" != typeof b) return k || (k = i ? a[h] = c.pop() || n.guid++ : h), j[k] || (j[k] = i ? {} : {
          toJSON: n.noop
        }), "object" != typeof b && "function" != typeof b || (e ? j[k] = n.extend(j[k], b) : j[k].data = n.extend(j[k].data, b)), g = j[k], e || (g.data || (g.data = {}), g = g.data), void 0 !== d && (g[n.camelCase(b)] = d), "string" == typeof b ? (f = g[b], null == f && (f = g[n.camelCase(b)])) : f = g, f
      }
    }

    function S(a, b, c) {
      if (M(a)) {
        var d, e, f = a.nodeType,
          g = f ? n.cache : a,
          h = f ? a[n.expando] : n.expando;
        if (g[h]) {
          if (b && (d = c ? g[h] : g[h].data)) {
            n.isArray(b) ? b = b.concat(n.map(b, n.camelCase)) : b in d ? b = [b] : (b = n.camelCase(b), b = b in d ? [b] : b.split(" ")), e = b.length;
            for (; e--;) delete d[b[e]];
            if (c ? !Q(d) : !n.isEmptyObject(d)) return
          }(c || (delete g[h].data, Q(g[h]))) && (f ? n.cleanData([a], !0) : l.deleteExpando || g != g.window ? delete g[h] : g[h] = void 0)
        }
      }
    }

    function X(a, b, c, d) {
      var e, f = 1,
        g = 20,
        h = d ? function() {
          return d.cur()
        } : function() {
          return n.css(a, b, "")
        },
        i = h(),
        j = c && c[3] || (n.cssNumber[b] ? "" : "px"),
        k = (n.cssNumber[b] || "px" !== j && +i) && U.exec(n.css(a, b));
      if (k && k[3] !== j) {
        j = j || k[3], c = c || [], k = +i || 1;
        do f = f || ".5", k /= f, n.style(a, b, k + j); while (f !== (f = h() / i) && 1 !== f && --g)
      }
      return c && (k = +k || +i || 0, e = c[1] ? k + (c[1] + 1) * c[2] : +c[2], d && (d.unit = j, d.start = k, d.end = e)), e
    }

    function ca(a) {
      var b = ba.split("|"),
        c = a.createDocumentFragment();
      if (c.createElement)
        for (; b.length;) c.createElement(b.pop());
      return c
    }

    function ea(a, b) {
      var c, d, e = 0,
        f = "undefined" != typeof a.getElementsByTagName ? a.getElementsByTagName(b || "*") : "undefined" != typeof a.querySelectorAll ? a.querySelectorAll(b || "*") : void 0;
      if (!f)
        for (f = [], c = a.childNodes || a; null != (d = c[e]); e++) !b || n.nodeName(d, b) ? f.push(d) : n.merge(f, ea(d, b));
      return void 0 === b || b && n.nodeName(a, b) ? n.merge([a], f) : f
    }

    function fa(a, b) {
      for (var c, d = 0; null != (c = a[d]); d++) n._data(c, "globalEval", !b || n._data(b[d], "globalEval"))
    }

    function ia(a) {
      Z.test(a.type) && (a.defaultChecked = a.checked)
    }

    function ja(a, b, c, d, e) {
      for (var f, g, h, i, j, k, m, o = a.length, p = ca(b), q = [], r = 0; o > r; r++)
        if (g = a[r], g || 0 === g)
          if ("object" === n.type(g)) n.merge(q, g.nodeType ? [g] : g);
          else if (ga.test(g)) {
        for (i = i || p.appendChild(b.createElement("div")), j = ($.exec(g) || ["", ""])[1].toLowerCase(), m = da[j] || da._default, i.innerHTML = m[1] + n.htmlPrefilter(g) + m[2], f = m[0]; f--;) i = i.lastChild;
        if (!l.leadingWhitespace && aa.test(g) && q.push(b.createTextNode(aa.exec(g)[0])), !l.tbody)
          for (g = "table" !== j || ha.test(g) ? "<table>" !== m[1] || ha.test(g) ? 0 : i : i.firstChild, f = g && g.childNodes.length; f--;) n.nodeName(k = g.childNodes[f], "tbody") && !k.childNodes.length && g.removeChild(k);
        for (n.merge(q, i.childNodes), i.textContent = ""; i.firstChild;) i.removeChild(i.firstChild);
        i = p.lastChild
      } else q.push(b.createTextNode(g));
      for (i && p.removeChild(i), l.appendChecked || n.grep(ea(q, "input"), ia), r = 0; g = q[r++];)
        if (d && n.inArray(g, d) > -1) e && e.push(g);
        else if (h = n.contains(g.ownerDocument, g), i = ea(p.appendChild(g), "script"), h && fa(i), c)
        for (f = 0; g = i[f++];) _.test(g.type || "") && c.push(g);
      return i = null, p
    }

    function pa() {
      return !0
    }

    function qa() {
      return !1
    }

    function ra() {
      try {
        return d.activeElement
      } catch (a) {}
    }

    function sa(a, b, c, d, e, f) {
      var g, h;
      if ("object" == typeof b) {
        "string" != typeof c && (d = d || c, c = void 0);
        for (h in b) sa(a, h, c, d, b[h], f);
        return a
      }
      if (null == d && null == e ? (e = c, d = c = void 0) : null == e && ("string" == typeof c ? (e = d, d = void 0) : (e = d, d = c, c = void 0)), e === !1) e = qa;
      else if (!e) return a;
      return 1 === f && (g = e, e = function(a) {
        return n().off(a), g.apply(this, arguments)
      }, e.guid = g.guid || (g.guid = n.guid++)), a.each(function() {
        n.event.add(this, b, e, d, c)
      })
    }

    function Ca(a, b) {
      return n.nodeName(a, "table") && n.nodeName(11 !== b.nodeType ? b : b.firstChild, "tr") ? a.getElementsByTagName("tbody")[0] || a.appendChild(a.ownerDocument.createElement("tbody")) : a
    }

    function Da(a) {
      return a.type = (null !== n.find.attr(a, "type")) + "/" + a.type, a
    }

    function Ea(a) {
      var b = ya.exec(a.type);
      return b ? a.type = b[1] : a.removeAttribute("type"), a
    }

    function Fa(a, b) {
      if (1 === b.nodeType && n.hasData(a)) {
        var c, d, e, f = n._data(a),
          g = n._data(b, f),
          h = f.events;
        if (h) {
          delete g.handle, g.events = {};
          for (c in h)
            for (d = 0, e = h[c].length; e > d; d++) n.event.add(b, c, h[c][d])
        }
        g.data && (g.data = n.extend({}, g.data))
      }
    }

    function Ga(a, b) {
      var c, d, e;
      if (1 === b.nodeType) {
        if (c = b.nodeName.toLowerCase(), !l.noCloneEvent && b[n.expando]) {
          e = n._data(b);
          for (d in e.events) n.removeEvent(b, d, e.handle);
          b.removeAttribute(n.expando)
        }
        "script" === c && b.text !== a.text ? (Da(b).text = a.text, Ea(b)) : "object" === c ? (b.parentNode && (b.outerHTML = a.outerHTML), l.html5Clone && a.innerHTML && !n.trim(b.innerHTML) && (b.innerHTML = a.innerHTML)) : "input" === c && Z.test(a.type) ? (b.defaultChecked = b.checked = a.checked, b.value !== a.value && (b.value = a.value)) : "option" === c ? b.defaultSelected = b.selected = a.defaultSelected : "input" !== c && "textarea" !== c || (b.defaultValue = a.defaultValue)
      }
    }

    function Ha(a, b, c, d) {
      b = f.apply([], b);
      var e, g, h, i, j, k, m = 0,
        o = a.length,
        p = o - 1,
        q = b[0],
        r = n.isFunction(q);
      if (r || o > 1 && "string" == typeof q && !l.checkClone && xa.test(q)) return a.each(function(e) {
        var f = a.eq(e);
        r && (b[0] = q.call(this, e, f.html())), Ha(f, b, c, d)
      });
      if (o && (k = ja(b, a[0].ownerDocument, !1, a, d), e = k.firstChild, 1 === k.childNodes.length && (k = e), e || d)) {
        for (i = n.map(ea(k, "script"), Da), h = i.length; o > m; m++) g = k, m !== p && (g = n.clone(g, !0, !0), h && n.merge(i, ea(g, "script"))), c.call(a[m], g, m);
        if (h)
          for (j = i[i.length - 1].ownerDocument, n.map(i, Ea), m = 0; h > m; m++) g = i[m], _.test(g.type || "") && !n._data(g, "globalEval") && n.contains(j, g) && (g.src ? n._evalUrl && n._evalUrl(g.src) : n.globalEval((g.text || g.textContent || g.innerHTML || "").replace(za, "")));
        k = e = null
      }
      return a
    }

    function Ia(a, b, c) {
      for (var d, e = b ? n.filter(b, a) : a, f = 0; null != (d = e[f]); f++) c || 1 !== d.nodeType || n.cleanData(ea(d)), d.parentNode && (c && n.contains(d.ownerDocument, d) && fa(ea(d, "script")), d.parentNode.removeChild(d));
      return a
    }

    function La(a, b) {
      var c = n(b.createElement(a)).appendTo(b.body),
        d = n.css(c[0], "display");
      return c.detach(), d
    }

    function Ma(a) {
      var b = d,
        c = Ka[a];
      return c || (c = La(a, b), "none" !== c && c || (Ja = (Ja || n("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement), b = (Ja[0].contentWindow || Ja[0].contentDocument).document, b.write(), b.close(), c = La(a, b), Ja.detach()), Ka[a] = c), c
    }

    function Ua(a, b) {
      return {
        get: function() {
          return a() ? void delete this.get : (this.get = b).apply(this, arguments)
        }
      }
    }

    function bb(a) {
      if (a in ab) return a;
      for (var b = a.charAt(0).toUpperCase() + a.slice(1), c = _a.length; c--;)
        if (a = _a[c] + b, a in ab) return a
    }

    function cb(a, b) {
      for (var c, d, e, f = [], g = 0, h = a.length; h > g; g++) d = a[g], d.style && (f[g] = n._data(d, "olddisplay"), c = d.style.display, b ? (f[g] || "none" !== c || (d.style.display = ""), "" === d.style.display && W(d) && (f[g] = n._data(d, "olddisplay", Ma(d.nodeName)))) : (e = W(d), (c && "none" !== c || !e) && n._data(d, "olddisplay", e ? c : n.css(d, "display"))));
      for (g = 0; h > g; g++) d = a[g], d.style && (b && "none" !== d.style.display && "" !== d.style.display || (d.style.display = b ? f[g] || "" : "none"));
      return a
    }

    function db(a, b, c) {
      var d = Ya.exec(b);
      return d ? Math.max(0, d[1] - (c || 0)) + (d[2] || "px") : b
    }

    function eb(a, b, c, d, e) {
      for (var f = c === (d ? "border" : "content") ? 4 : "width" === b ? 1 : 0, g = 0; 4 > f; f += 2) "margin" === c && (g += n.css(a, c + V[f], !0, e)), d ? ("content" === c && (g -= n.css(a, "padding" + V[f], !0, e)), "margin" !== c && (g -= n.css(a, "border" + V[f] + "Width", !0, e))) : (g += n.css(a, "padding" + V[f], !0, e), "padding" !== c && (g += n.css(a, "border" + V[f] + "Width", !0, e)));
      return g
    }

    function fb(b, c, e) {
      var f = !0,
        g = "width" === c ? b.offsetWidth : b.offsetHeight,
        h = Ra(b),
        i = l.boxSizing && "border-box" === n.css(b, "boxSizing", !1, h);
      if (d.msFullscreenElement && a.top !== a && b.getClientRects().length && (g = Math.round(100 * b.getBoundingClientRect()[c])), 0 >= g || null == g) {
        if (g = Sa(b, c, h), (0 > g || null == g) && (g = b.style[c]), Oa.test(g)) return g;
        f = i && (l.boxSizingReliable() || g === b.style[c]), g = parseFloat(g) || 0
      }
      return g + eb(b, c, e || (i ? "border" : "content"), f, h) + "px"
    }

    function gb(a, b, c, d, e) {
      return new gb.prototype.init(a, b, c, d, e)
    }

    function lb() {
      return a.setTimeout(function() {
        hb = void 0
      }), hb = n.now()
    }

    function mb(a, b) {
      var c, d = {
          height: a
        },
        e = 0;
      for (b = b ? 1 : 0; 4 > e; e += 2 - b) c = V[e], d["margin" + c] = d["padding" + c] = a;
      return b && (d.opacity = d.width = a), d
    }

    function nb(a, b, c) {
      for (var d, e = (qb.tweeners[b] || []).concat(qb.tweeners["*"]), f = 0, g = e.length; g > f; f++)
        if (d = e[f].call(c, b, a)) return d
    }

    function ob(a, b, c) {
      var d, e, f, g, h, i, j, k, m = this,
        o = {},
        p = a.style,
        q = a.nodeType && W(a),
        r = n._data(a, "fxshow");
      c.queue || (h = n._queueHooks(a, "fx"), null == h.unqueued && (h.unqueued = 0, i = h.empty.fire, h.empty.fire = function() {
        h.unqueued || i()
      }), h.unqueued++, m.always(function() {
        m.always(function() {
          h.unqueued--, n.queue(a, "fx").length || h.empty.fire()
        })
      })), 1 === a.nodeType && ("height" in b || "width" in b) && (c.overflow = [p.overflow, p.overflowX, p.overflowY], j = n.css(a, "display"), k = "none" === j ? n._data(a, "olddisplay") || Ma(a.nodeName) : j, "inline" === k && "none" === n.css(a, "float") && (l.inlineBlockNeedsLayout && "inline" !== Ma(a.nodeName) ? p.zoom = 1 : p.display = "inline-block")), c.overflow && (p.overflow = "hidden", l.shrinkWrapBlocks() || m.always(function() {
        p.overflow = c.overflow[0], p.overflowX = c.overflow[1], p.overflowY = c.overflow[2]
      }));
      for (d in b)
        if (e = b[d], jb.exec(e)) {
          if (delete b[d], f = f || "toggle" === e, e === (q ? "hide" : "show")) {
            if ("show" !== e || !r || void 0 === r[d]) continue;
            q = !0
          }
          o[d] = r && r[d] || n.style(a, d)
        } else j = void 0;
      if (n.isEmptyObject(o)) "inline" === ("none" === j ? Ma(a.nodeName) : j) && (p.display = j);
      else {
        r ? "hidden" in r && (q = r.hidden) : r = n._data(a, "fxshow", {}), f && (r.hidden = !q), q ? n(a).show() : m.done(function() {
          n(a).hide()
        }), m.done(function() {
          var b;
          n._removeData(a, "fxshow");
          for (b in o) n.style(a, b, o[b])
        });
        for (d in o) g = nb(q ? r[d] : 0, d, m), d in r || (r[d] = g.start, q && (g.end = g.start, g.start = "width" === d || "height" === d ? 1 : 0))
      }
    }

    function pb(a, b) {
      var c, d, e, f, g;
      for (c in a)
        if (d = n.camelCase(c), e = b[d], f = a[c], n.isArray(f) && (e = f[1], f = a[c] = f[0]), c !== d && (a[d] = f, delete a[c]), g = n.cssHooks[d], g && "expand" in g) {
          f = g.expand(f), delete a[d];
          for (c in f) c in a || (a[c] = f[c], b[c] = e)
        } else b[d] = e
    }

    function qb(a, b, c) {
      var d, e, f = 0,
        g = qb.prefilters.length,
        h = n.Deferred().always(function() {
          delete i.elem
        }),
        i = function() {
          if (e) return !1;
          for (var b = hb || lb(), c = Math.max(0, j.startTime + j.duration - b), d = c / j.duration || 0, f = 1 - d, g = 0, i = j.tweens.length; i > g; g++) j.tweens[g].run(f);
          return h.notifyWith(a, [j, f, c]), 1 > f && i ? c : (h.resolveWith(a, [j]), !1)
        },
        j = h.promise({
          elem: a,
          props: n.extend({}, b),
          opts: n.extend(!0, {
            specialEasing: {},
            easing: n.easing._default
          }, c),
          originalProperties: b,
          originalOptions: c,
          startTime: hb || lb(),
          duration: c.duration,
          tweens: [],
          createTween: function(b, c) {
            var d = n.Tween(a, j.opts, b, c, j.opts.specialEasing[b] || j.opts.easing);
            return j.tweens.push(d), d
          },
          stop: function(b) {
            var c = 0,
              d = b ? j.tweens.length : 0;
            if (e) return this;
            for (e = !0; d > c; c++) j.tweens[c].run(1);
            return b ? (h.notifyWith(a, [j, 1, 0]), h.resolveWith(a, [j, b])) : h.rejectWith(a, [j, b]), this
          }
        }),
        k = j.props;
      for (pb(k, j.opts.specialEasing); g > f; f++)
        if (d = qb.prefilters[f].call(j, a, k, j.opts)) return n.isFunction(d.stop) && (n._queueHooks(j.elem, j.opts.queue).stop = n.proxy(d.stop, d)), d;
      return n.map(k, nb, j), n.isFunction(j.opts.start) && j.opts.start.call(a, j), n.fx.timer(n.extend(i, {
        elem: a,
        anim: j,
        queue: j.opts.queue
      })), j.progress(j.opts.progress).done(j.opts.done, j.opts.complete).fail(j.opts.fail).always(j.opts.always)
    }

    function Cb(a) {
      return n.attr(a, "class") || ""
    }

    function Tb(a) {
      return function(b, c) {
        "string" != typeof b && (c = b, b = "*");
        var d, e = 0,
          f = b.toLowerCase().match(G) || [];
        if (n.isFunction(c))
          for (; d = f[e++];) "+" === d.charAt(0) ? (d = d.slice(1) || "*", (a[d] = a[d] || []).unshift(c)) : (a[d] = a[d] || []).push(c)
      }
    }

    function Ub(a, b, c, d) {
      function g(h) {
        var i;
        return e[h] = !0, n.each(a[h] || [], function(a, h) {
          var j = h(b, c, d);
          return "string" != typeof j || f || e[j] ? f ? !(i = j) : void 0 : (b.dataTypes.unshift(j), g(j), !1)
        }), i
      }
      var e = {},
        f = a === Pb;
      return g(b.dataTypes[0]) || !e["*"] && g("*")
    }

    function Vb(a, b) {
      var c, d, e = n.ajaxSettings.flatOptions || {};
      for (d in b) void 0 !== b[d] && ((e[d] ? a : c || (c = {}))[d] = b[d]);
      return c && n.extend(!0, a, c), a
    }

    function Wb(a, b, c) {
      for (var d, e, f, g, h = a.contents, i = a.dataTypes;
        "*" === i[0];) i.shift(), void 0 === e && (e = a.mimeType || b.getResponseHeader("Content-Type"));
      if (e)
        for (g in h)
          if (h[g] && h[g].test(e)) {
            i.unshift(g);
            break
          } if (i[0] in c) f = i[0];
      else {
        for (g in c) {
          if (!i[0] || a.converters[g + " " + i[0]]) {
            f = g;
            break
          }
          d || (d = g)
        }
        f = f || d
      }
      return f ? (f !== i[0] && i.unshift(f), c[f]) : void 0
    }

    function Xb(a, b, c, d) {
      var e, f, g, h, i, j = {},
        k = a.dataTypes.slice();
      if (k[1])
        for (g in a.converters) j[g.toLowerCase()] = a.converters[g];
      for (f = k.shift(); f;)
        if (a.responseFields[f] && (c[a.responseFields[f]] = b), !i && d && a.dataFilter && (b = a.dataFilter(b, a.dataType)), i = f, f = k.shift())
          if ("*" === f) f = i;
          else if ("*" !== i && i !== f) {
        if (g = j[i + " " + f] || j["* " + f], !g)
          for (e in j)
            if (h = e.split(" "), h[1] === f && (g = j[i + " " + h[0]] || j["* " + h[0]])) {
              g === !0 ? g = j[e] : j[e] !== !0 && (f = h[0], k.unshift(h[1]));
              break
            } if (g !== !0)
          if (g && a["throws"]) b = g(b);
          else try {
            b = g(b)
          } catch (l) {
            return {
              state: "parsererror",
              error: g ? l : "No conversion from " + i + " to " + f
            }
          }
      }
      return {
        state: "success",
        data: b
      }
    }

    function Yb(a) {
      return a.style && a.style.display || n.css(a, "display")
    }

    function Zb(a) {
      for (; a && 1 === a.nodeType;) {
        if ("none" === Yb(a) || "hidden" === a.type) return !0;
        a = a.parentNode
      }
      return !1
    }

    function dc(a, b, c, d) {
      var e;
      if (n.isArray(b)) n.each(b, function(b, e) {
        c || _b.test(a) ? d(a, e) : dc(a + "[" + ("object" == typeof e && null != e ? b : "") + "]", e, c, d)
      });
      else if (c || "object" !== n.type(b)) d(a, b);
      else
        for (e in b) dc(a + "[" + e + "]", b[e], c, d)
    }

    function hc() {
      try {
        return new a.XMLHttpRequest
      } catch (b) {}
    }

    function ic() {
      try {
        return new a.ActiveXObject("Microsoft.XMLHTTP")
      } catch (b) {}
    }

    function mc(a) {
      return n.isWindow(a) ? a : 9 === a.nodeType ? a.defaultView || a.parentWindow : !1
    }
    var c = [],
      d = a.document,
      e = c.slice,
      f = c.concat,
      g = c.push,
      h = c.indexOf,
      i = {},
      j = i.toString,
      k = i.hasOwnProperty,
      l = {},
      m = "1.12.3",
      n = function(a, b) {
        return new n.fn.init(a, b)
      },
      o = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
      p = /^-ms-/,
      q = /-([\da-z])/gi,
      r = function(a, b) {
        return b.toUpperCase()
      };
    n.fn = n.prototype = {
      jquery: m,
      constructor: n,
      selector: "",
      length: 0,
      toArray: function() {
        return e.call(this)
      },
      get: function(a) {
        return null != a ? 0 > a ? this[a + this.length] : this[a] : e.call(this)
      },
      pushStack: function(a) {
        var b = n.merge(this.constructor(), a);
        return b.prevObject = this, b.context = this.context, b
      },
      each: function(a) {
        return n.each(this, a)
      },
      map: function(a) {
        return this.pushStack(n.map(this, function(b, c) {
          return a.call(b, c, b)
        }))
      },
      slice: function() {
        return this.pushStack(e.apply(this, arguments))
      },
      first: function() {
        return this.eq(0)
      },
      last: function() {
        return this.eq(-1)
      },
      eq: function(a) {
        var b = this.length,
          c = +a + (0 > a ? b : 0);
        return this.pushStack(c >= 0 && b > c ? [this[c]] : [])
      },
      end: function() {
        return this.prevObject || this.constructor()
      },
      push: g,
      sort: c.sort,
      splice: c.splice
    }, n.extend = n.fn.extend = function() {
      var a, b, c, d, e, f, g = arguments[0] || {},
        h = 1,
        i = arguments.length,
        j = !1;
      for ("boolean" == typeof g && (j = g, g = arguments[h] || {}, h++), "object" == typeof g || n.isFunction(g) || (g = {}), h === i && (g = this, h--); i > h; h++)
        if (null != (e = arguments[h]))
          for (d in e) a = g[d], c = e[d], g !== c && (j && c && (n.isPlainObject(c) || (b = n.isArray(c))) ? (b ? (b = !1, f = a && n.isArray(a) ? a : []) : f = a && n.isPlainObject(a) ? a : {}, g[d] = n.extend(j, f, c)) : void 0 !== c && (g[d] = c));
      return g
    }, n.extend({
      expando: "jQuery" + (m + Math.random()).replace(/\D/g, ""),
      isReady: !0,
      error: function(a) {
        throw new Error(a)
      },
      noop: function() {},
      isFunction: function(a) {
        return "function" === n.type(a)
      },
      isArray: Array.isArray || function(a) {
        return "array" === n.type(a)
      },
      isWindow: function(a) {
        return null != a && a == a.window
      },
      isNumeric: function(a) {
        var b = a && a.toString();
        return !n.isArray(a) && b - parseFloat(b) + 1 >= 0
      },
      isEmptyObject: function(a) {
        var b;
        for (b in a) return !1;
        return !0
      },
      isPlainObject: function(a) {
        var b;
        if (!a || "object" !== n.type(a) || a.nodeType || n.isWindow(a)) return !1;
        try {
          if (a.constructor && !k.call(a, "constructor") && !k.call(a.constructor.prototype, "isPrototypeOf")) return !1
        } catch (c) {
          return !1
        }
        if (!l.ownFirst)
          for (b in a) return k.call(a, b);
        for (b in a);
        return void 0 === b || k.call(a, b)
      },
      type: function(a) {
        return null == a ? a + "" : "object" == typeof a || "function" == typeof a ? i[j.call(a)] || "object" : typeof a
      },
      globalEval: function(b) {
        b && n.trim(b) && (a.execScript || function(b) {
          a.eval.call(a, b)
        })(b)
      },
      camelCase: function(a) {
        return a.replace(p, "ms-").replace(q, r)
      },
      nodeName: function(a, b) {
        return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase()
      },
      each: function(a, b) {
        var c, d = 0;
        if (s(a))
          for (c = a.length; c > d && b.call(a[d], d, a[d]) !== !1; d++);
        else
          for (d in a)
            if (b.call(a[d], d, a[d]) === !1) break;
        return a
      },
      trim: function(a) {
        return null == a ? "" : (a + "").replace(o, "")
      },
      makeArray: function(a, b) {
        var c = b || [];
        return null != a && (s(Object(a)) ? n.merge(c, "string" == typeof a ? [a] : a) : g.call(c, a)), c
      },
      inArray: function(a, b, c) {
        var d;
        if (b) {
          if (h) return h.call(b, a, c);
          for (d = b.length, c = c ? 0 > c ? Math.max(0, d + c) : c : 0; d > c; c++)
            if (c in b && b[c] === a) return c
        }
        return -1
      },
      merge: function(a, b) {
        for (var c = +b.length, d = 0, e = a.length; c > d;) a[e++] = b[d++];
        if (c !== c)
          for (; void 0 !== b[d];) a[e++] = b[d++];
        return a.length = e, a
      },
      grep: function(a, b, c) {
        for (var d, e = [], f = 0, g = a.length, h = !c; g > f; f++) d = !b(a[f], f), d !== h && e.push(a[f]);
        return e
      },
      map: function(a, b, c) {
        var d, e, g = 0,
          h = [];
        if (s(a))
          for (d = a.length; d > g; g++) e = b(a[g], g, c), null != e && h.push(e);
        else
          for (g in a) e = b(a[g], g, c), null != e && h.push(e);
        return f.apply([], h)
      },
      guid: 1,
      proxy: function(a, b) {
        var c, d, f;
        return "string" == typeof b && (f = a[b], b = a, a = f), n.isFunction(a) ? (c = e.call(arguments, 2), d = function() {
          return a.apply(b || this, c.concat(e.call(arguments)))
        }, d.guid = a.guid = a.guid || n.guid++, d) : void 0
      },
      now: function() {
        return +new Date
      },
      support: l
    }), "function" == typeof Symbol && (n.fn[Symbol.iterator] = c[Symbol.iterator]), n.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(a, b) {
      i["[object " + b + "]"] = b.toLowerCase()
    });
    var t = function(a) {
      function fa(a, b, d, e) {
        var f, h, j, k, l, o, r, s, w = b && b.ownerDocument,
          x = b ? b.nodeType : 9;
        if (d = d || [], "string" != typeof a || !a || 1 !== x && 9 !== x && 11 !== x) return d;
        if (!e && ((b ? b.ownerDocument || b : v) !== n && m(b), b = b || n, p)) {
          if (11 !== x && (o = $.exec(a)))
            if (f = o[1]) {
              if (9 === x) {
                if (!(j = b.getElementById(f))) return d;
                if (j.id === f) return d.push(j), d
              } else if (w && (j = w.getElementById(f)) && t(b, j) && j.id === f) return d.push(j), d
            } else {
              if (o[2]) return H.apply(d, b.getElementsByTagName(a)), d;
              if ((f = o[3]) && c.getElementsByClassName && b.getElementsByClassName) return H.apply(d, b.getElementsByClassName(f)), d
            } if (!(!c.qsa || A[a + " "] || q && q.test(a))) {
            if (1 !== x) w = b, s = a;
            else if ("object" !== b.nodeName.toLowerCase()) {
              for ((k = b.getAttribute("id")) ? k = k.replace(aa, "\\$&") : b.setAttribute("id", k = u), r = g(a), h = r.length, l = V.test(k) ? "#" + k : "[id='" + k + "']"; h--;) r[h] = l + " " + qa(r[h]);
              s = r.join(","), w = _.test(a) && oa(b.parentNode) || b
            }
            if (s) try {
              return H.apply(d, w.querySelectorAll(s)), d
            } catch (y) {} finally {
              k === u && b.removeAttribute("id")
            }
          }
        }
        return i(a.replace(Q, "$1"), b, d, e)
      }

      function ga() {
        function b(c, e) {
          return a.push(c + " ") > d.cacheLength && delete b[a.shift()], b[c + " "] = e
        }
        var a = [];
        return b
      }

      function ha(a) {
        return a[u] = !0, a
      }

      function ia(a) {
        var b = n.createElement("div");
        try {
          return !!a(b)
        } catch (c) {
          return !1
        } finally {
          b.parentNode && b.parentNode.removeChild(b), b = null
        }
      }

      function ja(a, b) {
        for (var c = a.split("|"), e = c.length; e--;) d.attrHandle[c[e]] = b
      }

      function ka(a, b) {
        var c = b && a,
          d = c && 1 === a.nodeType && 1 === b.nodeType && (~b.sourceIndex || C) - (~a.sourceIndex || C);
        if (d) return d;
        if (c)
          for (; c = c.nextSibling;)
            if (c === b) return -1;
        return a ? 1 : -1
      }

      function la(a) {
        return function(b) {
          var c = b.nodeName.toLowerCase();
          return "input" === c && b.type === a
        }
      }

      function ma(a) {
        return function(b) {
          var c = b.nodeName.toLowerCase();
          return ("input" === c || "button" === c) && b.type === a
        }
      }

      function na(a) {
        return ha(function(b) {
          return b = +b, ha(function(c, d) {
            for (var e, f = a([], c.length, b), g = f.length; g--;) c[e = f[g]] && (c[e] = !(d[e] = c[e]))
          })
        })
      }

      function oa(a) {
        return a && "undefined" != typeof a.getElementsByTagName && a
      }

      function pa() {}

      function qa(a) {
        for (var b = 0, c = a.length, d = ""; c > b; b++) d += a[b].value;
        return d
      }

      function ra(a, b, c) {
        var d = b.dir,
          e = c && "parentNode" === d,
          f = x++;
        return b.first ? function(b, c, f) {
          for (; b = b[d];)
            if (1 === b.nodeType || e) return a(b, c, f)
        } : function(b, c, g) {
          var h, i, j, k = [w, f];
          if (g) {
            for (; b = b[d];)
              if ((1 === b.nodeType || e) && a(b, c, g)) return !0
          } else
            for (; b = b[d];)
              if (1 === b.nodeType || e) {
                if (j = b[u] || (b[u] = {}), i = j[b.uniqueID] || (j[b.uniqueID] = {}), (h = i[d]) && h[0] === w && h[1] === f) return k[2] = h[2];
                if (i[d] = k, k[2] = a(b, c, g)) return !0
              }
        }
      }

      function sa(a) {
        return a.length > 1 ? function(b, c, d) {
          for (var e = a.length; e--;)
            if (!a[e](b, c, d)) return !1;
          return !0
        } : a[0]
      }

      function ta(a, b, c) {
        for (var d = 0, e = b.length; e > d; d++) fa(a, b[d], c);
        return c
      }

      function ua(a, b, c, d, e) {
        for (var f, g = [], h = 0, i = a.length, j = null != b; i > h; h++)(f = a[h]) && (c && !c(f, d, e) || (g.push(f), j && b.push(h)));
        return g
      }

      function va(a, b, c, d, e, f) {
        return d && !d[u] && (d = va(d)), e && !e[u] && (e = va(e, f)), ha(function(f, g, h, i) {
          var j, k, l, m = [],
            n = [],
            o = g.length,
            p = f || ta(b || "*", h.nodeType ? [h] : h, []),
            q = !a || !f && b ? p : ua(p, m, a, h, i),
            r = c ? e || (f ? a : o || d) ? [] : g : q;
          if (c && c(q, r, h, i), d)
            for (j = ua(r, n), d(j, [], h, i), k = j.length; k--;)(l = j[k]) && (r[n[k]] = !(q[n[k]] = l));
          if (f) {
            if (e || a) {
              if (e) {
                for (j = [], k = r.length; k--;)(l = r[k]) && j.push(q[k] = l);
                e(null, r = [], j, i)
              }
              for (k = r.length; k--;)(l = r[k]) && (j = e ? J(f, l) : m[k]) > -1 && (f[j] = !(g[j] = l))
            }
          } else r = ua(r === g ? r.splice(o, r.length) : r), e ? e(null, g, r, i) : H.apply(g, r)
        })
      }

      function wa(a) {
        for (var b, c, e, f = a.length, g = d.relative[a[0].type], h = g || d.relative[" "], i = g ? 1 : 0, k = ra(function(a) {
            return a === b
          }, h, !0), l = ra(function(a) {
            return J(b, a) > -1
          }, h, !0), m = [function(a, c, d) {
            var e = !g && (d || c !== j) || ((b = c).nodeType ? k(a, c, d) : l(a, c, d));
            return b = null, e
          }]; f > i; i++)
          if (c = d.relative[a[i].type]) m = [ra(sa(m), c)];
          else {
            if (c = d.filter[a[i].type].apply(null, a[i].matches), c[u]) {
              for (e = ++i; f > e && !d.relative[a[e].type]; e++);
              return va(i > 1 && sa(m), i > 1 && qa(a.slice(0, i - 1).concat({
                value: " " === a[i - 2].type ? "*" : ""
              })).replace(Q, "$1"), c, e > i && wa(a.slice(i, e)), f > e && wa(a = a.slice(e)), f > e && qa(a))
            }
            m.push(c)
          } return sa(m)
      }

      function xa(a, b) {
        var c = b.length > 0,
          e = a.length > 0,
          f = function(f, g, h, i, k) {
            var l, o, q, r = 0,
              s = "0",
              t = f && [],
              u = [],
              v = j,
              x = f || e && d.find.TAG("*", k),
              y = w += null == v ? 1 : Math.random() || .1,
              z = x.length;
            for (k && (j = g === n || g || k); s !== z && null != (l = x[s]); s++) {
              if (e && l) {
                for (o = 0, g || l.ownerDocument === n || (m(l), h = !p); q = a[o++];)
                  if (q(l, g || n, h)) {
                    i.push(l);
                    break
                  } k && (w = y)
              }
              c && ((l = !q && l) && r--, f && t.push(l))
            }
            if (r += s, c && s !== r) {
              for (o = 0; q = b[o++];) q(t, u, g, h);
              if (f) {
                if (r > 0)
                  for (; s--;) t[s] || u[s] || (u[s] = F.call(i));
                u = ua(u)
              }
              H.apply(i, u), k && !f && u.length > 0 && r + b.length > 1 && fa.uniqueSort(i)
            }
            return k && (w = y, j = v), t
          };
        return c ? ha(f) : f
      }
      var b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u = "sizzle" + 1 * new Date,
        v = a.document,
        w = 0,
        x = 0,
        y = ga(),
        z = ga(),
        A = ga(),
        B = function(a, b) {
          return a === b && (l = !0), 0
        },
        C = 1 << 31,
        D = {}.hasOwnProperty,
        E = [],
        F = E.pop,
        G = E.push,
        H = E.push,
        I = E.slice,
        J = function(a, b) {
          for (var c = 0, d = a.length; d > c; c++)
            if (a[c] === b) return c;
          return -1
        },
        K = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
        L = "[\\x20\\t\\r\\n\\f]",
        M = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
        N = "\\[" + L + "*(" + M + ")(?:" + L + "*([*^$|!~]?=)" + L + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + M + "))|)" + L + "*\\]",
        O = ":(" + M + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + N + ")*)|.*)\\)|)",
        P = new RegExp(L + "+", "g"),
        Q = new RegExp("^" + L + "+|((?:^|[^\\\\])(?:\\\\.)*)" + L + "+$", "g"),
        R = new RegExp("^" + L + "*," + L + "*"),
        S = new RegExp("^" + L + "*([>+~]|" + L + ")" + L + "*"),
        T = new RegExp("=" + L + "*([^\\]'\"]*?)" + L + "*\\]", "g"),
        U = new RegExp(O),
        V = new RegExp("^" + M + "$"),
        W = {
          ID: new RegExp("^#(" + M + ")"),
          CLASS: new RegExp("^\\.(" + M + ")"),
          TAG: new RegExp("^(" + M + "|[*])"),
          ATTR: new RegExp("^" + N),
          PSEUDO: new RegExp("^" + O),
          CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + L + "*(even|odd|(([+-]|)(\\d*)n|)" + L + "*(?:([+-]|)" + L + "*(\\d+)|))" + L + "*\\)|)", "i"),
          bool: new RegExp("^(?:" + K + ")$", "i"),
          needsContext: new RegExp("^" + L + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + L + "*((?:-\\d)?\\d*)" + L + "*\\)|)(?=[^-]|$)", "i")
        },
        X = /^(?:input|select|textarea|button)$/i,
        Y = /^h\d$/i,
        Z = /^[^{]+\{\s*\[native \w/,
        $ = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
        _ = /[+~]/,
        aa = /'|\\/g,
        ba = new RegExp("\\\\([\\da-f]{1,6}" + L + "?|(" + L + ")|.)", "ig"),
        ca = function(a, b, c) {
          var d = "0x" + b - 65536;
          return d !== d || c ? b : 0 > d ? String.fromCharCode(d + 65536) : String.fromCharCode(d >> 10 | 55296, 1023 & d | 56320)
        },
        da = function() {
          m()
        };
      try {
        H.apply(E = I.call(v.childNodes), v.childNodes), E[v.childNodes.length].nodeType
      } catch (ea) {
        H = {
          apply: E.length ? function(a, b) {
            G.apply(a, I.call(b))
          } : function(a, b) {
            for (var c = a.length, d = 0; a[c++] = b[d++];);
            a.length = c - 1
          }
        }
      }
      c = fa.support = {}, f = fa.isXML = function(a) {
        var b = a && (a.ownerDocument || a).documentElement;
        return b ? "HTML" !== b.nodeName : !1
      }, m = fa.setDocument = function(a) {
        var b, e, g = a ? a.ownerDocument || a : v;
        return g !== n && 9 === g.nodeType && g.documentElement ? (n = g, o = n.documentElement, p = !f(n), (e = n.defaultView) && e.top !== e && (e.addEventListener ? e.addEventListener("unload", da, !1) : e.attachEvent && e.attachEvent("onunload", da)), c.attributes = ia(function(a) {
          return a.className = "i", !a.getAttribute("className")
        }), c.getElementsByTagName = ia(function(a) {
          return a.appendChild(n.createComment("")), !a.getElementsByTagName("*").length
        }), c.getElementsByClassName = Z.test(n.getElementsByClassName), c.getById = ia(function(a) {
          return o.appendChild(a).id = u, !n.getElementsByName || !n.getElementsByName(u).length
        }), c.getById ? (d.find.ID = function(a, b) {
          if ("undefined" != typeof b.getElementById && p) {
            var c = b.getElementById(a);
            return c ? [c] : []
          }
        }, d.filter.ID = function(a) {
          var b = a.replace(ba, ca);
          return function(a) {
            return a.getAttribute("id") === b
          }
        }) : (delete d.find.ID, d.filter.ID = function(a) {
          var b = a.replace(ba, ca);
          return function(a) {
            var c = "undefined" != typeof a.getAttributeNode && a.getAttributeNode("id");
            return c && c.value === b
          }
        }), d.find.TAG = c.getElementsByTagName ? function(a, b) {
          return "undefined" != typeof b.getElementsByTagName ? b.getElementsByTagName(a) : c.qsa ? b.querySelectorAll(a) : void 0
        } : function(a, b) {
          var c, d = [],
            e = 0,
            f = b.getElementsByTagName(a);
          if ("*" === a) {
            for (; c = f[e++];) 1 === c.nodeType && d.push(c);
            return d
          }
          return f
        }, d.find.CLASS = c.getElementsByClassName && function(a, b) {
          return "undefined" != typeof b.getElementsByClassName && p ? b.getElementsByClassName(a) : void 0
        }, r = [], q = [], (c.qsa = Z.test(n.querySelectorAll)) && (ia(function(a) {
          o.appendChild(a).innerHTML = "<a id='" + u + "'></a><select id='" + u + "-\r\\' msallowcapture=''><option selected=''></option></select>", a.querySelectorAll("[msallowcapture^='']").length && q.push("[*^$]=" + L + "*(?:''|\"\")"), a.querySelectorAll("[selected]").length || q.push("\\[" + L + "*(?:value|" + K + ")"), a.querySelectorAll("[id~=" + u + "-]").length || q.push("~="), a.querySelectorAll(":checked").length || q.push(":checked"), a.querySelectorAll("a#" + u + "+*").length || q.push(".#.+[+~]")
        }), ia(function(a) {
          var b = n.createElement("input");
          b.setAttribute("type", "hidden"), a.appendChild(b).setAttribute("name", "D"), a.querySelectorAll("[name=d]").length && q.push("name" + L + "*[*^$|!~]?="), a.querySelectorAll(":enabled").length || q.push(":enabled", ":disabled"), a.querySelectorAll("*,:x"), q.push(",.*:")
        })), (c.matchesSelector = Z.test(s = o.matches || o.webkitMatchesSelector || o.mozMatchesSelector || o.oMatchesSelector || o.msMatchesSelector)) && ia(function(a) {
          c.disconnectedMatch = s.call(a, "div"), s.call(a, "[s!='']:x"), r.push("!=", O)
        }), q = q.length && new RegExp(q.join("|")), r = r.length && new RegExp(r.join("|")), b = Z.test(o.compareDocumentPosition), t = b || Z.test(o.contains) ? function(a, b) {
          var c = 9 === a.nodeType ? a.documentElement : a,
            d = b && b.parentNode;
          return a === d || !(!d || 1 !== d.nodeType || !(c.contains ? c.contains(d) : a.compareDocumentPosition && 16 & a.compareDocumentPosition(d)))
        } : function(a, b) {
          if (b)
            for (; b = b.parentNode;)
              if (b === a) return !0;
          return !1
        }, B = b ? function(a, b) {
          if (a === b) return l = !0, 0;
          var d = !a.compareDocumentPosition - !b.compareDocumentPosition;
          return d ? d : (d = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1, 1 & d || !c.sortDetached && b.compareDocumentPosition(a) === d ? a === n || a.ownerDocument === v && t(v, a) ? -1 : b === n || b.ownerDocument === v && t(v, b) ? 1 : k ? J(k, a) - J(k, b) : 0 : 4 & d ? -1 : 1)
        } : function(a, b) {
          if (a === b) return l = !0, 0;
          var c, d = 0,
            e = a.parentNode,
            f = b.parentNode,
            g = [a],
            h = [b];
          if (!e || !f) return a === n ? -1 : b === n ? 1 : e ? -1 : f ? 1 : k ? J(k, a) - J(k, b) : 0;
          if (e === f) return ka(a, b);
          for (c = a; c = c.parentNode;) g.unshift(c);
          for (c = b; c = c.parentNode;) h.unshift(c);
          for (; g[d] === h[d];) d++;
          return d ? ka(g[d], h[d]) : g[d] === v ? -1 : h[d] === v ? 1 : 0
        }, n) : n
      }, fa.matches = function(a, b) {
        return fa(a, null, null, b)
      }, fa.matchesSelector = function(a, b) {
        if ((a.ownerDocument || a) !== n && m(a), b = b.replace(T, "='$1']"), !(!c.matchesSelector || !p || A[b + " "] || r && r.test(b) || q && q.test(b))) try {
          var d = s.call(a, b);
          if (d || c.disconnectedMatch || a.document && 11 !== a.document.nodeType) return d
        } catch (e) {}
        return fa(b, n, null, [a]).length > 0
      }, fa.contains = function(a, b) {
        return (a.ownerDocument || a) !== n && m(a), t(a, b)
      }, fa.attr = function(a, b) {
        (a.ownerDocument || a) !== n && m(a);
        var e = d.attrHandle[b.toLowerCase()],
          f = e && D.call(d.attrHandle, b.toLowerCase()) ? e(a, b, !p) : void 0;
        return void 0 !== f ? f : c.attributes || !p ? a.getAttribute(b) : (f = a.getAttributeNode(b)) && f.specified ? f.value : null
      }, fa.error = function(a) {
        throw new Error("Syntax error, unrecognized expression: " + a)
      }, fa.uniqueSort = function(a) {
        var b, d = [],
          e = 0,
          f = 0;
        if (l = !c.detectDuplicates, k = !c.sortStable && a.slice(0), a.sort(B), l) {
          for (; b = a[f++];) b === a[f] && (e = d.push(f));
          for (; e--;) a.splice(d[e], 1)
        }
        return k = null, a
      }, e = fa.getText = function(a) {
        var b, c = "",
          d = 0,
          f = a.nodeType;
        if (f) {
          if (1 === f || 9 === f || 11 === f) {
            if ("string" == typeof a.textContent) return a.textContent;
            for (a = a.firstChild; a; a = a.nextSibling) c += e(a)
          } else if (3 === f || 4 === f) return a.nodeValue
        } else
          for (; b = a[d++];) c += e(b);
        return c
      }, d = fa.selectors = {
        cacheLength: 50,
        createPseudo: ha,
        match: W,
        attrHandle: {},
        find: {},
        relative: {
          ">": {
            dir: "parentNode",
            first: !0
          },
          " ": {
            dir: "parentNode"
          },
          "+": {
            dir: "previousSibling",
            first: !0
          },
          "~": {
            dir: "previousSibling"
          }
        },
        preFilter: {
          ATTR: function(a) {
            return a[1] = a[1].replace(ba, ca), a[3] = (a[3] || a[4] || a[5] || "").replace(ba, ca), "~=" === a[2] && (a[3] = " " + a[3] + " "), a.slice(0, 4)
          },
          CHILD: function(a) {
            return a[1] = a[1].toLowerCase(), "nth" === a[1].slice(0, 3) ? (a[3] || fa.error(a[0]), a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * ("even" === a[3] || "odd" === a[3])), a[5] = +(a[7] + a[8] || "odd" === a[3])) : a[3] && fa.error(a[0]), a
          },
          PSEUDO: function(a) {
            var b, c = !a[6] && a[2];
            return W.CHILD.test(a[0]) ? null : (a[3] ? a[2] = a[4] || a[5] || "" : c && U.test(c) && (b = g(c, !0)) && (b = c.indexOf(")", c.length - b) - c.length) && (a[0] = a[0].slice(0, b), a[2] = c.slice(0, b)), a.slice(0, 3))
          }
        },
        filter: {
          TAG: function(a) {
            var b = a.replace(ba, ca).toLowerCase();
            return "*" === a ? function() {
              return !0
            } : function(a) {
              return a.nodeName && a.nodeName.toLowerCase() === b
            }
          },
          CLASS: function(a) {
            var b = y[a + " "];
            return b || (b = new RegExp("(^|" + L + ")" + a + "(" + L + "|$)")) && y(a, function(a) {
              return b.test("string" == typeof a.className && a.className || "undefined" != typeof a.getAttribute && a.getAttribute("class") || "")
            })
          },
          ATTR: function(a, b, c) {
            return function(d) {
              var e = fa.attr(d, a);
              return null == e ? "!=" === b : b ? (e += "", "=" === b ? e === c : "!=" === b ? e !== c : "^=" === b ? c && 0 === e.indexOf(c) : "*=" === b ? c && e.indexOf(c) > -1 : "$=" === b ? c && e.slice(-c.length) === c : "~=" === b ? (" " + e.replace(P, " ") + " ").indexOf(c) > -1 : "|=" === b ? e === c || e.slice(0, c.length + 1) === c + "-" : !1) : !0
            }
          },
          CHILD: function(a, b, c, d, e) {
            var f = "nth" !== a.slice(0, 3),
              g = "last" !== a.slice(-4),
              h = "of-type" === b;
            return 1 === d && 0 === e ? function(a) {
              return !!a.parentNode
            } : function(b, c, i) {
              var j, k, l, m, n, o, p = f !== g ? "nextSibling" : "previousSibling",
                q = b.parentNode,
                r = h && b.nodeName.toLowerCase(),
                s = !i && !h,
                t = !1;
              if (q) {
                if (f) {
                  for (; p;) {
                    for (m = b; m = m[p];)
                      if (h ? m.nodeName.toLowerCase() === r : 1 === m.nodeType) return !1;
                    o = p = "only" === a && !o && "nextSibling"
                  }
                  return !0
                }
                if (o = [g ? q.firstChild : q.lastChild], g && s) {
                  for (m = q, l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), j = k[a] || [], n = j[0] === w && j[1], t = n && j[2], m = n && q.childNodes[n]; m = ++n && m && m[p] || (t = n = 0) || o.pop();)
                    if (1 === m.nodeType && ++t && m === b) {
                      k[a] = [w, n, t];
                      break;
                    }
                } else if (s && (m = b, l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), j = k[a] || [], n = j[0] === w && j[1], t = n), t === !1)
                  for (;
                    (m = ++n && m && m[p] || (t = n = 0) || o.pop()) && ((h ? m.nodeName.toLowerCase() !== r : 1 !== m.nodeType) || !++t || (s && (l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), k[a] = [w, t]), m !== b)););
                return t -= e, t === d || t % d === 0 && t / d >= 0
              }
            }
          },
          PSEUDO: function(a, b) {
            var c, e = d.pseudos[a] || d.setFilters[a.toLowerCase()] || fa.error("unsupported pseudo: " + a);
            return e[u] ? e(b) : e.length > 1 ? (c = [a, a, "", b], d.setFilters.hasOwnProperty(a.toLowerCase()) ? ha(function(a, c) {
              for (var d, f = e(a, b), g = f.length; g--;) d = J(a, f[g]), a[d] = !(c[d] = f[g])
            }) : function(a) {
              return e(a, 0, c)
            }) : e
          }
        },
        pseudos: {
          not: ha(function(a) {
            var b = [],
              c = [],
              d = h(a.replace(Q, "$1"));
            return d[u] ? ha(function(a, b, c, e) {
              for (var f, g = d(a, null, e, []), h = a.length; h--;)(f = g[h]) && (a[h] = !(b[h] = f))
            }) : function(a, e, f) {
              return b[0] = a, d(b, null, f, c), b[0] = null, !c.pop()
            }
          }),
          has: ha(function(a) {
            return function(b) {
              return fa(a, b).length > 0
            }
          }),
          contains: ha(function(a) {
            return a = a.replace(ba, ca),
              function(b) {
                return (b.textContent || b.innerText || e(b)).indexOf(a) > -1
              }
          }),
          lang: ha(function(a) {
            return V.test(a || "") || fa.error("unsupported lang: " + a), a = a.replace(ba, ca).toLowerCase(),
              function(b) {
                var c;
                do
                  if (c = p ? b.lang : b.getAttribute("xml:lang") || b.getAttribute("lang")) return c = c.toLowerCase(), c === a || 0 === c.indexOf(a + "-"); while ((b = b.parentNode) && 1 === b.nodeType);
                return !1
              }
          }),
          target: function(b) {
            var c = a.location && a.location.hash;
            return c && c.slice(1) === b.id
          },
          root: function(a) {
            return a === o
          },
          focus: function(a) {
            return a === n.activeElement && (!n.hasFocus || n.hasFocus()) && !!(a.type || a.href || ~a.tabIndex)
          },
          enabled: function(a) {
            return a.disabled === !1
          },
          disabled: function(a) {
            return a.disabled === !0
          },
          checked: function(a) {
            var b = a.nodeName.toLowerCase();
            return "input" === b && !!a.checked || "option" === b && !!a.selected
          },
          selected: function(a) {
            return a.parentNode && a.parentNode.selectedIndex, a.selected === !0
          },
          empty: function(a) {
            for (a = a.firstChild; a; a = a.nextSibling)
              if (a.nodeType < 6) return !1;
            return !0
          },
          parent: function(a) {
            return !d.pseudos.empty(a)
          },
          header: function(a) {
            return Y.test(a.nodeName)
          },
          input: function(a) {
            return X.test(a.nodeName)
          },
          button: function(a) {
            var b = a.nodeName.toLowerCase();
            return "input" === b && "button" === a.type || "button" === b
          },
          text: function(a) {
            var b;
            return "input" === a.nodeName.toLowerCase() && "text" === a.type && (null == (b = a.getAttribute("type")) || "text" === b.toLowerCase())
          },
          first: na(function() {
            return [0]
          }),
          last: na(function(a, b) {
            return [b - 1]
          }),
          eq: na(function(a, b, c) {
            return [0 > c ? c + b : c]
          }),
          even: na(function(a, b) {
            for (var c = 0; b > c; c += 2) a.push(c);
            return a
          }),
          odd: na(function(a, b) {
            for (var c = 1; b > c; c += 2) a.push(c);
            return a
          }),
          lt: na(function(a, b, c) {
            for (var d = 0 > c ? c + b : c; --d >= 0;) a.push(d);
            return a
          }),
          gt: na(function(a, b, c) {
            for (var d = 0 > c ? c + b : c; ++d < b;) a.push(d);
            return a
          })
        }
      }, d.pseudos.nth = d.pseudos.eq;
      for (b in {
          radio: !0,
          checkbox: !0,
          file: !0,
          password: !0,
          image: !0
        }) d.pseudos[b] = la(b);
      for (b in {
          submit: !0,
          reset: !0
        }) d.pseudos[b] = ma(b);
      return pa.prototype = d.filters = d.pseudos, d.setFilters = new pa, g = fa.tokenize = function(a, b) {
        var c, e, f, g, h, i, j, k = z[a + " "];
        if (k) return b ? 0 : k.slice(0);
        for (h = a, i = [], j = d.preFilter; h;) {
          c && !(e = R.exec(h)) || (e && (h = h.slice(e[0].length) || h), i.push(f = [])), c = !1, (e = S.exec(h)) && (c = e.shift(), f.push({
            value: c,
            type: e[0].replace(Q, " ")
          }), h = h.slice(c.length));
          for (g in d.filter) !(e = W[g].exec(h)) || j[g] && !(e = j[g](e)) || (c = e.shift(), f.push({
            value: c,
            type: g,
            matches: e
          }), h = h.slice(c.length));
          if (!c) break
        }
        return b ? h.length : h ? fa.error(a) : z(a, i).slice(0)
      }, h = fa.compile = function(a, b) {
        var c, d = [],
          e = [],
          f = A[a + " "];
        if (!f) {
          for (b || (b = g(a)), c = b.length; c--;) f = wa(b[c]), f[u] ? d.push(f) : e.push(f);
          f = A(a, xa(e, d)), f.selector = a
        }
        return f
      }, i = fa.select = function(a, b, e, f) {
        var i, j, k, l, m, n = "function" == typeof a && a,
          o = !f && g(a = n.selector || a);
        if (e = e || [], 1 === o.length) {
          if (j = o[0] = o[0].slice(0), j.length > 2 && "ID" === (k = j[0]).type && c.getById && 9 === b.nodeType && p && d.relative[j[1].type]) {
            if (b = (d.find.ID(k.matches[0].replace(ba, ca), b) || [])[0], !b) return e;
            n && (b = b.parentNode), a = a.slice(j.shift().value.length)
          }
          for (i = W.needsContext.test(a) ? 0 : j.length; i-- && (k = j[i], !d.relative[l = k.type]);)
            if ((m = d.find[l]) && (f = m(k.matches[0].replace(ba, ca), _.test(j[0].type) && oa(b.parentNode) || b))) {
              if (j.splice(i, 1), a = f.length && qa(j), !a) return H.apply(e, f), e;
              break
            }
        }
        return (n || h(a, o))(f, b, !p, e, !b || _.test(a) && oa(b.parentNode) || b), e
      }, c.sortStable = u.split("").sort(B).join("") === u, c.detectDuplicates = !!l, m(), c.sortDetached = ia(function(a) {
        return 1 & a.compareDocumentPosition(n.createElement("div"))
      }), ia(function(a) {
        return a.innerHTML = "<a href='#'></a>", "#" === a.firstChild.getAttribute("href")
      }) || ja("type|href|height|width", function(a, b, c) {
        return c ? void 0 : a.getAttribute(b, "type" === b.toLowerCase() ? 1 : 2)
      }), c.attributes && ia(function(a) {
        return a.innerHTML = "<input/>", a.firstChild.setAttribute("value", ""), "" === a.firstChild.getAttribute("value")
      }) || ja("value", function(a, b, c) {
        return c || "input" !== a.nodeName.toLowerCase() ? void 0 : a.defaultValue
      }), ia(function(a) {
        return null == a.getAttribute("disabled")
      }) || ja(K, function(a, b, c) {
        var d;
        return c ? void 0 : a[b] === !0 ? b.toLowerCase() : (d = a.getAttributeNode(b)) && d.specified ? d.value : null
      }), fa
    }(a);
    n.find = t, n.expr = t.selectors, n.expr[":"] = n.expr.pseudos, n.uniqueSort = n.unique = t.uniqueSort, n.text = t.getText, n.isXMLDoc = t.isXML, n.contains = t.contains;
    var u = function(a, b, c) {
        for (var d = [], e = void 0 !== c;
          (a = a[b]) && 9 !== a.nodeType;)
          if (1 === a.nodeType) {
            if (e && n(a).is(c)) break;
            d.push(a)
          } return d
      },
      v = function(a, b) {
        for (var c = []; a; a = a.nextSibling) 1 === a.nodeType && a !== b && c.push(a);
        return c
      },
      w = n.expr.match.needsContext,
      x = /^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,
      y = /^.[^:#\[\.,]*$/;
    n.filter = function(a, b, c) {
      var d = b[0];
      return c && (a = ":not(" + a + ")"), 1 === b.length && 1 === d.nodeType ? n.find.matchesSelector(d, a) ? [d] : [] : n.find.matches(a, n.grep(b, function(a) {
        return 1 === a.nodeType
      }))
    }, n.fn.extend({
      find: function(a) {
        var b, c = [],
          d = this,
          e = d.length;
        if ("string" != typeof a) return this.pushStack(n(a).filter(function() {
          for (b = 0; e > b; b++)
            if (n.contains(d[b], this)) return !0
        }));
        for (b = 0; e > b; b++) n.find(a, d[b], c);
        return c = this.pushStack(e > 1 ? n.unique(c) : c), c.selector = this.selector ? this.selector + " " + a : a, c
      },
      filter: function(a) {
        return this.pushStack(z(this, a || [], !1))
      },
      not: function(a) {
        return this.pushStack(z(this, a || [], !0))
      },
      is: function(a) {
        return !!z(this, "string" == typeof a && w.test(a) ? n(a) : a || [], !1).length
      }
    });
    var A, B = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,
      C = n.fn.init = function(a, b, c) {
        var e, f;
        if (!a) return this;
        if (c = c || A, "string" == typeof a) {
          if (e = "<" === a.charAt(0) && ">" === a.charAt(a.length - 1) && a.length >= 3 ? [null, a, null] : B.exec(a), !e || !e[1] && b) return !b || b.jquery ? (b || c).find(a) : this.constructor(b).find(a);
          if (e[1]) {
            if (b = b instanceof n ? b[0] : b, n.merge(this, n.parseHTML(e[1], b && b.nodeType ? b.ownerDocument || b : d, !0)), x.test(e[1]) && n.isPlainObject(b))
              for (e in b) n.isFunction(this[e]) ? this[e](b[e]) : this.attr(e, b[e]);
            return this
          }
          if (f = d.getElementById(e[2]), f && f.parentNode) {
            if (f.id !== e[2]) return A.find(a);
            this.length = 1, this[0] = f
          }
          return this.context = d, this.selector = a, this
        }
        return a.nodeType ? (this.context = this[0] = a, this.length = 1, this) : n.isFunction(a) ? "undefined" != typeof c.ready ? c.ready(a) : a(n) : (void 0 !== a.selector && (this.selector = a.selector, this.context = a.context), n.makeArray(a, this))
      };
    C.prototype = n.fn, A = n(d);
    var D = /^(?:parents|prev(?:Until|All))/,
      E = {
        children: !0,
        contents: !0,
        next: !0,
        prev: !0
      };
    n.fn.extend({
      has: function(a) {
        var b, c = n(a, this),
          d = c.length;
        return this.filter(function() {
          for (b = 0; d > b; b++)
            if (n.contains(this, c[b])) return !0
        })
      },
      closest: function(a, b) {
        for (var c, d = 0, e = this.length, f = [], g = w.test(a) || "string" != typeof a ? n(a, b || this.context) : 0; e > d; d++)
          for (c = this[d]; c && c !== b; c = c.parentNode)
            if (c.nodeType < 11 && (g ? g.index(c) > -1 : 1 === c.nodeType && n.find.matchesSelector(c, a))) {
              f.push(c);
              break
            } return this.pushStack(f.length > 1 ? n.uniqueSort(f) : f)
      },
      index: function(a) {
        return a ? "string" == typeof a ? n.inArray(this[0], n(a)) : n.inArray(a.jquery ? a[0] : a, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
      },
      add: function(a, b) {
        return this.pushStack(n.uniqueSort(n.merge(this.get(), n(a, b))))
      },
      addBack: function(a) {
        return this.add(null == a ? this.prevObject : this.prevObject.filter(a))
      }
    }), n.each({
      parent: function(a) {
        var b = a.parentNode;
        return b && 11 !== b.nodeType ? b : null
      },
      parents: function(a) {
        return u(a, "parentNode")
      },
      parentsUntil: function(a, b, c) {
        return u(a, "parentNode", c)
      },
      next: function(a) {
        return F(a, "nextSibling")
      },
      prev: function(a) {
        return F(a, "previousSibling")
      },
      nextAll: function(a) {
        return u(a, "nextSibling")
      },
      prevAll: function(a) {
        return u(a, "previousSibling")
      },
      nextUntil: function(a, b, c) {
        return u(a, "nextSibling", c)
      },
      prevUntil: function(a, b, c) {
        return u(a, "previousSibling", c)
      },
      siblings: function(a) {
        return v((a.parentNode || {}).firstChild, a)
      },
      children: function(a) {
        return v(a.firstChild)
      },
      contents: function(a) {
        return n.nodeName(a, "iframe") ? a.contentDocument || a.contentWindow.document : n.merge([], a.childNodes)
      }
    }, function(a, b) {
      n.fn[a] = function(c, d) {
        var e = n.map(this, b, c);
        return "Until" !== a.slice(-5) && (d = c), d && "string" == typeof d && (e = n.filter(d, e)), this.length > 1 && (E[a] || (e = n.uniqueSort(e)), D.test(a) && (e = e.reverse())), this.pushStack(e)
      }
    });
    var G = /\S+/g;
    n.Callbacks = function(a) {
      a = "string" == typeof a ? H(a) : n.extend({}, a);
      var b, c, d, e, f = [],
        g = [],
        h = -1,
        i = function() {
          for (e = a.once, d = b = !0; g.length; h = -1)
            for (c = g.shift(); ++h < f.length;) f[h].apply(c[0], c[1]) === !1 && a.stopOnFalse && (h = f.length, c = !1);
          a.memory || (c = !1), b = !1, e && (f = c ? [] : "")
        },
        j = {
          add: function() {
            return f && (c && !b && (h = f.length - 1, g.push(c)), function d(b) {
              n.each(b, function(b, c) {
                n.isFunction(c) ? a.unique && j.has(c) || f.push(c) : c && c.length && "string" !== n.type(c) && d(c)
              })
            }(arguments), c && !b && i()), this
          },
          remove: function() {
            return n.each(arguments, function(a, b) {
              for (var c;
                (c = n.inArray(b, f, c)) > -1;) f.splice(c, 1), h >= c && h--
            }), this
          },
          has: function(a) {
            return a ? n.inArray(a, f) > -1 : f.length > 0
          },
          empty: function() {
            return f && (f = []), this
          },
          disable: function() {
            return e = g = [], f = c = "", this
          },
          disabled: function() {
            return !f
          },
          lock: function() {
            return e = !0, c || j.disable(), this
          },
          locked: function() {
            return !!e
          },
          fireWith: function(a, c) {
            return e || (c = c || [], c = [a, c.slice ? c.slice() : c], g.push(c), b || i()), this
          },
          fire: function() {
            return j.fireWith(this, arguments), this
          },
          fired: function() {
            return !!d
          }
        };
      return j
    }, n.extend({
      Deferred: function(a) {
        var b = [
            ["resolve", "done", n.Callbacks("once memory"), "resolved"],
            ["reject", "fail", n.Callbacks("once memory"), "rejected"],
            ["notify", "progress", n.Callbacks("memory")]
          ],
          c = "pending",
          d = {
            state: function() {
              return c
            },
            always: function() {
              return e.done(arguments).fail(arguments), this
            },
            then: function() {
              var a = arguments;
              return n.Deferred(function(c) {
                n.each(b, function(b, f) {
                  var g = n.isFunction(a[b]) && a[b];
                  e[f[1]](function() {
                    var a = g && g.apply(this, arguments);
                    a && n.isFunction(a.promise) ? a.promise().progress(c.notify).done(c.resolve).fail(c.reject) : c[f[0] + "With"](this === d ? c.promise() : this, g ? [a] : arguments)
                  })
                }), a = null
              }).promise()
            },
            promise: function(a) {
              return null != a ? n.extend(a, d) : d
            }
          },
          e = {};
        return d.pipe = d.then, n.each(b, function(a, f) {
          var g = f[2],
            h = f[3];
          d[f[1]] = g.add, h && g.add(function() {
            c = h
          }, b[1 ^ a][2].disable, b[2][2].lock), e[f[0]] = function() {
            return e[f[0] + "With"](this === e ? d : this, arguments), this
          }, e[f[0] + "With"] = g.fireWith
        }), d.promise(e), a && a.call(e, e), e
      },
      when: function(a) {
        var i, j, k, b = 0,
          c = e.call(arguments),
          d = c.length,
          f = 1 !== d || a && n.isFunction(a.promise) ? d : 0,
          g = 1 === f ? a : n.Deferred(),
          h = function(a, b, c) {
            return function(d) {
              b[a] = this, c[a] = arguments.length > 1 ? e.call(arguments) : d, c === i ? g.notifyWith(b, c) : --f || g.resolveWith(b, c)
            }
          };
        if (d > 1)
          for (i = new Array(d), j = new Array(d), k = new Array(d); d > b; b++) c[b] && n.isFunction(c[b].promise) ? c[b].promise().progress(h(b, j, i)).done(h(b, k, c)).fail(g.reject) : --f;
        return f || g.resolveWith(k, c), g.promise()
      }
    });
    var I;
    n.fn.ready = function(a) {
      return n.ready.promise().done(a), this
    }, n.extend({
      isReady: !1,
      readyWait: 1,
      holdReady: function(a) {
        a ? n.readyWait++ : n.ready(!0)
      },
      ready: function(a) {
        (a === !0 ? --n.readyWait : n.isReady) || (n.isReady = !0, a !== !0 && --n.readyWait > 0 || (I.resolveWith(d, [n]), n.fn.triggerHandler && (n(d).triggerHandler("ready"), n(d).off("ready"))))
      }
    }), n.ready.promise = function(b) {
      if (!I)
        if (I = n.Deferred(), "complete" === d.readyState || "loading" !== d.readyState && !d.documentElement.doScroll) a.setTimeout(n.ready);
        else if (d.addEventListener) d.addEventListener("DOMContentLoaded", K), a.addEventListener("load", K);
      else {
        d.attachEvent("onreadystatechange", K), a.attachEvent("onload", K);
        var c = !1;
        try {
          c = null == a.frameElement && d.documentElement
        } catch (e) {}
        c && c.doScroll && ! function f() {
          if (!n.isReady) {
            try {
              c.doScroll("left")
            } catch (b) {
              return a.setTimeout(f, 50)
            }
            J(), n.ready()
          }
        }()
      }
      return I.promise(b)
    }, n.ready.promise();
    var L;
    for (L in n(l)) break;
    l.ownFirst = "0" === L, l.inlineBlockNeedsLayout = !1, n(function() {
        var a, b, c, e;
        c = d.getElementsByTagName("body")[0], c && c.style && (b = d.createElement("div"), e = d.createElement("div"), e.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", c.appendChild(e).appendChild(b), "undefined" != typeof b.style.zoom && (b.style.cssText = "display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1", l.inlineBlockNeedsLayout = a = 3 === b.offsetWidth, a && (c.style.zoom = 1)), c.removeChild(e))
      }),
      function() {
        var a = d.createElement("div");
        l.deleteExpando = !0;
        try {
          delete a.test
        } catch (b) {
          l.deleteExpando = !1
        }
        a = null
      }();
    var M = function(a) {
        var b = n.noData[(a.nodeName + " ").toLowerCase()],
          c = +a.nodeType || 1;
        return 1 !== c && 9 !== c ? !1 : !b || b !== !0 && a.getAttribute("classid") === b
      },
      N = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
      O = /([A-Z])/g;
    n.extend({
        cache: {},
        noData: {
          "applet ": !0,
          "embed ": !0,
          "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
        },
        hasData: function(a) {
          return a = a.nodeType ? n.cache[a[n.expando]] : a[n.expando], !!a && !Q(a)
        },
        data: function(a, b, c) {
          return R(a, b, c)
        },
        removeData: function(a, b) {
          return S(a, b)
        },
        _data: function(a, b, c) {
          return R(a, b, c, !0)
        },
        _removeData: function(a, b) {
          return S(a, b, !0)
        }
      }), n.fn.extend({
        data: function(a, b) {
          var c, d, e, f = this[0],
            g = f && f.attributes;
          if (void 0 === a) {
            if (this.length && (e = n.data(f), 1 === f.nodeType && !n._data(f, "parsedAttrs"))) {
              for (c = g.length; c--;) g[c] && (d = g[c].name, 0 === d.indexOf("data-") && (d = n.camelCase(d.slice(5)), P(f, d, e[d])));
              n._data(f, "parsedAttrs", !0)
            }
            return e
          }
          return "object" == typeof a ? this.each(function() {
            n.data(this, a)
          }) : arguments.length > 1 ? this.each(function() {
            n.data(this, a, b)
          }) : f ? P(f, a, n.data(f, a)) : void 0
        },
        removeData: function(a) {
          return this.each(function() {
            n.removeData(this, a)
          })
        }
      }), n.extend({
        queue: function(a, b, c) {
          var d;
          return a ? (b = (b || "fx") + "queue", d = n._data(a, b), c && (!d || n.isArray(c) ? d = n._data(a, b, n.makeArray(c)) : d.push(c)), d || []) : void 0
        },
        dequeue: function(a, b) {
          b = b || "fx";
          var c = n.queue(a, b),
            d = c.length,
            e = c.shift(),
            f = n._queueHooks(a, b),
            g = function() {
              n.dequeue(a, b)
            };
          "inprogress" === e && (e = c.shift(), d--), e && ("fx" === b && c.unshift("inprogress"), delete f.stop, e.call(a, g, f)), !d && f && f.empty.fire()
        },
        _queueHooks: function(a, b) {
          var c = b + "queueHooks";
          return n._data(a, c) || n._data(a, c, {
            empty: n.Callbacks("once memory").add(function() {
              n._removeData(a, b + "queue"), n._removeData(a, c)
            })
          })
        }
      }), n.fn.extend({
        queue: function(a, b) {
          var c = 2;
          return "string" != typeof a && (b = a, a = "fx", c--), arguments.length < c ? n.queue(this[0], a) : void 0 === b ? this : this.each(function() {
            var c = n.queue(this, a, b);
            n._queueHooks(this, a), "fx" === a && "inprogress" !== c[0] && n.dequeue(this, a)
          })
        },
        dequeue: function(a) {
          return this.each(function() {
            n.dequeue(this, a)
          })
        },
        clearQueue: function(a) {
          return this.queue(a || "fx", [])
        },
        promise: function(a, b) {
          var c, d = 1,
            e = n.Deferred(),
            f = this,
            g = this.length,
            h = function() {
              --d || e.resolveWith(f, [f])
            };
          for ("string" != typeof a && (b = a, a = void 0), a = a || "fx"; g--;) c = n._data(f[g], a + "queueHooks"), c && c.empty && (d++, c.empty.add(h));
          return h(), e.promise(b)
        }
      }),
      function() {
        var a;
        l.shrinkWrapBlocks = function() {
          if (null != a) return a;
          a = !1;
          var b, c, e;
          return c = d.getElementsByTagName("body")[0], c && c.style ? (b = d.createElement("div"), e = d.createElement("div"), e.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", c.appendChild(e).appendChild(b), "undefined" != typeof b.style.zoom && (b.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1", b.appendChild(d.createElement("div")).style.width = "5px", a = 3 !== b.offsetWidth), c.removeChild(e), a) : void 0
        }
      }();
    var T = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
      U = new RegExp("^(?:([+-])=|)(" + T + ")([a-z%]*)$", "i"),
      V = ["Top", "Right", "Bottom", "Left"],
      W = function(a, b) {
        return a = b || a, "none" === n.css(a, "display") || !n.contains(a.ownerDocument, a)
      },
      Y = function(a, b, c, d, e, f, g) {
        var h = 0,
          i = a.length,
          j = null == c;
        if ("object" === n.type(c)) {
          e = !0;
          for (h in c) Y(a, b, h, c[h], !0, f, g)
        } else if (void 0 !== d && (e = !0, n.isFunction(d) || (g = !0), j && (g ? (b.call(a, d), b = null) : (j = b, b = function(a, b, c) {
            return j.call(n(a), c)
          })), b))
          for (; i > h; h++) b(a[h], c, g ? d : d.call(a[h], h, b(a[h], c)));
        return e ? a : j ? b.call(a) : i ? b(a[0], c) : f
      },
      Z = /^(?:checkbox|radio)$/i,
      $ = /<([\w:-]+)/,
      _ = /^$|\/(?:java|ecma)script/i,
      aa = /^\s+/,
      ba = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|dialog|figcaption|figure|footer|header|hgroup|main|mark|meter|nav|output|picture|progress|section|summary|template|time|video";
    ! function() {
      var a = d.createElement("div"),
        b = d.createDocumentFragment(),
        c = d.createElement("input");
      a.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", l.leadingWhitespace = 3 === a.firstChild.nodeType, l.tbody = !a.getElementsByTagName("tbody").length, l.htmlSerialize = !!a.getElementsByTagName("link").length, l.html5Clone = "<:nav></:nav>" !== d.createElement("nav").cloneNode(!0).outerHTML, c.type = "checkbox", c.checked = !0, b.appendChild(c), l.appendChecked = c.checked, a.innerHTML = "<textarea>x</textarea>", l.noCloneChecked = !!a.cloneNode(!0).lastChild.defaultValue, b.appendChild(a), c = d.createElement("input"), c.setAttribute("type", "radio"), c.setAttribute("checked", "checked"), c.setAttribute("name", "t"), a.appendChild(c), l.checkClone = a.cloneNode(!0).cloneNode(!0).lastChild.checked, l.noCloneEvent = !!a.addEventListener, a[n.expando] = 1, l.attributes = !a.getAttribute(n.expando)
    }();
    var da = {
      option: [1, "<select multiple='multiple'>", "</select>"],
      legend: [1, "<fieldset>", "</fieldset>"],
      area: [1, "<map>", "</map>"],
      param: [1, "<object>", "</object>"],
      thead: [1, "<table>", "</table>"],
      tr: [2, "<table><tbody>", "</tbody></table>"],
      col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
      td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
      _default: l.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
    };
    da.optgroup = da.option, da.tbody = da.tfoot = da.colgroup = da.caption = da.thead, da.th = da.td;
    var ga = /<|&#?\w+;/,
      ha = /<tbody/i;
    ! function() {
      var b, c, e = d.createElement("div");
      for (b in {
          submit: !0,
          change: !0,
          focusin: !0
        }) c = "on" + b, (l[b] = c in a) || (e.setAttribute(c, "t"), l[b] = e.attributes[c].expando === !1);
      e = null
    }();
    var ka = /^(?:input|select|textarea)$/i,
      la = /^key/,
      ma = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
      na = /^(?:focusinfocus|focusoutblur)$/,
      oa = /^([^.]*)(?:\.(.+)|)/;
    n.event = {
      global: {},
      add: function(a, b, c, d, e) {
        var f, g, h, i, j, k, l, m, o, p, q, r = n._data(a);
        if (r) {
          for (c.handler && (i = c, c = i.handler, e = i.selector), c.guid || (c.guid = n.guid++), (g = r.events) || (g = r.events = {}), (k = r.handle) || (k = r.handle = function(a) {
              return "undefined" == typeof n || a && n.event.triggered === a.type ? void 0 : n.event.dispatch.apply(k.elem, arguments)
            }, k.elem = a), b = (b || "").match(G) || [""], h = b.length; h--;) f = oa.exec(b[h]) || [], o = q = f[1], p = (f[2] || "").split(".").sort(), o && (j = n.event.special[o] || {}, o = (e ? j.delegateType : j.bindType) || o, j = n.event.special[o] || {}, l = n.extend({
            type: o,
            origType: q,
            data: d,
            handler: c,
            guid: c.guid,
            selector: e,
            needsContext: e && n.expr.match.needsContext.test(e),
            namespace: p.join(".")
          }, i), (m = g[o]) || (m = g[o] = [], m.delegateCount = 0, j.setup && j.setup.call(a, d, p, k) !== !1 || (a.addEventListener ? a.addEventListener(o, k, !1) : a.attachEvent && a.attachEvent("on" + o, k))), j.add && (j.add.call(a, l), l.handler.guid || (l.handler.guid = c.guid)), e ? m.splice(m.delegateCount++, 0, l) : m.push(l), n.event.global[o] = !0);
          a = null
        }
      },
      remove: function(a, b, c, d, e) {
        var f, g, h, i, j, k, l, m, o, p, q, r = n.hasData(a) && n._data(a);
        if (r && (k = r.events)) {
          for (b = (b || "").match(G) || [""], j = b.length; j--;)
            if (h = oa.exec(b[j]) || [], o = q = h[1], p = (h[2] || "").split(".").sort(), o) {
              for (l = n.event.special[o] || {}, o = (d ? l.delegateType : l.bindType) || o, m = k[o] || [], h = h[2] && new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)"), i = f = m.length; f--;) g = m[f], !e && q !== g.origType || c && c.guid !== g.guid || h && !h.test(g.namespace) || d && d !== g.selector && ("**" !== d || !g.selector) || (m.splice(f, 1), g.selector && m.delegateCount--, l.remove && l.remove.call(a, g));
              i && !m.length && (l.teardown && l.teardown.call(a, p, r.handle) !== !1 || n.removeEvent(a, o, r.handle), delete k[o])
            } else
              for (o in k) n.event.remove(a, o + b[j], c, d, !0);
          n.isEmptyObject(k) && (delete r.handle, n._removeData(a, "events"))
        }
      },
      trigger: function(b, c, e, f) {
        var g, h, i, j, l, m, o, p = [e || d],
          q = k.call(b, "type") ? b.type : b,
          r = k.call(b, "namespace") ? b.namespace.split(".") : [];
        if (i = m = e = e || d, 3 !== e.nodeType && 8 !== e.nodeType && !na.test(q + n.event.triggered) && (q.indexOf(".") > -1 && (r = q.split("."), q = r.shift(), r.sort()), h = q.indexOf(":") < 0 && "on" + q, b = b[n.expando] ? b : new n.Event(q, "object" == typeof b && b), b.isTrigger = f ? 2 : 3, b.namespace = r.join("."), b.rnamespace = b.namespace ? new RegExp("(^|\\.)" + r.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, b.result = void 0, b.target || (b.target = e), c = null == c ? [b] : n.makeArray(c, [b]), l = n.event.special[q] || {}, f || !l.trigger || l.trigger.apply(e, c) !== !1)) {
          if (!f && !l.noBubble && !n.isWindow(e)) {
            for (j = l.delegateType || q, na.test(j + q) || (i = i.parentNode); i; i = i.parentNode) p.push(i), m = i;
            m === (e.ownerDocument || d) && p.push(m.defaultView || m.parentWindow || a)
          }
          for (o = 0;
            (i = p[o++]) && !b.isPropagationStopped();) b.type = o > 1 ? j : l.bindType || q, g = (n._data(i, "events") || {})[b.type] && n._data(i, "handle"), g && g.apply(i, c), g = h && i[h], g && g.apply && M(i) && (b.result = g.apply(i, c), b.result === !1 && b.preventDefault());
          if (b.type = q, !f && !b.isDefaultPrevented() && (!l._default || l._default.apply(p.pop(), c) === !1) && M(e) && h && e[q] && !n.isWindow(e)) {
            m = e[h], m && (e[h] = null), n.event.triggered = q;
            try {
              e[q]()
            } catch (s) {}
            n.event.triggered = void 0, m && (e[h] = m)
          }
          return b.result
        }
      },
      dispatch: function(a) {
        a = n.event.fix(a);
        var b, c, d, f, g, h = [],
          i = e.call(arguments),
          j = (n._data(this, "events") || {})[a.type] || [],
          k = n.event.special[a.type] || {};
        if (i[0] = a, a.delegateTarget = this, !k.preDispatch || k.preDispatch.call(this, a) !== !1) {
          for (h = n.event.handlers.call(this, a, j), b = 0;
            (f = h[b++]) && !a.isPropagationStopped();)
            for (a.currentTarget = f.elem, c = 0;
              (g = f.handlers[c++]) && !a.isImmediatePropagationStopped();) a.rnamespace && !a.rnamespace.test(g.namespace) || (a.handleObj = g, a.data = g.data, d = ((n.event.special[g.origType] || {}).handle || g.handler).apply(f.elem, i), void 0 !== d && (a.result = d) === !1 && (a.preventDefault(), a.stopPropagation()));
          return k.postDispatch && k.postDispatch.call(this, a), a.result
        }
      },
      handlers: function(a, b) {
        var c, d, e, f, g = [],
          h = b.delegateCount,
          i = a.target;
        if (h && i.nodeType && ("click" !== a.type || isNaN(a.button) || a.button < 1))
          for (; i != this; i = i.parentNode || this)
            if (1 === i.nodeType && (i.disabled !== !0 || "click" !== a.type)) {
              for (d = [], c = 0; h > c; c++) f = b[c], e = f.selector + " ", void 0 === d[e] && (d[e] = f.needsContext ? n(e, this).index(i) > -1 : n.find(e, this, null, [i]).length), d[e] && d.push(f);
              d.length && g.push({
                elem: i,
                handlers: d
              })
            } return h < b.length && g.push({
          elem: this,
          handlers: b.slice(h)
        }), g
      },
      fix: function(a) {
        if (a[n.expando]) return a;
        var b, c, e, f = a.type,
          g = a,
          h = this.fixHooks[f];
        for (h || (this.fixHooks[f] = h = ma.test(f) ? this.mouseHooks : la.test(f) ? this.keyHooks : {}), e = h.props ? this.props.concat(h.props) : this.props, a = new n.Event(g), b = e.length; b--;) c = e[b], a[c] = g[c];
        return a.target || (a.target = g.srcElement || d), 3 === a.target.nodeType && (a.target = a.target.parentNode), a.metaKey = !!a.metaKey, h.filter ? h.filter(a, g) : a
      },
      props: "altKey bubbles cancelable ctrlKey currentTarget detail eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
      fixHooks: {},
      keyHooks: {
        props: "char charCode key keyCode".split(" "),
        filter: function(a, b) {
          return null == a.which && (a.which = null != b.charCode ? b.charCode : b.keyCode), a
        }
      },
      mouseHooks: {
        props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
        filter: function(a, b) {
          var c, e, f, g = b.button,
            h = b.fromElement;
          return null == a.pageX && null != b.clientX && (e = a.target.ownerDocument || d, f = e.documentElement, c = e.body, a.pageX = b.clientX + (f && f.scrollLeft || c && c.scrollLeft || 0) - (f && f.clientLeft || c && c.clientLeft || 0), a.pageY = b.clientY + (f && f.scrollTop || c && c.scrollTop || 0) - (f && f.clientTop || c && c.clientTop || 0)), !a.relatedTarget && h && (a.relatedTarget = h === a.target ? b.toElement : h), a.which || void 0 === g || (a.which = 1 & g ? 1 : 2 & g ? 3 : 4 & g ? 2 : 0), a
        }
      },
      special: {
        load: {
          noBubble: !0
        },
        focus: {
          trigger: function() {
            if (this !== ra() && this.focus) try {
              return this.focus(), !1
            } catch (a) {}
          },
          delegateType: "focusin"
        },
        blur: {
          trigger: function() {
            return this === ra() && this.blur ? (this.blur(), !1) : void 0
          },
          delegateType: "focusout"
        },
        click: {
          trigger: function() {
            return n.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(), !1) : void 0
          },
          _default: function(a) {
            return n.nodeName(a.target, "a")
          }
        },
        beforeunload: {
          postDispatch: function(a) {
            void 0 !== a.result && a.originalEvent && (a.originalEvent.returnValue = a.result)
          }
        }
      },
      simulate: function(a, b, c) {
        var d = n.extend(new n.Event, c, {
          type: a,
          isSimulated: !0
        });
        n.event.trigger(d, null, b), d.isDefaultPrevented() && c.preventDefault()
      }
    }, n.removeEvent = d.removeEventListener ? function(a, b, c) {
      a.removeEventListener && a.removeEventListener(b, c)
    } : function(a, b, c) {
      var d = "on" + b;
      a.detachEvent && ("undefined" == typeof a[d] && (a[d] = null), a.detachEvent(d, c))
    }, n.Event = function(a, b) {
      return this instanceof n.Event ? (a && a.type ? (this.originalEvent = a, this.type = a.type, this.isDefaultPrevented = a.defaultPrevented || void 0 === a.defaultPrevented && a.returnValue === !1 ? pa : qa) : this.type = a, b && n.extend(this, b), this.timeStamp = a && a.timeStamp || n.now(), void(this[n.expando] = !0)) : new n.Event(a, b)
    }, n.Event.prototype = {
      constructor: n.Event,
      isDefaultPrevented: qa,
      isPropagationStopped: qa,
      isImmediatePropagationStopped: qa,
      preventDefault: function() {
        var a = this.originalEvent;
        this.isDefaultPrevented = pa, a && (a.preventDefault ? a.preventDefault() : a.returnValue = !1)
      },
      stopPropagation: function() {
        var a = this.originalEvent;
        this.isPropagationStopped = pa, a && !this.isSimulated && (a.stopPropagation && a.stopPropagation(), a.cancelBubble = !0)
      },
      stopImmediatePropagation: function() {
        var a = this.originalEvent;
        this.isImmediatePropagationStopped = pa, a && a.stopImmediatePropagation && a.stopImmediatePropagation(), this.stopPropagation()
      }
    }, n.each({
      mouseenter: "mouseover",
      mouseleave: "mouseout",
      pointerenter: "pointerover",
      pointerleave: "pointerout"
    }, function(a, b) {
      n.event.special[a] = {
        delegateType: b,
        bindType: b,
        handle: function(a) {
          var c, d = this,
            e = a.relatedTarget,
            f = a.handleObj;
          return e && (e === d || n.contains(d, e)) || (a.type = f.origType, c = f.handler.apply(this, arguments), a.type = b), c
        }
      }
    }), l.submit || (n.event.special.submit = {
      setup: function() {
        return n.nodeName(this, "form") ? !1 : void n.event.add(this, "click._submit keypress._submit", function(a) {
          var b = a.target,
            c = n.nodeName(b, "input") || n.nodeName(b, "button") ? n.prop(b, "form") : void 0;
          c && !n._data(c, "submit") && (n.event.add(c, "submit._submit", function(a) {
            a._submitBubble = !0
          }), n._data(c, "submit", !0))
        })
      },
      postDispatch: function(a) {
        a._submitBubble && (delete a._submitBubble, this.parentNode && !a.isTrigger && n.event.simulate("submit", this.parentNode, a))
      },
      teardown: function() {
        return n.nodeName(this, "form") ? !1 : void n.event.remove(this, "._submit")
      }
    }), l.change || (n.event.special.change = {
      setup: function() {
        return ka.test(this.nodeName) ? ("checkbox" !== this.type && "radio" !== this.type || (n.event.add(this, "propertychange._change", function(a) {
          "checked" === a.originalEvent.propertyName && (this._justChanged = !0)
        }), n.event.add(this, "click._change", function(a) {
          this._justChanged && !a.isTrigger && (this._justChanged = !1), n.event.simulate("change", this, a)
        })), !1) : void n.event.add(this, "beforeactivate._change", function(a) {
          var b = a.target;
          ka.test(b.nodeName) && !n._data(b, "change") && (n.event.add(b, "change._change", function(a) {
            !this.parentNode || a.isSimulated || a.isTrigger || n.event.simulate("change", this.parentNode, a)
          }), n._data(b, "change", !0))
        })
      },
      handle: function(a) {
        var b = a.target;
        return this !== b || a.isSimulated || a.isTrigger || "radio" !== b.type && "checkbox" !== b.type ? a.handleObj.handler.apply(this, arguments) : void 0
      },
      teardown: function() {
        return n.event.remove(this, "._change"), !ka.test(this.nodeName)
      }
    }), l.focusin || n.each({
      focus: "focusin",
      blur: "focusout"
    }, function(a, b) {
      var c = function(a) {
        n.event.simulate(b, a.target, n.event.fix(a))
      };
      n.event.special[b] = {
        setup: function() {
          var d = this.ownerDocument || this,
            e = n._data(d, b);
          e || d.addEventListener(a, c, !0), n._data(d, b, (e || 0) + 1)
        },
        teardown: function() {
          var d = this.ownerDocument || this,
            e = n._data(d, b) - 1;
          e ? n._data(d, b, e) : (d.removeEventListener(a, c, !0), n._removeData(d, b))
        }
      }
    }), n.fn.extend({
      on: function(a, b, c, d) {
        return sa(this, a, b, c, d)
      },
      one: function(a, b, c, d) {
        return sa(this, a, b, c, d, 1)
      },
      off: function(a, b, c) {
        var d, e;
        if (a && a.preventDefault && a.handleObj) return d = a.handleObj, n(a.delegateTarget).off(d.namespace ? d.origType + "." + d.namespace : d.origType, d.selector, d.handler), this;
        if ("object" == typeof a) {
          for (e in a) this.off(e, b, a[e]);
          return this
        }
        return b !== !1 && "function" != typeof b || (c = b, b = void 0), c === !1 && (c = qa), this.each(function() {
          n.event.remove(this, a, c, b)
        })
      },
      trigger: function(a, b) {
        return this.each(function() {
          n.event.trigger(a, b, this)
        })
      },
      triggerHandler: function(a, b) {
        var c = this[0];
        return c ? n.event.trigger(a, b, c, !0) : void 0
      }
    });
    var ta = / jQuery\d+="(?:null|\d+)"/g,
      ua = new RegExp("<(?:" + ba + ")[\\s/>]", "i"),
      va = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,
      wa = /<script|<style|<link/i,
      xa = /checked\s*(?:[^=]|=\s*.checked.)/i,
      ya = /^true\/(.*)/,
      za = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
      Aa = ca(d),
      Ba = Aa.appendChild(d.createElement("div"));
    n.extend({
      htmlPrefilter: function(a) {
        return a.replace(va, "<$1></$2>")
      },
      clone: function(a, b, c) {
        var d, e, f, g, h, i = n.contains(a.ownerDocument, a);
        if (l.html5Clone || n.isXMLDoc(a) || !ua.test("<" + a.nodeName + ">") ? f = a.cloneNode(!0) : (Ba.innerHTML = a.outerHTML, Ba.removeChild(f = Ba.firstChild)), !(l.noCloneEvent && l.noCloneChecked || 1 !== a.nodeType && 11 !== a.nodeType || n.isXMLDoc(a)))
          for (d = ea(f), h = ea(a), g = 0; null != (e = h[g]); ++g) d[g] && Ga(e, d[g]);
        if (b)
          if (c)
            for (h = h || ea(a), d = d || ea(f), g = 0; null != (e = h[g]); g++) Fa(e, d[g]);
          else Fa(a, f);
        return d = ea(f, "script"), d.length > 0 && fa(d, !i && ea(a, "script")), d = h = e = null, f
      },
      cleanData: function(a, b) {
        for (var d, e, f, g, h = 0, i = n.expando, j = n.cache, k = l.attributes, m = n.event.special; null != (d = a[h]); h++)
          if ((b || M(d)) && (f = d[i], g = f && j[f])) {
            if (g.events)
              for (e in g.events) m[e] ? n.event.remove(d, e) : n.removeEvent(d, e, g.handle);
            j[f] && (delete j[f], k || "undefined" == typeof d.removeAttribute ? d[i] = void 0 : d.removeAttribute(i), c.push(f))
          }
      }
    }), n.fn.extend({
      domManip: Ha,
      detach: function(a) {
        return Ia(this, a, !0)
      },
      remove: function(a) {
        return Ia(this, a)
      },
      text: function(a) {
        return Y(this, function(a) {
          return void 0 === a ? n.text(this) : this.empty().append((this[0] && this[0].ownerDocument || d).createTextNode(a))
        }, null, a, arguments.length)
      },
      append: function() {
        return Ha(this, arguments, function(a) {
          if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
            var b = Ca(this, a);
            b.appendChild(a)
          }
        })
      },
      prepend: function() {
        return Ha(this, arguments, function(a) {
          if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
            var b = Ca(this, a);
            b.insertBefore(a, b.firstChild)
          }
        })
      },
      before: function() {
        return Ha(this, arguments, function(a) {
          this.parentNode && this.parentNode.insertBefore(a, this)
        })
      },
      after: function() {
        return Ha(this, arguments, function(a) {
          this.parentNode && this.parentNode.insertBefore(a, this.nextSibling)
        })
      },
      empty: function() {
        for (var a, b = 0; null != (a = this[b]); b++) {
          for (1 === a.nodeType && n.cleanData(ea(a, !1)); a.firstChild;) a.removeChild(a.firstChild);
          a.options && n.nodeName(a, "select") && (a.options.length = 0)
        }
        return this
      },
      clone: function(a, b) {
        return a = null == a ? !1 : a, b = null == b ? a : b, this.map(function() {
          return n.clone(this, a, b)
        })
      },
      html: function(a) {
        return Y(this, function(a) {
          var b = this[0] || {},
            c = 0,
            d = this.length;
          if (void 0 === a) return 1 === b.nodeType ? b.innerHTML.replace(ta, "") : void 0;
          if (!("string" != typeof a || wa.test(a) || !l.htmlSerialize && ua.test(a) || !l.leadingWhitespace && aa.test(a) || da[($.exec(a) || ["", ""])[1].toLowerCase()])) {
            a = n.htmlPrefilter(a);
            try {
              for (; d > c; c++) b = this[c] || {}, 1 === b.nodeType && (n.cleanData(ea(b, !1)), b.innerHTML = a);
              b = 0
            } catch (e) {}
          }
          b && this.empty().append(a)
        }, null, a, arguments.length)
      },
      replaceWith: function() {
        var a = [];
        return Ha(this, arguments, function(b) {
          var c = this.parentNode;
          n.inArray(this, a) < 0 && (n.cleanData(ea(this)), c && c.replaceChild(b, this))
        }, a)
      }
    }), n.each({
      appendTo: "append",
      prependTo: "prepend",
      insertBefore: "before",
      insertAfter: "after",
      replaceAll: "replaceWith"
    }, function(a, b) {
      n.fn[a] = function(a) {
        for (var c, d = 0, e = [], f = n(a), h = f.length - 1; h >= d; d++) c = d === h ? this : this.clone(!0), n(f[d])[b](c), g.apply(e, c.get());
        return this.pushStack(e)
      }
    });
    var Ja, Ka = {
        HTML: "block",
        BODY: "block"
      },
      Na = /^margin/,
      Oa = new RegExp("^(" + T + ")(?!px)[a-z%]+$", "i"),
      Pa = function(a, b, c, d) {
        var e, f, g = {};
        for (f in b) g[f] = a.style[f], a.style[f] = b[f];
        e = c.apply(a, d || []);
        for (f in b) a.style[f] = g[f];
        return e
      },
      Qa = d.documentElement;
    ! function() {
      function k() {
        var k, l, m = d.documentElement;
        m.appendChild(i), j.style.cssText = "-webkit-box-sizing:border-box;box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", b = e = h = !1, c = g = !0, a.getComputedStyle && (l = a.getComputedStyle(j), b = "1%" !== (l || {}).top, h = "2px" === (l || {}).marginLeft, e = "4px" === (l || {
          width: "4px"
        }).width, j.style.marginRight = "50%", c = "4px" === (l || {
          marginRight: "4px"
        }).marginRight, k = j.appendChild(d.createElement("div")), k.style.cssText = j.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", k.style.marginRight = k.style.width = "0", j.style.width = "1px", g = !parseFloat((a.getComputedStyle(k) || {}).marginRight), j.removeChild(k)), j.style.display = "none", f = 0 === j.getClientRects().length, f && (j.style.display = "", j.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", k = j.getElementsByTagName("td"), k[0].style.cssText = "margin:0;border:0;padding:0;display:none", f = 0 === k[0].offsetHeight, f && (k[0].style.display = "", k[1].style.display = "none", f = 0 === k[0].offsetHeight)), m.removeChild(i)
      }
      var b, c, e, f, g, h, i = d.createElement("div"),
        j = d.createElement("div");
      j.style && (j.style.cssText = "float:left;opacity:.5", l.opacity = "0.5" === j.style.opacity, l.cssFloat = !!j.style.cssFloat, j.style.backgroundClip = "content-box", j.cloneNode(!0).style.backgroundClip = "", l.clearCloneStyle = "content-box" === j.style.backgroundClip, i = d.createElement("div"), i.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", j.innerHTML = "", i.appendChild(j), l.boxSizing = "" === j.style.boxSizing || "" === j.style.MozBoxSizing || "" === j.style.WebkitBoxSizing, n.extend(l, {
        reliableHiddenOffsets: function() {
          return null == b && k(), f
        },
        boxSizingReliable: function() {
          return null == b && k(), e
        },
        pixelMarginRight: function() {
          return null == b && k(), c
        },
        pixelPosition: function() {
          return null == b && k(), b
        },
        reliableMarginRight: function() {
          return null == b && k(), g
        },
        reliableMarginLeft: function() {
          return null == b && k(), h
        }
      }))
    }();
    var Ra, Sa, Ta = /^(top|right|bottom|left)$/;
    a.getComputedStyle ? (Ra = function(b) {
      var c = b.ownerDocument.defaultView;
      return c && c.opener || (c = a), c.getComputedStyle(b)
    }, Sa = function(a, b, c) {
      var d, e, f, g, h = a.style;
      return c = c || Ra(a), g = c ? c.getPropertyValue(b) || c[b] : void 0, "" !== g && void 0 !== g || n.contains(a.ownerDocument, a) || (g = n.style(a, b)), c && !l.pixelMarginRight() && Oa.test(g) && Na.test(b) && (d = h.width, e = h.minWidth, f = h.maxWidth, h.minWidth = h.maxWidth = h.width = g, g = c.width, h.width = d, h.minWidth = e, h.maxWidth = f), void 0 === g ? g : g + ""
    }) : Qa.currentStyle && (Ra = function(a) {
      return a.currentStyle
    }, Sa = function(a, b, c) {
      var d, e, f, g, h = a.style;
      return c = c || Ra(a), g = c ? c[b] : void 0, null == g && h && h[b] && (g = h[b]), Oa.test(g) && !Ta.test(b) && (d = h.left, e = a.runtimeStyle, f = e && e.left, f && (e.left = a.currentStyle.left), h.left = "fontSize" === b ? "1em" : g, g = h.pixelLeft + "px", h.left = d, f && (e.left = f)), void 0 === g ? g : g + "" || "auto"
    });
    var Va = /alpha\([^)]*\)/i,
      Wa = /opacity\s*=\s*([^)]*)/i,
      Xa = /^(none|table(?!-c[ea]).+)/,
      Ya = new RegExp("^(" + T + ")(.*)$", "i"),
      Za = {
        position: "absolute",
        visibility: "hidden",
        display: "block"
      },
      $a = {
        letterSpacing: "0",
        fontWeight: "400"
      },
      _a = ["Webkit", "O", "Moz", "ms"],
      ab = d.createElement("div").style;
    n.extend({
      cssHooks: {
        opacity: {
          get: function(a, b) {
            if (b) {
              var c = Sa(a, "opacity");
              return "" === c ? "1" : c
            }
          }
        }
      },
      cssNumber: {
        animationIterationCount: !0,
        columnCount: !0,
        fillOpacity: !0,
        flexGrow: !0,
        flexShrink: !0,
        fontWeight: !0,
        lineHeight: !0,
        opacity: !0,
        order: !0,
        orphans: !0,
        widows: !0,
        zIndex: !0,
        zoom: !0
      },
      cssProps: {
        "float": l.cssFloat ? "cssFloat" : "styleFloat"
      },
      style: function(a, b, c, d) {
        if (a && 3 !== a.nodeType && 8 !== a.nodeType && a.style) {
          var e, f, g, h = n.camelCase(b),
            i = a.style;
          if (b = n.cssProps[h] || (n.cssProps[h] = bb(h) || h), g = n.cssHooks[b] || n.cssHooks[h], void 0 === c) return g && "get" in g && void 0 !== (e = g.get(a, !1, d)) ? e : i[b];
          if (f = typeof c, "string" === f && (e = U.exec(c)) && e[1] && (c = X(a, b, e), f = "number"), null != c && c === c && ("number" === f && (c += e && e[3] || (n.cssNumber[h] ? "" : "px")), l.clearCloneStyle || "" !== c || 0 !== b.indexOf("background") || (i[b] = "inherit"), !(g && "set" in g && void 0 === (c = g.set(a, c, d))))) try {
            i[b] = c
          } catch (j) {}
        }
      },
      css: function(a, b, c, d) {
        var e, f, g, h = n.camelCase(b);
        return b = n.cssProps[h] || (n.cssProps[h] = bb(h) || h), g = n.cssHooks[b] || n.cssHooks[h], g && "get" in g && (f = g.get(a, !0, c)), void 0 === f && (f = Sa(a, b, d)), "normal" === f && b in $a && (f = $a[b]), "" === c || c ? (e = parseFloat(f), c === !0 || isFinite(e) ? e || 0 : f) : f
      }
    }), n.each(["height", "width"], function(a, b) {
      n.cssHooks[b] = {
        get: function(a, c, d) {
          return c ? Xa.test(n.css(a, "display")) && 0 === a.offsetWidth ? Pa(a, Za, function() {
            return fb(a, b, d)
          }) : fb(a, b, d) : void 0
        },
        set: function(a, c, d) {
          var e = d && Ra(a);
          return db(a, c, d ? eb(a, b, d, l.boxSizing && "border-box" === n.css(a, "boxSizing", !1, e), e) : 0)
        }
      }
    }), l.opacity || (n.cssHooks.opacity = {
      get: function(a, b) {
        return Wa.test((b && a.currentStyle ? a.currentStyle.filter : a.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : b ? "1" : ""
      },
      set: function(a, b) {
        var c = a.style,
          d = a.currentStyle,
          e = n.isNumeric(b) ? "alpha(opacity=" + 100 * b + ")" : "",
          f = d && d.filter || c.filter || "";
        c.zoom = 1, (b >= 1 || "" === b) && "" === n.trim(f.replace(Va, "")) && c.removeAttribute && (c.removeAttribute("filter"), "" === b || d && !d.filter) || (c.filter = Va.test(f) ? f.replace(Va, e) : f + " " + e)
      }
    }), n.cssHooks.marginRight = Ua(l.reliableMarginRight, function(a, b) {
      return b ? Pa(a, {
        display: "inline-block"
      }, Sa, [a, "marginRight"]) : void 0
    }), n.cssHooks.marginLeft = Ua(l.reliableMarginLeft, function(a, b) {
      return b ? (parseFloat(Sa(a, "marginLeft")) || (n.contains(a.ownerDocument, a) ? a.getBoundingClientRect().left - Pa(a, {
        marginLeft: 0
      }, function() {
        return a.getBoundingClientRect().left
      }) : 0)) + "px" : void 0
    }), n.each({
      margin: "",
      padding: "",
      border: "Width"
    }, function(a, b) {
      n.cssHooks[a + b] = {
        expand: function(c) {
          for (var d = 0, e = {}, f = "string" == typeof c ? c.split(" ") : [c]; 4 > d; d++) e[a + V[d] + b] = f[d] || f[d - 2] || f[0];
          return e
        }
      }, Na.test(a) || (n.cssHooks[a + b].set = db)
    }), n.fn.extend({
      css: function(a, b) {
        return Y(this, function(a, b, c) {
          var d, e, f = {},
            g = 0;
          if (n.isArray(b)) {
            for (d = Ra(a), e = b.length; e > g; g++) f[b[g]] = n.css(a, b[g], !1, d);
            return f
          }
          return void 0 !== c ? n.style(a, b, c) : n.css(a, b)
        }, a, b, arguments.length > 1)
      },
      show: function() {
        return cb(this, !0)
      },
      hide: function() {
        return cb(this)
      },
      toggle: function(a) {
        return "boolean" == typeof a ? a ? this.show() : this.hide() : this.each(function() {
          W(this) ? n(this).show() : n(this).hide()
        })
      }
    }), n.Tween = gb, gb.prototype = {
      constructor: gb,
      init: function(a, b, c, d, e, f) {
        this.elem = a, this.prop = c, this.easing = e || n.easing._default, this.options = b, this.start = this.now = this.cur(), this.end = d, this.unit = f || (n.cssNumber[c] ? "" : "px")
      },
      cur: function() {
        var a = gb.propHooks[this.prop];
        return a && a.get ? a.get(this) : gb.propHooks._default.get(this)
      },
      run: function(a) {
        var b, c = gb.propHooks[this.prop];
        return this.options.duration ? this.pos = b = n.easing[this.easing](a, this.options.duration * a, 0, 1, this.options.duration) : this.pos = b = a, this.now = (this.end - this.start) * b + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), c && c.set ? c.set(this) : gb.propHooks._default.set(this), this
      }
    }, gb.prototype.init.prototype = gb.prototype, gb.propHooks = {
      _default: {
        get: function(a) {
          var b;
          return 1 !== a.elem.nodeType || null != a.elem[a.prop] && null == a.elem.style[a.prop] ? a.elem[a.prop] : (b = n.css(a.elem, a.prop, ""), b && "auto" !== b ? b : 0)
        },
        set: function(a) {
          n.fx.step[a.prop] ? n.fx.step[a.prop](a) : 1 !== a.elem.nodeType || null == a.elem.style[n.cssProps[a.prop]] && !n.cssHooks[a.prop] ? a.elem[a.prop] = a.now : n.style(a.elem, a.prop, a.now + a.unit)
        }
      }
    }, gb.propHooks.scrollTop = gb.propHooks.scrollLeft = {
      set: function(a) {
        a.elem.nodeType && a.elem.parentNode && (a.elem[a.prop] = a.now)
      }
    }, n.easing = {
      linear: function(a) {
        return a
      },
      swing: function(a) {
        return .5 - Math.cos(a * Math.PI) / 2
      },
      _default: "swing"
    }, n.fx = gb.prototype.init, n.fx.step = {};
    var hb, ib, jb = /^(?:toggle|show|hide)$/,
      kb = /queueHooks$/;
    n.Animation = n.extend(qb, {
        tweeners: {
          "*": [function(a, b) {
            var c = this.createTween(a, b);
            return X(c.elem, a, U.exec(b), c), c
          }]
        },
        tweener: function(a, b) {
          n.isFunction(a) ? (b = a, a = ["*"]) : a = a.match(G);
          for (var c, d = 0, e = a.length; e > d; d++) c = a[d], qb.tweeners[c] = qb.tweeners[c] || [], qb.tweeners[c].unshift(b)
        },
        prefilters: [ob],
        prefilter: function(a, b) {
          b ? qb.prefilters.unshift(a) : qb.prefilters.push(a)
        }
      }), n.speed = function(a, b, c) {
        var d = a && "object" == typeof a ? n.extend({}, a) : {
          complete: c || !c && b || n.isFunction(a) && a,
          duration: a,
          easing: c && b || b && !n.isFunction(b) && b
        };
        return d.duration = n.fx.off ? 0 : "number" == typeof d.duration ? d.duration : d.duration in n.fx.speeds ? n.fx.speeds[d.duration] : n.fx.speeds._default, null != d.queue && d.queue !== !0 || (d.queue = "fx"), d.old = d.complete, d.complete = function() {
          n.isFunction(d.old) && d.old.call(this), d.queue && n.dequeue(this, d.queue)
        }, d
      }, n.fn.extend({
        fadeTo: function(a, b, c, d) {
          return this.filter(W).css("opacity", 0).show().end().animate({
            opacity: b
          }, a, c, d)
        },
        animate: function(a, b, c, d) {
          var e = n.isEmptyObject(a),
            f = n.speed(b, c, d),
            g = function() {
              var b = qb(this, n.extend({}, a), f);
              (e || n._data(this, "finish")) && b.stop(!0)
            };
          return g.finish = g, e || f.queue === !1 ? this.each(g) : this.queue(f.queue, g)
        },
        stop: function(a, b, c) {
          var d = function(a) {
            var b = a.stop;
            delete a.stop, b(c)
          };
          return "string" != typeof a && (c = b, b = a, a = void 0), b && a !== !1 && this.queue(a || "fx", []), this.each(function() {
            var b = !0,
              e = null != a && a + "queueHooks",
              f = n.timers,
              g = n._data(this);
            if (e) g[e] && g[e].stop && d(g[e]);
            else
              for (e in g) g[e] && g[e].stop && kb.test(e) && d(g[e]);
            for (e = f.length; e--;) f[e].elem !== this || null != a && f[e].queue !== a || (f[e].anim.stop(c), b = !1, f.splice(e, 1));
            !b && c || n.dequeue(this, a)
          })
        },
        finish: function(a) {
          return a !== !1 && (a = a || "fx"), this.each(function() {
            var b, c = n._data(this),
              d = c[a + "queue"],
              e = c[a + "queueHooks"],
              f = n.timers,
              g = d ? d.length : 0;
            for (c.finish = !0, n.queue(this, a, []), e && e.stop && e.stop.call(this, !0), b = f.length; b--;) f[b].elem === this && f[b].queue === a && (f[b].anim.stop(!0), f.splice(b, 1));
            for (b = 0; g > b; b++) d[b] && d[b].finish && d[b].finish.call(this);
            delete c.finish
          })
        }
      }), n.each(["toggle", "show", "hide"], function(a, b) {
        var c = n.fn[b];
        n.fn[b] = function(a, d, e) {
          return null == a || "boolean" == typeof a ? c.apply(this, arguments) : this.animate(mb(b, !0), a, d, e)
        }
      }), n.each({
        slideDown: mb("show"),
        slideUp: mb("hide"),
        slideToggle: mb("toggle"),
        fadeIn: {
          opacity: "show"
        },
        fadeOut: {
          opacity: "hide"
        },
        fadeToggle: {
          opacity: "toggle"
        }
      }, function(a, b) {
        n.fn[a] = function(a, c, d) {
          return this.animate(b, a, c, d)
        }
      }), n.timers = [], n.fx.tick = function() {
        var a, b = n.timers,
          c = 0;
        for (hb = n.now(); c < b.length; c++) a = b[c], a() || b[c] !== a || b.splice(c--, 1);
        b.length || n.fx.stop(), hb = void 0
      }, n.fx.timer = function(a) {
        n.timers.push(a), a() ? n.fx.start() : n.timers.pop()
      }, n.fx.interval = 13, n.fx.start = function() {
        ib || (ib = a.setInterval(n.fx.tick, n.fx.interval))
      }, n.fx.stop = function() {
        a.clearInterval(ib), ib = null
      }, n.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
      }, n.fn.delay = function(b, c) {
        return b = n.fx ? n.fx.speeds[b] || b : b, c = c || "fx", this.queue(c, function(c, d) {
          var e = a.setTimeout(c, b);
          d.stop = function() {
            a.clearTimeout(e)
          }
        })
      },
      function() {
        var a, b = d.createElement("input"),
          c = d.createElement("div"),
          e = d.createElement("select"),
          f = e.appendChild(d.createElement("option"));
        c = d.createElement("div"), c.setAttribute("className", "t"), c.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", a = c.getElementsByTagName("a")[0], b.setAttribute("type", "checkbox"), c.appendChild(b), a = c.getElementsByTagName("a")[0], a.style.cssText = "top:1px", l.getSetAttribute = "t" !== c.className, l.style = /top/.test(a.getAttribute("style")), l.hrefNormalized = "/a" === a.getAttribute("href"), l.checkOn = !!b.value, l.optSelected = f.selected, l.enctype = !!d.createElement("form").enctype, e.disabled = !0, l.optDisabled = !f.disabled, b = d.createElement("input"), b.setAttribute("value", ""), l.input = "" === b.getAttribute("value"), b.value = "t", b.setAttribute("type", "radio"), l.radioValue = "t" === b.value
      }();
    var rb = /\r/g,
      sb = /[\x20\t\r\n\f]+/g;
    n.fn.extend({
      val: function(a) {
        var b, c, d, e = this[0];
        return arguments.length ? (d = n.isFunction(a), this.each(function(c) {
          var e;
          1 === this.nodeType && (e = d ? a.call(this, c, n(this).val()) : a, null == e ? e = "" : "number" == typeof e ? e += "" : n.isArray(e) && (e = n.map(e, function(a) {
            return null == a ? "" : a + ""
          })), b = n.valHooks[this.type] || n.valHooks[this.nodeName.toLowerCase()], b && "set" in b && void 0 !== b.set(this, e, "value") || (this.value = e))
        })) : e ? (b = n.valHooks[e.type] || n.valHooks[e.nodeName.toLowerCase()], b && "get" in b && void 0 !== (c = b.get(e, "value")) ? c : (c = e.value, "string" == typeof c ? c.replace(rb, "") : null == c ? "" : c)) : void 0
      }
    }), n.extend({
      valHooks: {
        option: {
          get: function(a) {
            var b = n.find.attr(a, "value");
            return null != b ? b : n.trim(n.text(a)).replace(sb, " ")
          }
        },
        select: {
          get: function(a) {
            for (var b, c, d = a.options, e = a.selectedIndex, f = "select-one" === a.type || 0 > e, g = f ? null : [], h = f ? e + 1 : d.length, i = 0 > e ? h : f ? e : 0; h > i; i++)
              if (c = d[i], !(!c.selected && i !== e || (l.optDisabled ? c.disabled : null !== c.getAttribute("disabled")) || c.parentNode.disabled && n.nodeName(c.parentNode, "optgroup"))) {
                if (b = n(c).val(), f) return b;
                g.push(b)
              } return g
          },
          set: function(a, b) {
            for (var c, d, e = a.options, f = n.makeArray(b), g = e.length; g--;)
              if (d = e[g], n.inArray(n.valHooks.option.get(d), f) > -1) try {
                d.selected = c = !0
              } catch (h) {
                d.scrollHeight
              } else d.selected = !1;
            return c || (a.selectedIndex = -1), e
          }
        }
      }
    }), n.each(["radio", "checkbox"], function() {
      n.valHooks[this] = {
        set: function(a, b) {
          return n.isArray(b) ? a.checked = n.inArray(n(a).val(), b) > -1 : void 0
        }
      }, l.checkOn || (n.valHooks[this].get = function(a) {
        return null === a.getAttribute("value") ? "on" : a.value
      })
    });
    var tb, ub, vb = n.expr.attrHandle,
      wb = /^(?:checked|selected)$/i,
      xb = l.getSetAttribute,
      yb = l.input;
    n.fn.extend({
      attr: function(a, b) {
        return Y(this, n.attr, a, b, arguments.length > 1)
      },
      removeAttr: function(a) {
        return this.each(function() {
          n.removeAttr(this, a)
        })
      }
    }), n.extend({
      attr: function(a, b, c) {
        var d, e, f = a.nodeType;
        return 3 !== f && 8 !== f && 2 !== f ? "undefined" == typeof a.getAttribute ? n.prop(a, b, c) : (1 === f && n.isXMLDoc(a) || (b = b.toLowerCase(), e = n.attrHooks[b] || (n.expr.match.bool.test(b) ? ub : tb)), void 0 !== c ? null === c ? void n.removeAttr(a, b) : e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : (a.setAttribute(b, c + ""), c) : e && "get" in e && null !== (d = e.get(a, b)) ? d : (d = n.find.attr(a, b), null == d ? void 0 : d)) : void 0
      },
      attrHooks: {
        type: {
          set: function(a, b) {
            if (!l.radioValue && "radio" === b && n.nodeName(a, "input")) {
              var c = a.value;
              return a.setAttribute("type", b), c && (a.value = c), b
            }
          }
        }
      },
      removeAttr: function(a, b) {
        var c, d, e = 0,
          f = b && b.match(G);
        if (f && 1 === a.nodeType)
          for (; c = f[e++];) d = n.propFix[c] || c, n.expr.match.bool.test(c) ? yb && xb || !wb.test(c) ? a[d] = !1 : a[n.camelCase("default-" + c)] = a[d] = !1 : n.attr(a, c, ""), a.removeAttribute(xb ? c : d)
      }
    }), ub = {
      set: function(a, b, c) {
        return b === !1 ? n.removeAttr(a, c) : yb && xb || !wb.test(c) ? a.setAttribute(!xb && n.propFix[c] || c, c) : a[n.camelCase("default-" + c)] = a[c] = !0, c
      }
    }, n.each(n.expr.match.bool.source.match(/\w+/g), function(a, b) {
      var c = vb[b] || n.find.attr;
      yb && xb || !wb.test(b) ? vb[b] = function(a, b, d) {
        var e, f;
        return d || (f = vb[b], vb[b] = e, e = null != c(a, b, d) ? b.toLowerCase() : null, vb[b] = f), e
      } : vb[b] = function(a, b, c) {
        return c ? void 0 : a[n.camelCase("default-" + b)] ? b.toLowerCase() : null
      }
    }), yb && xb || (n.attrHooks.value = {
      set: function(a, b, c) {
        return n.nodeName(a, "input") ? void(a.defaultValue = b) : tb && tb.set(a, b, c)
      }
    }), xb || (tb = {
      set: function(a, b, c) {
        var d = a.getAttributeNode(c);
        return d || a.setAttributeNode(d = a.ownerDocument.createAttribute(c)), d.value = b += "", "value" === c || b === a.getAttribute(c) ? b : void 0
      }
    }, vb.id = vb.name = vb.coords = function(a, b, c) {
      var d;
      return c ? void 0 : (d = a.getAttributeNode(b)) && "" !== d.value ? d.value : null
    }, n.valHooks.button = {
      get: function(a, b) {
        var c = a.getAttributeNode(b);
        return c && c.specified ? c.value : void 0
      },
      set: tb.set
    }, n.attrHooks.contenteditable = {
      set: function(a, b, c) {
        tb.set(a, "" === b ? !1 : b, c)
      }
    }, n.each(["width", "height"], function(a, b) {
      n.attrHooks[b] = {
        set: function(a, c) {
          return "" === c ? (a.setAttribute(b, "auto"), c) : void 0
        }
      }
    })), l.style || (n.attrHooks.style = {
      get: function(a) {
        return a.style.cssText || void 0
      },
      set: function(a, b) {
        return a.style.cssText = b + ""
      }
    });
    var zb = /^(?:input|select|textarea|button|object)$/i,
      Ab = /^(?:a|area)$/i;
    n.fn.extend({
      prop: function(a, b) {
        return Y(this, n.prop, a, b, arguments.length > 1)
      },
      removeProp: function(a) {
        return a = n.propFix[a] || a, this.each(function() {
          try {
            this[a] = void 0, delete this[a]
          } catch (b) {}
        })
      }
    }), n.extend({
      prop: function(a, b, c) {
        var d, e, f = a.nodeType;
        return 3 !== f && 8 !== f && 2 !== f ? (1 === f && n.isXMLDoc(a) || (b = n.propFix[b] || b, e = n.propHooks[b]), void 0 !== c ? e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : a[b] = c : e && "get" in e && null !== (d = e.get(a, b)) ? d : a[b]) : void 0
      },
      propHooks: {
        tabIndex: {
          get: function(a) {
            var b = n.find.attr(a, "tabindex");
            return b ? parseInt(b, 10) : zb.test(a.nodeName) || Ab.test(a.nodeName) && a.href ? 0 : -1
          }
        }
      },
      propFix: {
        "for": "htmlFor",
        "class": "className"
      }
    }), l.hrefNormalized || n.each(["href", "src"], function(a, b) {
      n.propHooks[b] = {
        get: function(a) {
          return a.getAttribute(b, 4)
        }
      }
    }), l.optSelected || (n.propHooks.selected = {
      get: function(a) {
        var b = a.parentNode;
        return b && (b.selectedIndex, b.parentNode && b.parentNode.selectedIndex), null
      },
      set: function(a) {
        var b = a.parentNode;
        b && (b.selectedIndex, b.parentNode && b.parentNode.selectedIndex)
      }
    }), n.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
      n.propFix[this.toLowerCase()] = this
    }), l.enctype || (n.propFix.enctype = "encoding");
    var Bb = /[\t\r\n\f]/g;
    n.fn.extend({
      addClass: function(a) {
        var b, c, d, e, f, g, h, i = 0;
        if (n.isFunction(a)) return this.each(function(b) {
          n(this).addClass(a.call(this, b, Cb(this)))
        });
        if ("string" == typeof a && a)
          for (b = a.match(G) || []; c = this[i++];)
            if (e = Cb(c), d = 1 === c.nodeType && (" " + e + " ").replace(Bb, " ")) {
              for (g = 0; f = b[g++];) d.indexOf(" " + f + " ") < 0 && (d += f + " ");
              h = n.trim(d), e !== h && n.attr(c, "class", h)
            } return this
      },
      removeClass: function(a) {
        var b, c, d, e, f, g, h, i = 0;
        if (n.isFunction(a)) return this.each(function(b) {
          n(this).removeClass(a.call(this, b, Cb(this)))
        });
        if (!arguments.length) return this.attr("class", "");
        if ("string" == typeof a && a)
          for (b = a.match(G) || []; c = this[i++];)
            if (e = Cb(c), d = 1 === c.nodeType && (" " + e + " ").replace(Bb, " ")) {
              for (g = 0; f = b[g++];)
                for (; d.indexOf(" " + f + " ") > -1;) d = d.replace(" " + f + " ", " ");
              h = n.trim(d), e !== h && n.attr(c, "class", h)
            } return this
      },
      toggleClass: function(a, b) {
        var c = typeof a;
        return "boolean" == typeof b && "string" === c ? b ? this.addClass(a) : this.removeClass(a) : n.isFunction(a) ? this.each(function(c) {
          n(this).toggleClass(a.call(this, c, Cb(this), b), b)
        }) : this.each(function() {
          var b, d, e, f;
          if ("string" === c)
            for (d = 0, e = n(this), f = a.match(G) || []; b = f[d++];) e.hasClass(b) ? e.removeClass(b) : e.addClass(b);
          else void 0 !== a && "boolean" !== c || (b = Cb(this), b && n._data(this, "__className__", b), n.attr(this, "class", b || a === !1 ? "" : n._data(this, "__className__") || ""))
        })
      },
      hasClass: function(a) {
        var b, c, d = 0;
        for (b = " " + a + " "; c = this[d++];)
          if (1 === c.nodeType && (" " + Cb(c) + " ").replace(Bb, " ").indexOf(b) > -1) return !0;
        return !1
      }
    }), n.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(a, b) {
      n.fn[b] = function(a, c) {
        return arguments.length > 0 ? this.on(b, null, a, c) : this.trigger(b)
      }
    }), n.fn.extend({
      hover: function(a, b) {
        return this.mouseenter(a).mouseleave(b || a)
      }
    });
    var Db = a.location,
      Eb = n.now(),
      Fb = /\?/,
      Gb = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
    n.parseJSON = function(b) {
      if (a.JSON && a.JSON.parse) return a.JSON.parse(b + "");
      var c, d = null,
        e = n.trim(b + "");
      return e && !n.trim(e.replace(Gb, function(a, b, e, f) {
        return c && b && (d = 0), 0 === d ? a : (c = e || b, d += !f - !e, "")
      })) ? Function("return " + e)() : n.error("Invalid JSON: " + b)
    }, n.parseXML = function(b) {
      var c, d;
      if (!b || "string" != typeof b) return null;
      try {
        a.DOMParser ? (d = new a.DOMParser, c = d.parseFromString(b, "text/xml")) : (c = new a.ActiveXObject("Microsoft.XMLDOM"), c.async = "false", c.loadXML(b))
      } catch (e) {
        c = void 0
      }
      return c && c.documentElement && !c.getElementsByTagName("parsererror").length || n.error("Invalid XML: " + b), c
    };
    var Hb = /#.*$/,
      Ib = /([?&])_=[^&]*/,
      Jb = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
      Kb = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
      Lb = /^(?:GET|HEAD)$/,
      Mb = /^\/\//,
      Nb = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,
      Ob = {},
      Pb = {},
      Qb = "*/".concat("*"),
      Rb = Db.href,
      Sb = Nb.exec(Rb.toLowerCase()) || [];
    n.extend({
      active: 0,
      lastModified: {},
      etag: {},
      ajaxSettings: {
        url: Rb,
        type: "GET",
        isLocal: Kb.test(Sb[1]),
        global: !0,
        processData: !0,
        async: !0,
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        accepts: {
          "*": Qb,
          text: "text/plain",
          html: "text/html",
          xml: "application/xml, text/xml",
          json: "application/json, text/javascript"
        },
        contents: {
          xml: /\bxml\b/,
          html: /\bhtml/,
          json: /\bjson\b/
        },
        responseFields: {
          xml: "responseXML",
          text: "responseText",
          json: "responseJSON"
        },
        converters: {
          "* text": String,
          "text html": !0,
          "text json": n.parseJSON,
          "text xml": n.parseXML
        },
        flatOptions: {
          url: !0,
          context: !0
        }
      },
      ajaxSetup: function(a, b) {
        return b ? Vb(Vb(a, n.ajaxSettings), b) : Vb(n.ajaxSettings, a)
      },
      ajaxPrefilter: Tb(Ob),
      ajaxTransport: Tb(Pb),
      ajax: function(b, c) {
        function y(b, c, d, e) {
          var k, s, t, v, x, y = c;
          2 !== u && (u = 2, h && a.clearTimeout(h), j = void 0, g = e || "", w.readyState = b > 0 ? 4 : 0, k = b >= 200 && 300 > b || 304 === b, d && (v = Wb(l, w, d)), v = Xb(l, v, w, k), k ? (l.ifModified && (x = w.getResponseHeader("Last-Modified"), x && (n.lastModified[f] = x), x = w.getResponseHeader("etag"), x && (n.etag[f] = x)), 204 === b || "HEAD" === l.type ? y = "nocontent" : 304 === b ? y = "notmodified" : (y = v.state, s = v.data, t = v.error, k = !t)) : (t = y, !b && y || (y = "error", 0 > b && (b = 0))), w.status = b, w.statusText = (c || y) + "", k ? p.resolveWith(m, [s, y, w]) : p.rejectWith(m, [w, y, t]), w.statusCode(r), r = void 0, i && o.trigger(k ? "ajaxSuccess" : "ajaxError", [w, l, k ? s : t]), q.fireWith(m, [w, y]), i && (o.trigger("ajaxComplete", [w, l]), --n.active || n.event.trigger("ajaxStop")))
        }
        "object" == typeof b && (c = b, b = void 0), c = c || {};
        var d, e, f, g, h, i, j, k, l = n.ajaxSetup({}, c),
          m = l.context || l,
          o = l.context && (m.nodeType || m.jquery) ? n(m) : n.event,
          p = n.Deferred(),
          q = n.Callbacks("once memory"),
          r = l.statusCode || {},
          s = {},
          t = {},
          u = 0,
          v = "canceled",
          w = {
            readyState: 0,
            getResponseHeader: function(a) {
              var b;
              if (2 === u) {
                if (!k)
                  for (k = {}; b = Jb.exec(g);) k[b[1].toLowerCase()] = b[2];
                b = k[a.toLowerCase()]
              }
              return null == b ? null : b
            },
            getAllResponseHeaders: function() {
              return 2 === u ? g : null
            },
            setRequestHeader: function(a, b) {
              var c = a.toLowerCase();
              return u || (a = t[c] = t[c] || a, s[a] = b), this
            },
            overrideMimeType: function(a) {
              return u || (l.mimeType = a), this
            },
            statusCode: function(a) {
              var b;
              if (a)
                if (2 > u)
                  for (b in a) r[b] = [r[b], a[b]];
                else w.always(a[w.status]);
              return this
            },
            abort: function(a) {
              var b = a || v;
              return j && j.abort(b), y(0, b), this
            }
          };
        if (p.promise(w).complete = q.add, w.success = w.done, w.error = w.fail, l.url = ((b || l.url || Rb) + "").replace(Hb, "").replace(Mb, Sb[1] + "//"), l.type = c.method || c.type || l.method || l.type, l.dataTypes = n.trim(l.dataType || "*").toLowerCase().match(G) || [""], null == l.crossDomain && (d = Nb.exec(l.url.toLowerCase()), l.crossDomain = !(!d || d[1] === Sb[1] && d[2] === Sb[2] && (d[3] || ("http:" === d[1] ? "80" : "443")) === (Sb[3] || ("http:" === Sb[1] ? "80" : "443")))), l.data && l.processData && "string" != typeof l.data && (l.data = n.param(l.data, l.traditional)), Ub(Ob, l, c, w), 2 === u) return w;
        i = n.event && l.global, i && 0 === n.active++ && n.event.trigger("ajaxStart"), l.type = l.type.toUpperCase(), l.hasContent = !Lb.test(l.type), f = l.url, l.hasContent || (l.data && (f = l.url += (Fb.test(f) ? "&" : "?") + l.data, delete l.data), l.cache === !1 && (l.url = Ib.test(f) ? f.replace(Ib, "$1_=" + Eb++) : f + (Fb.test(f) ? "&" : "?") + "_=" + Eb++)), l.ifModified && (n.lastModified[f] && w.setRequestHeader("If-Modified-Since", n.lastModified[f]), n.etag[f] && w.setRequestHeader("If-None-Match", n.etag[f])), (l.data && l.hasContent && l.contentType !== !1 || c.contentType) && w.setRequestHeader("Content-Type", l.contentType), w.setRequestHeader("Accept", l.dataTypes[0] && l.accepts[l.dataTypes[0]] ? l.accepts[l.dataTypes[0]] + ("*" !== l.dataTypes[0] ? ", " + Qb + "; q=0.01" : "") : l.accepts["*"]);
        for (e in l.headers) w.setRequestHeader(e, l.headers[e]);
        if (l.beforeSend && (l.beforeSend.call(m, w, l) === !1 || 2 === u)) return w.abort();
        v = "abort";
        for (e in {
            success: 1,
            error: 1,
            complete: 1
          }) w[e](l[e]);
        if (j = Ub(Pb, l, c, w)) {
          if (w.readyState = 1, i && o.trigger("ajaxSend", [w, l]), 2 === u) return w;
          l.async && l.timeout > 0 && (h = a.setTimeout(function() {
            w.abort("timeout")
          }, l.timeout));
          try {
            u = 1, j.send(s, y)
          } catch (x) {
            if (!(2 > u)) throw x;
            y(-1, x)
          }
        } else y(-1, "No Transport");
        return w
      },
      getJSON: function(a, b, c) {
        return n.get(a, b, c, "json")
      },
      getScript: function(a, b) {
        return n.get(a, void 0, b, "script")
      }
    }), n.each(["get", "post"], function(a, b) {
      n[b] = function(a, c, d, e) {
        return n.isFunction(c) && (e = e || d, d = c, c = void 0), n.ajax(n.extend({
          url: a,
          type: b,
          dataType: e,
          data: c,
          success: d
        }, n.isPlainObject(a) && a))
      }
    }), n._evalUrl = function(a) {
      return n.ajax({
        url: a,
        type: "GET",
        dataType: "script",
        cache: !0,
        async: !1,
        global: !1,
        "throws": !0
      })
    }, n.fn.extend({
      wrapAll: function(a) {
        if (n.isFunction(a)) return this.each(function(b) {
          n(this).wrapAll(a.call(this, b))
        });
        if (this[0]) {
          var b = n(a, this[0].ownerDocument).eq(0).clone(!0);
          this[0].parentNode && b.insertBefore(this[0]), b.map(function() {
            for (var a = this; a.firstChild && 1 === a.firstChild.nodeType;) a = a.firstChild;
            return a
          }).append(this)
        }
        return this
      },
      wrapInner: function(a) {
        return n.isFunction(a) ? this.each(function(b) {
          n(this).wrapInner(a.call(this, b))
        }) : this.each(function() {
          var b = n(this),
            c = b.contents();
          c.length ? c.wrapAll(a) : b.append(a)
        })
      },
      wrap: function(a) {
        var b = n.isFunction(a);
        return this.each(function(c) {
          n(this).wrapAll(b ? a.call(this, c) : a)
        })
      },
      unwrap: function() {
        return this.parent().each(function() {
          n.nodeName(this, "body") || n(this).replaceWith(this.childNodes)
        }).end()
      }
    }), n.expr.filters.hidden = function(a) {
      return l.reliableHiddenOffsets() ? a.offsetWidth <= 0 && a.offsetHeight <= 0 && !a.getClientRects().length : Zb(a)
    }, n.expr.filters.visible = function(a) {
      return !n.expr.filters.hidden(a)
    };
    var $b = /%20/g,
      _b = /\[\]$/,
      ac = /\r?\n/g,
      bc = /^(?:submit|button|image|reset|file)$/i,
      cc = /^(?:input|select|textarea|keygen)/i;
    n.param = function(a, b) {
      var c, d = [],
        e = function(a, b) {
          b = n.isFunction(b) ? b() : null == b ? "" : b, d[d.length] = encodeURIComponent(a) + "=" + encodeURIComponent(b)
        };
      if (void 0 === b && (b = n.ajaxSettings && n.ajaxSettings.traditional), n.isArray(a) || a.jquery && !n.isPlainObject(a)) n.each(a, function() {
        e(this.name, this.value)
      });
      else
        for (c in a) dc(c, a[c], b, e);
      return d.join("&").replace($b, "+")
    }, n.fn.extend({
      serialize: function() {
        return n.param(this.serializeArray())
      },
      serializeArray: function() {
        return this.map(function() {
          var a = n.prop(this, "elements");
          return a ? n.makeArray(a) : this
        }).filter(function() {
          var a = this.type;
          return this.name && !n(this).is(":disabled") && cc.test(this.nodeName) && !bc.test(a) && (this.checked || !Z.test(a))
        }).map(function(a, b) {
          var c = n(this).val();
          return null == c ? null : n.isArray(c) ? n.map(c, function(a) {
            return {
              name: b.name,
              value: a.replace(ac, "\r\n")
            }
          }) : {
            name: b.name,
            value: c.replace(ac, "\r\n")
          }
        }).get()
      }
    }), n.ajaxSettings.xhr = void 0 !== a.ActiveXObject ? function() {
      return this.isLocal ? ic() : d.documentMode > 8 ? hc() : /^(get|post|head|put|delete|options)$/i.test(this.type) && hc() || ic()
    } : hc;
    var ec = 0,
      fc = {},
      gc = n.ajaxSettings.xhr();
    a.attachEvent && a.attachEvent("onunload", function() {
      for (var a in fc) fc[a](void 0, !0)
    }), l.cors = !!gc && "withCredentials" in gc, gc = l.ajax = !!gc, gc && n.ajaxTransport(function(b) {
      if (!b.crossDomain || l.cors) {
        var c;
        return {
          send: function(d, e) {
            var f, g = b.xhr(),
              h = ++ec;
            if (g.open(b.type, b.url, b.async, b.username, b.password), b.xhrFields)
              for (f in b.xhrFields) g[f] = b.xhrFields[f];
            b.mimeType && g.overrideMimeType && g.overrideMimeType(b.mimeType), b.crossDomain || d["X-Requested-With"] || (d["X-Requested-With"] = "XMLHttpRequest");
            for (f in d) void 0 !== d[f] && g.setRequestHeader(f, d[f] + "");
            g.send(b.hasContent && b.data || null), c = function(a, d) {
              var f, i, j;
              if (c && (d || 4 === g.readyState))
                if (delete fc[h], c = void 0, g.onreadystatechange = n.noop, d) 4 !== g.readyState && g.abort();
                else {
                  j = {}, f = g.status, "string" == typeof g.responseText && (j.text = g.responseText);
                  try {
                    i = g.statusText
                  } catch (k) {
                    i = ""
                  }
                  f || !b.isLocal || b.crossDomain ? 1223 === f && (f = 204) : f = j.text ? 200 : 404
                } j && e(f, i, j, g.getAllResponseHeaders())
            }, b.async ? 4 === g.readyState ? a.setTimeout(c) : g.onreadystatechange = fc[h] = c : c()
          },
          abort: function() {
            c && c(void 0, !0)
          }
        }
      }
    }), n.ajaxSetup({
      accepts: {
        script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
      },
      contents: {
        script: /\b(?:java|ecma)script\b/
      },
      converters: {
        "text script": function(a) {
          return n.globalEval(a), a
        }
      }
    }), n.ajaxPrefilter("script", function(a) {
      void 0 === a.cache && (a.cache = !1), a.crossDomain && (a.type = "GET", a.global = !1)
    }), n.ajaxTransport("script", function(a) {
      if (a.crossDomain) {
        var b, c = d.head || n("head")[0] || d.documentElement;
        return {
          send: function(e, f) {
            b = d.createElement("script"), b.async = !0, a.scriptCharset && (b.charset = a.scriptCharset), b.src = a.url, b.onload = b.onreadystatechange = function(a, c) {
              (c || !b.readyState || /loaded|complete/.test(b.readyState)) && (b.onload = b.onreadystatechange = null, b.parentNode && b.parentNode.removeChild(b), b = null, c || f(200, "success"))
            }, c.insertBefore(b, c.firstChild)
          },
          abort: function() {
            b && b.onload(void 0, !0)
          }
        }
      }
    });
    var jc = [],
      kc = /(=)\?(?=&|$)|\?\?/;
    n.ajaxSetup({
      jsonp: "callback",
      jsonpCallback: function() {
        var a = jc.pop() || n.expando + "_" + Eb++;
        return this[a] = !0, a
      }
    }), n.ajaxPrefilter("json jsonp", function(b, c, d) {
      var e, f, g, h = b.jsonp !== !1 && (kc.test(b.url) ? "url" : "string" == typeof b.data && 0 === (b.contentType || "").indexOf("application/x-www-form-urlencoded") && kc.test(b.data) && "data");
      return h || "jsonp" === b.dataTypes[0] ? (e = b.jsonpCallback = n.isFunction(b.jsonpCallback) ? b.jsonpCallback() : b.jsonpCallback, h ? b[h] = b[h].replace(kc, "$1" + e) : b.jsonp !== !1 && (b.url += (Fb.test(b.url) ? "&" : "?") + b.jsonp + "=" + e), b.converters["script json"] = function() {
        return g || n.error(e + " was not called"), g[0]
      }, b.dataTypes[0] = "json", f = a[e], a[e] = function() {
        g = arguments
      }, d.always(function() {
        void 0 === f ? n(a).removeProp(e) : a[e] = f, b[e] && (b.jsonpCallback = c.jsonpCallback, jc.push(e)), g && n.isFunction(f) && f(g[0]), g = f = void 0
      }), "script") : void 0
    }), n.parseHTML = function(a, b, c) {
      if (!a || "string" != typeof a) return null;
      "boolean" == typeof b && (c = b, b = !1), b = b || d;
      var e = x.exec(a),
        f = !c && [];
      return e ? [b.createElement(e[1])] : (e = ja([a], b, f), f && f.length && n(f).remove(), n.merge([], e.childNodes))
    };
    var lc = n.fn.load;
    n.fn.load = function(a, b, c) {
      if ("string" != typeof a && lc) return lc.apply(this, arguments);
      var d, e, f, g = this,
        h = a.indexOf(" ");
      return h > -1 && (d = n.trim(a.slice(h, a.length)), a = a.slice(0, h)), n.isFunction(b) ? (c = b, b = void 0) : b && "object" == typeof b && (e = "POST"), g.length > 0 && n.ajax({
        url: a,
        type: e || "GET",
        dataType: "html",
        data: b
      }).done(function(a) {
        f = arguments, g.html(d ? n("<div>").append(n.parseHTML(a)).find(d) : a)
      }).always(c && function(a, b) {
        g.each(function() {
          c.apply(this, f || [a.responseText, b, a])
        })
      }), this
    }, n.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(a, b) {
      n.fn[b] = function(a) {
        return this.on(b, a)
      }
    }), n.expr.filters.animated = function(a) {
      return n.grep(n.timers, function(b) {
        return a === b.elem
      }).length
    }, n.offset = {
      setOffset: function(a, b, c) {
        var d, e, f, g, h, i, j, k = n.css(a, "position"),
          l = n(a),
          m = {};
        "static" === k && (a.style.position = "relative"), h = l.offset(), f = n.css(a, "top"), i = n.css(a, "left"), j = ("absolute" === k || "fixed" === k) && n.inArray("auto", [f, i]) > -1, j ? (d = l.position(), g = d.top, e = d.left) : (g = parseFloat(f) || 0, e = parseFloat(i) || 0), n.isFunction(b) && (b = b.call(a, c, n.extend({}, h))), null != b.top && (m.top = b.top - h.top + g), null != b.left && (m.left = b.left - h.left + e), "using" in b ? b.using.call(a, m) : l.css(m)
      }
    }, n.fn.extend({
      offset: function(a) {
        if (arguments.length) return void 0 === a ? this : this.each(function(b) {
          n.offset.setOffset(this, a, b)
        });
        var b, c, d = {
            top: 0,
            left: 0
          },
          e = this[0],
          f = e && e.ownerDocument;
        return f ? (b = f.documentElement, n.contains(b, e) ? ("undefined" != typeof e.getBoundingClientRect && (d = e.getBoundingClientRect()), c = mc(f), {
          top: d.top + (c.pageYOffset || b.scrollTop) - (b.clientTop || 0),
          left: d.left + (c.pageXOffset || b.scrollLeft) - (b.clientLeft || 0)
        }) : d) : void 0
      },
      position: function() {
        if (this[0]) {
          var a, b, c = {
              top: 0,
              left: 0
            },
            d = this[0];
          return "fixed" === n.css(d, "position") ? b = d.getBoundingClientRect() : (a = this.offsetParent(), b = this.offset(), n.nodeName(a[0], "html") || (c = a.offset()), c.top += n.css(a[0], "borderTopWidth", !0), c.left += n.css(a[0], "borderLeftWidth", !0)), {
            top: b.top - c.top - n.css(d, "marginTop", !0),
            left: b.left - c.left - n.css(d, "marginLeft", !0)
          }
        }
      },
      offsetParent: function() {
        return this.map(function() {
          for (var a = this.offsetParent; a && !n.nodeName(a, "html") && "static" === n.css(a, "position");) a = a.offsetParent;
          return a || Qa
        })
      }
    }), n.each({
      scrollLeft: "pageXOffset",
      scrollTop: "pageYOffset"
    }, function(a, b) {
      var c = /Y/.test(b);
      n.fn[a] = function(d) {
        return Y(this, function(a, d, e) {
          var f = mc(a);
          return void 0 === e ? f ? b in f ? f[b] : f.document.documentElement[d] : a[d] : void(f ? f.scrollTo(c ? n(f).scrollLeft() : e, c ? e : n(f).scrollTop()) : a[d] = e)
        }, a, d, arguments.length, null)
      }
    }), n.each(["top", "left"], function(a, b) {
      n.cssHooks[b] = Ua(l.pixelPosition, function(a, c) {
        return c ? (c = Sa(a, b), Oa.test(c) ? n(a).position()[b] + "px" : c) : void 0
      })
    }), n.each({
      Height: "height",
      Width: "width"
    }, function(a, b) {
      n.each({
        padding: "inner" + a,
        content: b,
        "": "outer" + a
      }, function(c, d) {
        n.fn[d] = function(d, e) {
          var f = arguments.length && (c || "boolean" != typeof d),
            g = c || (d === !0 || e === !0 ? "margin" : "border");
          return Y(this, function(b, c, d) {
            var e;
            return n.isWindow(b) ? b.document.documentElement["client" + a] : 9 === b.nodeType ? (e = b.documentElement, Math.max(b.body["scroll" + a], e["scroll" + a], b.body["offset" + a], e["offset" + a], e["client" + a])) : void 0 === d ? n.css(b, c, g) : n.style(b, c, d, g)
          }, b, f ? d : void 0, f, null)
        }
      })
    }), n.fn.extend({
      bind: function(a, b, c) {
        return this.on(a, null, b, c)
      },
      unbind: function(a, b) {
        return this.off(a, null, b)
      },
      delegate: function(a, b, c, d) {
        return this.on(b, a, c, d)
      },
      undelegate: function(a, b, c) {
        return 1 === arguments.length ? this.off(a, "**") : this.off(b, a || "**", c)
      }
    }), n.fn.size = function() {
      return this.length
    }, n.fn.andSelf = n.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function() {
      return n
    });
    var nc = a.jQuery,
      oc = a.$;
    return n.noConflict = function(b) {
      return a.$ === n && (a.$ = oc), b && a.jQuery === n && (a.jQuery = nc), n
    }, b || (a.jQuery = a.$ = n), n
  }), function(factory) {
    "function" == typeof define && define.amd ? define(["jquery"], factory) : factory(jQuery)
  }(function($) {
    function focusable(element, isTabIndexNotNaN) {
      var map, mapName, img, nodeName = element.nodeName.toLowerCase();
      return "area" === nodeName ? (map = element.parentNode, mapName = map.name, element.href && mapName && "map" === map.nodeName.toLowerCase() ? (img = $("img[usemap=#" + mapName + "]")[0], !!img && visible(img)) : !1) : (/input|select|textarea|button|object/.test(nodeName) ? !element.disabled : "a" === nodeName ? element.href || isTabIndexNotNaN : isTabIndexNotNaN) && visible(element)
    }

    function visible(element) {
      return $.expr.filters.visible(element) && !$(element).parents().addBack().filter(function() {
        return "hidden" === $.css(this, "visibility")
      }).length
    }

    function spinner_modifier(fn) {
      return function() {
        var previous = this.element.val();
        fn.apply(this, arguments), this._refresh(), previous !== this.element.val() && this._trigger("change")
      }
    }
    $.ui = $.ui || {}, $.extend($.ui, {
      version: "1.11.0",
      keyCode: {
        BACKSPACE: 8,
        COMMA: 188,
        DELETE: 46,
        DOWN: 40,
        END: 35,
        ENTER: 13,
        ESCAPE: 27,
        HOME: 36,
        LEFT: 37,
        PAGE_DOWN: 34,
        PAGE_UP: 33,
        PERIOD: 190,
        RIGHT: 39,
        SPACE: 32,
        TAB: 9,
        UP: 38
      }
    }), $.fn.extend({
      scrollParent: function() {
        var position = this.css("position"),
          excludeStaticParent = "absolute" === position,
          scrollParent = this.parents().filter(function() {
            var parent = $(this);
            return excludeStaticParent && "static" === parent.css("position") ? !1 : /(auto|scroll)/.test(parent.css("overflow") + parent.css("overflow-y") + parent.css("overflow-x"))
          }).eq(0);
        return "fixed" !== position && scrollParent.length ? scrollParent : $(this[0].ownerDocument || document)
      },
      uniqueId: function() {
        var uuid = 0;
        return function() {
          return this.each(function() {
            this.id || (this.id = "ui-id-" + ++uuid)
          })
        }
      }(),
      removeUniqueId: function() {
        return this.each(function() {
          /^ui-id-\d+$/.test(this.id) && $(this).removeAttr("id")
        })
      }
    }), $.extend($.expr[":"], {
      data: $.expr.createPseudo ? $.expr.createPseudo(function(dataName) {
        return function(elem) {
          return !!$.data(elem, dataName)
        }
      }) : function(elem, i, match) {
        return !!$.data(elem, match[3])
      },
      focusable: function(element) {
        return focusable(element, !isNaN($.attr(element, "tabindex")))
      },
      tabbable: function(element) {
        var tabIndex = $.attr(element, "tabindex"),
          isTabIndexNaN = isNaN(tabIndex);
        return (isTabIndexNaN || tabIndex >= 0) && focusable(element, !isTabIndexNaN)
      }
    }), $("<a>").outerWidth(1).jquery || $.each(["Width", "Height"], function(i, name) {
      function reduce(elem, size, border, margin) {
        return $.each(side, function() {
          size -= parseFloat($.css(elem, "padding" + this)) || 0, border && (size -= parseFloat($.css(elem, "border" + this + "Width")) || 0), margin && (size -= parseFloat($.css(elem, "margin" + this)) || 0)
        }), size
      }
      var side = "Width" === name ? ["Left", "Right"] : ["Top", "Bottom"],
        type = name.toLowerCase(),
        orig = {
          innerWidth: $.fn.innerWidth,
          innerHeight: $.fn.innerHeight,
          outerWidth: $.fn.outerWidth,
          outerHeight: $.fn.outerHeight
        };
      $.fn["inner" + name] = function(size) {
        return void 0 === size ? orig["inner" + name].call(this) : this.each(function() {
          $(this).css(type, reduce(this, size) + "px")
        })
      }, $.fn["outer" + name] = function(size, margin) {
        return "number" != typeof size ? orig["outer" + name].call(this, size) : this.each(function() {
          $(this).css(type, reduce(this, size, !0, margin) + "px")
        })
      }
    }), $.fn.addBack || ($.fn.addBack = function(selector) {
      return this.add(null == selector ? this.prevObject : this.prevObject.filter(selector))
    }), $("<a>").data("a-b", "a").removeData("a-b").data("a-b") && ($.fn.removeData = function(removeData) {
      return function(key) {
        return arguments.length ? removeData.call(this, $.camelCase(key)) : removeData.call(this)
      }
    }($.fn.removeData)), $.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()), $.fn.extend({
      focus: function(orig) {
        return function(delay, fn) {
          return "number" == typeof delay ? this.each(function() {
            var elem = this;
            setTimeout(function() {
              $(elem).focus(), fn && fn.call(elem)
            }, delay)
          }) : orig.apply(this, arguments)
        }
      }($.fn.focus),
      disableSelection: function() {
        var eventType = "onselectstart" in document.createElement("div") ? "selectstart" : "mousedown";
        return function() {
          return this.bind(eventType + ".ui-disableSelection", function(event) {
            event.preventDefault()
          })
        }
      }(),
      enableSelection: function() {
        return this.unbind(".ui-disableSelection")
      },
      zIndex: function(zIndex) {
        if (void 0 !== zIndex) return this.css("zIndex", zIndex);
        if (this.length)
          for (var position, value, elem = $(this[0]); elem.length && elem[0] !== document;) {
            if (position = elem.css("position"), ("absolute" === position || "relative" === position || "fixed" === position) && (value = parseInt(elem.css("zIndex"), 10), !isNaN(value) && 0 !== value)) return value;
            elem = elem.parent()
          }
        return 0
      }
    }), $.ui.plugin = {
      add: function(module, option, set) {
        var i, proto = $.ui[module].prototype;
        for (i in set) proto.plugins[i] = proto.plugins[i] || [], proto.plugins[i].push([option, set[i]])
      },
      call: function(instance, name, args, allowDisconnected) {
        var i, set = instance.plugins[name];
        if (set && (allowDisconnected || instance.element[0].parentNode && 11 !== instance.element[0].parentNode.nodeType))
          for (i = 0; i < set.length; i++) instance.options[set[i][0]] && set[i][1].apply(instance.element, args)
      }
    };
    var widget_uuid = 0,
      widget_slice = Array.prototype.slice;
    $.cleanData = function(orig) {
      return function(elems) {
        for (var elem, i = 0; null != (elem = elems[i]); i++) try {
          $(elem).triggerHandler("remove")
        } catch (e) {}
        orig(elems)
      }
    }($.cleanData), $.widget = function(name, base, prototype) {
      var fullName, existingConstructor, constructor, basePrototype, proxiedPrototype = {},
        namespace = name.split(".")[0];
      return name = name.split(".")[1], fullName = namespace + "-" + name, prototype || (prototype = base, base = $.Widget), $.expr[":"][fullName.toLowerCase()] = function(elem) {
        return !!$.data(elem, fullName)
      }, $[namespace] = $[namespace] || {}, existingConstructor = $[namespace][name], constructor = $[namespace][name] = function(options, element) {
        return this._createWidget ? void(arguments.length && this._createWidget(options, element)) : new constructor(options, element)
      }, $.extend(constructor, existingConstructor, {
        version: prototype.version,
        _proto: $.extend({}, prototype),
        _childConstructors: []
      }), basePrototype = new base, basePrototype.options = $.widget.extend({}, basePrototype.options), $.each(prototype, function(prop, value) {
        return $.isFunction(value) ? void(proxiedPrototype[prop] = function() {
          var _super = function() {
              return base.prototype[prop].apply(this, arguments)
            },
            _superApply = function(args) {
              return base.prototype[prop].apply(this, args)
            };
          return function() {
            var returnValue, __super = this._super,
              __superApply = this._superApply;
            return this._super = _super, this._superApply = _superApply, returnValue = value.apply(this, arguments), this._super = __super, this._superApply = __superApply, returnValue
          }
        }()) : void(proxiedPrototype[prop] = value)
      }), constructor.prototype = $.widget.extend(basePrototype, {
        widgetEventPrefix: existingConstructor ? basePrototype.widgetEventPrefix || name : name
      }, proxiedPrototype, {
        constructor: constructor,
        namespace: namespace,
        widgetName: name,
        widgetFullName: fullName
      }), existingConstructor ? ($.each(existingConstructor._childConstructors, function(i, child) {
        var childPrototype = child.prototype;
        $.widget(childPrototype.namespace + "." + childPrototype.widgetName, constructor, child._proto)
      }), delete existingConstructor._childConstructors) : base._childConstructors.push(constructor), $.widget.bridge(name, constructor), constructor
    }, $.widget.extend = function(target) {
      for (var key, value, input = widget_slice.call(arguments, 1), inputIndex = 0, inputLength = input.length; inputLength > inputIndex; inputIndex++)
        for (key in input[inputIndex]) value = input[inputIndex][key], input[inputIndex].hasOwnProperty(key) && void 0 !== value && ($.isPlainObject(value) ? target[key] = $.isPlainObject(target[key]) ? $.widget.extend({}, target[key], value) : $.widget.extend({}, value) : target[key] = value);
      return target
    }, $.widget.bridge = function(name, object) {
      var fullName = object.prototype.widgetFullName || name;
      $.fn[name] = function(options) {
        var isMethodCall = "string" == typeof options,
          args = widget_slice.call(arguments, 1),
          returnValue = this;
        return options = !isMethodCall && args.length ? $.widget.extend.apply(null, [options].concat(args)) : options, isMethodCall ? this.each(function() {
          var methodValue, instance = $.data(this, fullName);
          return "instance" === options ? (returnValue = instance, !1) : instance ? $.isFunction(instance[options]) && "_" !== options.charAt(0) ? (methodValue = instance[options].apply(instance, args), methodValue !== instance && void 0 !== methodValue ? (returnValue = methodValue && methodValue.jquery ? returnValue.pushStack(methodValue.get()) : methodValue, !1) : void 0) : $.error("no such method '" + options + "' for " + name + " widget instance") : $.error("cannot call methods on " + name + " prior to initialization; attempted to call method '" + options + "'")
        }) : this.each(function() {
          var instance = $.data(this, fullName);
          instance ? (instance.option(options || {}), instance._init && instance._init()) : $.data(this, fullName, new object(options, this))
        }), returnValue
      }
    }, $.Widget = function() {}, $.Widget._childConstructors = [], $.Widget.prototype = {
      widgetName: "widget",
      widgetEventPrefix: "",
      defaultElement: "<div>",
      options: {
        disabled: !1,
        create: null
      },
      _createWidget: function(options, element) {
        element = $(element || this.defaultElement || this)[0], this.element = $(element), this.uuid = widget_uuid++, this.eventNamespace = "." + this.widgetName + this.uuid, this.options = $.widget.extend({}, this.options, this._getCreateOptions(), options), this.bindings = $(), this.hoverable = $(), this.focusable = $(), element !== this && ($.data(element, this.widgetFullName, this), this._on(!0, this.element, {
          remove: function(event) {
            event.target === element && this.destroy()
          }
        }), this.document = $(element.style ? element.ownerDocument : element.document || element), this.window = $(this.document[0].defaultView || this.document[0].parentWindow)), this._create(), this._trigger("create", null, this._getCreateEventData()), this._init()
      },
      _getCreateOptions: $.noop,
      _getCreateEventData: $.noop,
      _create: $.noop,
      _init: $.noop,
      destroy: function() {
        this._destroy(), this.element.unbind(this.eventNamespace).removeData(this.widgetFullName).removeData($.camelCase(this.widgetFullName)), this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled ui-state-disabled"), this.bindings.unbind(this.eventNamespace), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")
      },
      _destroy: $.noop,
      widget: function() {
        return this.element
      },
      option: function(key, value) {
        var parts, curOption, i, options = key;
        if (0 === arguments.length) return $.widget.extend({}, this.options);
        if ("string" == typeof key)
          if (options = {}, parts = key.split("."), key = parts.shift(), parts.length) {
            for (curOption = options[key] = $.widget.extend({}, this.options[key]), i = 0; i < parts.length - 1; i++) curOption[parts[i]] = curOption[parts[i]] || {}, curOption = curOption[parts[i]];
            if (key = parts.pop(), 1 === arguments.length) return void 0 === curOption[key] ? null : curOption[key];
            curOption[key] = value
          } else {
            if (1 === arguments.length) return void 0 === this.options[key] ? null : this.options[key];
            options[key] = value
          } return this._setOptions(options), this
      },
      _setOptions: function(options) {
        var key;
        for (key in options) this._setOption(key, options[key]);
        return this
      },
      _setOption: function(key, value) {
        return this.options[key] = value, "disabled" === key && (this.widget().toggleClass(this.widgetFullName + "-disabled", !!value), value && (this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus"))), this
      },
      enable: function() {
        return this._setOptions({
          disabled: !1
        })
      },
      disable: function() {
        return this._setOptions({
          disabled: !0
        })
      },
      _on: function(suppressDisabledCheck, element, handlers) {
        var delegateElement, instance = this;
        "boolean" != typeof suppressDisabledCheck && (handlers = element, element = suppressDisabledCheck, suppressDisabledCheck = !1), handlers ? (element = delegateElement = $(element), this.bindings = this.bindings.add(element)) : (handlers = element, element = this.element, delegateElement = this.widget()), $.each(handlers, function(event, handler) {
          function handlerProxy() {
            return suppressDisabledCheck || instance.options.disabled !== !0 && !$(this).hasClass("ui-state-disabled") ? ("string" == typeof handler ? instance[handler] : handler).apply(instance, arguments) : void 0
          }
          "string" != typeof handler && (handlerProxy.guid = handler.guid = handler.guid || handlerProxy.guid || $.guid++);
          var match = event.match(/^([\w:-]*)\s*(.*)$/),
            eventName = match[1] + instance.eventNamespace,
            selector = match[2];
          selector ? delegateElement.delegate(selector, eventName, handlerProxy) : element.bind(eventName, handlerProxy)
        })
      },
      _off: function(element, eventName) {
        eventName = (eventName || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, element.unbind(eventName).undelegate(eventName)
      },
      _delay: function(handler, delay) {
        function handlerProxy() {
          return ("string" == typeof handler ? instance[handler] : handler).apply(instance, arguments)
        }
        var instance = this;
        return setTimeout(handlerProxy, delay || 0)
      },
      _hoverable: function(element) {
        this.hoverable = this.hoverable.add(element), this._on(element, {
          mouseenter: function(event) {
            $(event.currentTarget).addClass("ui-state-hover")
          },
          mouseleave: function(event) {
            $(event.currentTarget).removeClass("ui-state-hover")
          }
        })
      },
      _focusable: function(element) {
        this.focusable = this.focusable.add(element), this._on(element, {
          focusin: function(event) {
            $(event.currentTarget).addClass("ui-state-focus")
          },
          focusout: function(event) {
            $(event.currentTarget).removeClass("ui-state-focus")
          }
        })
      },
      _trigger: function(type, event, data) {
        var prop, orig, callback = this.options[type];
        if (data = data || {}, event = $.Event(event), event.type = (type === this.widgetEventPrefix ? type : this.widgetEventPrefix + type).toLowerCase(), event.target = this.element[0], orig = event.originalEvent)
          for (prop in orig) prop in event || (event[prop] = orig[prop]);
        return this.element.trigger(event, data), !($.isFunction(callback) && callback.apply(this.element[0], [event].concat(data)) === !1 || event.isDefaultPrevented())
      }
    }, $.each({
      show: "fadeIn",
      hide: "fadeOut"
    }, function(method, defaultEffect) {
      $.Widget.prototype["_" + method] = function(element, options, callback) {
        "string" == typeof options && (options = {
          effect: options
        });
        var hasOptions, effectName = options ? options === !0 || "number" == typeof options ? defaultEffect : options.effect || defaultEffect : method;
        options = options || {}, "number" == typeof options && (options = {
          duration: options
        }), hasOptions = !$.isEmptyObject(options), options.complete = callback, options.delay && element.delay(options.delay), hasOptions && $.effects && $.effects.effect[effectName] ? element[method](options) : effectName !== method && element[effectName] ? element[effectName](options.duration, options.easing, callback) : element.queue(function(next) {
          $(this)[method](), callback && callback.call(element[0]), next()
        })
      }
    });
    var mouseHandled = ($.widget, !1);
    $(document).mouseup(function() {
      mouseHandled = !1
    });
    $.widget("ui.mouse", {
      version: "1.11.0",
      options: {
        cancel: "input,textarea,button,select,option",
        distance: 1,
        delay: 0
      },
      _mouseInit: function() {
        var that = this;
        this.element.bind("mousedown." + this.widgetName, function(event) {
          return that._mouseDown(event)
        }).bind("click." + this.widgetName, function(event) {
          return !0 === $.data(event.target, that.widgetName + ".preventClickEvent") ? ($.removeData(event.target, that.widgetName + ".preventClickEvent"), event.stopImmediatePropagation(), !1) : void 0
        }), this.started = !1
      },
      _mouseDestroy: function() {
        this.element.unbind("." + this.widgetName), this._mouseMoveDelegate && this.document.unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate)
      },
      _mouseDown: function(event) {
        if (!mouseHandled) {
          this._mouseStarted && this._mouseUp(event), this._mouseDownEvent = event;
          var that = this,
            btnIsLeft = 1 === event.which,
            elIsCancel = "string" == typeof this.options.cancel && event.target.nodeName ? $(event.target).closest(this.options.cancel).length : !1;
          return btnIsLeft && !elIsCancel && this._mouseCapture(event) ? (this.mouseDelayMet = !this.options.delay, this.mouseDelayMet || (this._mouseDelayTimer = setTimeout(function() {
            that.mouseDelayMet = !0
          }, this.options.delay)), this._mouseDistanceMet(event) && this._mouseDelayMet(event) && (this._mouseStarted = this._mouseStart(event) !== !1, !this._mouseStarted) ? (event.preventDefault(), !0) : (!0 === $.data(event.target, this.widgetName + ".preventClickEvent") && $.removeData(event.target, this.widgetName + ".preventClickEvent"), this._mouseMoveDelegate = function(event) {
            return that._mouseMove(event)
          }, this._mouseUpDelegate = function(event) {
            return that._mouseUp(event)
          }, this.document.bind("mousemove." + this.widgetName, this._mouseMoveDelegate).bind("mouseup." + this.widgetName, this._mouseUpDelegate), event.preventDefault(), mouseHandled = !0, !0)) : !0
        }
      },
      _mouseMove: function(event) {
        return $.ui.ie && (!document.documentMode || document.documentMode < 9) && !event.button ? this._mouseUp(event) : event.which ? this._mouseStarted ? (this._mouseDrag(event), event.preventDefault()) : (this._mouseDistanceMet(event) && this._mouseDelayMet(event) && (this._mouseStarted = this._mouseStart(this._mouseDownEvent, event) !== !1, this._mouseStarted ? this._mouseDrag(event) : this._mouseUp(event)), !this._mouseStarted) : this._mouseUp(event)
      },
      _mouseUp: function(event) {
        return this.document.unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate), this._mouseStarted && (this._mouseStarted = !1, event.target === this._mouseDownEvent.target && $.data(event.target, this.widgetName + ".preventClickEvent", !0), this._mouseStop(event)), mouseHandled = !1, !1
      },
      _mouseDistanceMet: function(event) {
        return Math.max(Math.abs(this._mouseDownEvent.pageX - event.pageX), Math.abs(this._mouseDownEvent.pageY - event.pageY)) >= this.options.distance
      },
      _mouseDelayMet: function() {
        return this.mouseDelayMet
      },
      _mouseStart: function() {},
      _mouseDrag: function() {},
      _mouseStop: function() {},
      _mouseCapture: function() {
        return !0
      }
    });
    ! function() {
      function getOffsets(offsets, width, height) {
        return [parseFloat(offsets[0]) * (rpercent.test(offsets[0]) ? width / 100 : 1), parseFloat(offsets[1]) * (rpercent.test(offsets[1]) ? height / 100 : 1)]
      }

      function parseCss(element, property) {
        return parseInt($.css(element, property), 10) || 0
      }

      function getDimensions(elem) {
        var raw = elem[0];
        return 9 === raw.nodeType ? {
          width: elem.width(),
          height: elem.height(),
          offset: {
            top: 0,
            left: 0
          }
        } : $.isWindow(raw) ? {
          width: elem.width(),
          height: elem.height(),
          offset: {
            top: elem.scrollTop(),
            left: elem.scrollLeft()
          }
        } : raw.preventDefault ? {
          width: 0,
          height: 0,
          offset: {
            top: raw.pageY,
            left: raw.pageX
          }
        } : {
          width: elem.outerWidth(),
          height: elem.outerHeight(),
          offset: elem.offset()
        }
      }
      $.ui = $.ui || {};
      var cachedScrollbarWidth, supportsOffsetFractions, max = Math.max,
        abs = Math.abs,
        round = Math.round,
        rhorizontal = /left|center|right/,
        rvertical = /top|center|bottom/,
        roffset = /[\+\-]\d+(\.[\d]+)?%?/,
        rposition = /^\w+/,
        rpercent = /%$/,
        _position = $.fn.position;
      $.position = {
          scrollbarWidth: function() {
            if (void 0 !== cachedScrollbarWidth) return cachedScrollbarWidth;
            var w1, w2, div = $("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"),
              innerDiv = div.children()[0];
            return $("body").append(div), w1 = innerDiv.offsetWidth, div.css("overflow", "scroll"), w2 = innerDiv.offsetWidth, w1 === w2 && (w2 = div[0].clientWidth), div.remove(), cachedScrollbarWidth = w1 - w2
          },
          getScrollInfo: function(within) {
            var overflowX = within.isWindow || within.isDocument ? "" : within.element.css("overflow-x"),
              overflowY = within.isWindow || within.isDocument ? "" : within.element.css("overflow-y"),
              hasOverflowX = "scroll" === overflowX || "auto" === overflowX && within.width < within.element[0].scrollWidth,
              hasOverflowY = "scroll" === overflowY || "auto" === overflowY && within.height < within.element[0].scrollHeight;
            return {
              width: hasOverflowY ? $.position.scrollbarWidth() : 0,
              height: hasOverflowX ? $.position.scrollbarWidth() : 0
            }
          },
          getWithinInfo: function(element) {
            var withinElement = $(element || window),
              isWindow = $.isWindow(withinElement[0]),
              isDocument = !!withinElement[0] && 9 === withinElement[0].nodeType;
            return {
              element: withinElement,
              isWindow: isWindow,
              isDocument: isDocument,
              offset: withinElement.offset() || {
                left: 0,
                top: 0
              },
              scrollLeft: withinElement.scrollLeft(),
              scrollTop: withinElement.scrollTop(),
              width: isWindow ? withinElement.width() : withinElement.outerWidth(),
              height: isWindow ? withinElement.height() : withinElement.outerHeight()
            }
          }
        }, $.fn.position = function(options) {
          if (!options || !options.of) return _position.apply(this, arguments);
          options = $.extend({}, options);
          var atOffset, targetWidth, targetHeight, targetOffset, basePosition, dimensions, target = $(options.of),
            within = $.position.getWithinInfo(options.within),
            scrollInfo = $.position.getScrollInfo(within),
            collision = (options.collision || "flip").split(" "),
            offsets = {};
          return dimensions = getDimensions(target), target[0].preventDefault && (options.at = "left top"), targetWidth = dimensions.width, targetHeight = dimensions.height, targetOffset = dimensions.offset, basePosition = $.extend({}, targetOffset), $.each(["my", "at"], function() {
            var horizontalOffset, verticalOffset, pos = (options[this] || "").split(" ");
            1 === pos.length && (pos = rhorizontal.test(pos[0]) ? pos.concat(["center"]) : rvertical.test(pos[0]) ? ["center"].concat(pos) : ["center", "center"]), pos[0] = rhorizontal.test(pos[0]) ? pos[0] : "center", pos[1] = rvertical.test(pos[1]) ? pos[1] : "center", horizontalOffset = roffset.exec(pos[0]), verticalOffset = roffset.exec(pos[1]), offsets[this] = [horizontalOffset ? horizontalOffset[0] : 0, verticalOffset ? verticalOffset[0] : 0], options[this] = [rposition.exec(pos[0])[0], rposition.exec(pos[1])[0]]
          }), 1 === collision.length && (collision[1] = collision[0]), "right" === options.at[0] ? basePosition.left += targetWidth : "center" === options.at[0] && (basePosition.left += targetWidth / 2), "bottom" === options.at[1] ? basePosition.top += targetHeight : "center" === options.at[1] && (basePosition.top += targetHeight / 2), atOffset = getOffsets(offsets.at, targetWidth, targetHeight), basePosition.left += atOffset[0], basePosition.top += atOffset[1], this.each(function() {
            var collisionPosition, using, elem = $(this),
              elemWidth = elem.outerWidth(),
              elemHeight = elem.outerHeight(),
              marginLeft = parseCss(this, "marginLeft"),
              marginTop = parseCss(this, "marginTop"),
              collisionWidth = elemWidth + marginLeft + parseCss(this, "marginRight") + scrollInfo.width,
              collisionHeight = elemHeight + marginTop + parseCss(this, "marginBottom") + scrollInfo.height,
              position = $.extend({}, basePosition),
              myOffset = getOffsets(offsets.my, elem.outerWidth(), elem.outerHeight());
            "right" === options.my[0] ? position.left -= elemWidth : "center" === options.my[0] && (position.left -= elemWidth / 2), "bottom" === options.my[1] ? position.top -= elemHeight : "center" === options.my[1] && (position.top -= elemHeight / 2), position.left += myOffset[0], position.top += myOffset[1], supportsOffsetFractions || (position.left = round(position.left), position.top = round(position.top)), collisionPosition = {
              marginLeft: marginLeft,
              marginTop: marginTop
            }, $.each(["left", "top"], function(i, dir) {
              $.ui.position[collision[i]] && $.ui.position[collision[i]][dir](position, {
                targetWidth: targetWidth,
                targetHeight: targetHeight,
                elemWidth: elemWidth,
                elemHeight: elemHeight,
                collisionPosition: collisionPosition,
                collisionWidth: collisionWidth,
                collisionHeight: collisionHeight,
                offset: [atOffset[0] + myOffset[0], atOffset[1] + myOffset[1]],
                my: options.my,
                at: options.at,
                within: within,
                elem: elem
              })
            }), options.using && (using = function(props) {
              var left = targetOffset.left - position.left,
                right = left + targetWidth - elemWidth,
                top = targetOffset.top - position.top,
                bottom = top + targetHeight - elemHeight,
                feedback = {
                  target: {
                    element: target,
                    left: targetOffset.left,
                    top: targetOffset.top,
                    width: targetWidth,
                    height: targetHeight
                  },
                  element: {
                    element: elem,
                    left: position.left,
                    top: position.top,
                    width: elemWidth,
                    height: elemHeight
                  },
                  horizontal: 0 > right ? "left" : left > 0 ? "right" : "center",
                  vertical: 0 > bottom ? "top" : top > 0 ? "bottom" : "middle"
                };
              elemWidth > targetWidth && abs(left + right) < targetWidth && (feedback.horizontal = "center"), elemHeight > targetHeight && abs(top + bottom) < targetHeight && (feedback.vertical = "middle"), max(abs(left), abs(right)) > max(abs(top), abs(bottom)) ? feedback.important = "horizontal" : feedback.important = "vertical", options.using.call(this, props, feedback)
            }), elem.offset($.extend(position, {
              using: using
            }))
          })
        }, $.ui.position = {
          fit: {
            left: function(position, data) {
              var newOverRight, within = data.within,
                withinOffset = within.isWindow ? within.scrollLeft : within.offset.left,
                outerWidth = within.width,
                collisionPosLeft = position.left - data.collisionPosition.marginLeft,
                overLeft = withinOffset - collisionPosLeft,
                overRight = collisionPosLeft + data.collisionWidth - outerWidth - withinOffset;
              data.collisionWidth > outerWidth ? overLeft > 0 && 0 >= overRight ? (newOverRight = position.left + overLeft + data.collisionWidth - outerWidth - withinOffset, position.left += overLeft - newOverRight) : overRight > 0 && 0 >= overLeft ? position.left = withinOffset : overLeft > overRight ? position.left = withinOffset + outerWidth - data.collisionWidth : position.left = withinOffset : overLeft > 0 ? position.left += overLeft : overRight > 0 ? position.left -= overRight : position.left = max(position.left - collisionPosLeft, position.left)
            },
            top: function(position, data) {
              var newOverBottom, within = data.within,
                withinOffset = within.isWindow ? within.scrollTop : within.offset.top,
                outerHeight = data.within.height,
                collisionPosTop = position.top - data.collisionPosition.marginTop,
                overTop = withinOffset - collisionPosTop,
                overBottom = collisionPosTop + data.collisionHeight - outerHeight - withinOffset;
              data.collisionHeight > outerHeight ? overTop > 0 && 0 >= overBottom ? (newOverBottom = position.top + overTop + data.collisionHeight - outerHeight - withinOffset, position.top += overTop - newOverBottom) : overBottom > 0 && 0 >= overTop ? position.top = withinOffset : overTop > overBottom ? position.top = withinOffset + outerHeight - data.collisionHeight : position.top = withinOffset : overTop > 0 ? position.top += overTop : overBottom > 0 ? position.top -= overBottom : position.top = max(position.top - collisionPosTop, position.top)
            }
          },
          flip: {
            left: function(position, data) {
              var newOverRight, newOverLeft, within = data.within,
                withinOffset = within.offset.left + within.scrollLeft,
                outerWidth = within.width,
                offsetLeft = within.isWindow ? within.scrollLeft : within.offset.left,
                collisionPosLeft = position.left - data.collisionPosition.marginLeft,
                overLeft = collisionPosLeft - offsetLeft,
                overRight = collisionPosLeft + data.collisionWidth - outerWidth - offsetLeft,
                myOffset = "left" === data.my[0] ? -data.elemWidth : "right" === data.my[0] ? data.elemWidth : 0,
                atOffset = "left" === data.at[0] ? data.targetWidth : "right" === data.at[0] ? -data.targetWidth : 0,
                offset = -2 * data.offset[0];
              0 > overLeft ? (newOverRight = position.left + myOffset + atOffset + offset + data.collisionWidth - outerWidth - withinOffset, (0 > newOverRight || newOverRight < abs(overLeft)) && (position.left += myOffset + atOffset + offset)) : overRight > 0 && (newOverLeft = position.left - data.collisionPosition.marginLeft + myOffset + atOffset + offset - offsetLeft, (newOverLeft > 0 || abs(newOverLeft) < overRight) && (position.left += myOffset + atOffset + offset))
            },
            top: function(position, data) {
              var newOverTop, newOverBottom, within = data.within,
                withinOffset = within.offset.top + within.scrollTop,
                outerHeight = within.height,
                offsetTop = within.isWindow ? within.scrollTop : within.offset.top,
                collisionPosTop = position.top - data.collisionPosition.marginTop,
                overTop = collisionPosTop - offsetTop,
                overBottom = collisionPosTop + data.collisionHeight - outerHeight - offsetTop,
                top = "top" === data.my[1],
                myOffset = top ? -data.elemHeight : "bottom" === data.my[1] ? data.elemHeight : 0,
                atOffset = "top" === data.at[1] ? data.targetHeight : "bottom" === data.at[1] ? -data.targetHeight : 0,
                offset = -2 * data.offset[1];
              0 > overTop ? (newOverBottom = position.top + myOffset + atOffset + offset + data.collisionHeight - outerHeight - withinOffset, position.top + myOffset + atOffset + offset > overTop && (0 > newOverBottom || newOverBottom < abs(overTop)) && (position.top += myOffset + atOffset + offset)) : overBottom > 0 && (newOverTop = position.top - data.collisionPosition.marginTop + myOffset + atOffset + offset - offsetTop, position.top + myOffset + atOffset + offset > overBottom && (newOverTop > 0 || abs(newOverTop) < overBottom) && (position.top += myOffset + atOffset + offset))
            }
          },
          flipfit: {
            left: function() {
              $.ui.position.flip.left.apply(this, arguments), $.ui.position.fit.left.apply(this, arguments)
            },
            top: function() {
              $.ui.position.flip.top.apply(this, arguments), $.ui.position.fit.top.apply(this, arguments)
            }
          }
        },
        function() {
          var testElement, testElementParent, testElementStyle, offsetLeft, i, body = document.getElementsByTagName("body")[0],
            div = document.createElement("div");
          testElement = document.createElement(body ? "div" : "body"), testElementStyle = {
            visibility: "hidden",
            width: 0,
            height: 0,
            border: 0,
            margin: 0,
            background: "none"
          }, body && $.extend(testElementStyle, {
            position: "absolute",
            left: "-1000px",
            top: "-1000px"
          });
          for (i in testElementStyle) testElement.style[i] = testElementStyle[i];
          testElement.appendChild(div), testElementParent = body || document.documentElement, testElementParent.insertBefore(testElement, testElementParent.firstChild), div.style.cssText = "position: absolute; left: 10.7432222px;", offsetLeft = $(div).offset().left, supportsOffsetFractions = offsetLeft > 10 && 11 > offsetLeft, testElement.innerHTML = "", testElementParent.removeChild(testElement)
        }()
    }();
    $.ui.position;
    $.widget("ui.draggable", $.ui.mouse, {
      version: "1.11.0",
      widgetEventPrefix: "drag",
      options: {
        addClasses: !0,
        appendTo: "parent",
        axis: !1,
        connectToSortable: !1,
        containment: !1,
        cursor: "auto",
        cursorAt: !1,
        grid: !1,
        handle: !1,
        helper: "original",
        iframeFix: !1,
        opacity: !1,
        refreshPositions: !1,
        revert: !1,
        revertDuration: 500,
        scope: "default",
        scroll: !0,
        scrollSensitivity: 20,
        scrollSpeed: 20,
        snap: !1,
        snapMode: "both",
        snapTolerance: 20,
        stack: !1,
        zIndex: !1,
        drag: null,
        start: null,
        stop: null
      },
      _create: function() {
        "original" !== this.options.helper || /^(?:r|a|f)/.test(this.element.css("position")) || (this.element[0].style.position = "relative"), this.options.addClasses && this.element.addClass("ui-draggable"), this.options.disabled && this.element.addClass("ui-draggable-disabled"), this._setHandleClassName(), this._mouseInit()
      },
      _setOption: function(key, value) {
        this._super(key, value), "handle" === key && this._setHandleClassName()
      },
      _destroy: function() {
        return (this.helper || this.element).is(".ui-draggable-dragging") ? void(this.destroyOnClear = !0) : (this.element.removeClass("ui-draggable ui-draggable-dragging ui-draggable-disabled"), this._removeHandleClassName(), void this._mouseDestroy())
      },
      _mouseCapture: function(event) {
        var document = this.document[0],
          o = this.options;
        try {
          document.activeElement && "body" !== document.activeElement.nodeName.toLowerCase() && $(document.activeElement).blur()
        } catch (error) {}
        return this.helper || o.disabled || $(event.target).closest(".ui-resizable-handle").length > 0 ? !1 : (this.handle = this._getHandle(event), this.handle ? ($(o.iframeFix === !0 ? "iframe" : o.iframeFix).each(function() {
          $("<div class='ui-draggable-iframeFix' style='background: #fff;'></div>").css({
            width: this.offsetWidth + "px",
            height: this.offsetHeight + "px",
            position: "absolute",
            opacity: "0.001",
            zIndex: 1e3
          }).css($(this).offset()).appendTo("body")
        }), !0) : !1)
      },
      _mouseStart: function(event) {
        var o = this.options;
        return this.helper = this._createHelper(event), this.helper.addClass("ui-draggable-dragging"), this._cacheHelperProportions(), $.ui.ddmanager && ($.ui.ddmanager.current = this), this._cacheMargins(), this.cssPosition = this.helper.css("position"), this.scrollParent = this.helper.scrollParent(), this.offsetParent = this.helper.offsetParent(), this.offsetParentCssPosition = this.offsetParent.css("position"), this.offset = this.positionAbs = this.element.offset(), this.offset = {
          top: this.offset.top - this.margins.top,
          left: this.offset.left - this.margins.left
        }, this.offset.scroll = !1, $.extend(this.offset, {
          click: {
            left: event.pageX - this.offset.left,
            top: event.pageY - this.offset.top
          },
          parent: this._getParentOffset(),
          relative: this._getRelativeOffset()
        }), this.originalPosition = this.position = this._generatePosition(event, !1), this.originalPageX = event.pageX, this.originalPageY = event.pageY, o.cursorAt && this._adjustOffsetFromHelper(o.cursorAt), this._setContainment(), this._trigger("start", event) === !1 ? (this._clear(), !1) : (this._cacheHelperProportions(), $.ui.ddmanager && !o.dropBehaviour && $.ui.ddmanager.prepareOffsets(this, event), this._mouseDrag(event, !0), $.ui.ddmanager && $.ui.ddmanager.dragStart(this, event), !0)
      },
      _mouseDrag: function(event, noPropagation) {
        if ("fixed" === this.offsetParentCssPosition && (this.offset.parent = this._getParentOffset()),
          this.position = this._generatePosition(event, !0), this.positionAbs = this._convertPositionTo("absolute"), !noPropagation) {
          var ui = this._uiHash();
          if (this._trigger("drag", event, ui) === !1) return this._mouseUp({}), !1;
          this.position = ui.position
        }
        return this.helper[0].style.left = this.position.left + "px", this.helper[0].style.top = this.position.top + "px", $.ui.ddmanager && $.ui.ddmanager.drag(this, event), !1
      },
      _mouseStop: function(event) {
        var that = this,
          dropped = !1;
        return $.ui.ddmanager && !this.options.dropBehaviour && (dropped = $.ui.ddmanager.drop(this, event)), this.dropped && (dropped = this.dropped, this.dropped = !1), "invalid" === this.options.revert && !dropped || "valid" === this.options.revert && dropped || this.options.revert === !0 || $.isFunction(this.options.revert) && this.options.revert.call(this.element, dropped) ? $(this.helper).animate(this.originalPosition, parseInt(this.options.revertDuration, 10), function() {
          that._trigger("stop", event) !== !1 && that._clear()
        }) : this._trigger("stop", event) !== !1 && this._clear(), !1
      },
      _mouseUp: function(event) {
        return $("div.ui-draggable-iframeFix").each(function() {
          this.parentNode.removeChild(this)
        }), $.ui.ddmanager && $.ui.ddmanager.dragStop(this, event), this.element.focus(), $.ui.mouse.prototype._mouseUp.call(this, event)
      },
      cancel: function() {
        return this.helper.is(".ui-draggable-dragging") ? this._mouseUp({}) : this._clear(), this
      },
      _getHandle: function(event) {
        return this.options.handle ? !!$(event.target).closest(this.element.find(this.options.handle)).length : !0
      },
      _setHandleClassName: function() {
        this._removeHandleClassName(), $(this.options.handle || this.element).addClass("ui-draggable-handle")
      },
      _removeHandleClassName: function() {
        this.element.find(".ui-draggable-handle").addBack().removeClass("ui-draggable-handle")
      },
      _createHelper: function(event) {
        var o = this.options,
          helper = $.isFunction(o.helper) ? $(o.helper.apply(this.element[0], [event])) : "clone" === o.helper ? this.element.clone().removeAttr("id") : this.element;
        return helper.parents("body").length || helper.appendTo("parent" === o.appendTo ? this.element[0].parentNode : o.appendTo), helper[0] === this.element[0] || /(fixed|absolute)/.test(helper.css("position")) || helper.css("position", "absolute"), helper
      },
      _adjustOffsetFromHelper: function(obj) {
        "string" == typeof obj && (obj = obj.split(" ")), $.isArray(obj) && (obj = {
          left: +obj[0],
          top: +obj[1] || 0
        }), "left" in obj && (this.offset.click.left = obj.left + this.margins.left), "right" in obj && (this.offset.click.left = this.helperProportions.width - obj.right + this.margins.left), "top" in obj && (this.offset.click.top = obj.top + this.margins.top), "bottom" in obj && (this.offset.click.top = this.helperProportions.height - obj.bottom + this.margins.top)
      },
      _isRootNode: function(element) {
        return /(html|body)/i.test(element.tagName) || element === this.document[0]
      },
      _getParentOffset: function() {
        var po = this.offsetParent.offset(),
          document = this.document[0];
        return "absolute" === this.cssPosition && this.scrollParent[0] !== document && $.contains(this.scrollParent[0], this.offsetParent[0]) && (po.left += this.scrollParent.scrollLeft(), po.top += this.scrollParent.scrollTop()), this._isRootNode(this.offsetParent[0]) && (po = {
          top: 0,
          left: 0
        }), {
          top: po.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
          left: po.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
        }
      },
      _getRelativeOffset: function() {
        if ("relative" !== this.cssPosition) return {
          top: 0,
          left: 0
        };
        var p = this.element.position(),
          scrollIsRootNode = this._isRootNode(this.scrollParent[0]);
        return {
          top: p.top - (parseInt(this.helper.css("top"), 10) || 0) + (scrollIsRootNode ? 0 : this.scrollParent.scrollTop()),
          left: p.left - (parseInt(this.helper.css("left"), 10) || 0) + (scrollIsRootNode ? 0 : this.scrollParent.scrollLeft())
        }
      },
      _cacheMargins: function() {
        this.margins = {
          left: parseInt(this.element.css("marginLeft"), 10) || 0,
          top: parseInt(this.element.css("marginTop"), 10) || 0,
          right: parseInt(this.element.css("marginRight"), 10) || 0,
          bottom: parseInt(this.element.css("marginBottom"), 10) || 0
        }
      },
      _cacheHelperProportions: function() {
        this.helperProportions = {
          width: this.helper.outerWidth(),
          height: this.helper.outerHeight()
        }
      },
      _setContainment: function() {
        var over, c, ce, o = this.options,
          document = this.document[0];
        return this.relative_container = null, o.containment ? "window" === o.containment ? void(this.containment = [$(window).scrollLeft() - this.offset.relative.left - this.offset.parent.left, $(window).scrollTop() - this.offset.relative.top - this.offset.parent.top, $(window).scrollLeft() + $(window).width() - this.helperProportions.width - this.margins.left, $(window).scrollTop() + ($(window).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]) : "document" === o.containment ? void(this.containment = [0, 0, $(document).width() - this.helperProportions.width - this.margins.left, ($(document).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]) : o.containment.constructor === Array ? void(this.containment = o.containment) : ("parent" === o.containment && (o.containment = this.helper[0].parentNode), c = $(o.containment), ce = c[0], void(ce && (over = "hidden" !== c.css("overflow"), this.containment = [(parseInt(c.css("borderLeftWidth"), 10) || 0) + (parseInt(c.css("paddingLeft"), 10) || 0), (parseInt(c.css("borderTopWidth"), 10) || 0) + (parseInt(c.css("paddingTop"), 10) || 0), (over ? Math.max(ce.scrollWidth, ce.offsetWidth) : ce.offsetWidth) - (parseInt(c.css("borderRightWidth"), 10) || 0) - (parseInt(c.css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left - this.margins.right, (over ? Math.max(ce.scrollHeight, ce.offsetHeight) : ce.offsetHeight) - (parseInt(c.css("borderBottomWidth"), 10) || 0) - (parseInt(c.css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top - this.margins.bottom], this.relative_container = c))) : void(this.containment = null)
      },
      _convertPositionTo: function(d, pos) {
        pos || (pos = this.position);
        var mod = "absolute" === d ? 1 : -1,
          scrollIsRootNode = this._isRootNode(this.scrollParent[0]);
        return {
          top: pos.top + this.offset.relative.top * mod + this.offset.parent.top * mod - ("fixed" === this.cssPosition ? -this.offset.scroll.top : scrollIsRootNode ? 0 : this.offset.scroll.top) * mod,
          left: pos.left + this.offset.relative.left * mod + this.offset.parent.left * mod - ("fixed" === this.cssPosition ? -this.offset.scroll.left : scrollIsRootNode ? 0 : this.offset.scroll.left) * mod
        }
      },
      _generatePosition: function(event, constrainPosition) {
        var containment, co, top, left, o = this.options,
          scrollIsRootNode = this._isRootNode(this.scrollParent[0]),
          pageX = event.pageX,
          pageY = event.pageY;
        return scrollIsRootNode && this.offset.scroll || (this.offset.scroll = {
          top: this.scrollParent.scrollTop(),
          left: this.scrollParent.scrollLeft()
        }), constrainPosition && (this.containment && (this.relative_container ? (co = this.relative_container.offset(), containment = [this.containment[0] + co.left, this.containment[1] + co.top, this.containment[2] + co.left, this.containment[3] + co.top]) : containment = this.containment, event.pageX - this.offset.click.left < containment[0] && (pageX = containment[0] + this.offset.click.left), event.pageY - this.offset.click.top < containment[1] && (pageY = containment[1] + this.offset.click.top), event.pageX - this.offset.click.left > containment[2] && (pageX = containment[2] + this.offset.click.left), event.pageY - this.offset.click.top > containment[3] && (pageY = containment[3] + this.offset.click.top)), o.grid && (top = o.grid[1] ? this.originalPageY + Math.round((pageY - this.originalPageY) / o.grid[1]) * o.grid[1] : this.originalPageY, pageY = containment ? top - this.offset.click.top >= containment[1] || top - this.offset.click.top > containment[3] ? top : top - this.offset.click.top >= containment[1] ? top - o.grid[1] : top + o.grid[1] : top, left = o.grid[0] ? this.originalPageX + Math.round((pageX - this.originalPageX) / o.grid[0]) * o.grid[0] : this.originalPageX, pageX = containment ? left - this.offset.click.left >= containment[0] || left - this.offset.click.left > containment[2] ? left : left - this.offset.click.left >= containment[0] ? left - o.grid[0] : left + o.grid[0] : left), "y" === o.axis && (pageX = this.originalPageX), "x" === o.axis && (pageY = this.originalPageY)), {
          top: pageY - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.offset.scroll.top : scrollIsRootNode ? 0 : this.offset.scroll.top),
          left: pageX - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.offset.scroll.left : scrollIsRootNode ? 0 : this.offset.scroll.left)
        }
      },
      _clear: function() {
        this.helper.removeClass("ui-draggable-dragging"), this.helper[0] === this.element[0] || this.cancelHelperRemoval || this.helper.remove(), this.helper = null, this.cancelHelperRemoval = !1, this.destroyOnClear && this.destroy()
      },
      _trigger: function(type, event, ui) {
        return ui = ui || this._uiHash(), $.ui.plugin.call(this, type, [event, ui, this], !0), "drag" === type && (this.positionAbs = this._convertPositionTo("absolute")), $.Widget.prototype._trigger.call(this, type, event, ui)
      },
      plugins: {},
      _uiHash: function() {
        return {
          helper: this.helper,
          position: this.position,
          originalPosition: this.originalPosition,
          offset: this.positionAbs
        }
      }
    }), $.ui.plugin.add("draggable", "connectToSortable", {
      start: function(event, ui, inst) {
        var o = inst.options,
          uiSortable = $.extend({}, ui, {
            item: inst.element
          });
        inst.sortables = [], $(o.connectToSortable).each(function() {
          var sortable = $(this).sortable("instance");
          sortable && !sortable.options.disabled && (inst.sortables.push({
            instance: sortable,
            shouldRevert: sortable.options.revert
          }), sortable.refreshPositions(), sortable._trigger("activate", event, uiSortable))
        })
      },
      stop: function(event, ui, inst) {
        var uiSortable = $.extend({}, ui, {
          item: inst.element
        });
        $.each(inst.sortables, function() {
          this.instance.isOver ? (this.instance.isOver = 0, inst.cancelHelperRemoval = !0, this.instance.cancelHelperRemoval = !1, this.shouldRevert && (this.instance.options.revert = this.shouldRevert), this.instance._mouseStop(event), this.instance.options.helper = this.instance.options._helper, "original" === inst.options.helper && this.instance.currentItem.css({
            top: "auto",
            left: "auto"
          })) : (this.instance.cancelHelperRemoval = !1, this.instance._trigger("deactivate", event, uiSortable))
        })
      },
      drag: function(event, ui, inst) {
        var that = this;
        $.each(inst.sortables, function() {
          var innermostIntersecting = !1,
            thisSortable = this;
          this.instance.positionAbs = inst.positionAbs, this.instance.helperProportions = inst.helperProportions, this.instance.offset.click = inst.offset.click, this.instance._intersectsWith(this.instance.containerCache) && (innermostIntersecting = !0, $.each(inst.sortables, function() {
            return this.instance.positionAbs = inst.positionAbs, this.instance.helperProportions = inst.helperProportions, this.instance.offset.click = inst.offset.click, this !== thisSortable && this.instance._intersectsWith(this.instance.containerCache) && $.contains(thisSortable.instance.element[0], this.instance.element[0]) && (innermostIntersecting = !1), innermostIntersecting
          })), innermostIntersecting ? (this.instance.isOver || (this.instance.isOver = 1, this.instance.currentItem = $(that).clone().removeAttr("id").appendTo(this.instance.element).data("ui-sortable-item", !0), this.instance.options._helper = this.instance.options.helper, this.instance.options.helper = function() {
            return ui.helper[0]
          }, event.target = this.instance.currentItem[0], this.instance._mouseCapture(event, !0), this.instance._mouseStart(event, !0, !0), this.instance.offset.click.top = inst.offset.click.top, this.instance.offset.click.left = inst.offset.click.left, this.instance.offset.parent.left -= inst.offset.parent.left - this.instance.offset.parent.left, this.instance.offset.parent.top -= inst.offset.parent.top - this.instance.offset.parent.top, inst._trigger("toSortable", event), inst.dropped = this.instance.element, inst.currentItem = inst.element, this.instance.fromOutside = inst), this.instance.currentItem && this.instance._mouseDrag(event)) : this.instance.isOver && (this.instance.isOver = 0, this.instance.cancelHelperRemoval = !0, this.instance.options.revert = !1, this.instance._trigger("out", event, this.instance._uiHash(this.instance)), this.instance._mouseStop(event, !0), this.instance.options.helper = this.instance.options._helper, this.instance.currentItem.remove(), this.instance.placeholder && this.instance.placeholder.remove(), inst._trigger("fromSortable", event), inst.dropped = !1)
        })
      }
    }), $.ui.plugin.add("draggable", "cursor", {
      start: function(event, ui, instance) {
        var t = $("body"),
          o = instance.options;
        t.css("cursor") && (o._cursor = t.css("cursor")), t.css("cursor", o.cursor)
      },
      stop: function(event, ui, instance) {
        var o = instance.options;
        o._cursor && $("body").css("cursor", o._cursor)
      }
    }), $.ui.plugin.add("draggable", "opacity", {
      start: function(event, ui, instance) {
        var t = $(ui.helper),
          o = instance.options;
        t.css("opacity") && (o._opacity = t.css("opacity")), t.css("opacity", o.opacity)
      },
      stop: function(event, ui, instance) {
        var o = instance.options;
        o._opacity && $(ui.helper).css("opacity", o._opacity)
      }
    }), $.ui.plugin.add("draggable", "scroll", {
      start: function(event, ui, i) {
        i.scrollParent[0] !== i.document[0] && "HTML" !== i.scrollParent[0].tagName && (i.overflowOffset = i.scrollParent.offset())
      },
      drag: function(event, ui, i) {
        var o = i.options,
          scrolled = !1,
          document = i.document[0];
        i.scrollParent[0] !== document && "HTML" !== i.scrollParent[0].tagName ? (o.axis && "x" === o.axis || (i.overflowOffset.top + i.scrollParent[0].offsetHeight - event.pageY < o.scrollSensitivity ? i.scrollParent[0].scrollTop = scrolled = i.scrollParent[0].scrollTop + o.scrollSpeed : event.pageY - i.overflowOffset.top < o.scrollSensitivity && (i.scrollParent[0].scrollTop = scrolled = i.scrollParent[0].scrollTop - o.scrollSpeed)), o.axis && "y" === o.axis || (i.overflowOffset.left + i.scrollParent[0].offsetWidth - event.pageX < o.scrollSensitivity ? i.scrollParent[0].scrollLeft = scrolled = i.scrollParent[0].scrollLeft + o.scrollSpeed : event.pageX - i.overflowOffset.left < o.scrollSensitivity && (i.scrollParent[0].scrollLeft = scrolled = i.scrollParent[0].scrollLeft - o.scrollSpeed))) : (o.axis && "x" === o.axis || (event.pageY - $(document).scrollTop() < o.scrollSensitivity ? scrolled = $(document).scrollTop($(document).scrollTop() - o.scrollSpeed) : $(window).height() - (event.pageY - $(document).scrollTop()) < o.scrollSensitivity && (scrolled = $(document).scrollTop($(document).scrollTop() + o.scrollSpeed))), o.axis && "y" === o.axis || (event.pageX - $(document).scrollLeft() < o.scrollSensitivity ? scrolled = $(document).scrollLeft($(document).scrollLeft() - o.scrollSpeed) : $(window).width() - (event.pageX - $(document).scrollLeft()) < o.scrollSensitivity && (scrolled = $(document).scrollLeft($(document).scrollLeft() + o.scrollSpeed)))), scrolled !== !1 && $.ui.ddmanager && !o.dropBehaviour && $.ui.ddmanager.prepareOffsets(i, event)
      }
    }), $.ui.plugin.add("draggable", "snap", {
      start: function(event, ui, i) {
        var o = i.options;
        i.snapElements = [], $(o.snap.constructor !== String ? o.snap.items || ":data(ui-draggable)" : o.snap).each(function() {
          var $t = $(this),
            $o = $t.offset();
          this !== i.element[0] && i.snapElements.push({
            item: this,
            width: $t.outerWidth(),
            height: $t.outerHeight(),
            top: $o.top,
            left: $o.left
          })
        })
      },
      drag: function(event, ui, inst) {
        var ts, bs, ls, rs, l, r, t, b, i, first, o = inst.options,
          d = o.snapTolerance,
          x1 = ui.offset.left,
          x2 = x1 + inst.helperProportions.width,
          y1 = ui.offset.top,
          y2 = y1 + inst.helperProportions.height;
        for (i = inst.snapElements.length - 1; i >= 0; i--) l = inst.snapElements[i].left, r = l + inst.snapElements[i].width, t = inst.snapElements[i].top, b = t + inst.snapElements[i].height, l - d > x2 || x1 > r + d || t - d > y2 || y1 > b + d || !$.contains(inst.snapElements[i].item.ownerDocument, inst.snapElements[i].item) ? (inst.snapElements[i].snapping && inst.options.snap.release && inst.options.snap.release.call(inst.element, event, $.extend(inst._uiHash(), {
          snapItem: inst.snapElements[i].item
        })), inst.snapElements[i].snapping = !1) : ("inner" !== o.snapMode && (ts = Math.abs(t - y2) <= d, bs = Math.abs(b - y1) <= d, ls = Math.abs(l - x2) <= d, rs = Math.abs(r - x1) <= d, ts && (ui.position.top = inst._convertPositionTo("relative", {
          top: t - inst.helperProportions.height,
          left: 0
        }).top - inst.margins.top), bs && (ui.position.top = inst._convertPositionTo("relative", {
          top: b,
          left: 0
        }).top - inst.margins.top), ls && (ui.position.left = inst._convertPositionTo("relative", {
          top: 0,
          left: l - inst.helperProportions.width
        }).left - inst.margins.left), rs && (ui.position.left = inst._convertPositionTo("relative", {
          top: 0,
          left: r
        }).left - inst.margins.left)), first = ts || bs || ls || rs, "outer" !== o.snapMode && (ts = Math.abs(t - y1) <= d, bs = Math.abs(b - y2) <= d, ls = Math.abs(l - x1) <= d, rs = Math.abs(r - x2) <= d, ts && (ui.position.top = inst._convertPositionTo("relative", {
          top: t,
          left: 0
        }).top - inst.margins.top), bs && (ui.position.top = inst._convertPositionTo("relative", {
          top: b - inst.helperProportions.height,
          left: 0
        }).top - inst.margins.top), ls && (ui.position.left = inst._convertPositionTo("relative", {
          top: 0,
          left: l
        }).left - inst.margins.left), rs && (ui.position.left = inst._convertPositionTo("relative", {
          top: 0,
          left: r - inst.helperProportions.width
        }).left - inst.margins.left)), !inst.snapElements[i].snapping && (ts || bs || ls || rs || first) && inst.options.snap.snap && inst.options.snap.snap.call(inst.element, event, $.extend(inst._uiHash(), {
          snapItem: inst.snapElements[i].item
        })), inst.snapElements[i].snapping = ts || bs || ls || rs || first)
      }
    }), $.ui.plugin.add("draggable", "stack", {
      start: function(event, ui, instance) {
        var min, o = instance.options,
          group = $.makeArray($(o.stack)).sort(function(a, b) {
            return (parseInt($(a).css("zIndex"), 10) || 0) - (parseInt($(b).css("zIndex"), 10) || 0)
          });
        group.length && (min = parseInt($(group[0]).css("zIndex"), 10) || 0, $(group).each(function(i) {
          $(this).css("zIndex", min + i)
        }), this.css("zIndex", min + group.length))
      }
    }), $.ui.plugin.add("draggable", "zIndex", {
      start: function(event, ui, instance) {
        var t = $(ui.helper),
          o = instance.options;
        t.css("zIndex") && (o._zIndex = t.css("zIndex")), t.css("zIndex", o.zIndex)
      },
      stop: function(event, ui, instance) {
        var o = instance.options;
        o._zIndex && $(ui.helper).css("zIndex", o._zIndex)
      }
    });
    $.ui.draggable;
    $.widget("ui.droppable", {
      version: "1.11.0",
      widgetEventPrefix: "drop",
      options: {
        accept: "*",
        activeClass: !1,
        addClasses: !0,
        greedy: !1,
        hoverClass: !1,
        scope: "default",
        tolerance: "intersect",
        activate: null,
        deactivate: null,
        drop: null,
        out: null,
        over: null
      },
      _create: function() {
        var proportions, o = this.options,
          accept = o.accept;
        this.isover = !1, this.isout = !0, this.accept = $.isFunction(accept) ? accept : function(d) {
          return d.is(accept)
        }, this.proportions = function() {
          return arguments.length ? void(proportions = arguments[0]) : proportions ? proportions : proportions = {
            width: this.element[0].offsetWidth,
            height: this.element[0].offsetHeight
          }
        }, this._addToManager(o.scope), o.addClasses && this.element.addClass("ui-droppable")
      },
      _addToManager: function(scope) {
        $.ui.ddmanager.droppables[scope] = $.ui.ddmanager.droppables[scope] || [], $.ui.ddmanager.droppables[scope].push(this)
      },
      _splice: function(drop) {
        for (var i = 0; i < drop.length; i++) drop[i] === this && drop.splice(i, 1)
      },
      _destroy: function() {
        var drop = $.ui.ddmanager.droppables[this.options.scope];
        this._splice(drop), this.element.removeClass("ui-droppable ui-droppable-disabled")
      },
      _setOption: function(key, value) {
        if ("accept" === key) this.accept = $.isFunction(value) ? value : function(d) {
          return d.is(value)
        };
        else if ("scope" === key) {
          var drop = $.ui.ddmanager.droppables[this.options.scope];
          this._splice(drop), this._addToManager(value)
        }
        this._super(key, value)
      },
      _activate: function(event) {
        var draggable = $.ui.ddmanager.current;
        this.options.activeClass && this.element.addClass(this.options.activeClass), draggable && this._trigger("activate", event, this.ui(draggable))
      },
      _deactivate: function(event) {
        var draggable = $.ui.ddmanager.current;
        this.options.activeClass && this.element.removeClass(this.options.activeClass), draggable && this._trigger("deactivate", event, this.ui(draggable))
      },
      _over: function(event) {
        var draggable = $.ui.ddmanager.current;
        draggable && (draggable.currentItem || draggable.element)[0] !== this.element[0] && this.accept.call(this.element[0], draggable.currentItem || draggable.element) && (this.options.hoverClass && this.element.addClass(this.options.hoverClass), this._trigger("over", event, this.ui(draggable)))
      },
      _out: function(event) {
        var draggable = $.ui.ddmanager.current;
        draggable && (draggable.currentItem || draggable.element)[0] !== this.element[0] && this.accept.call(this.element[0], draggable.currentItem || draggable.element) && (this.options.hoverClass && this.element.removeClass(this.options.hoverClass), this._trigger("out", event, this.ui(draggable)))
      },
      _drop: function(event, custom) {
        var draggable = custom || $.ui.ddmanager.current,
          childrenIntersection = !1;
        return draggable && (draggable.currentItem || draggable.element)[0] !== this.element[0] ? (this.element.find(":data(ui-droppable)").not(".ui-draggable-dragging").each(function() {
          var inst = $(this).droppable("instance");
          return inst.options.greedy && !inst.options.disabled && inst.options.scope === draggable.options.scope && inst.accept.call(inst.element[0], draggable.currentItem || draggable.element) && $.ui.intersect(draggable, $.extend(inst, {
            offset: inst.element.offset()
          }), inst.options.tolerance) ? (childrenIntersection = !0, !1) : void 0
        }), childrenIntersection ? !1 : this.accept.call(this.element[0], draggable.currentItem || draggable.element) ? (this.options.activeClass && this.element.removeClass(this.options.activeClass), this.options.hoverClass && this.element.removeClass(this.options.hoverClass), this._trigger("drop", event, this.ui(draggable)), this.element) : !1) : !1
      },
      ui: function(c) {
        return {
          draggable: c.currentItem || c.element,
          helper: c.helper,
          position: c.position,
          offset: c.positionAbs
        }
      }
    }), $.ui.intersect = function() {
      function isOverAxis(x, reference, size) {
        return x >= reference && reference + size > x
      }
      return function(draggable, droppable, toleranceMode) {
        if (!droppable.offset) return !1;
        var draggableLeft, draggableTop, x1 = (draggable.positionAbs || draggable.position.absolute).left,
          y1 = (draggable.positionAbs || draggable.position.absolute).top,
          x2 = x1 + draggable.helperProportions.width,
          y2 = y1 + draggable.helperProportions.height,
          l = droppable.offset.left,
          t = droppable.offset.top,
          r = l + droppable.proportions().width,
          b = t + droppable.proportions().height;
        switch (toleranceMode) {
          case "fit":
            return x1 >= l && r >= x2 && y1 >= t && b >= y2;
          case "intersect":
            return l < x1 + draggable.helperProportions.width / 2 && x2 - draggable.helperProportions.width / 2 < r && t < y1 + draggable.helperProportions.height / 2 && y2 - draggable.helperProportions.height / 2 < b;
          case "pointer":
            return draggableLeft = (draggable.positionAbs || draggable.position.absolute).left + (draggable.clickOffset || draggable.offset.click).left, draggableTop = (draggable.positionAbs || draggable.position.absolute).top + (draggable.clickOffset || draggable.offset.click).top, isOverAxis(draggableTop, t, droppable.proportions().height) && isOverAxis(draggableLeft, l, droppable.proportions().width);
          case "touch":
            return (y1 >= t && b >= y1 || y2 >= t && b >= y2 || t > y1 && y2 > b) && (x1 >= l && r >= x1 || x2 >= l && r >= x2 || l > x1 && x2 > r);
          default:
            return !1
        }
      }
    }(), $.ui.ddmanager = {
      current: null,
      droppables: {
        "default": []
      },
      prepareOffsets: function(t, event) {
        var i, j, m = $.ui.ddmanager.droppables[t.options.scope] || [],
          type = event ? event.type : null,
          list = (t.currentItem || t.element).find(":data(ui-droppable)").addBack();
        droppablesLoop: for (i = 0; i < m.length; i++)
          if (!(m[i].options.disabled || t && !m[i].accept.call(m[i].element[0], t.currentItem || t.element))) {
            for (j = 0; j < list.length; j++)
              if (list[j] === m[i].element[0]) {
                m[i].proportions().height = 0;
                continue droppablesLoop
              } m[i].visible = "none" !== m[i].element.css("display"), m[i].visible && ("mousedown" === type && m[i]._activate.call(m[i], event), m[i].offset = m[i].element.offset(), m[i].proportions({
              width: m[i].element[0].offsetWidth,
              height: m[i].element[0].offsetHeight
            }))
          }
      },
      drop: function(draggable, event) {
        var dropped = !1;
        return $.each(($.ui.ddmanager.droppables[draggable.options.scope] || []).slice(), function() {
          this.options && (!this.options.disabled && this.visible && $.ui.intersect(draggable, this, this.options.tolerance) && (dropped = this._drop.call(this, event) || dropped), !this.options.disabled && this.visible && this.accept.call(this.element[0], draggable.currentItem || draggable.element) && (this.isout = !0, this.isover = !1, this._deactivate.call(this, event)))
        }), dropped
      },
      dragStart: function(draggable, event) {
        draggable.element.parentsUntil("body").bind("scroll.droppable", function() {
          draggable.options.refreshPositions || $.ui.ddmanager.prepareOffsets(draggable, event)
        })
      },
      drag: function(draggable, event) {
        draggable.options.refreshPositions && $.ui.ddmanager.prepareOffsets(draggable, event), $.each($.ui.ddmanager.droppables[draggable.options.scope] || [], function() {
          if (!this.options.disabled && !this.greedyChild && this.visible) {
            var parentInstance, scope, parent, intersects = $.ui.intersect(draggable, this, this.options.tolerance),
              c = !intersects && this.isover ? "isout" : intersects && !this.isover ? "isover" : null;
            c && (this.options.greedy && (scope = this.options.scope, parent = this.element.parents(":data(ui-droppable)").filter(function() {
              return $(this).droppable("instance").options.scope === scope
            }), parent.length && (parentInstance = $(parent[0]).droppable("instance"), parentInstance.greedyChild = "isover" === c)), parentInstance && "isover" === c && (parentInstance.isover = !1, parentInstance.isout = !0, parentInstance._out.call(parentInstance, event)), this[c] = !0, this["isout" === c ? "isover" : "isout"] = !1, this["isover" === c ? "_over" : "_out"].call(this, event), parentInstance && "isout" === c && (parentInstance.isout = !1, parentInstance.isover = !0, parentInstance._over.call(parentInstance, event)))
          }
        })
      },
      dragStop: function(draggable, event) {
        draggable.element.parentsUntil("body").unbind("scroll.droppable"), draggable.options.refreshPositions || $.ui.ddmanager.prepareOffsets(draggable, event)
      }
    };
    $.ui.droppable;
    $.widget("ui.resizable", $.ui.mouse, {
      version: "1.11.0",
      widgetEventPrefix: "resize",
      options: {
        alsoResize: !1,
        animate: !1,
        animateDuration: "slow",
        animateEasing: "swing",
        aspectRatio: !1,
        autoHide: !1,
        containment: !1,
        ghost: !1,
        grid: !1,
        handles: "e,s,se",
        helper: !1,
        maxHeight: null,
        maxWidth: null,
        minHeight: 10,
        minWidth: 10,
        zIndex: 90,
        resize: null,
        start: null,
        stop: null
      },
      _num: function(value) {
        return parseInt(value, 10) || 0
      },
      _isNumber: function(value) {
        return !isNaN(parseInt(value, 10))
      },
      _hasScroll: function(el, a) {
        if ("hidden" === $(el).css("overflow")) return !1;
        var scroll = a && "left" === a ? "scrollLeft" : "scrollTop",
          has = !1;
        return el[scroll] > 0 ? !0 : (el[scroll] = 1, has = el[scroll] > 0, el[scroll] = 0, has)
      },
      _create: function() {
        var n, i, handle, axis, hname, that = this,
          o = this.options;
        if (this.element.addClass("ui-resizable"), $.extend(this, {
            _aspectRatio: !!o.aspectRatio,
            aspectRatio: o.aspectRatio,
            originalElement: this.element,
            _proportionallyResizeElements: [],
            _helper: o.helper || o.ghost || o.animate ? o.helper || "ui-resizable-helper" : null
          }), this.element[0].nodeName.match(/canvas|textarea|input|select|button|img/i) && (this.element.wrap($("<div class='ui-wrapper' style='overflow: hidden;'></div>").css({
            position: this.element.css("position"),
            width: this.element.outerWidth(),
            height: this.element.outerHeight(),
            top: this.element.css("top"),
            left: this.element.css("left")
          })), this.element = this.element.parent().data("ui-resizable", this.element.resizable("instance")), this.elementIsWrapper = !0, this.element.css({
            marginLeft: this.originalElement.css("marginLeft"),
            marginTop: this.originalElement.css("marginTop"),
            marginRight: this.originalElement.css("marginRight"),
            marginBottom: this.originalElement.css("marginBottom")
          }), this.originalElement.css({
            marginLeft: 0,
            marginTop: 0,
            marginRight: 0,
            marginBottom: 0
          }), this.originalResizeStyle = this.originalElement.css("resize"), this.originalElement.css("resize", "none"), this._proportionallyResizeElements.push(this.originalElement.css({
            position: "static",
            zoom: 1,
            display: "block"
          })), this.originalElement.css({
            margin: this.originalElement.css("margin")
          }), this._proportionallyResize()), this.handles = o.handles || ($(".ui-resizable-handle", this.element).length ? {
            n: ".ui-resizable-n",
            e: ".ui-resizable-e",
            s: ".ui-resizable-s",
            w: ".ui-resizable-w",
            se: ".ui-resizable-se",
            sw: ".ui-resizable-sw",
            ne: ".ui-resizable-ne",
            nw: ".ui-resizable-nw"
          } : "e,s,se"), this.handles.constructor === String)
          for ("all" === this.handles && (this.handles = "n,e,s,w,se,sw,ne,nw"), n = this.handles.split(","), this.handles = {}, i = 0; i < n.length; i++) handle = $.trim(n[i]), hname = "ui-resizable-" + handle, axis = $("<div class='ui-resizable-handle " + hname + "'></div>"), axis.css({
            zIndex: o.zIndex
          }), "se" === handle && axis.addClass("ui-icon ui-icon-gripsmall-diagonal-se"), this.handles[handle] = ".ui-resizable-" + handle, this.element.append(axis);
        this._renderAxis = function(target) {
          var i, axis, padPos, padWrapper;
          target = target || this.element;
          for (i in this.handles) this.handles[i].constructor === String && (this.handles[i] = this.element.children(this.handles[i]).first().show()), this.elementIsWrapper && this.originalElement[0].nodeName.match(/textarea|input|select|button/i) && (axis = $(this.handles[i], this.element), padWrapper = /sw|ne|nw|se|n|s/.test(i) ? axis.outerHeight() : axis.outerWidth(), padPos = ["padding", /ne|nw|n/.test(i) ? "Top" : /se|sw|s/.test(i) ? "Bottom" : /^e$/.test(i) ? "Right" : "Left"].join(""), target.css(padPos, padWrapper), this._proportionallyResize()), $(this.handles[i]).length
        }, this._renderAxis(this.element), this._handles = $(".ui-resizable-handle", this.element).disableSelection(), this._handles.mouseover(function() {
          that.resizing || (this.className && (axis = this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i)), that.axis = axis && axis[1] ? axis[1] : "se")
        }), o.autoHide && (this._handles.hide(), $(this.element).addClass("ui-resizable-autohide").mouseenter(function() {
          o.disabled || ($(this).removeClass("ui-resizable-autohide"), that._handles.show())
        }).mouseleave(function() {
          o.disabled || that.resizing || ($(this).addClass("ui-resizable-autohide"), that._handles.hide())
        })), this._mouseInit()
      },
      _destroy: function() {
        this._mouseDestroy();
        var wrapper, _destroy = function(exp) {
          $(exp).removeClass("ui-resizable ui-resizable-disabled ui-resizable-resizing").removeData("resizable").removeData("ui-resizable").unbind(".resizable").find(".ui-resizable-handle").remove()
        };
        return this.elementIsWrapper && (_destroy(this.element), wrapper = this.element, this.originalElement.css({
          position: wrapper.css("position"),
          width: wrapper.outerWidth(),
          height: wrapper.outerHeight(),
          top: wrapper.css("top"),
          left: wrapper.css("left")
        }).insertAfter(wrapper), wrapper.remove()), this.originalElement.css("resize", this.originalResizeStyle), _destroy(this.originalElement), this
      },
      _mouseCapture: function(event) {
        var i, handle, capture = !1;
        for (i in this.handles) handle = $(this.handles[i])[0], (handle === event.target || $.contains(handle, event.target)) && (capture = !0);
        return !this.options.disabled && capture
      },
      _mouseStart: function(event) {
        var curleft, curtop, cursor, o = this.options,
          el = this.element;
        return this.resizing = !0, this._renderProxy(), curleft = this._num(this.helper.css("left")), curtop = this._num(this.helper.css("top")), o.containment && (curleft += $(o.containment).scrollLeft() || 0, curtop += $(o.containment).scrollTop() || 0), this.offset = this.helper.offset(), this.position = {
          left: curleft,
          top: curtop
        }, this.size = this._helper ? {
          width: this.helper.width(),
          height: this.helper.height()
        } : {
          width: el.width(),
          height: el.height()
        }, this.originalSize = this._helper ? {
          width: el.outerWidth(),
          height: el.outerHeight()
        } : {
          width: el.width(),
          height: el.height()
        }, this.originalPosition = {
          left: curleft,
          top: curtop
        }, this.sizeDiff = {
          width: el.outerWidth() - el.width(),
          height: el.outerHeight() - el.height()
        }, this.originalMousePosition = {
          left: event.pageX,
          top: event.pageY
        }, this.aspectRatio = "number" == typeof o.aspectRatio ? o.aspectRatio : this.originalSize.width / this.originalSize.height || 1, cursor = $(".ui-resizable-" + this.axis).css("cursor"), $("body").css("cursor", "auto" === cursor ? this.axis + "-resize" : cursor), el.addClass("ui-resizable-resizing"), this._propagate("start", event), !0
      },
      _mouseDrag: function(event) {
        var data, el = this.helper,
          props = {},
          smp = this.originalMousePosition,
          a = this.axis,
          dx = event.pageX - smp.left || 0,
          dy = event.pageY - smp.top || 0,
          trigger = this._change[a];
        return this.prevPosition = {
          top: this.position.top,
          left: this.position.left
        }, this.prevSize = {
          width: this.size.width,
          height: this.size.height
        }, trigger ? (data = trigger.apply(this, [event, dx, dy]), this._updateVirtualBoundaries(event.shiftKey), (this._aspectRatio || event.shiftKey) && (data = this._updateRatio(data, event)), data = this._respectSize(data, event), this._updateCache(data), this._propagate("resize", event), this.position.top !== this.prevPosition.top && (props.top = this.position.top + "px"), this.position.left !== this.prevPosition.left && (props.left = this.position.left + "px"), this.size.width !== this.prevSize.width && (props.width = this.size.width + "px"), this.size.height !== this.prevSize.height && (props.height = this.size.height + "px"), el.css(props), !this._helper && this._proportionallyResizeElements.length && this._proportionallyResize(), $.isEmptyObject(props) || this._trigger("resize", event, this.ui()), !1) : !1
      },
      _mouseStop: function(event) {
        this.resizing = !1;
        var pr, ista, soffseth, soffsetw, s, left, top, o = this.options,
          that = this;
        return this._helper && (pr = this._proportionallyResizeElements, ista = pr.length && /textarea/i.test(pr[0].nodeName), soffseth = ista && this._hasScroll(pr[0], "left") ? 0 : that.sizeDiff.height, soffsetw = ista ? 0 : that.sizeDiff.width, s = {
            width: that.helper.width() - soffsetw,
            height: that.helper.height() - soffseth
          }, left = parseInt(that.element.css("left"), 10) + (that.position.left - that.originalPosition.left) || null,
          top = parseInt(that.element.css("top"), 10) + (that.position.top - that.originalPosition.top) || null, o.animate || this.element.css($.extend(s, {
            top: top,
            left: left
          })), that.helper.height(that.size.height), that.helper.width(that.size.width), this._helper && !o.animate && this._proportionallyResize()), $("body").css("cursor", "auto"), this.element.removeClass("ui-resizable-resizing"), this._propagate("stop", event), this._helper && this.helper.remove(), !1
      },
      _updateVirtualBoundaries: function(forceAspectRatio) {
        var pMinWidth, pMaxWidth, pMinHeight, pMaxHeight, b, o = this.options;
        b = {
          minWidth: this._isNumber(o.minWidth) ? o.minWidth : 0,
          maxWidth: this._isNumber(o.maxWidth) ? o.maxWidth : 1 / 0,
          minHeight: this._isNumber(o.minHeight) ? o.minHeight : 0,
          maxHeight: this._isNumber(o.maxHeight) ? o.maxHeight : 1 / 0
        }, (this._aspectRatio || forceAspectRatio) && (pMinWidth = b.minHeight * this.aspectRatio, pMinHeight = b.minWidth / this.aspectRatio, pMaxWidth = b.maxHeight * this.aspectRatio, pMaxHeight = b.maxWidth / this.aspectRatio, pMinWidth > b.minWidth && (b.minWidth = pMinWidth), pMinHeight > b.minHeight && (b.minHeight = pMinHeight), pMaxWidth < b.maxWidth && (b.maxWidth = pMaxWidth), pMaxHeight < b.maxHeight && (b.maxHeight = pMaxHeight)), this._vBoundaries = b
      },
      _updateCache: function(data) {
        this.offset = this.helper.offset(), this._isNumber(data.left) && (this.position.left = data.left), this._isNumber(data.top) && (this.position.top = data.top), this._isNumber(data.height) && (this.size.height = data.height), this._isNumber(data.width) && (this.size.width = data.width)
      },
      _updateRatio: function(data) {
        var cpos = this.position,
          csize = this.size,
          a = this.axis;
        return this._isNumber(data.height) ? data.width = data.height * this.aspectRatio : this._isNumber(data.width) && (data.height = data.width / this.aspectRatio), "sw" === a && (data.left = cpos.left + (csize.width - data.width), data.top = null), "nw" === a && (data.top = cpos.top + (csize.height - data.height), data.left = cpos.left + (csize.width - data.width)), data
      },
      _respectSize: function(data) {
        var o = this._vBoundaries,
          a = this.axis,
          ismaxw = this._isNumber(data.width) && o.maxWidth && o.maxWidth < data.width,
          ismaxh = this._isNumber(data.height) && o.maxHeight && o.maxHeight < data.height,
          isminw = this._isNumber(data.width) && o.minWidth && o.minWidth > data.width,
          isminh = this._isNumber(data.height) && o.minHeight && o.minHeight > data.height,
          dw = this.originalPosition.left + this.originalSize.width,
          dh = this.position.top + this.size.height,
          cw = /sw|nw|w/.test(a),
          ch = /nw|ne|n/.test(a);
        return isminw && (data.width = o.minWidth), isminh && (data.height = o.minHeight), ismaxw && (data.width = o.maxWidth), ismaxh && (data.height = o.maxHeight), isminw && cw && (data.left = dw - o.minWidth), ismaxw && cw && (data.left = dw - o.maxWidth), isminh && ch && (data.top = dh - o.minHeight), ismaxh && ch && (data.top = dh - o.maxHeight), data.width || data.height || data.left || !data.top ? data.width || data.height || data.top || !data.left || (data.left = null) : data.top = null, data
      },
      _proportionallyResize: function() {
        if (this._proportionallyResizeElements.length) {
          var i, j, borders, paddings, prel, element = this.helper || this.element;
          for (i = 0; i < this._proportionallyResizeElements.length; i++) {
            if (prel = this._proportionallyResizeElements[i], !this.borderDif)
              for (this.borderDif = [], borders = [prel.css("borderTopWidth"), prel.css("borderRightWidth"), prel.css("borderBottomWidth"), prel.css("borderLeftWidth")], paddings = [prel.css("paddingTop"), prel.css("paddingRight"), prel.css("paddingBottom"), prel.css("paddingLeft")], j = 0; j < borders.length; j++) this.borderDif[j] = (parseInt(borders[j], 10) || 0) + (parseInt(paddings[j], 10) || 0);
            prel.css({
              height: element.height() - this.borderDif[0] - this.borderDif[2] || 0,
              width: element.width() - this.borderDif[1] - this.borderDif[3] || 0
            })
          }
        }
      },
      _renderProxy: function() {
        var el = this.element,
          o = this.options;
        this.elementOffset = el.offset(), this._helper ? (this.helper = this.helper || $("<div style='overflow:hidden;'></div>"), this.helper.addClass(this._helper).css({
          width: this.element.outerWidth() - 1,
          height: this.element.outerHeight() - 1,
          position: "absolute",
          left: this.elementOffset.left + "px",
          top: this.elementOffset.top + "px",
          zIndex: ++o.zIndex
        }), this.helper.appendTo("body").disableSelection()) : this.helper = this.element
      },
      _change: {
        e: function(event, dx) {
          return {
            width: this.originalSize.width + dx
          }
        },
        w: function(event, dx) {
          var cs = this.originalSize,
            sp = this.originalPosition;
          return {
            left: sp.left + dx,
            width: cs.width - dx
          }
        },
        n: function(event, dx, dy) {
          var cs = this.originalSize,
            sp = this.originalPosition;
          return {
            top: sp.top + dy,
            height: cs.height - dy
          }
        },
        s: function(event, dx, dy) {
          return {
            height: this.originalSize.height + dy
          }
        },
        se: function(event, dx, dy) {
          return $.extend(this._change.s.apply(this, arguments), this._change.e.apply(this, [event, dx, dy]))
        },
        sw: function(event, dx, dy) {
          return $.extend(this._change.s.apply(this, arguments), this._change.w.apply(this, [event, dx, dy]))
        },
        ne: function(event, dx, dy) {
          return $.extend(this._change.n.apply(this, arguments), this._change.e.apply(this, [event, dx, dy]))
        },
        nw: function(event, dx, dy) {
          return $.extend(this._change.n.apply(this, arguments), this._change.w.apply(this, [event, dx, dy]))
        }
      },
      _propagate: function(n, event) {
        $.ui.plugin.call(this, n, [event, this.ui()]), "resize" !== n && this._trigger(n, event, this.ui())
      },
      plugins: {},
      ui: function() {
        return {
          originalElement: this.originalElement,
          element: this.element,
          helper: this.helper,
          position: this.position,
          size: this.size,
          originalSize: this.originalSize,
          originalPosition: this.originalPosition,
          prevSize: this.prevSize,
          prevPosition: this.prevPosition
        }
      }
    }), $.ui.plugin.add("resizable", "animate", {
      stop: function(event) {
        var that = $(this).resizable("instance"),
          o = that.options,
          pr = that._proportionallyResizeElements,
          ista = pr.length && /textarea/i.test(pr[0].nodeName),
          soffseth = ista && that._hasScroll(pr[0], "left") ? 0 : that.sizeDiff.height,
          soffsetw = ista ? 0 : that.sizeDiff.width,
          style = {
            width: that.size.width - soffsetw,
            height: that.size.height - soffseth
          },
          left = parseInt(that.element.css("left"), 10) + (that.position.left - that.originalPosition.left) || null,
          top = parseInt(that.element.css("top"), 10) + (that.position.top - that.originalPosition.top) || null;
        that.element.animate($.extend(style, top && left ? {
          top: top,
          left: left
        } : {}), {
          duration: o.animateDuration,
          easing: o.animateEasing,
          step: function() {
            var data = {
              width: parseInt(that.element.css("width"), 10),
              height: parseInt(that.element.css("height"), 10),
              top: parseInt(that.element.css("top"), 10),
              left: parseInt(that.element.css("left"), 10)
            };
            pr && pr.length && $(pr[0]).css({
              width: data.width,
              height: data.height
            }), that._updateCache(data), that._propagate("resize", event)
          }
        })
      }
    }), $.ui.plugin.add("resizable", "containment", {
      start: function() {
        var element, p, co, ch, cw, width, height, that = $(this).resizable("instance"),
          o = that.options,
          el = that.element,
          oc = o.containment,
          ce = oc instanceof $ ? oc.get(0) : /parent/.test(oc) ? el.parent().get(0) : oc;
        ce && (that.containerElement = $(ce), /document/.test(oc) || oc === document ? (that.containerOffset = {
          left: 0,
          top: 0
        }, that.containerPosition = {
          left: 0,
          top: 0
        }, that.parentData = {
          element: $(document),
          left: 0,
          top: 0,
          width: $(document).width(),
          height: $(document).height() || document.body.parentNode.scrollHeight
        }) : (element = $(ce), p = [], $(["Top", "Right", "Left", "Bottom"]).each(function(i, name) {
          p[i] = that._num(element.css("padding" + name))
        }), that.containerOffset = element.offset(), that.containerPosition = element.position(), that.containerSize = {
          height: element.innerHeight() - p[3],
          width: element.innerWidth() - p[1]
        }, co = that.containerOffset, ch = that.containerSize.height, cw = that.containerSize.width, width = that._hasScroll(ce, "left") ? ce.scrollWidth : cw, height = that._hasScroll(ce) ? ce.scrollHeight : ch, that.parentData = {
          element: ce,
          left: co.left,
          top: co.top,
          width: width,
          height: height
        }))
      },
      resize: function(event, ui) {
        var woset, hoset, isParent, isOffsetRelative, that = $(this).resizable("instance"),
          o = that.options,
          co = that.containerOffset,
          cp = that.position,
          pRatio = that._aspectRatio || event.shiftKey,
          cop = {
            top: 0,
            left: 0
          },
          ce = that.containerElement,
          continueResize = !0;
        ce[0] !== document && /static/.test(ce.css("position")) && (cop = co), cp.left < (that._helper ? co.left : 0) && (that.size.width = that.size.width + (that._helper ? that.position.left - co.left : that.position.left - cop.left), pRatio && (that.size.height = that.size.width / that.aspectRatio, continueResize = !1), that.position.left = o.helper ? co.left : 0), cp.top < (that._helper ? co.top : 0) && (that.size.height = that.size.height + (that._helper ? that.position.top - co.top : that.position.top), pRatio && (that.size.width = that.size.height * that.aspectRatio, continueResize = !1), that.position.top = that._helper ? co.top : 0), that.offset.left = that.parentData.left + that.position.left, that.offset.top = that.parentData.top + that.position.top, woset = Math.abs((that._helper ? that.offset.left - cop.left : that.offset.left - co.left) + that.sizeDiff.width), hoset = Math.abs((that._helper ? that.offset.top - cop.top : that.offset.top - co.top) + that.sizeDiff.height), isParent = that.containerElement.get(0) === that.element.parent().get(0), isOffsetRelative = /relative|absolute/.test(that.containerElement.css("position")), isParent && isOffsetRelative && (woset -= Math.abs(that.parentData.left)), woset + that.size.width >= that.parentData.width && (that.size.width = that.parentData.width - woset, pRatio && (that.size.height = that.size.width / that.aspectRatio, continueResize = !1)), hoset + that.size.height >= that.parentData.height && (that.size.height = that.parentData.height - hoset, pRatio && (that.size.width = that.size.height * that.aspectRatio, continueResize = !1)), continueResize || (that.position.left = ui.prevPosition.left, that.position.top = ui.prevPosition.top, that.size.width = ui.prevSize.width, that.size.height = ui.prevSize.height)
      },
      stop: function() {
        var that = $(this).resizable("instance"),
          o = that.options,
          co = that.containerOffset,
          cop = that.containerPosition,
          ce = that.containerElement,
          helper = $(that.helper),
          ho = helper.offset(),
          w = helper.outerWidth() - that.sizeDiff.width,
          h = helper.outerHeight() - that.sizeDiff.height;
        that._helper && !o.animate && /relative/.test(ce.css("position")) && $(this).css({
          left: ho.left - cop.left - co.left,
          width: w,
          height: h
        }), that._helper && !o.animate && /static/.test(ce.css("position")) && $(this).css({
          left: ho.left - cop.left - co.left,
          width: w,
          height: h
        })
      }
    }), $.ui.plugin.add("resizable", "alsoResize", {
      start: function() {
        var that = $(this).resizable("instance"),
          o = that.options,
          _store = function(exp) {
            $(exp).each(function() {
              var el = $(this);
              el.data("ui-resizable-alsoresize", {
                width: parseInt(el.width(), 10),
                height: parseInt(el.height(), 10),
                left: parseInt(el.css("left"), 10),
                top: parseInt(el.css("top"), 10)
              })
            })
          };
        "object" != typeof o.alsoResize || o.alsoResize.parentNode ? _store(o.alsoResize) : o.alsoResize.length ? (o.alsoResize = o.alsoResize[0], _store(o.alsoResize)) : $.each(o.alsoResize, function(exp) {
          _store(exp)
        })
      },
      resize: function(event, ui) {
        var that = $(this).resizable("instance"),
          o = that.options,
          os = that.originalSize,
          op = that.originalPosition,
          delta = {
            height: that.size.height - os.height || 0,
            width: that.size.width - os.width || 0,
            top: that.position.top - op.top || 0,
            left: that.position.left - op.left || 0
          },
          _alsoResize = function(exp, c) {
            $(exp).each(function() {
              var el = $(this),
                start = $(this).data("ui-resizable-alsoresize"),
                style = {},
                css = c && c.length ? c : el.parents(ui.originalElement[0]).length ? ["width", "height"] : ["width", "height", "top", "left"];
              $.each(css, function(i, prop) {
                var sum = (start[prop] || 0) + (delta[prop] || 0);
                sum && sum >= 0 && (style[prop] = sum || null)
              }), el.css(style)
            })
          };
        "object" != typeof o.alsoResize || o.alsoResize.nodeType ? _alsoResize(o.alsoResize) : $.each(o.alsoResize, function(exp, c) {
          _alsoResize(exp, c)
        })
      },
      stop: function() {
        $(this).removeData("resizable-alsoresize")
      }
    }), $.ui.plugin.add("resizable", "ghost", {
      start: function() {
        var that = $(this).resizable("instance"),
          o = that.options,
          cs = that.size;
        that.ghost = that.originalElement.clone(), that.ghost.css({
          opacity: .25,
          display: "block",
          position: "relative",
          height: cs.height,
          width: cs.width,
          margin: 0,
          left: 0,
          top: 0
        }).addClass("ui-resizable-ghost").addClass("string" == typeof o.ghost ? o.ghost : ""), that.ghost.appendTo(that.helper)
      },
      resize: function() {
        var that = $(this).resizable("instance");
        that.ghost && that.ghost.css({
          position: "relative",
          height: that.size.height,
          width: that.size.width
        })
      },
      stop: function() {
        var that = $(this).resizable("instance");
        that.ghost && that.helper && that.helper.get(0).removeChild(that.ghost.get(0))
      }
    }), $.ui.plugin.add("resizable", "grid", {
      resize: function() {
        var that = $(this).resizable("instance"),
          o = that.options,
          cs = that.size,
          os = that.originalSize,
          op = that.originalPosition,
          a = that.axis,
          grid = "number" == typeof o.grid ? [o.grid, o.grid] : o.grid,
          gridX = grid[0] || 1,
          gridY = grid[1] || 1,
          ox = Math.round((cs.width - os.width) / gridX) * gridX,
          oy = Math.round((cs.height - os.height) / gridY) * gridY,
          newWidth = os.width + ox,
          newHeight = os.height + oy,
          isMaxWidth = o.maxWidth && o.maxWidth < newWidth,
          isMaxHeight = o.maxHeight && o.maxHeight < newHeight,
          isMinWidth = o.minWidth && o.minWidth > newWidth,
          isMinHeight = o.minHeight && o.minHeight > newHeight;
        o.grid = grid, isMinWidth && (newWidth += gridX), isMinHeight && (newHeight += gridY), isMaxWidth && (newWidth -= gridX), isMaxHeight && (newHeight -= gridY), /^(se|s|e)$/.test(a) ? (that.size.width = newWidth, that.size.height = newHeight) : /^(ne)$/.test(a) ? (that.size.width = newWidth, that.size.height = newHeight, that.position.top = op.top - oy) : /^(sw)$/.test(a) ? (that.size.width = newWidth, that.size.height = newHeight, that.position.left = op.left - ox) : (newHeight - gridY > 0 ? (that.size.height = newHeight, that.position.top = op.top - oy) : (that.size.height = gridY, that.position.top = op.top + os.height - gridY), newWidth - gridX > 0 ? (that.size.width = newWidth, that.position.left = op.left - ox) : (that.size.width = gridX, that.position.left = op.left + os.width - gridX))
      }
    });
    $.ui.resizable, $.widget("ui.selectable", $.ui.mouse, {
      version: "1.11.0",
      options: {
        appendTo: "body",
        autoRefresh: !0,
        distance: 0,
        filter: "*",
        tolerance: "touch",
        selected: null,
        selecting: null,
        start: null,
        stop: null,
        unselected: null,
        unselecting: null
      },
      _create: function() {
        var selectees, that = this;
        this.element.addClass("ui-selectable"), this.dragged = !1, this.refresh = function() {
          selectees = $(that.options.filter, that.element[0]), selectees.addClass("ui-selectee"), selectees.each(function() {
            var $this = $(this),
              pos = $this.offset();
            $.data(this, "selectable-item", {
              element: this,
              $element: $this,
              left: pos.left,
              top: pos.top,
              right: pos.left + $this.outerWidth(),
              bottom: pos.top + $this.outerHeight(),
              startselected: !1,
              selected: $this.hasClass("ui-selected"),
              selecting: $this.hasClass("ui-selecting"),
              unselecting: $this.hasClass("ui-unselecting")
            })
          })
        }, this.refresh(), this.selectees = selectees.addClass("ui-selectee"), this._mouseInit(), this.helper = $("<div class='ui-selectable-helper'></div>")
      },
      _destroy: function() {
        this.selectees.removeClass("ui-selectee").removeData("selectable-item"), this.element.removeClass("ui-selectable ui-selectable-disabled"), this._mouseDestroy()
      },
      _mouseStart: function(event) {
        var that = this,
          options = this.options;
        this.opos = [event.pageX, event.pageY], this.options.disabled || (this.selectees = $(options.filter, this.element[0]), this._trigger("start", event), $(options.appendTo).append(this.helper), this.helper.css({
          left: event.pageX,
          top: event.pageY,
          width: 0,
          height: 0
        }), options.autoRefresh && this.refresh(), this.selectees.filter(".ui-selected").each(function() {
          var selectee = $.data(this, "selectable-item");
          selectee.startselected = !0, event.metaKey || event.ctrlKey || (selectee.$element.removeClass("ui-selected"), selectee.selected = !1, selectee.$element.addClass("ui-unselecting"), selectee.unselecting = !0, that._trigger("unselecting", event, {
            unselecting: selectee.element
          }))
        }), $(event.target).parents().addBack().each(function() {
          var doSelect, selectee = $.data(this, "selectable-item");
          return selectee ? (doSelect = !event.metaKey && !event.ctrlKey || !selectee.$element.hasClass("ui-selected"), selectee.$element.removeClass(doSelect ? "ui-unselecting" : "ui-selected").addClass(doSelect ? "ui-selecting" : "ui-unselecting"), selectee.unselecting = !doSelect, selectee.selecting = doSelect, selectee.selected = doSelect, doSelect ? that._trigger("selecting", event, {
            selecting: selectee.element
          }) : that._trigger("unselecting", event, {
            unselecting: selectee.element
          }), !1) : void 0
        }))
      },
      _mouseDrag: function(event) {
        if (this.dragged = !0, !this.options.disabled) {
          var tmp, that = this,
            options = this.options,
            x1 = this.opos[0],
            y1 = this.opos[1],
            x2 = event.pageX,
            y2 = event.pageY;
          return x1 > x2 && (tmp = x2, x2 = x1, x1 = tmp), y1 > y2 && (tmp = y2, y2 = y1, y1 = tmp), this.helper.css({
            left: x1,
            top: y1,
            width: x2 - x1,
            height: y2 - y1
          }), this.selectees.each(function() {
            var selectee = $.data(this, "selectable-item"),
              hit = !1;
            selectee && selectee.element !== that.element[0] && ("touch" === options.tolerance ? hit = !(selectee.left > x2 || selectee.right < x1 || selectee.top > y2 || selectee.bottom < y1) : "fit" === options.tolerance && (hit = selectee.left > x1 && selectee.right < x2 && selectee.top > y1 && selectee.bottom < y2), hit ? (selectee.selected && (selectee.$element.removeClass("ui-selected"), selectee.selected = !1), selectee.unselecting && (selectee.$element.removeClass("ui-unselecting"), selectee.unselecting = !1), selectee.selecting || (selectee.$element.addClass("ui-selecting"), selectee.selecting = !0, that._trigger("selecting", event, {
              selecting: selectee.element
            }))) : (selectee.selecting && ((event.metaKey || event.ctrlKey) && selectee.startselected ? (selectee.$element.removeClass("ui-selecting"), selectee.selecting = !1, selectee.$element.addClass("ui-selected"), selectee.selected = !0) : (selectee.$element.removeClass("ui-selecting"), selectee.selecting = !1, selectee.startselected && (selectee.$element.addClass("ui-unselecting"), selectee.unselecting = !0), that._trigger("unselecting", event, {
              unselecting: selectee.element
            }))), selectee.selected && (event.metaKey || event.ctrlKey || selectee.startselected || (selectee.$element.removeClass("ui-selected"), selectee.selected = !1, selectee.$element.addClass("ui-unselecting"), selectee.unselecting = !0, that._trigger("unselecting", event, {
              unselecting: selectee.element
            })))))
          }), !1
        }
      },
      _mouseStop: function(event) {
        var that = this;
        return this.dragged = !1, $(".ui-unselecting", this.element[0]).each(function() {
          var selectee = $.data(this, "selectable-item");
          selectee.$element.removeClass("ui-unselecting"), selectee.unselecting = !1, selectee.startselected = !1, that._trigger("unselected", event, {
            unselected: selectee.element
          })
        }), $(".ui-selecting", this.element[0]).each(function() {
          var selectee = $.data(this, "selectable-item");
          selectee.$element.removeClass("ui-selecting").addClass("ui-selected"), selectee.selecting = !1, selectee.selected = !0, selectee.startselected = !0, that._trigger("selected", event, {
            selected: selectee.element
          })
        }), this._trigger("stop", event), this.helper.remove(), !1
      }
    }), $.widget("ui.sortable", $.ui.mouse, {
      version: "1.11.0",
      widgetEventPrefix: "sort",
      ready: !1,
      options: {
        appendTo: "parent",
        axis: !1,
        connectWith: !1,
        containment: !1,
        cursor: "auto",
        cursorAt: !1,
        dropOnEmpty: !0,
        forcePlaceholderSize: !1,
        forceHelperSize: !1,
        grid: !1,
        handle: !1,
        helper: "original",
        items: "> *",
        opacity: !1,
        placeholder: !1,
        revert: !1,
        scroll: !0,
        scrollSensitivity: 20,
        scrollSpeed: 20,
        scope: "default",
        tolerance: "intersect",
        zIndex: 1e3,
        activate: null,
        beforeStop: null,
        change: null,
        deactivate: null,
        out: null,
        over: null,
        receive: null,
        remove: null,
        sort: null,
        start: null,
        stop: null,
        update: null
      },
      _isOverAxis: function(x, reference, size) {
        return x >= reference && reference + size > x
      },
      _isFloating: function(item) {
        return /left|right/.test(item.css("float")) || /inline|table-cell/.test(item.css("display"))
      },
      _create: function() {
        var o = this.options;
        this.containerCache = {}, this.element.addClass("ui-sortable"), this.refresh(), this.floating = this.items.length ? "x" === o.axis || this._isFloating(this.items[0].item) : !1, this.offset = this.element.offset(), this._mouseInit(), this._setHandleClassName(), this.ready = !0
      },
      _setOption: function(key, value) {
        this._super(key, value), "handle" === key && this._setHandleClassName()
      },
      _setHandleClassName: function() {
        this.element.find(".ui-sortable-handle").removeClass("ui-sortable-handle"), $.each(this.items, function() {
          (this.instance.options.handle ? this.item.find(this.instance.options.handle) : this.item).addClass("ui-sortable-handle")
        })
      },
      _destroy: function() {
        this.element.removeClass("ui-sortable ui-sortable-disabled").find(".ui-sortable-handle").removeClass("ui-sortable-handle"), this._mouseDestroy();
        for (var i = this.items.length - 1; i >= 0; i--) this.items[i].item.removeData(this.widgetName + "-item");
        return this
      },
      _mouseCapture: function(event, overrideHandle) {
        var currentItem = null,
          validHandle = !1,
          that = this;
        return this.reverting ? !1 : this.options.disabled || "static" === this.options.type ? !1 : (this._refreshItems(event), $(event.target).parents().each(function() {
          return $.data(this, that.widgetName + "-item") === that ? (currentItem = $(this), !1) : void 0
        }), $.data(event.target, that.widgetName + "-item") === that && (currentItem = $(event.target)), currentItem && (!this.options.handle || overrideHandle || ($(this.options.handle, currentItem).find("*").addBack().each(function() {
          this === event.target && (validHandle = !0)
        }), validHandle)) ? (this.currentItem = currentItem, this._removeCurrentsFromItems(), !0) : !1)
      },
      _mouseStart: function(event, overrideHandle, noActivation) {
        var i, body, o = this.options;
        if (this.currentContainer = this, this.refreshPositions(), this.helper = this._createHelper(event), this._cacheHelperProportions(), this._cacheMargins(), this.scrollParent = this.helper.scrollParent(), this.offset = this.currentItem.offset(), this.offset = {
            top: this.offset.top - this.margins.top,
            left: this.offset.left - this.margins.left
          }, $.extend(this.offset, {
            click: {
              left: event.pageX - this.offset.left,
              top: event.pageY - this.offset.top
            },
            parent: this._getParentOffset(),
            relative: this._getRelativeOffset()
          }), this.helper.css("position", "absolute"), this.cssPosition = this.helper.css("position"), this.originalPosition = this._generatePosition(event), this.originalPageX = event.pageX, this.originalPageY = event.pageY, o.cursorAt && this._adjustOffsetFromHelper(o.cursorAt), this.domPosition = {
            prev: this.currentItem.prev()[0],
            parent: this.currentItem.parent()[0]
          }, this.helper[0] !== this.currentItem[0] && this.currentItem.hide(), this._createPlaceholder(), o.containment && this._setContainment(), o.cursor && "auto" !== o.cursor && (body = this.document.find("body"), this.storedCursor = body.css("cursor"), body.css("cursor", o.cursor), this.storedStylesheet = $("<style>*{ cursor: " + o.cursor + " !important; }</style>").appendTo(body)), o.opacity && (this.helper.css("opacity") && (this._storedOpacity = this.helper.css("opacity")), this.helper.css("opacity", o.opacity)), o.zIndex && (this.helper.css("zIndex") && (this._storedZIndex = this.helper.css("zIndex")), this.helper.css("zIndex", o.zIndex)), this.scrollParent[0] !== document && "HTML" !== this.scrollParent[0].tagName && (this.overflowOffset = this.scrollParent.offset()), this._trigger("start", event, this._uiHash()), this._preserveHelperProportions || this._cacheHelperProportions(), !noActivation)
          for (i = this.containers.length - 1; i >= 0; i--) this.containers[i]._trigger("activate", event, this._uiHash(this));
        return $.ui.ddmanager && ($.ui.ddmanager.current = this), $.ui.ddmanager && !o.dropBehaviour && $.ui.ddmanager.prepareOffsets(this, event), this.dragging = !0, this.helper.addClass("ui-sortable-helper"), this._mouseDrag(event), !0
      },
      _mouseDrag: function(event) {
        var i, item, itemElement, intersection, o = this.options,
          scrolled = !1;
        for (this.position = this._generatePosition(event), this.positionAbs = this._convertPositionTo("absolute"), this.lastPositionAbs || (this.lastPositionAbs = this.positionAbs), this.options.scroll && (this.scrollParent[0] !== document && "HTML" !== this.scrollParent[0].tagName ? (this.overflowOffset.top + this.scrollParent[0].offsetHeight - event.pageY < o.scrollSensitivity ? this.scrollParent[0].scrollTop = scrolled = this.scrollParent[0].scrollTop + o.scrollSpeed : event.pageY - this.overflowOffset.top < o.scrollSensitivity && (this.scrollParent[0].scrollTop = scrolled = this.scrollParent[0].scrollTop - o.scrollSpeed), this.overflowOffset.left + this.scrollParent[0].offsetWidth - event.pageX < o.scrollSensitivity ? this.scrollParent[0].scrollLeft = scrolled = this.scrollParent[0].scrollLeft + o.scrollSpeed : event.pageX - this.overflowOffset.left < o.scrollSensitivity && (this.scrollParent[0].scrollLeft = scrolled = this.scrollParent[0].scrollLeft - o.scrollSpeed)) : (event.pageY - $(document).scrollTop() < o.scrollSensitivity ? scrolled = $(document).scrollTop($(document).scrollTop() - o.scrollSpeed) : $(window).height() - (event.pageY - $(document).scrollTop()) < o.scrollSensitivity && (scrolled = $(document).scrollTop($(document).scrollTop() + o.scrollSpeed)), event.pageX - $(document).scrollLeft() < o.scrollSensitivity ? scrolled = $(document).scrollLeft($(document).scrollLeft() - o.scrollSpeed) : $(window).width() - (event.pageX - $(document).scrollLeft()) < o.scrollSensitivity && (scrolled = $(document).scrollLeft($(document).scrollLeft() + o.scrollSpeed))), scrolled !== !1 && $.ui.ddmanager && !o.dropBehaviour && $.ui.ddmanager.prepareOffsets(this, event)), this.positionAbs = this._convertPositionTo("absolute"), this.options.axis && "y" === this.options.axis || (this.helper[0].style.left = this.position.left + "px"), this.options.axis && "x" === this.options.axis || (this.helper[0].style.top = this.position.top + "px"), i = this.items.length - 1; i >= 0; i--)
          if (item = this.items[i], itemElement = item.item[0], intersection = this._intersectsWithPointer(item), intersection && item.instance === this.currentContainer && itemElement !== this.currentItem[0] && this.placeholder[1 === intersection ? "next" : "prev"]()[0] !== itemElement && !$.contains(this.placeholder[0], itemElement) && ("semi-dynamic" === this.options.type ? !$.contains(this.element[0], itemElement) : !0)) {
            if (this.direction = 1 === intersection ? "down" : "up", "pointer" !== this.options.tolerance && !this._intersectsWithSides(item)) break;
            this._rearrange(event, item), this._trigger("change", event, this._uiHash());
            break
          } return this._contactContainers(event), $.ui.ddmanager && $.ui.ddmanager.drag(this, event), this._trigger("sort", event, this._uiHash()), this.lastPositionAbs = this.positionAbs, !1
      },
      _mouseStop: function(event, noPropagation) {
        if (event) {
          if ($.ui.ddmanager && !this.options.dropBehaviour && $.ui.ddmanager.drop(this, event), this.options.revert) {
            var that = this,
              cur = this.placeholder.offset(),
              axis = this.options.axis,
              animation = {};
            axis && "x" !== axis || (animation.left = cur.left - this.offset.parent.left - this.margins.left + (this.offsetParent[0] === document.body ? 0 : this.offsetParent[0].scrollLeft)), axis && "y" !== axis || (animation.top = cur.top - this.offset.parent.top - this.margins.top + (this.offsetParent[0] === document.body ? 0 : this.offsetParent[0].scrollTop)), this.reverting = !0, $(this.helper).animate(animation, parseInt(this.options.revert, 10) || 500, function() {
              that._clear(event)
            })
          } else this._clear(event, noPropagation);
          return !1
        }
      },
      cancel: function() {
        if (this.dragging) {
          this._mouseUp({
            target: null
          }), "original" === this.options.helper ? this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper") : this.currentItem.show();
          for (var i = this.containers.length - 1; i >= 0; i--) this.containers[i]._trigger("deactivate", null, this._uiHash(this)), this.containers[i].containerCache.over && (this.containers[i]._trigger("out", null, this._uiHash(this)), this.containers[i].containerCache.over = 0)
        }
        return this.placeholder && (this.placeholder[0].parentNode && this.placeholder[0].parentNode.removeChild(this.placeholder[0]), "original" !== this.options.helper && this.helper && this.helper[0].parentNode && this.helper.remove(), $.extend(this, {
          helper: null,
          dragging: !1,
          reverting: !1,
          _noFinalSort: null
        }), this.domPosition.prev ? $(this.domPosition.prev).after(this.currentItem) : $(this.domPosition.parent).prepend(this.currentItem)), this
      },
      serialize: function(o) {
        var items = this._getItemsAsjQuery(o && o.connected),
          str = [];
        return o = o || {}, $(items).each(function() {
          var res = ($(o.item || this).attr(o.attribute || "id") || "").match(o.expression || /(.+)[\-=_](.+)/);
          res && str.push((o.key || res[1] + "[]") + "=" + (o.key && o.expression ? res[1] : res[2]))
        }), !str.length && o.key && str.push(o.key + "="), str.join("&")
      },
      toArray: function(o) {
        var items = this._getItemsAsjQuery(o && o.connected),
          ret = [];
        return o = o || {}, items.each(function() {
          ret.push($(o.item || this).attr(o.attribute || "id") || "")
        }), ret
      },
      _intersectsWith: function(item) {
        var x1 = this.positionAbs.left,
          x2 = x1 + this.helperProportions.width,
          y1 = this.positionAbs.top,
          y2 = y1 + this.helperProportions.height,
          l = item.left,
          r = l + item.width,
          t = item.top,
          b = t + item.height,
          dyClick = this.offset.click.top,
          dxClick = this.offset.click.left,
          isOverElementHeight = "x" === this.options.axis || y1 + dyClick > t && b > y1 + dyClick,
          isOverElementWidth = "y" === this.options.axis || x1 + dxClick > l && r > x1 + dxClick,
          isOverElement = isOverElementHeight && isOverElementWidth;
        return "pointer" === this.options.tolerance || this.options.forcePointerForContainers || "pointer" !== this.options.tolerance && this.helperProportions[this.floating ? "width" : "height"] > item[this.floating ? "width" : "height"] ? isOverElement : l < x1 + this.helperProportions.width / 2 && x2 - this.helperProportions.width / 2 < r && t < y1 + this.helperProportions.height / 2 && y2 - this.helperProportions.height / 2 < b
      },
      _intersectsWithPointer: function(item) {
        var isOverElementHeight = "x" === this.options.axis || this._isOverAxis(this.positionAbs.top + this.offset.click.top, item.top, item.height),
          isOverElementWidth = "y" === this.options.axis || this._isOverAxis(this.positionAbs.left + this.offset.click.left, item.left, item.width),
          isOverElement = isOverElementHeight && isOverElementWidth,
          verticalDirection = this._getDragVerticalDirection(),
          horizontalDirection = this._getDragHorizontalDirection();
        return isOverElement ? this.floating ? horizontalDirection && "right" === horizontalDirection || "down" === verticalDirection ? 2 : 1 : verticalDirection && ("down" === verticalDirection ? 2 : 1) : !1
      },
      _intersectsWithSides: function(item) {
        var isOverBottomHalf = this._isOverAxis(this.positionAbs.top + this.offset.click.top, item.top + item.height / 2, item.height),
          isOverRightHalf = this._isOverAxis(this.positionAbs.left + this.offset.click.left, item.left + item.width / 2, item.width),
          verticalDirection = this._getDragVerticalDirection(),
          horizontalDirection = this._getDragHorizontalDirection();
        return this.floating && horizontalDirection ? "right" === horizontalDirection && isOverRightHalf || "left" === horizontalDirection && !isOverRightHalf : verticalDirection && ("down" === verticalDirection && isOverBottomHalf || "up" === verticalDirection && !isOverBottomHalf)
      },
      _getDragVerticalDirection: function() {
        var delta = this.positionAbs.top - this.lastPositionAbs.top;
        return 0 !== delta && (delta > 0 ? "down" : "up")
      },
      _getDragHorizontalDirection: function() {
        var delta = this.positionAbs.left - this.lastPositionAbs.left;
        return 0 !== delta && (delta > 0 ? "right" : "left")
      },
      refresh: function(event) {
        return this._refreshItems(event), this._setHandleClassName(), this.refreshPositions(), this
      },
      _connectWith: function() {
        var options = this.options;
        return options.connectWith.constructor === String ? [options.connectWith] : options.connectWith
      },
      _getItemsAsjQuery: function(connected) {
        function addItems() {
          items.push(this)
        }
        var i, j, cur, inst, items = [],
          queries = [],
          connectWith = this._connectWith();
        if (connectWith && connected)
          for (i = connectWith.length - 1; i >= 0; i--)
            for (cur = $(connectWith[i]), j = cur.length - 1; j >= 0; j--) inst = $.data(cur[j], this.widgetFullName), inst && inst !== this && !inst.options.disabled && queries.push([$.isFunction(inst.options.items) ? inst.options.items.call(inst.element) : $(inst.options.items, inst.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), inst]);
        for (queries.push([$.isFunction(this.options.items) ? this.options.items.call(this.element, null, {
            options: this.options,
            item: this.currentItem
          }) : $(this.options.items, this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), this]), i = queries.length - 1; i >= 0; i--) queries[i][0].each(addItems);
        return $(items)
      },
      _removeCurrentsFromItems: function() {
        var list = this.currentItem.find(":data(" + this.widgetName + "-item)");
        this.items = $.grep(this.items, function(item) {
          for (var j = 0; j < list.length; j++)
            if (list[j] === item.item[0]) return !1;
          return !0
        })
      },
      _refreshItems: function(event) {
        this.items = [], this.containers = [this];
        var i, j, cur, inst, targetData, _queries, item, queriesLength, items = this.items,
          queries = [
            [$.isFunction(this.options.items) ? this.options.items.call(this.element[0], event, {
              item: this.currentItem
            }) : $(this.options.items, this.element), this]
          ],
          connectWith = this._connectWith();
        if (connectWith && this.ready)
          for (i = connectWith.length - 1; i >= 0; i--)
            for (cur = $(connectWith[i]), j = cur.length - 1; j >= 0; j--) inst = $.data(cur[j], this.widgetFullName), inst && inst !== this && !inst.options.disabled && (queries.push([$.isFunction(inst.options.items) ? inst.options.items.call(inst.element[0], event, {
              item: this.currentItem
            }) : $(inst.options.items, inst.element), inst]), this.containers.push(inst));
        for (i = queries.length - 1; i >= 0; i--)
          for (targetData = queries[i][1], _queries = queries[i][0], j = 0, queriesLength = _queries.length; queriesLength > j; j++) item = $(_queries[j]), item.data(this.widgetName + "-item", targetData), items.push({
            item: item,
            instance: targetData,
            width: 0,
            height: 0,
            left: 0,
            top: 0
          })
      },
      refreshPositions: function(fast) {
        this.offsetParent && this.helper && (this.offset.parent = this._getParentOffset());
        var i, item, t, p;
        for (i = this.items.length - 1; i >= 0; i--) item = this.items[i], item.instance !== this.currentContainer && this.currentContainer && item.item[0] !== this.currentItem[0] || (t = this.options.toleranceElement ? $(this.options.toleranceElement, item.item) : item.item,
          fast || (item.width = t.outerWidth(), item.height = t.outerHeight()), p = t.offset(), item.left = p.left, item.top = p.top);
        if (this.options.custom && this.options.custom.refreshContainers) this.options.custom.refreshContainers.call(this);
        else
          for (i = this.containers.length - 1; i >= 0; i--) p = this.containers[i].element.offset(), this.containers[i].containerCache.left = p.left, this.containers[i].containerCache.top = p.top, this.containers[i].containerCache.width = this.containers[i].element.outerWidth(), this.containers[i].containerCache.height = this.containers[i].element.outerHeight();
        return this
      },
      _createPlaceholder: function(that) {
        that = that || this;
        var className, o = that.options;
        o.placeholder && o.placeholder.constructor !== String || (className = o.placeholder, o.placeholder = {
          element: function() {
            var nodeName = that.currentItem[0].nodeName.toLowerCase(),
              element = $("<" + nodeName + ">", that.document[0]).addClass(className || that.currentItem[0].className + " ui-sortable-placeholder").removeClass("ui-sortable-helper");
            return "tr" === nodeName ? that.currentItem.children().each(function() {
              $("<td>&#160;</td>", that.document[0]).attr("colspan", $(this).attr("colspan") || 1).appendTo(element)
            }) : "img" === nodeName && element.attr("src", that.currentItem.attr("src")), className || element.css("visibility", "hidden"), element
          },
          update: function(container, p) {
            (!className || o.forcePlaceholderSize) && (p.height() || p.height(that.currentItem.innerHeight() - parseInt(that.currentItem.css("paddingTop") || 0, 10) - parseInt(that.currentItem.css("paddingBottom") || 0, 10)), p.width() || p.width(that.currentItem.innerWidth() - parseInt(that.currentItem.css("paddingLeft") || 0, 10) - parseInt(that.currentItem.css("paddingRight") || 0, 10)))
          }
        }), that.placeholder = $(o.placeholder.element.call(that.element, that.currentItem)), that.currentItem.after(that.placeholder), o.placeholder.update(that, that.placeholder)
      },
      _contactContainers: function(event) {
        var i, j, dist, itemWithLeastDistance, posProperty, sizeProperty, cur, nearBottom, floating, axis, innermostContainer = null,
          innermostIndex = null;
        for (i = this.containers.length - 1; i >= 0; i--)
          if (!$.contains(this.currentItem[0], this.containers[i].element[0]))
            if (this._intersectsWith(this.containers[i].containerCache)) {
              if (innermostContainer && $.contains(this.containers[i].element[0], innermostContainer.element[0])) continue;
              innermostContainer = this.containers[i], innermostIndex = i
            } else this.containers[i].containerCache.over && (this.containers[i]._trigger("out", event, this._uiHash(this)), this.containers[i].containerCache.over = 0);
        if (innermostContainer)
          if (1 === this.containers.length) this.containers[innermostIndex].containerCache.over || (this.containers[innermostIndex]._trigger("over", event, this._uiHash(this)), this.containers[innermostIndex].containerCache.over = 1);
          else {
            for (dist = 1e4, itemWithLeastDistance = null, floating = innermostContainer.floating || this._isFloating(this.currentItem), posProperty = floating ? "left" : "top", sizeProperty = floating ? "width" : "height", axis = floating ? "clientX" : "clientY", j = this.items.length - 1; j >= 0; j--) $.contains(this.containers[innermostIndex].element[0], this.items[j].item[0]) && this.items[j].item[0] !== this.currentItem[0] && (cur = this.items[j].item.offset()[posProperty], nearBottom = !1, event[axis] - cur > this.items[j][sizeProperty] / 2 && (nearBottom = !0), Math.abs(event[axis] - cur) < dist && (dist = Math.abs(event[axis] - cur), itemWithLeastDistance = this.items[j], this.direction = nearBottom ? "up" : "down"));
            if (!itemWithLeastDistance && !this.options.dropOnEmpty) return;
            if (this.currentContainer === this.containers[innermostIndex]) return;
            itemWithLeastDistance ? this._rearrange(event, itemWithLeastDistance, null, !0) : this._rearrange(event, null, this.containers[innermostIndex].element, !0), this._trigger("change", event, this._uiHash()), this.containers[innermostIndex]._trigger("change", event, this._uiHash(this)), this.currentContainer = this.containers[innermostIndex], this.options.placeholder.update(this.currentContainer, this.placeholder), this.containers[innermostIndex]._trigger("over", event, this._uiHash(this)), this.containers[innermostIndex].containerCache.over = 1
          }
      },
      _createHelper: function(event) {
        var o = this.options,
          helper = $.isFunction(o.helper) ? $(o.helper.apply(this.element[0], [event, this.currentItem])) : "clone" === o.helper ? this.currentItem.clone() : this.currentItem;
        return helper.parents("body").length || $("parent" !== o.appendTo ? o.appendTo : this.currentItem[0].parentNode)[0].appendChild(helper[0]), helper[0] === this.currentItem[0] && (this._storedCSS = {
          width: this.currentItem[0].style.width,
          height: this.currentItem[0].style.height,
          position: this.currentItem.css("position"),
          top: this.currentItem.css("top"),
          left: this.currentItem.css("left")
        }), (!helper[0].style.width || o.forceHelperSize) && helper.width(this.currentItem.width()), (!helper[0].style.height || o.forceHelperSize) && helper.height(this.currentItem.height()), helper
      },
      _adjustOffsetFromHelper: function(obj) {
        "string" == typeof obj && (obj = obj.split(" ")), $.isArray(obj) && (obj = {
          left: +obj[0],
          top: +obj[1] || 0
        }), "left" in obj && (this.offset.click.left = obj.left + this.margins.left), "right" in obj && (this.offset.click.left = this.helperProportions.width - obj.right + this.margins.left), "top" in obj && (this.offset.click.top = obj.top + this.margins.top), "bottom" in obj && (this.offset.click.top = this.helperProportions.height - obj.bottom + this.margins.top)
      },
      _getParentOffset: function() {
        this.offsetParent = this.helper.offsetParent();
        var po = this.offsetParent.offset();
        return "absolute" === this.cssPosition && this.scrollParent[0] !== document && $.contains(this.scrollParent[0], this.offsetParent[0]) && (po.left += this.scrollParent.scrollLeft(), po.top += this.scrollParent.scrollTop()), (this.offsetParent[0] === document.body || this.offsetParent[0].tagName && "html" === this.offsetParent[0].tagName.toLowerCase() && $.ui.ie) && (po = {
          top: 0,
          left: 0
        }), {
          top: po.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
          left: po.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
        }
      },
      _getRelativeOffset: function() {
        if ("relative" === this.cssPosition) {
          var p = this.currentItem.position();
          return {
            top: p.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(),
            left: p.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft()
          }
        }
        return {
          top: 0,
          left: 0
        }
      },
      _cacheMargins: function() {
        this.margins = {
          left: parseInt(this.currentItem.css("marginLeft"), 10) || 0,
          top: parseInt(this.currentItem.css("marginTop"), 10) || 0
        }
      },
      _cacheHelperProportions: function() {
        this.helperProportions = {
          width: this.helper.outerWidth(),
          height: this.helper.outerHeight()
        }
      },
      _setContainment: function() {
        var ce, co, over, o = this.options;
        "parent" === o.containment && (o.containment = this.helper[0].parentNode), ("document" === o.containment || "window" === o.containment) && (this.containment = [0 - this.offset.relative.left - this.offset.parent.left, 0 - this.offset.relative.top - this.offset.parent.top, $("document" === o.containment ? document : window).width() - this.helperProportions.width - this.margins.left, ($("document" === o.containment ? document : window).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]), /^(document|window|parent)$/.test(o.containment) || (ce = $(o.containment)[0], co = $(o.containment).offset(), over = "hidden" !== $(ce).css("overflow"), this.containment = [co.left + (parseInt($(ce).css("borderLeftWidth"), 10) || 0) + (parseInt($(ce).css("paddingLeft"), 10) || 0) - this.margins.left, co.top + (parseInt($(ce).css("borderTopWidth"), 10) || 0) + (parseInt($(ce).css("paddingTop"), 10) || 0) - this.margins.top, co.left + (over ? Math.max(ce.scrollWidth, ce.offsetWidth) : ce.offsetWidth) - (parseInt($(ce).css("borderLeftWidth"), 10) || 0) - (parseInt($(ce).css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left, co.top + (over ? Math.max(ce.scrollHeight, ce.offsetHeight) : ce.offsetHeight) - (parseInt($(ce).css("borderTopWidth"), 10) || 0) - (parseInt($(ce).css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top])
      },
      _convertPositionTo: function(d, pos) {
        pos || (pos = this.position);
        var mod = "absolute" === d ? 1 : -1,
          scroll = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && $.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent,
          scrollIsRootNode = /(html|body)/i.test(scroll[0].tagName);
        return {
          top: pos.top + this.offset.relative.top * mod + this.offset.parent.top * mod - ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : scrollIsRootNode ? 0 : scroll.scrollTop()) * mod,
          left: pos.left + this.offset.relative.left * mod + this.offset.parent.left * mod - ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : scrollIsRootNode ? 0 : scroll.scrollLeft()) * mod
        }
      },
      _generatePosition: function(event) {
        var top, left, o = this.options,
          pageX = event.pageX,
          pageY = event.pageY,
          scroll = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && $.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent,
          scrollIsRootNode = /(html|body)/i.test(scroll[0].tagName);
        return "relative" !== this.cssPosition || this.scrollParent[0] !== document && this.scrollParent[0] !== this.offsetParent[0] || (this.offset.relative = this._getRelativeOffset()), this.originalPosition && (this.containment && (event.pageX - this.offset.click.left < this.containment[0] && (pageX = this.containment[0] + this.offset.click.left), event.pageY - this.offset.click.top < this.containment[1] && (pageY = this.containment[1] + this.offset.click.top), event.pageX - this.offset.click.left > this.containment[2] && (pageX = this.containment[2] + this.offset.click.left), event.pageY - this.offset.click.top > this.containment[3] && (pageY = this.containment[3] + this.offset.click.top)), o.grid && (top = this.originalPageY + Math.round((pageY - this.originalPageY) / o.grid[1]) * o.grid[1], pageY = this.containment ? top - this.offset.click.top >= this.containment[1] && top - this.offset.click.top <= this.containment[3] ? top : top - this.offset.click.top >= this.containment[1] ? top - o.grid[1] : top + o.grid[1] : top, left = this.originalPageX + Math.round((pageX - this.originalPageX) / o.grid[0]) * o.grid[0], pageX = this.containment ? left - this.offset.click.left >= this.containment[0] && left - this.offset.click.left <= this.containment[2] ? left : left - this.offset.click.left >= this.containment[0] ? left - o.grid[0] : left + o.grid[0] : left)), {
          top: pageY - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : scrollIsRootNode ? 0 : scroll.scrollTop()),
          left: pageX - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : scrollIsRootNode ? 0 : scroll.scrollLeft())
        }
      },
      _rearrange: function(event, i, a, hardRefresh) {
        a ? a[0].appendChild(this.placeholder[0]) : i.item[0].parentNode.insertBefore(this.placeholder[0], "down" === this.direction ? i.item[0] : i.item[0].nextSibling), this.counter = this.counter ? ++this.counter : 1;
        var counter = this.counter;
        this._delay(function() {
          counter === this.counter && this.refreshPositions(!hardRefresh)
        })
      },
      _clear: function(event, noPropagation) {
        function delayEvent(type, instance, container) {
          return function(event) {
            container._trigger(type, event, instance._uiHash(instance))
          }
        }
        this.reverting = !1;
        var i, delayedTriggers = [];
        if (!this._noFinalSort && this.currentItem.parent().length && this.placeholder.before(this.currentItem), this._noFinalSort = null, this.helper[0] === this.currentItem[0]) {
          for (i in this._storedCSS)("auto" === this._storedCSS[i] || "static" === this._storedCSS[i]) && (this._storedCSS[i] = "");
          this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper")
        } else this.currentItem.show();
        for (this.fromOutside && !noPropagation && delayedTriggers.push(function(event) {
            this._trigger("receive", event, this._uiHash(this.fromOutside))
          }), !this.fromOutside && this.domPosition.prev === this.currentItem.prev().not(".ui-sortable-helper")[0] && this.domPosition.parent === this.currentItem.parent()[0] || noPropagation || delayedTriggers.push(function(event) {
            this._trigger("update", event, this._uiHash())
          }), this !== this.currentContainer && (noPropagation || (delayedTriggers.push(function(event) {
            this._trigger("remove", event, this._uiHash())
          }), delayedTriggers.push(function(c) {
            return function(event) {
              c._trigger("receive", event, this._uiHash(this))
            }
          }.call(this, this.currentContainer)), delayedTriggers.push(function(c) {
            return function(event) {
              c._trigger("update", event, this._uiHash(this))
            }
          }.call(this, this.currentContainer)))), i = this.containers.length - 1; i >= 0; i--) noPropagation || delayedTriggers.push(delayEvent("deactivate", this, this.containers[i])), this.containers[i].containerCache.over && (delayedTriggers.push(delayEvent("out", this, this.containers[i])), this.containers[i].containerCache.over = 0);
        if (this.storedCursor && (this.document.find("body").css("cursor", this.storedCursor), this.storedStylesheet.remove()), this._storedOpacity && this.helper.css("opacity", this._storedOpacity), this._storedZIndex && this.helper.css("zIndex", "auto" === this._storedZIndex ? "" : this._storedZIndex), this.dragging = !1, this.cancelHelperRemoval) {
          if (!noPropagation) {
            for (this._trigger("beforeStop", event, this._uiHash()), i = 0; i < delayedTriggers.length; i++) delayedTriggers[i].call(this, event);
            this._trigger("stop", event, this._uiHash())
          }
          return this.fromOutside = !1, !1
        }
        if (noPropagation || this._trigger("beforeStop", event, this._uiHash()), this.placeholder[0].parentNode.removeChild(this.placeholder[0]), this.helper[0] !== this.currentItem[0] && this.helper.remove(), this.helper = null, !noPropagation) {
          for (i = 0; i < delayedTriggers.length; i++) delayedTriggers[i].call(this, event);
          this._trigger("stop", event, this._uiHash())
        }
        return this.fromOutside = !1, !0
      },
      _trigger: function() {
        $.Widget.prototype._trigger.apply(this, arguments) === !1 && this.cancel()
      },
      _uiHash: function(_inst) {
        var inst = _inst || this;
        return {
          helper: inst.helper,
          placeholder: inst.placeholder || $([]),
          position: inst.position,
          originalPosition: inst.originalPosition,
          offset: inst.positionAbs,
          item: inst.currentItem,
          sender: _inst ? _inst.element : null
        }
      }
    }), $.widget("ui.accordion", {
      version: "1.11.0",
      options: {
        active: 0,
        animate: {},
        collapsible: !1,
        event: "click",
        header: "> li > :first-child,> :not(li):even",
        heightStyle: "auto",
        icons: {
          activeHeader: "ui-icon-triangle-1-s",
          header: "ui-icon-triangle-1-e"
        },
        activate: null,
        beforeActivate: null
      },
      hideProps: {
        borderTopWidth: "hide",
        borderBottomWidth: "hide",
        paddingTop: "hide",
        paddingBottom: "hide",
        height: "hide"
      },
      showProps: {
        borderTopWidth: "show",
        borderBottomWidth: "show",
        paddingTop: "show",
        paddingBottom: "show",
        height: "show"
      },
      _create: function() {
        var options = this.options;
        this.prevShow = this.prevHide = $(), this.element.addClass("ui-accordion ui-widget ui-helper-reset").attr("role", "tablist"), options.collapsible || options.active !== !1 && null != options.active || (options.active = 0), this._processPanels(), options.active < 0 && (options.active += this.headers.length), this._refresh()
      },
      _getCreateEventData: function() {
        return {
          header: this.active,
          panel: this.active.length ? this.active.next() : $()
        }
      },
      _createIcons: function() {
        var icons = this.options.icons;
        icons && ($("<span>").addClass("ui-accordion-header-icon ui-icon " + icons.header).prependTo(this.headers), this.active.children(".ui-accordion-header-icon").removeClass(icons.header).addClass(icons.activeHeader), this.headers.addClass("ui-accordion-icons"))
      },
      _destroyIcons: function() {
        this.headers.removeClass("ui-accordion-icons").children(".ui-accordion-header-icon").remove()
      },
      _destroy: function() {
        var contents;
        this.element.removeClass("ui-accordion ui-widget ui-helper-reset").removeAttr("role"), this.headers.removeClass("ui-accordion-header ui-accordion-header-active ui-state-default ui-corner-all ui-state-active ui-state-disabled ui-corner-top").removeAttr("role").removeAttr("aria-expanded").removeAttr("aria-selected").removeAttr("aria-controls").removeAttr("tabIndex").removeUniqueId(), this._destroyIcons(), contents = this.headers.next().removeClass("ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content ui-accordion-content-active ui-state-disabled").css("display", "").removeAttr("role").removeAttr("aria-hidden").removeAttr("aria-labelledby").removeUniqueId(), "content" !== this.options.heightStyle && contents.css("height", "")
      },
      _setOption: function(key, value) {
        return "active" === key ? void this._activate(value) : ("event" === key && (this.options.event && this._off(this.headers, this.options.event), this._setupEvents(value)), this._super(key, value), "collapsible" !== key || value || this.options.active !== !1 || this._activate(0), "icons" === key && (this._destroyIcons(), value && this._createIcons()), void("disabled" === key && (this.element.toggleClass("ui-state-disabled", !!value).attr("aria-disabled", value), this.headers.add(this.headers.next()).toggleClass("ui-state-disabled", !!value))))
      },
      _keydown: function(event) {
        if (!event.altKey && !event.ctrlKey) {
          var keyCode = $.ui.keyCode,
            length = this.headers.length,
            currentIndex = this.headers.index(event.target),
            toFocus = !1;
          switch (event.keyCode) {
            case keyCode.RIGHT:
            case keyCode.DOWN:
              toFocus = this.headers[(currentIndex + 1) % length];
              break;
            case keyCode.LEFT:
            case keyCode.UP:
              toFocus = this.headers[(currentIndex - 1 + length) % length];
              break;
            case keyCode.SPACE:
            case keyCode.ENTER:
              this._eventHandler(event);
              break;
            case keyCode.HOME:
              toFocus = this.headers[0];
              break;
            case keyCode.END:
              toFocus = this.headers[length - 1]
          }
          toFocus && ($(event.target).attr("tabIndex", -1), $(toFocus).attr("tabIndex", 0), toFocus.focus(), event.preventDefault())
        }
      },
      _panelKeyDown: function(event) {
        event.keyCode === $.ui.keyCode.UP && event.ctrlKey && $(event.currentTarget).prev().focus()
      },
      refresh: function() {
        var options = this.options;
        this._processPanels(), options.active === !1 && options.collapsible === !0 || !this.headers.length ? (options.active = !1, this.active = $()) : options.active === !1 ? this._activate(0) : this.active.length && !$.contains(this.element[0], this.active[0]) ? this.headers.length === this.headers.find(".ui-state-disabled").length ? (options.active = !1, this.active = $()) : this._activate(Math.max(0, options.active - 1)) : options.active = this.headers.index(this.active), this._destroyIcons(), this._refresh()
      },
      _processPanels: function() {
        this.headers = this.element.find(this.options.header).addClass("ui-accordion-header ui-state-default ui-corner-all"), this.headers.next().addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom").filter(":not(.ui-accordion-content-active)").hide()
      },
      _refresh: function() {
        var maxHeight, options = this.options,
          heightStyle = options.heightStyle,
          parent = this.element.parent();
        this.active = this._findActive(options.active).addClass("ui-accordion-header-active ui-state-active ui-corner-top").removeClass("ui-corner-all"), this.active.next().addClass("ui-accordion-content-active").show(), this.headers.attr("role", "tab").each(function() {
          var header = $(this),
            headerId = header.uniqueId().attr("id"),
            panel = header.next(),
            panelId = panel.uniqueId().attr("id");
          header.attr("aria-controls", panelId), panel.attr("aria-labelledby", headerId)
        }).next().attr("role", "tabpanel"), this.headers.not(this.active).attr({
          "aria-selected": "false",
          "aria-expanded": "false",
          tabIndex: -1
        }).next().attr({
          "aria-hidden": "true"
        }).hide(), this.active.length ? this.active.attr({
          "aria-selected": "true",
          "aria-expanded": "true",
          tabIndex: 0
        }).next().attr({
          "aria-hidden": "false"
        }) : this.headers.eq(0).attr("tabIndex", 0), this._createIcons(), this._setupEvents(options.event), "fill" === heightStyle ? (maxHeight = parent.height(), this.element.siblings(":visible").each(function() {
          var elem = $(this),
            position = elem.css("position");
          "absolute" !== position && "fixed" !== position && (maxHeight -= elem.outerHeight(!0))
        }), this.headers.each(function() {
          maxHeight -= $(this).outerHeight(!0)
        }), this.headers.next().each(function() {
          $(this).height(Math.max(0, maxHeight - $(this).innerHeight() + $(this).height()))
        }).css("overflow", "auto")) : "auto" === heightStyle && (maxHeight = 0, this.headers.next().each(function() {
          maxHeight = Math.max(maxHeight, $(this).css("height", "").height())
        }).height(maxHeight))
      },
      _activate: function(index) {
        var active = this._findActive(index)[0];
        active !== this.active[0] && (active = active || this.active[0], this._eventHandler({
          target: active,
          currentTarget: active,
          preventDefault: $.noop
        }))
      },
      _findActive: function(selector) {
        return "number" == typeof selector ? this.headers.eq(selector) : $()
      },
      _setupEvents: function(event) {
        var events = {
          keydown: "_keydown"
        };
        event && $.each(event.split(" "), function(index, eventName) {
          events[eventName] = "_eventHandler"
        }), this._off(this.headers.add(this.headers.next())), this._on(this.headers, events), this._on(this.headers.next(), {
          keydown: "_panelKeyDown"
        }), this._hoverable(this.headers), this._focusable(this.headers)
      },
      _eventHandler: function(event) {
        var options = this.options,
          active = this.active,
          clicked = $(event.currentTarget),
          clickedIsActive = clicked[0] === active[0],
          collapsing = clickedIsActive && options.collapsible,
          toShow = collapsing ? $() : clicked.next(),
          toHide = active.next(),
          eventData = {
            oldHeader: active,
            oldPanel: toHide,
            newHeader: collapsing ? $() : clicked,
            newPanel: toShow
          };
        event.preventDefault(), clickedIsActive && !options.collapsible || this._trigger("beforeActivate", event, eventData) === !1 || (options.active = collapsing ? !1 : this.headers.index(clicked), this.active = clickedIsActive ? $() : clicked, this._toggle(eventData), active.removeClass("ui-accordion-header-active ui-state-active"), options.icons && active.children(".ui-accordion-header-icon").removeClass(options.icons.activeHeader).addClass(options.icons.header), clickedIsActive || (clicked.removeClass("ui-corner-all").addClass("ui-accordion-header-active ui-state-active ui-corner-top"), options.icons && clicked.children(".ui-accordion-header-icon").removeClass(options.icons.header).addClass(options.icons.activeHeader), clicked.next().addClass("ui-accordion-content-active")))
      },
      _toggle: function(data) {
        var toShow = data.newPanel,
          toHide = this.prevShow.length ? this.prevShow : data.oldPanel;
        this.prevShow.add(this.prevHide).stop(!0, !0), this.prevShow = toShow, this.prevHide = toHide, this.options.animate ? this._animate(toShow, toHide, data) : (toHide.hide(), toShow.show(), this._toggleComplete(data)), toHide.attr({
          "aria-hidden": "true"
        }), toHide.prev().attr("aria-selected", "false"), toShow.length && toHide.length ? toHide.prev().attr({
          tabIndex: -1,
          "aria-expanded": "false"
        }) : toShow.length && this.headers.filter(function() {
          return 0 === $(this).attr("tabIndex")
        }).attr("tabIndex", -1), toShow.attr("aria-hidden", "false").prev().attr({
          "aria-selected": "true",
          tabIndex: 0,
          "aria-expanded": "true"
        })
      },
      _animate: function(toShow, toHide, data) {
        var total, easing, duration, that = this,
          adjust = 0,
          down = toShow.length && (!toHide.length || toShow.index() < toHide.index()),
          animate = this.options.animate || {},
          options = down && animate.down || animate,
          complete = function() {
            that._toggleComplete(data)
          };
        return "number" == typeof options && (duration = options), "string" == typeof options && (easing = options), easing = easing || options.easing || animate.easing, duration = duration || options.duration || animate.duration, toHide.length ? toShow.length ? (total = toShow.show().outerHeight(), toHide.animate(this.hideProps, {
          duration: duration,
          easing: easing,
          step: function(now, fx) {
            fx.now = Math.round(now)
          }
        }), void toShow.hide().animate(this.showProps, {
          duration: duration,
          easing: easing,
          complete: complete,
          step: function(now, fx) {
            fx.now = Math.round(now), "height" !== fx.prop ? adjust += fx.now : "content" !== that.options.heightStyle && (fx.now = Math.round(total - toHide.outerHeight() - adjust), adjust = 0)
          }
        })) : toHide.animate(this.hideProps, duration, easing, complete) : toShow.animate(this.showProps, duration, easing, complete)
      },
      _toggleComplete: function(data) {
        var toHide = data.oldPanel;
        toHide.removeClass("ui-accordion-content-active").prev().removeClass("ui-corner-top").addClass("ui-corner-all"), toHide.length && (toHide.parent()[0].className = toHide.parent()[0].className), this._trigger("activate", null, data)
      }
    }), $.widget("ui.menu", {
      version: "1.11.0",
      defaultElement: "<ul>",
      delay: 300,
      options: {
        icons: {
          submenu: "ui-icon-carat-1-e"
        },
        items: "> *",
        menus: "ul",
        position: {
          my: "left-1 top",
          at: "right top"
        },
        role: "menu",
        blur: null,
        focus: null,
        select: null
      },
      _create: function() {
        this.activeMenu = this.element, this.mouseHandled = !1, this.element.uniqueId().addClass("ui-menu ui-widget ui-widget-content").toggleClass("ui-menu-icons", !!this.element.find(".ui-icon").length).attr({
          role: this.options.role,
          tabIndex: 0
        }), this.options.disabled && this.element.addClass("ui-state-disabled").attr("aria-disabled", "true"), this._on({
          "mousedown .ui-menu-item": function(event) {
            event.preventDefault()
          },
          "click .ui-menu-item": function(event) {
            var target = $(event.target);
            !this.mouseHandled && target.not(".ui-state-disabled").length && (this.select(event), event.isPropagationStopped() || (this.mouseHandled = !0), target.has(".ui-menu").length ? this.expand(event) : !this.element.is(":focus") && $(this.document[0].activeElement).closest(".ui-menu").length && (this.element.trigger("focus", [!0]), this.active && 1 === this.active.parents(".ui-menu").length && clearTimeout(this.timer)))
          },
          "mouseenter .ui-menu-item": function(event) {
            var target = $(event.currentTarget);
            target.siblings(".ui-state-active").removeClass("ui-state-active"), this.focus(event, target)
          },
          mouseleave: "collapseAll",
          "mouseleave .ui-menu": "collapseAll",
          focus: function(event, keepActiveItem) {
            var item = this.active || this.element.find(this.options.items).eq(0);
            keepActiveItem || this.focus(event, item)
          },
          blur: function(event) {
            this._delay(function() {
              $.contains(this.element[0], this.document[0].activeElement) || this.collapseAll(event)
            })
          },
          keydown: "_keydown"
        }), this.refresh(), this._on(this.document, {
          click: function(event) {
            this._closeOnDocumentClick(event) && this.collapseAll(event), this.mouseHandled = !1
          }
        })
      },
      _destroy: function() {
        this.element.removeAttr("aria-activedescendant").find(".ui-menu").addBack().removeClass("ui-menu ui-widget ui-widget-content ui-menu-icons ui-front").removeAttr("role").removeAttr("tabIndex").removeAttr("aria-labelledby").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-disabled").removeUniqueId().show(), this.element.find(".ui-menu-item").removeClass("ui-menu-item").removeAttr("role").removeAttr("aria-disabled").removeUniqueId().removeClass("ui-state-hover").removeAttr("tabIndex").removeAttr("role").removeAttr("aria-haspopup").children().each(function() {
          var elem = $(this);
          elem.data("ui-menu-submenu-carat") && elem.remove()
        }), this.element.find(".ui-menu-divider").removeClass("ui-menu-divider ui-widget-content")
      },
      _keydown: function(event) {
        function escape(value) {
          return value.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&")
        }
        var match, prev, character, skip, regex, preventDefault = !0;
        switch (event.keyCode) {
          case $.ui.keyCode.PAGE_UP:
            this.previousPage(event);
            break;
          case $.ui.keyCode.PAGE_DOWN:
            this.nextPage(event);
            break;
          case $.ui.keyCode.HOME:
            this._move("first", "first", event);
            break;
          case $.ui.keyCode.END:
            this._move("last", "last", event);
            break;
          case $.ui.keyCode.UP:
            this.previous(event);
            break;
          case $.ui.keyCode.DOWN:
            this.next(event);
            break;
          case $.ui.keyCode.LEFT:
            this.collapse(event);
            break;
          case $.ui.keyCode.RIGHT:
            this.active && !this.active.is(".ui-state-disabled") && this.expand(event);
            break;
          case $.ui.keyCode.ENTER:
          case $.ui.keyCode.SPACE:
            this._activate(event);
            break;
          case $.ui.keyCode.ESCAPE:
            this.collapse(event);
            break;
          default:
            preventDefault = !1, prev = this.previousFilter || "", character = String.fromCharCode(event.keyCode), skip = !1, clearTimeout(this.filterTimer), character === prev ? skip = !0 : character = prev + character, regex = new RegExp("^" + escape(character), "i"), match = this.activeMenu.find(this.options.items).filter(function() {
              return regex.test($(this).text())
            }), match = skip && -1 !== match.index(this.active.next()) ? this.active.nextAll(".ui-menu-item") : match, match.length || (character = String.fromCharCode(event.keyCode), regex = new RegExp("^" + escape(character), "i"), match = this.activeMenu.find(this.options.items).filter(function() {
              return regex.test($(this).text())
            })), match.length ? (this.focus(event, match), match.length > 1 ? (this.previousFilter = character, this.filterTimer = this._delay(function() {
              delete this.previousFilter
            }, 1e3)) : delete this.previousFilter) : delete this.previousFilter
        }
        preventDefault && event.preventDefault()
      },
      _activate: function(event) {
        this.active.is(".ui-state-disabled") || (this.active.is("[aria-haspopup='true']") ? this.expand(event) : this.select(event))
      },
      refresh: function() {
        var menus, items, that = this,
          icon = this.options.icons.submenu,
          submenus = this.element.find(this.options.menus);
        this.element.toggleClass("ui-menu-icons", !!this.element.find(".ui-icon").length), submenus.filter(":not(.ui-menu)").addClass("ui-menu ui-widget ui-widget-content ui-front").hide().attr({
          role: this.options.role,
          "aria-hidden": "true",
          "aria-expanded": "false"
        }).each(function() {
          var menu = $(this),
            item = menu.parent(),
            submenuCarat = $("<span>").addClass("ui-menu-icon ui-icon " + icon).data("ui-menu-submenu-carat", !0);
          item.attr("aria-haspopup", "true").prepend(submenuCarat), menu.attr("aria-labelledby", item.attr("id"))
        }), menus = submenus.add(this.element), items = menus.find(this.options.items), items.not(".ui-menu-item").each(function() {
          var item = $(this);
          that._isDivider(item) && item.addClass("ui-widget-content ui-menu-divider")
        }), items.not(".ui-menu-item, .ui-menu-divider").addClass("ui-menu-item").uniqueId().attr({
          tabIndex: -1,
          role: this._itemRole()
        }), items.filter(".ui-state-disabled").attr("aria-disabled", "true"), this.active && !$.contains(this.element[0], this.active[0]) && this.blur()
      },
      _itemRole: function() {
        return {
          menu: "menuitem",
          listbox: "option"
        } [this.options.role]
      },
      _setOption: function(key, value) {
        "icons" === key && this.element.find(".ui-menu-icon").removeClass(this.options.icons.submenu).addClass(value.submenu), "disabled" === key && this.element.toggleClass("ui-state-disabled", !!value).attr("aria-disabled", value), this._super(key, value)
      },
      focus: function(event, item) {
        var nested, focused;
        this.blur(event, event && "focus" === event.type), this._scrollIntoView(item), this.active = item.first(), focused = this.active.addClass("ui-state-focus").removeClass("ui-state-active"), this.options.role && this.element.attr("aria-activedescendant", focused.attr("id")), this.active.parent().closest(".ui-menu-item").addClass("ui-state-active"), event && "keydown" === event.type ? this._close() : this.timer = this._delay(function() {
          this._close()
        }, this.delay), nested = item.children(".ui-menu"), nested.length && event && /^mouse/.test(event.type) && this._startOpening(nested), this.activeMenu = item.parent(), this._trigger("focus", event, {
          item: item
        })
      },
      _scrollIntoView: function(item) {
        var borderTop, paddingTop, offset, scroll, elementHeight, itemHeight;
        this._hasScroll() && (borderTop = parseFloat($.css(this.activeMenu[0], "borderTopWidth")) || 0, paddingTop = parseFloat($.css(this.activeMenu[0], "paddingTop")) || 0, offset = item.offset().top - this.activeMenu.offset().top - borderTop - paddingTop, scroll = this.activeMenu.scrollTop(), elementHeight = this.activeMenu.height(), itemHeight = item.outerHeight(), 0 > offset ? this.activeMenu.scrollTop(scroll + offset) : offset + itemHeight > elementHeight && this.activeMenu.scrollTop(scroll + offset - elementHeight + itemHeight))
      },
      blur: function(event, fromFocus) {
        fromFocus || clearTimeout(this.timer), this.active && (this.active.removeClass("ui-state-focus"), this.active = null, this._trigger("blur", event, {
          item: this.active
        }))
      },
      _startOpening: function(submenu) {
        clearTimeout(this.timer), "true" === submenu.attr("aria-hidden") && (this.timer = this._delay(function() {
          this._close(), this._open(submenu)
        }, this.delay))
      },
      _open: function(submenu) {
        var position = $.extend({
          of: this.active
        }, this.options.position);
        clearTimeout(this.timer), this.element.find(".ui-menu").not(submenu.parents(".ui-menu")).hide().attr("aria-hidden", "true"), submenu.show().removeAttr("aria-hidden").attr("aria-expanded", "true").position(position)
      },
      collapseAll: function(event, all) {
        clearTimeout(this.timer), this.timer = this._delay(function() {
          var currentMenu = all ? this.element : $(event && event.target).closest(this.element.find(".ui-menu"));
          currentMenu.length || (currentMenu = this.element), this._close(currentMenu), this.blur(event), this.activeMenu = currentMenu
        }, this.delay)
      },
      _close: function(startMenu) {
        startMenu || (startMenu = this.active ? this.active.parent() : this.element), startMenu.find(".ui-menu").hide().attr("aria-hidden", "true").attr("aria-expanded", "false").end().find(".ui-state-active").not(".ui-state-focus").removeClass("ui-state-active")
      },
      _closeOnDocumentClick: function(event) {
        return !$(event.target).closest(".ui-menu").length
      },
      _isDivider: function(item) {
        return !/[^\-\u2014\u2013\s]/.test(item.text())
      },
      collapse: function(event) {
        var newItem = this.active && this.active.parent().closest(".ui-menu-item", this.element);
        newItem && newItem.length && (this._close(), this.focus(event, newItem))
      },
      expand: function(event) {
        var newItem = this.active && this.active.children(".ui-menu ").find(this.options.items).first();
        newItem && newItem.length && (this._open(newItem.parent()), this._delay(function() {
          this.focus(event, newItem)
        }))
      },
      next: function(event) {
        this._move("next", "first", event)
      },
      previous: function(event) {
        this._move("prev", "last", event);
      },
      isFirstItem: function() {
        return this.active && !this.active.prevAll(".ui-menu-item").length
      },
      isLastItem: function() {
        return this.active && !this.active.nextAll(".ui-menu-item").length
      },
      _move: function(direction, filter, event) {
        var next;
        this.active && (next = "first" === direction || "last" === direction ? this.active["first" === direction ? "prevAll" : "nextAll"](".ui-menu-item").eq(-1) : this.active[direction + "All"](".ui-menu-item").eq(0)), next && next.length && this.active || (next = this.activeMenu.find(this.options.items)[filter]()), this.focus(event, next)
      },
      nextPage: function(event) {
        var item, base, height;
        return this.active ? void(this.isLastItem() || (this._hasScroll() ? (base = this.active.offset().top, height = this.element.height(), this.active.nextAll(".ui-menu-item").each(function() {
          return item = $(this), item.offset().top - base - height < 0
        }), this.focus(event, item)) : this.focus(event, this.activeMenu.find(this.options.items)[this.active ? "last" : "first"]()))) : void this.next(event)
      },
      previousPage: function(event) {
        var item, base, height;
        return this.active ? void(this.isFirstItem() || (this._hasScroll() ? (base = this.active.offset().top, height = this.element.height(), this.active.prevAll(".ui-menu-item").each(function() {
          return item = $(this), item.offset().top - base + height > 0
        }), this.focus(event, item)) : this.focus(event, this.activeMenu.find(this.options.items).first()))) : void this.next(event)
      },
      _hasScroll: function() {
        return this.element.outerHeight() < this.element.prop("scrollHeight")
      },
      select: function(event) {
        this.active = this.active || $(event.target).closest(".ui-menu-item");
        var ui = {
          item: this.active
        };
        this.active.has(".ui-menu").length || this.collapseAll(event, !0), this._trigger("select", event, ui)
      }
    });
    $.widget("ui.autocomplete", {
      version: "1.11.0",
      defaultElement: "<input>",
      options: {
        appendTo: null,
        autoFocus: !1,
        delay: 300,
        minLength: 1,
        position: {
          my: "left top",
          at: "left bottom",
          collision: "none"
        },
        source: null,
        change: null,
        close: null,
        focus: null,
        open: null,
        response: null,
        search: null,
        select: null
      },
      requestIndex: 0,
      pending: 0,
      _create: function() {
        var suppressKeyPress, suppressKeyPressRepeat, suppressInput, nodeName = this.element[0].nodeName.toLowerCase(),
          isTextarea = "textarea" === nodeName,
          isInput = "input" === nodeName;
        this.isMultiLine = isTextarea ? !0 : isInput ? !1 : this.element.prop("isContentEditable"), this.valueMethod = this.element[isTextarea || isInput ? "val" : "text"], this.isNewMenu = !0, this.element.addClass("ui-autocomplete-input").attr("autocomplete", "off"), this._on(this.element, {
          keydown: function(event) {
            if (this.element.prop("readOnly")) return suppressKeyPress = !0, suppressInput = !0, void(suppressKeyPressRepeat = !0);
            suppressKeyPress = !1, suppressInput = !1, suppressKeyPressRepeat = !1;
            var keyCode = $.ui.keyCode;
            switch (event.keyCode) {
              case keyCode.PAGE_UP:
                suppressKeyPress = !0, this._move("previousPage", event);
                break;
              case keyCode.PAGE_DOWN:
                suppressKeyPress = !0, this._move("nextPage", event);
                break;
              case keyCode.UP:
                suppressKeyPress = !0, this._keyEvent("previous", event);
                break;
              case keyCode.DOWN:
                suppressKeyPress = !0, this._keyEvent("next", event);
                break;
              case keyCode.ENTER:
                this.menu.active && (suppressKeyPress = !0, event.preventDefault(), this.menu.select(event));
                break;
              case keyCode.TAB:
                this.menu.active && this.menu.select(event);
                break;
              case keyCode.ESCAPE:
                this.menu.element.is(":visible") && (this._value(this.term), this.close(event), event.preventDefault());
                break;
              default:
                suppressKeyPressRepeat = !0, this._searchTimeout(event)
            }
          },
          keypress: function(event) {
            if (suppressKeyPress) return suppressKeyPress = !1, void((!this.isMultiLine || this.menu.element.is(":visible")) && event.preventDefault());
            if (!suppressKeyPressRepeat) {
              var keyCode = $.ui.keyCode;
              switch (event.keyCode) {
                case keyCode.PAGE_UP:
                  this._move("previousPage", event);
                  break;
                case keyCode.PAGE_DOWN:
                  this._move("nextPage", event);
                  break;
                case keyCode.UP:
                  this._keyEvent("previous", event);
                  break;
                case keyCode.DOWN:
                  this._keyEvent("next", event)
              }
            }
          },
          input: function(event) {
            return suppressInput ? (suppressInput = !1, void event.preventDefault()) : void this._searchTimeout(event)
          },
          focus: function() {
            this.selectedItem = null, this.previous = this._value()
          },
          blur: function(event) {
            return this.cancelBlur ? void delete this.cancelBlur : (clearTimeout(this.searching), this.close(event), void this._change(event))
          }
        }), this._initSource(), this.menu = $("<ul>").addClass("ui-autocomplete ui-front").appendTo(this._appendTo()).menu({
          role: null
        }).hide().menu("instance"), this._on(this.menu.element, {
          mousedown: function(event) {
            event.preventDefault(), this.cancelBlur = !0, this._delay(function() {
              delete this.cancelBlur
            });
            var menuElement = this.menu.element[0];
            $(event.target).closest(".ui-menu-item").length || this._delay(function() {
              var that = this;
              this.document.one("mousedown", function(event) {
                event.target === that.element[0] || event.target === menuElement || $.contains(menuElement, event.target) || that.close()
              })
            })
          },
          menufocus: function(event, ui) {
            var label, item;
            return this.isNewMenu && (this.isNewMenu = !1, event.originalEvent && /^mouse/.test(event.originalEvent.type)) ? (this.menu.blur(), void this.document.one("mousemove", function() {
              $(event.target).trigger(event.originalEvent)
            })) : (item = ui.item.data("ui-autocomplete-item"), !1 !== this._trigger("focus", event, {
              item: item
            }) && event.originalEvent && /^key/.test(event.originalEvent.type) && this._value(item.value), label = ui.item.attr("aria-label") || item.value, void(label && jQuery.trim(label).length && (this.liveRegion.children().hide(), $("<div>").text(label).appendTo(this.liveRegion))))
          },
          menuselect: function(event, ui) {
            var item = ui.item.data("ui-autocomplete-item"),
              previous = this.previous;
            this.element[0] !== this.document[0].activeElement && (this.element.focus(), this.previous = previous, this._delay(function() {
              this.previous = previous, this.selectedItem = item
            })), !1 !== this._trigger("select", event, {
              item: item
            }) && this._value(item.value), this.term = this._value(), this.close(event), this.selectedItem = item
          }
        }), this.liveRegion = $("<span>", {
          role: "status",
          "aria-live": "assertive",
          "aria-relevant": "additions"
        }).addClass("ui-helper-hidden-accessible").appendTo(this.document[0].body), this._on(this.window, {
          beforeunload: function() {
            this.element.removeAttr("autocomplete")
          }
        })
      },
      _destroy: function() {
        clearTimeout(this.searching), this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete"), this.menu.element.remove(), this.liveRegion.remove()
      },
      _setOption: function(key, value) {
        this._super(key, value), "source" === key && this._initSource(), "appendTo" === key && this.menu.element.appendTo(this._appendTo()), "disabled" === key && value && this.xhr && this.xhr.abort()
      },
      _appendTo: function() {
        var element = this.options.appendTo;
        return element && (element = element.jquery || element.nodeType ? $(element) : this.document.find(element).eq(0)), element && element[0] || (element = this.element.closest(".ui-front")), element.length || (element = this.document[0].body), element
      },
      _initSource: function() {
        var array, url, that = this;
        $.isArray(this.options.source) ? (array = this.options.source, this.source = function(request, response) {
          response($.ui.autocomplete.filter(array, request.term))
        }) : "string" == typeof this.options.source ? (url = this.options.source, this.source = function(request, response) {
          that.xhr && that.xhr.abort(), that.xhr = $.ajax({
            url: url,
            data: request,
            dataType: "json",
            success: function(data) {
              response(data)
            },
            error: function() {
              response([])
            }
          })
        }) : this.source = this.options.source
      },
      _searchTimeout: function(event) {
        clearTimeout(this.searching), this.searching = this._delay(function() {
          var equalValues = this.term === this._value(),
            menuVisible = this.menu.element.is(":visible"),
            modifierKey = event.altKey || event.ctrlKey || event.metaKey || event.shiftKey;
          (!equalValues || equalValues && !menuVisible && !modifierKey) && (this.selectedItem = null, this.search(null, event))
        }, this.options.delay)
      },
      search: function(value, event) {
        return value = null != value ? value : this._value(), this.term = this._value(), value.length < this.options.minLength ? this.close(event) : this._trigger("search", event) !== !1 ? this._search(value) : void 0
      },
      _search: function(value) {
        this.pending++, this.element.addClass("ui-autocomplete-loading"), this.cancelSearch = !1, this.source({
          term: value
        }, this._response())
      },
      _response: function() {
        var index = ++this.requestIndex;
        return $.proxy(function(content) {
          index === this.requestIndex && this.__response(content), this.pending--, this.pending || this.element.removeClass("ui-autocomplete-loading")
        }, this)
      },
      __response: function(content) {
        content && (content = this._normalize(content)), this._trigger("response", null, {
          content: content
        }), !this.options.disabled && content && content.length && !this.cancelSearch ? (this._suggest(content), this._trigger("open")) : this._close()
      },
      close: function(event) {
        this.cancelSearch = !0, this._close(event)
      },
      _close: function(event) {
        this.menu.element.is(":visible") && (this.menu.element.hide(), this.menu.blur(), this.isNewMenu = !0, this._trigger("close", event))
      },
      _change: function(event) {
        this.previous !== this._value() && this._trigger("change", event, {
          item: this.selectedItem
        })
      },
      _normalize: function(items) {
        return items.length && items[0].label && items[0].value ? items : $.map(items, function(item) {
          return "string" == typeof item ? {
            label: item,
            value: item
          } : $.extend({}, item, {
            label: item.label || item.value,
            value: item.value || item.label
          })
        })
      },
      _suggest: function(items) {
        var ul = this.menu.element.empty();
        this._renderMenu(ul, items), this.isNewMenu = !0, this.menu.refresh(), ul.show(), this._resizeMenu(), ul.position($.extend({
          of: this.element
        }, this.options.position)), this.options.autoFocus && this.menu.next()
      },
      _resizeMenu: function() {
        var ul = this.menu.element;
        ul.outerWidth(Math.max(ul.width("").outerWidth() + 1, this.element.outerWidth()))
      },
      _renderMenu: function(ul, items) {
        var that = this;
        $.each(items, function(index, item) {
          that._renderItemData(ul, item)
        })
      },
      _renderItemData: function(ul, item) {
        return this._renderItem(ul, item).data("ui-autocomplete-item", item)
      },
      _renderItem: function(ul, item) {
        return $("<li>").text(item.label).appendTo(ul)
      },
      _move: function(direction, event) {
        return this.menu.element.is(":visible") ? this.menu.isFirstItem() && /^previous/.test(direction) || this.menu.isLastItem() && /^next/.test(direction) ? (this.isMultiLine || this._value(this.term), void this.menu.blur()) : void this.menu[direction](event) : void this.search(null, event)
      },
      widget: function() {
        return this.menu.element
      },
      _value: function() {
        return this.valueMethod.apply(this.element, arguments)
      },
      _keyEvent: function(keyEvent, event) {
        (!this.isMultiLine || this.menu.element.is(":visible")) && (this._move(keyEvent, event), event.preventDefault())
      }
    }), $.extend($.ui.autocomplete, {
      escapeRegex: function(value) {
        return value.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&")
      },
      filter: function(array, term) {
        var matcher = new RegExp($.ui.autocomplete.escapeRegex(term), "i");
        return $.grep(array, function(value) {
          return matcher.test(value.label || value.value || value)
        })
      }
    }), $.widget("ui.autocomplete", $.ui.autocomplete, {
      options: {
        messages: {
          noResults: "No search results.",
          results: function(amount) {
            return amount + (amount > 1 ? " results are" : " result is") + " available, use up and down arrow keys to navigate."
          }
        }
      },
      __response: function(content) {
        var message;
        this._superApply(arguments), this.options.disabled || this.cancelSearch || (message = content && content.length ? this.options.messages.results(content.length) : this.options.messages.noResults, this.liveRegion.children().hide(), $("<div>").text(message).appendTo(this.liveRegion))
      }
    });
    var lastActive, baseClasses = ($.ui.autocomplete, "ui-button ui-widget ui-state-default ui-corner-all"),
      typeClasses = "ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon-primary ui-button-text-icon-secondary ui-button-text-only",
      formResetHandler = function() {
        var form = $(this);
        setTimeout(function() {
          form.find(":ui-button").button("refresh")
        }, 1)
      },
      radioGroup = function(radio) {
        var name = radio.name,
          form = radio.form,
          radios = $([]);
        return name && (name = name.replace(/'/g, "\\'"), radios = form ? $(form).find("[name='" + name + "'][type=radio]") : $("[name='" + name + "'][type=radio]", radio.ownerDocument).filter(function() {
          return !this.form
        })), radios
      };
    $.widget("ui.button", {
      version: "1.11.0",
      defaultElement: "<button>",
      options: {
        disabled: null,
        text: !0,
        label: null,
        icons: {
          primary: null,
          secondary: null
        }
      },
      _create: function() {
        this.element.closest("form").unbind("reset" + this.eventNamespace).bind("reset" + this.eventNamespace, formResetHandler), "boolean" != typeof this.options.disabled ? this.options.disabled = !!this.element.prop("disabled") : this.element.prop("disabled", this.options.disabled), this._determineButtonType(), this.hasTitle = !!this.buttonElement.attr("title");
        var that = this,
          options = this.options,
          toggleButton = "checkbox" === this.type || "radio" === this.type,
          activeClass = toggleButton ? "" : "ui-state-active";
        null === options.label && (options.label = "input" === this.type ? this.buttonElement.val() : this.buttonElement.html()), this._hoverable(this.buttonElement), this.buttonElement.addClass(baseClasses).attr("role", "button").bind("mouseenter" + this.eventNamespace, function() {
          options.disabled || this === lastActive && $(this).addClass("ui-state-active")
        }).bind("mouseleave" + this.eventNamespace, function() {
          options.disabled || $(this).removeClass(activeClass)
        }).bind("click" + this.eventNamespace, function(event) {
          options.disabled && (event.preventDefault(), event.stopImmediatePropagation())
        }), this._on({
          focus: function() {
            this.buttonElement.addClass("ui-state-focus")
          },
          blur: function() {
            this.buttonElement.removeClass("ui-state-focus")
          }
        }), toggleButton && this.element.bind("change" + this.eventNamespace, function() {
          that.refresh()
        }), "checkbox" === this.type ? this.buttonElement.bind("click" + this.eventNamespace, function() {
          return options.disabled ? !1 : void 0
        }) : "radio" === this.type ? this.buttonElement.bind("click" + this.eventNamespace, function() {
          if (options.disabled) return !1;
          $(this).addClass("ui-state-active"), that.buttonElement.attr("aria-pressed", "true");
          var radio = that.element[0];
          radioGroup(radio).not(radio).map(function() {
            return $(this).button("widget")[0]
          }).removeClass("ui-state-active").attr("aria-pressed", "false")
        }) : (this.buttonElement.bind("mousedown" + this.eventNamespace, function() {
          return options.disabled ? !1 : ($(this).addClass("ui-state-active"), lastActive = this, void that.document.one("mouseup", function() {
            lastActive = null
          }))
        }).bind("mouseup" + this.eventNamespace, function() {
          return options.disabled ? !1 : void $(this).removeClass("ui-state-active")
        }).bind("keydown" + this.eventNamespace, function(event) {
          return options.disabled ? !1 : void((event.keyCode === $.ui.keyCode.SPACE || event.keyCode === $.ui.keyCode.ENTER) && $(this).addClass("ui-state-active"))
        }).bind("keyup" + this.eventNamespace + " blur" + this.eventNamespace, function() {
          $(this).removeClass("ui-state-active")
        }), this.buttonElement.is("a") && this.buttonElement.keyup(function(event) {
          event.keyCode === $.ui.keyCode.SPACE && $(this).click()
        })), this._setOption("disabled", options.disabled), this._resetButton()
      },
      _determineButtonType: function() {
        var ancestor, labelSelector, checked;
        this.element.is("[type=checkbox]") ? this.type = "checkbox" : this.element.is("[type=radio]") ? this.type = "radio" : this.element.is("input") ? this.type = "input" : this.type = "button", "checkbox" === this.type || "radio" === this.type ? (ancestor = this.element.parents().last(), labelSelector = "label[for='" + this.element.attr("id") + "']", this.buttonElement = ancestor.find(labelSelector), this.buttonElement.length || (ancestor = ancestor.length ? ancestor.siblings() : this.element.siblings(), this.buttonElement = ancestor.filter(labelSelector), this.buttonElement.length || (this.buttonElement = ancestor.find(labelSelector))), this.element.addClass("ui-helper-hidden-accessible"), checked = this.element.is(":checked"), checked && this.buttonElement.addClass("ui-state-active"), this.buttonElement.prop("aria-pressed", checked)) : this.buttonElement = this.element
      },
      widget: function() {
        return this.buttonElement
      },
      _destroy: function() {
        this.element.removeClass("ui-helper-hidden-accessible"), this.buttonElement.removeClass(baseClasses + " ui-state-active " + typeClasses).removeAttr("role").removeAttr("aria-pressed").html(this.buttonElement.find(".ui-button-text").html()), this.hasTitle || this.buttonElement.removeAttr("title")
      },
      _setOption: function(key, value) {
        return this._super(key, value), "disabled" === key ? (this.widget().toggleClass("ui-state-disabled", !!value), this.element.prop("disabled", !!value), void(value && ("checkbox" === this.type || "radio" === this.type ? this.buttonElement.removeClass("ui-state-focus") : this.buttonElement.removeClass("ui-state-focus ui-state-active")))) : void this._resetButton()
      },
      refresh: function() {
        var isDisabled = this.element.is("input, button") ? this.element.is(":disabled") : this.element.hasClass("ui-button-disabled");
        isDisabled !== this.options.disabled && this._setOption("disabled", isDisabled), "radio" === this.type ? radioGroup(this.element[0]).each(function() {
          $(this).is(":checked") ? $(this).button("widget").addClass("ui-state-active").attr("aria-pressed", "true") : $(this).button("widget").removeClass("ui-state-active").attr("aria-pressed", "false")
        }) : "checkbox" === this.type && (this.element.is(":checked") ? this.buttonElement.addClass("ui-state-active").attr("aria-pressed", "true") : this.buttonElement.removeClass("ui-state-active").attr("aria-pressed", "false"))
      },
      _resetButton: function() {
        if ("input" === this.type) return void(this.options.label && this.element.val(this.options.label));
        var buttonElement = this.buttonElement.removeClass(typeClasses),
          buttonText = $("<span></span>", this.document[0]).addClass("ui-button-text").html(this.options.label).appendTo(buttonElement.empty()).text(),
          icons = this.options.icons,
          multipleIcons = icons.primary && icons.secondary,
          buttonClasses = [];
        icons.primary || icons.secondary ? (this.options.text && buttonClasses.push("ui-button-text-icon" + (multipleIcons ? "s" : icons.primary ? "-primary" : "-secondary")), icons.primary && buttonElement.prepend("<span class='ui-button-icon-primary ui-icon " + icons.primary + "'></span>"), icons.secondary && buttonElement.append("<span class='ui-button-icon-secondary ui-icon " + icons.secondary + "'></span>"), this.options.text || (buttonClasses.push(multipleIcons ? "ui-button-icons-only" : "ui-button-icon-only"), this.hasTitle || buttonElement.attr("title", $.trim(buttonText)))) : buttonClasses.push("ui-button-text-only"), buttonElement.addClass(buttonClasses.join(" "))
      }
    }), $.widget("ui.buttonset", {
      version: "1.11.0",
      options: {
        items: "button, input[type=button], input[type=submit], input[type=reset], input[type=checkbox], input[type=radio], a, :data(ui-button)"
      },
      _create: function() {
        this.element.addClass("ui-buttonset")
      },
      _init: function() {
        this.refresh()
      },
      _setOption: function(key, value) {
        "disabled" === key && this.buttons.button("option", key, value), this._super(key, value)
      },
      refresh: function() {
        var rtl = "rtl" === this.element.css("direction"),
          allButtons = this.element.find(this.options.items),
          existingButtons = allButtons.filter(":ui-button");
        allButtons.not(":ui-button").button(), existingButtons.button("refresh"), this.buttons = allButtons.map(function() {
          return $(this).button("widget")[0]
        }).removeClass("ui-corner-all ui-corner-left ui-corner-right").filter(":first").addClass(rtl ? "ui-corner-right" : "ui-corner-left").end().filter(":last").addClass(rtl ? "ui-corner-left" : "ui-corner-right").end().end()
      },
      _destroy: function() {
        this.element.removeClass("ui-buttonset"), this.buttons.map(function() {
          return $(this).button("widget")[0]
        }).removeClass("ui-corner-left ui-corner-right").end().button("destroy")
      }
    });
    var dataSpace = ($.ui.button, $.widget("ui.dialog", {
      version: "1.11.0",
      options: {
        appendTo: "body",
        autoOpen: !0,
        buttons: [],
        closeOnEscape: !0,
        closeText: "Close",
        dialogClass: "",
        draggable: !0,
        hide: null,
        height: "auto",
        maxHeight: null,
        maxWidth: null,
        minHeight: 150,
        minWidth: 150,
        modal: !1,
        position: {
          my: "center",
          at: "center",
          of: window,
          collision: "fit",
          using: function(pos) {
            var topOffset = $(this).css(pos).offset().top;
            0 > topOffset && $(this).css("top", pos.top - topOffset)
          }
        },
        resizable: !0,
        show: null,
        title: null,
        width: 300,
        beforeClose: null,
        close: null,
        drag: null,
        dragStart: null,
        dragStop: null,
        focus: null,
        open: null,
        resize: null,
        resizeStart: null,
        resizeStop: null
      },
      sizeRelatedOptions: {
        buttons: !0,
        height: !0,
        maxHeight: !0,
        maxWidth: !0,
        minHeight: !0,
        minWidth: !0,
        width: !0
      },
      resizableRelatedOptions: {
        maxHeight: !0,
        maxWidth: !0,
        minHeight: !0,
        minWidth: !0
      },
      _create: function() {
        this.originalCss = {
          display: this.element[0].style.display,
          width: this.element[0].style.width,
          minHeight: this.element[0].style.minHeight,
          maxHeight: this.element[0].style.maxHeight,
          height: this.element[0].style.height
        }, this.originalPosition = {
          parent: this.element.parent(),
          index: this.element.parent().children().index(this.element)
        }, this.originalTitle = this.element.attr("title"), this.options.title = this.options.title || this.originalTitle, this._createWrapper(), this.element.show().removeAttr("title").addClass("ui-dialog-content ui-widget-content").appendTo(this.uiDialog), this._createTitlebar(), this._createButtonPane(), this.options.draggable && $.fn.draggable && this._makeDraggable(), this.options.resizable && $.fn.resizable && this._makeResizable(), this._isOpen = !1, this._trackFocus()
      },
      _init: function() {
        this.options.autoOpen && this.open()
      },
      _appendTo: function() {
        var element = this.options.appendTo;
        return element && (element.jquery || element.nodeType) ? $(element) : this.document.find(element || "body").eq(0)
      },
      _destroy: function() {
        var next, originalPosition = this.originalPosition;
        this._destroyOverlay(), this.element.removeUniqueId().removeClass("ui-dialog-content ui-widget-content").css(this.originalCss).detach(), this.uiDialog.stop(!0, !0).remove(), this.originalTitle && this.element.attr("title", this.originalTitle), next = originalPosition.parent.children().eq(originalPosition.index), next.length && next[0] !== this.element[0] ? next.before(this.element) : originalPosition.parent.append(this.element)
      },
      widget: function() {
        return this.uiDialog
      },
      disable: $.noop,
      enable: $.noop,
      close: function(event) {
        var activeElement, that = this;
        if (this._isOpen && this._trigger("beforeClose", event) !== !1) {
          if (this._isOpen = !1, this._focusedElement = null, this._destroyOverlay(), this._untrackInstance(), !this.opener.filter(":focusable").focus().length) try {
            activeElement = this.document[0].activeElement, activeElement && "body" !== activeElement.nodeName.toLowerCase() && $(activeElement).blur()
          } catch (error) {}
          this._hide(this.uiDialog, this.options.hide, function() {
            that._trigger("close", event)
          })
        }
      },
      isOpen: function() {
        return this._isOpen
      },
      moveToTop: function() {
        this._moveToTop()
      },
      _moveToTop: function(event, silent) {
        var moved = !1,
          zIndicies = this.uiDialog.siblings(".ui-front:visible").map(function() {
            return +$(this).css("z-index")
          }).get(),
          zIndexMax = Math.max.apply(null, zIndicies);
        return zIndexMax >= +this.uiDialog.css("z-index") && (this.uiDialog.css("z-index", zIndexMax + 1), moved = !0), moved && !silent && this._trigger("focus", event), moved
      },
      open: function() {
        var that = this;
        return this._isOpen ? void(this._moveToTop() && this._focusTabbable()) : (this._isOpen = !0, this.opener = $(this.document[0].activeElement), this._size(), this._position(), this._createOverlay(), this._moveToTop(null, !0), this._show(this.uiDialog, this.options.show, function() {
          that._focusTabbable(), that._trigger("focus")
        }), void this._trigger("open"))
      },
      _focusTabbable: function() {
        var hasFocus = this._focusedElement;
        hasFocus || (hasFocus = this.element.find("[autofocus]")), hasFocus.length || (hasFocus = this.element.find(":tabbable")), hasFocus.length || (hasFocus = this.uiDialogButtonPane.find(":tabbable")), hasFocus.length || (hasFocus = this.uiDialogTitlebarClose.filter(":tabbable")), hasFocus.length || (hasFocus = this.uiDialog), hasFocus.eq(0).focus()
      },
      _keepFocus: function(event) {
        function checkFocus() {
          var activeElement = this.document[0].activeElement,
            isActive = this.uiDialog[0] === activeElement || $.contains(this.uiDialog[0], activeElement);
          isActive || this._focusTabbable()
        }
        event.preventDefault(), checkFocus.call(this), this._delay(checkFocus)
      },
      _createWrapper: function() {
        this.uiDialog = $("<div>").addClass("ui-dialog ui-widget ui-widget-content ui-corner-all ui-front " + this.options.dialogClass).hide().attr({
          tabIndex: -1,
          role: "dialog"
        }).appendTo(this._appendTo()), this._on(this.uiDialog, {
          keydown: function(event) {
            if (this.options.closeOnEscape && !event.isDefaultPrevented() && event.keyCode && event.keyCode === $.ui.keyCode.ESCAPE) return event.preventDefault(), void this.close(event);
            if (event.keyCode === $.ui.keyCode.TAB && !event.isDefaultPrevented()) {
              var tabbables = this.uiDialog.find(":tabbable"),
                first = tabbables.filter(":first"),
                last = tabbables.filter(":last");
              event.target !== last[0] && event.target !== this.uiDialog[0] || event.shiftKey ? event.target !== first[0] && event.target !== this.uiDialog[0] || !event.shiftKey || (this._delay(function() {
                last.focus()
              }), event.preventDefault()) : (this._delay(function() {
                first.focus()
              }), event.preventDefault())
            }
          },
          mousedown: function(event) {
            this._moveToTop(event) && this._focusTabbable()
          }
        }), this.element.find("[aria-describedby]").length || this.uiDialog.attr({
          "aria-describedby": this.element.uniqueId().attr("id")
        })
      },
      _createTitlebar: function() {
        var uiDialogTitle;
        this.uiDialogTitlebar = $("<div>").addClass("ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix").prependTo(this.uiDialog), this._on(this.uiDialogTitlebar, {
          mousedown: function(event) {
            $(event.target).closest(".ui-dialog-titlebar-close") || this.uiDialog.focus()
          }
        }), this.uiDialogTitlebarClose = $("<button type='button'></button>").button({
          label: this.options.closeText,
          icons: {
            primary: "ui-icon-closethick"
          },
          text: !1
        }).addClass("ui-dialog-titlebar-close").appendTo(this.uiDialogTitlebar), this._on(this.uiDialogTitlebarClose, {
          click: function(event) {
            event.preventDefault(), this.close(event)
          }
        }), uiDialogTitle = $("<span>").uniqueId().addClass("ui-dialog-title").prependTo(this.uiDialogTitlebar), this._title(uiDialogTitle), this.uiDialog.attr({
          "aria-labelledby": uiDialogTitle.attr("id")
        })
      },
      _title: function(title) {
        this.options.title || title.html("&#160;"), title.text(this.options.title)
      },
      _createButtonPane: function() {
        this.uiDialogButtonPane = $("<div>").addClass("ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"), this.uiButtonSet = $("<div>").addClass("ui-dialog-buttonset").appendTo(this.uiDialogButtonPane), this._createButtons()
      },
      _createButtons: function() {
        var that = this,
          buttons = this.options.buttons;
        return this.uiDialogButtonPane.remove(), this.uiButtonSet.empty(), $.isEmptyObject(buttons) || $.isArray(buttons) && !buttons.length ? void this.uiDialog.removeClass("ui-dialog-buttons") : ($.each(buttons, function(name, props) {
          var click, buttonOptions;
          props = $.isFunction(props) ? {
            click: props,
            text: name
          } : props, props = $.extend({
            type: "button"
          }, props), click = props.click, props.click = function() {
            click.apply(that.element[0], arguments)
          }, buttonOptions = {
            icons: props.icons,
            text: props.showText
          }, delete props.icons, delete props.showText, $("<button></button>", props).button(buttonOptions).appendTo(that.uiButtonSet)
        }), this.uiDialog.addClass("ui-dialog-buttons"), void this.uiDialogButtonPane.appendTo(this.uiDialog))
      },
      _makeDraggable: function() {
        function filteredUi(ui) {
          return {
            position: ui.position,
            offset: ui.offset
          }
        }
        var that = this,
          options = this.options;
        this.uiDialog.draggable({
          cancel: ".ui-dialog-content, .ui-dialog-titlebar-close",
          handle: ".ui-dialog-titlebar",
          containment: "document",
          start: function(event, ui) {
            $(this).addClass("ui-dialog-dragging"), that._blockFrames(), that._trigger("dragStart", event, filteredUi(ui))
          },
          drag: function(event, ui) {
            that._trigger("drag", event, filteredUi(ui))
          },
          stop: function(event, ui) {
            var left = ui.offset.left - that.document.scrollLeft(),
              top = ui.offset.top - that.document.scrollTop();
            options.position = {
              my: "left top",
              at: "left" + (left >= 0 ? "+" : "") + left + " top" + (top >= 0 ? "+" : "") + top,
              of: that.window
            }, $(this).removeClass("ui-dialog-dragging"), that._unblockFrames(), that._trigger("dragStop", event, filteredUi(ui))
          }
        })
      },
      _makeResizable: function() {
        function filteredUi(ui) {
          return {
            originalPosition: ui.originalPosition,
            originalSize: ui.originalSize,
            position: ui.position,
            size: ui.size
          }
        }
        var that = this,
          options = this.options,
          handles = options.resizable,
          position = this.uiDialog.css("position"),
          resizeHandles = "string" == typeof handles ? handles : "n,e,s,w,se,sw,ne,nw";
        this.uiDialog.resizable({
          cancel: ".ui-dialog-content",
          containment: "document",
          alsoResize: this.element,
          maxWidth: options.maxWidth,
          maxHeight: options.maxHeight,
          minWidth: options.minWidth,
          minHeight: this._minHeight(),
          handles: resizeHandles,
          start: function(event, ui) {
            $(this).addClass("ui-dialog-resizing"), that._blockFrames(), that._trigger("resizeStart", event, filteredUi(ui))
          },
          resize: function(event, ui) {
            that._trigger("resize", event, filteredUi(ui))
          },
          stop: function(event, ui) {
            var offset = that.uiDialog.offset(),
              left = offset.left - that.document.scrollLeft(),
              top = offset.top - that.document.scrollTop();
            options.height = that.uiDialog.height(), options.width = that.uiDialog.width(), options.position = {
              my: "left top",
              at: "left" + (left >= 0 ? "+" : "") + left + " top" + (top >= 0 ? "+" : "") + top,
              of: that.window
            }, $(this).removeClass("ui-dialog-resizing"), that._unblockFrames(), that._trigger("resizeStop", event, filteredUi(ui))
          }
        }).css("position", position)
      },
      _trackFocus: function() {
        this._on(this.widget(), {
          focusin: function(event) {
            this._untrackInstance(), this._trackingInstances().unshift(this), this._focusedElement = $(event.target)
          }
        })
      },
      _untrackInstance: function() {
        var instances = this._trackingInstances(),
          exists = $.inArray(this, instances); - 1 !== exists && instances.splice(exists, 1)
      },
      _trackingInstances: function() {
        var instances = this.document.data("ui-dialog-instances");
        return instances || (instances = [], this.document.data("ui-dialog-instances", instances)), instances
      },
      _minHeight: function() {
        var options = this.options;
        return "auto" === options.height ? options.minHeight : Math.min(options.minHeight, options.height)
      },
      _position: function() {
        var isVisible = this.uiDialog.is(":visible");
        isVisible || this.uiDialog.show(), this.uiDialog.position(this.options.position), isVisible || this.uiDialog.hide()
      },
      _setOptions: function(options) {
        var that = this,
          resize = !1,
          resizableOptions = {};
        $.each(options, function(key, value) {
          that._setOption(key, value), key in that.sizeRelatedOptions && (resize = !0), key in that.resizableRelatedOptions && (resizableOptions[key] = value)
        }), resize && (this._size(), this._position()), this.uiDialog.is(":data(ui-resizable)") && this.uiDialog.resizable("option", resizableOptions)
      },
      _setOption: function(key, value) {
        var isDraggable, isResizable, uiDialog = this.uiDialog;
        "dialogClass" === key && uiDialog.removeClass(this.options.dialogClass).addClass(value), "disabled" !== key && (this._super(key, value), "appendTo" === key && this.uiDialog.appendTo(this._appendTo()), "buttons" === key && this._createButtons(), "closeText" === key && this.uiDialogTitlebarClose.button({
          label: "" + value
        }), "draggable" === key && (isDraggable = uiDialog.is(":data(ui-draggable)"), isDraggable && !value && uiDialog.draggable("destroy"), !isDraggable && value && this._makeDraggable()), "position" === key && this._position(), "resizable" === key && (isResizable = uiDialog.is(":data(ui-resizable)"), isResizable && !value && uiDialog.resizable("destroy"), isResizable && "string" == typeof value && uiDialog.resizable("option", "handles", value), isResizable || value === !1 || this._makeResizable()), "title" === key && this._title(this.uiDialogTitlebar.find(".ui-dialog-title")))
      },
      _size: function() {
        var nonContentHeight, minContentHeight, maxContentHeight, options = this.options;
        this.element.show().css({
          width: "auto",
          minHeight: 0,
          maxHeight: "none",
          height: 0
        }), options.minWidth > options.width && (options.width = options.minWidth), nonContentHeight = this.uiDialog.css({
          height: "auto",
          width: options.width
        }).outerHeight(), minContentHeight = Math.max(0, options.minHeight - nonContentHeight), maxContentHeight = "number" == typeof options.maxHeight ? Math.max(0, options.maxHeight - nonContentHeight) : "none", "auto" === options.height ? this.element.css({
          minHeight: minContentHeight,
          maxHeight: maxContentHeight,
          height: "auto"
        }) : this.element.height(Math.max(0, options.height - nonContentHeight)), this.uiDialog.is(":data(ui-resizable)") && this.uiDialog.resizable("option", "minHeight", this._minHeight())
      },
      _blockFrames: function() {
        this.iframeBlocks = this.document.find("iframe").map(function() {
          var iframe = $(this);
          return $("<div>").css({
            position: "absolute",
            width: iframe.outerWidth(),
            height: iframe.outerHeight()
          }).appendTo(iframe.parent()).offset(iframe.offset())[0]
        })
      },
      _unblockFrames: function() {
        this.iframeBlocks && (this.iframeBlocks.remove(), delete this.iframeBlocks)
      },
      _allowInteraction: function(event) {
        return $(event.target).closest(".ui-dialog").length ? !0 : !!$(event.target).closest(".ui-datepicker").length
      },
      _createOverlay: function() {
        if (this.options.modal) {
          var isOpening = !0;
          this._delay(function() {
            isOpening = !1
          }), this.document.data("ui-dialog-overlays") || this._on(this.document, {
            focusin: function(event) {
              isOpening || this._allowInteraction(event) || (event.preventDefault(), this._trackingInstances()[0]._focusTabbable())
            }
          }), this.overlay = $("<div>").addClass("ui-widget-overlay ui-front").appendTo(this._appendTo()), this._on(this.overlay, {
            mousedown: "_keepFocus"
          }), this.document.data("ui-dialog-overlays", (this.document.data("ui-dialog-overlays") || 0) + 1);
        }
      },
      _destroyOverlay: function() {
        if (this.options.modal && this.overlay) {
          var overlays = this.document.data("ui-dialog-overlays") - 1;
          overlays ? this.document.data("ui-dialog-overlays", overlays) : this.document.unbind("focusin").removeData("ui-dialog-overlays"), this.overlay.remove(), this.overlay = null
        }
      }
    }), $.widget("ui.progressbar", {
      version: "1.11.0",
      options: {
        max: 100,
        value: 0,
        change: null,
        complete: null
      },
      min: 0,
      _create: function() {
        this.oldValue = this.options.value = this._constrainedValue(), this.element.addClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").attr({
          role: "progressbar",
          "aria-valuemin": this.min
        }), this.valueDiv = $("<div class='ui-progressbar-value ui-widget-header ui-corner-left'></div>").appendTo(this.element), this._refreshValue()
      },
      _destroy: function() {
        this.element.removeClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow"), this.valueDiv.remove()
      },
      value: function(newValue) {
        return void 0 === newValue ? this.options.value : (this.options.value = this._constrainedValue(newValue), void this._refreshValue())
      },
      _constrainedValue: function(newValue) {
        return void 0 === newValue && (newValue = this.options.value), this.indeterminate = newValue === !1, "number" != typeof newValue && (newValue = 0), this.indeterminate ? !1 : Math.min(this.options.max, Math.max(this.min, newValue))
      },
      _setOptions: function(options) {
        var value = options.value;
        delete options.value, this._super(options), this.options.value = this._constrainedValue(value), this._refreshValue()
      },
      _setOption: function(key, value) {
        "max" === key && (value = Math.max(this.min, value)), "disabled" === key && this.element.toggleClass("ui-state-disabled", !!value).attr("aria-disabled", value), this._super(key, value)
      },
      _percentage: function() {
        return this.indeterminate ? 100 : 100 * (this.options.value - this.min) / (this.options.max - this.min)
      },
      _refreshValue: function() {
        var value = this.options.value,
          percentage = this._percentage();
        this.valueDiv.toggle(this.indeterminate || value > this.min).toggleClass("ui-corner-right", value === this.options.max).width(percentage.toFixed(0) + "%"), this.element.toggleClass("ui-progressbar-indeterminate", this.indeterminate), this.indeterminate ? (this.element.removeAttr("aria-valuenow"), this.overlayDiv || (this.overlayDiv = $("<div class='ui-progressbar-overlay'></div>").appendTo(this.valueDiv))) : (this.element.attr({
          "aria-valuemax": this.options.max,
          "aria-valuenow": value
        }), this.overlayDiv && (this.overlayDiv.remove(), this.overlayDiv = null)), this.oldValue !== value && (this.oldValue = value, this._trigger("change")), value === this.options.max && this._trigger("complete")
      }
    }), $.widget("ui.selectmenu", {
      version: "1.11.0",
      defaultElement: "<select>",
      options: {
        appendTo: null,
        disabled: null,
        icons: {
          button: "ui-icon-triangle-1-s"
        },
        position: {
          my: "left top",
          at: "left bottom",
          collision: "none"
        },
        width: null,
        change: null,
        close: null,
        focus: null,
        open: null,
        select: null
      },
      _create: function() {
        var selectmenuId = this.element.uniqueId().attr("id");
        this.ids = {
          element: selectmenuId,
          button: selectmenuId + "-button",
          menu: selectmenuId + "-menu"
        }, this._drawButton(), this._drawMenu(), this.options.disabled && this.disable()
      },
      _drawButton: function() {
        var that = this,
          tabindex = this.element.attr("tabindex");
        this.label = $("label[for='" + this.ids.element + "']").attr("for", this.ids.button), this._on(this.label, {
          click: function(event) {
            this.button.focus(), event.preventDefault()
          }
        }), this.element.hide(), this.button = $("<span>", {
          "class": "ui-selectmenu-button ui-widget ui-state-default ui-corner-all",
          tabindex: tabindex || this.options.disabled ? -1 : 0,
          id: this.ids.button,
          role: "combobox",
          "aria-expanded": "false",
          "aria-autocomplete": "list",
          "aria-owns": this.ids.menu,
          "aria-haspopup": "true"
        }).insertAfter(this.element), $("<span>", {
          "class": "ui-icon " + this.options.icons.button
        }).prependTo(this.button), this.buttonText = $("<span>", {
          "class": "ui-selectmenu-text"
        }).appendTo(this.button), this._setText(this.buttonText, this.element.find("option:selected").text()), this._setOption("width", this.options.width), this._on(this.button, this._buttonEvents), this.button.one("focusin", function() {
          that.menuItems || that._refreshMenu()
        }), this._hoverable(this.button), this._focusable(this.button)
      },
      _drawMenu: function() {
        var that = this;
        this.menu = $("<ul>", {
          "aria-hidden": "true",
          "aria-labelledby": this.ids.button,
          id: this.ids.menu
        }), this.menuWrap = $("<div>", {
          "class": "ui-selectmenu-menu ui-front"
        }).append(this.menu).appendTo(this._appendTo()), this.menuInstance = this.menu.menu({
          role: "listbox",
          select: function(event, ui) {
            event.preventDefault(), that._select(ui.item.data("ui-selectmenu-item"), event)
          },
          focus: function(event, ui) {
            var item = ui.item.data("ui-selectmenu-item");
            null != that.focusIndex && item.index !== that.focusIndex && (that._trigger("focus", event, {
              item: item
            }), that.isOpen || that._select(item, event)), that.focusIndex = item.index, that.button.attr("aria-activedescendant", that.menuItems.eq(item.index).attr("id"))
          }
        }).menu("instance"), this.menu.addClass("ui-corner-bottom").removeClass("ui-corner-all"), this.menuInstance._off(this.menu, "mouseleave"), this.menuInstance._closeOnDocumentClick = function() {
          return !1
        }, this.menuInstance._isDivider = function() {
          return !1
        }
      },
      refresh: function() {
        this._refreshMenu(), this._setText(this.buttonText, this._getSelectedItem().text()), this._setOption("width", this.options.width)
      },
      _refreshMenu: function() {
        this.menu.empty();
        var item, options = this.element.find("option");
        options.length && (this._parseOptions(options), this._renderMenu(this.menu, this.items), this.menuInstance.refresh(), this.menuItems = this.menu.find("li").not(".ui-selectmenu-optgroup"), item = this._getSelectedItem(), this.menuInstance.focus(null, item), this._setAria(item.data("ui-selectmenu-item")), this._setOption("disabled", this.element.prop("disabled")))
      },
      open: function(event) {
        this.options.disabled || (this.menuItems ? (this.menu.find(".ui-state-focus").removeClass("ui-state-focus"), this.menuInstance.focus(null, this._getSelectedItem())) : this._refreshMenu(), this.isOpen = !0, this._toggleAttr(), this._resizeMenu(), this._position(), this._on(this.document, this._documentClick), this._trigger("open", event))
      },
      _position: function() {
        this.menuWrap.position($.extend({
          of: this.button
        }, this.options.position))
      },
      close: function(event) {
        this.isOpen && (this.isOpen = !1, this._toggleAttr(), this._off(this.document), this._trigger("close", event))
      },
      widget: function() {
        return this.button
      },
      menuWidget: function() {
        return this.menu
      },
      _renderMenu: function(ul, items) {
        var that = this,
          currentOptgroup = "";
        $.each(items, function(index, item) {
          item.optgroup !== currentOptgroup && ($("<li>", {
            "class": "ui-selectmenu-optgroup ui-menu-divider" + (item.element.parent("optgroup").prop("disabled") ? " ui-state-disabled" : ""),
            text: item.optgroup
          }).appendTo(ul), currentOptgroup = item.optgroup), that._renderItemData(ul, item)
        })
      },
      _renderItemData: function(ul, item) {
        return this._renderItem(ul, item).data("ui-selectmenu-item", item)
      },
      _renderItem: function(ul, item) {
        var li = $("<li>");
        return item.disabled && li.addClass("ui-state-disabled"), this._setText(li, item.label), li.appendTo(ul)
      },
      _setText: function(element, value) {
        value ? element.text(value) : element.html("&#160;")
      },
      _move: function(direction, event) {
        var item, next, filter = ".ui-menu-item";
        this.isOpen ? item = this.menuItems.eq(this.focusIndex) : (item = this.menuItems.eq(this.element[0].selectedIndex), filter += ":not(.ui-state-disabled)"), next = "first" === direction || "last" === direction ? item["first" === direction ? "prevAll" : "nextAll"](filter).eq(-1) : item[direction + "All"](filter).eq(0), next.length && this.menuInstance.focus(event, next)
      },
      _getSelectedItem: function() {
        return this.menuItems.eq(this.element[0].selectedIndex)
      },
      _toggle: function(event) {
        this[this.isOpen ? "close" : "open"](event)
      },
      _documentClick: {
        mousedown: function(event) {
          this.isOpen && ($(event.target).closest(".ui-selectmenu-menu, #" + this.ids.button).length || this.close(event))
        }
      },
      _buttonEvents: {
        click: "_toggle",
        keydown: function(event) {
          var preventDefault = !0;
          switch (event.keyCode) {
            case $.ui.keyCode.TAB:
            case $.ui.keyCode.ESCAPE:
              this.close(event), preventDefault = !1;
              break;
            case $.ui.keyCode.ENTER:
              this.isOpen && this._selectFocusedItem(event);
              break;
            case $.ui.keyCode.UP:
              event.altKey ? this._toggle(event) : this._move("prev", event);
              break;
            case $.ui.keyCode.DOWN:
              event.altKey ? this._toggle(event) : this._move("next", event);
              break;
            case $.ui.keyCode.SPACE:
              this.isOpen ? this._selectFocusedItem(event) : this._toggle(event);
              break;
            case $.ui.keyCode.LEFT:
              this._move("prev", event);
              break;
            case $.ui.keyCode.RIGHT:
              this._move("next", event);
              break;
            case $.ui.keyCode.HOME:
            case $.ui.keyCode.PAGE_UP:
              this._move("first", event);
              break;
            case $.ui.keyCode.END:
            case $.ui.keyCode.PAGE_DOWN:
              this._move("last", event);
              break;
            default:
              this.menu.trigger(event), preventDefault = !1
          }
          preventDefault && event.preventDefault()
        }
      },
      _selectFocusedItem: function(event) {
        var item = this.menuItems.eq(this.focusIndex);
        item.hasClass("ui-state-disabled") || this._select(item.data("ui-selectmenu-item"), event)
      },
      _select: function(item, event) {
        var oldIndex = this.element[0].selectedIndex;
        this.element[0].selectedIndex = item.index, this._setText(this.buttonText, item.label), this._setAria(item), this._trigger("select", event, {
          item: item
        }), item.index !== oldIndex && this._trigger("change", event, {
          item: item
        }), this.close(event)
      },
      _setAria: function(item) {
        var id = this.menuItems.eq(item.index).attr("id");
        this.button.attr({
          "aria-labelledby": id,
          "aria-activedescendant": id
        }), this.menu.attr("aria-activedescendant", id)
      },
      _setOption: function(key, value) {
        "icons" === key && this.button.find("span.ui-icon").removeClass(this.options.icons.button).addClass(value.button), this._super(key, value), "appendTo" === key && this.menuWrap.appendTo(this._appendTo()), "disabled" === key && (this.menuInstance.option("disabled", value), this.button.toggleClass("ui-state-disabled", value).attr("aria-disabled", value), this.element.prop("disabled", value), value ? (this.button.attr("tabindex", -1), this.close()) : this.button.attr("tabindex", 0)), "width" === key && (value || (value = this.element.outerWidth()), this.button.outerWidth(value))
      },
      _appendTo: function() {
        var element = this.options.appendTo;
        return element && (element = element.jquery || element.nodeType ? $(element) : this.document.find(element).eq(0)), element && element[0] || (element = this.element.closest(".ui-front")), element.length || (element = this.document[0].body), element
      },
      _toggleAttr: function() {
        this.button.toggleClass("ui-corner-top", this.isOpen).toggleClass("ui-corner-all", !this.isOpen).attr("aria-expanded", this.isOpen), this.menuWrap.toggleClass("ui-selectmenu-open", this.isOpen), this.menu.attr("aria-hidden", !this.isOpen)
      },
      _resizeMenu: function() {
        this.menu.outerWidth(Math.max(this.button.outerWidth(), this.menu.width("").outerWidth() + 1))
      },
      _getCreateOptions: function() {
        return {
          disabled: this.element.prop("disabled")
        }
      },
      _parseOptions: function(options) {
        var data = [];
        options.each(function(index, item) {
          var option = $(item),
            optgroup = option.parent("optgroup");
          data.push({
            element: option,
            index: index,
            value: option.attr("value"),
            label: option.text(),
            optgroup: optgroup.attr("label") || "",
            disabled: optgroup.prop("disabled") || option.prop("disabled")
          })
        }), this.items = data
      },
      _destroy: function() {
        this.menuWrap.remove(), this.button.remove(), this.element.show(), this.element.removeUniqueId(), this.label.attr("for", this.ids.element)
      }
    }), $.widget("ui.slider", $.ui.mouse, {
      version: "1.11.0",
      widgetEventPrefix: "slide",
      options: {
        animate: !1,
        distance: 0,
        max: 100,
        min: 0,
        orientation: "horizontal",
        range: !1,
        step: 1,
        value: 0,
        values: null,
        change: null,
        slide: null,
        start: null,
        stop: null
      },
      numPages: 5,
      _create: function() {
        this._keySliding = !1, this._mouseSliding = !1, this._animateOff = !0, this._handleIndex = null, this._detectOrientation(), this._mouseInit(), this.element.addClass("ui-slider ui-slider-" + this.orientation + " ui-widget ui-widget-content ui-corner-all"), this._refresh(), this._setOption("disabled", this.options.disabled), this._animateOff = !1
      },
      _refresh: function() {
        this._createRange(), this._createHandles(), this._setupEvents(), this._refreshValue()
      },
      _createHandles: function() {
        var i, handleCount, options = this.options,
          existingHandles = this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"),
          handle = "<span class='ui-slider-handle ui-state-default ui-corner-all' tabindex='0'></span>",
          handles = [];
        for (handleCount = options.values && options.values.length || 1, existingHandles.length > handleCount && (existingHandles.slice(handleCount).remove(), existingHandles = existingHandles.slice(0, handleCount)), i = existingHandles.length; handleCount > i; i++) handles.push(handle);
        this.handles = existingHandles.add($(handles.join("")).appendTo(this.element)), this.handle = this.handles.eq(0), this.handles.each(function(i) {
          $(this).data("ui-slider-handle-index", i)
        })
      },
      _createRange: function() {
        var options = this.options,
          classes = "";
        options.range ? (options.range === !0 && (options.values ? options.values.length && 2 !== options.values.length ? options.values = [options.values[0], options.values[0]] : $.isArray(options.values) && (options.values = options.values.slice(0)) : options.values = [this._valueMin(), this._valueMin()]), this.range && this.range.length ? this.range.removeClass("ui-slider-range-min ui-slider-range-max").css({
          left: "",
          bottom: ""
        }) : (this.range = $("<div></div>").appendTo(this.element), classes = "ui-slider-range ui-widget-header ui-corner-all"), this.range.addClass(classes + ("min" === options.range || "max" === options.range ? " ui-slider-range-" + options.range : ""))) : (this.range && this.range.remove(), this.range = null)
      },
      _setupEvents: function() {
        this._off(this.handles), this._on(this.handles, this._handleEvents), this._hoverable(this.handles), this._focusable(this.handles)
      },
      _destroy: function() {
        this.handles.remove(), this.range && this.range.remove(), this.element.removeClass("ui-slider ui-slider-horizontal ui-slider-vertical ui-widget ui-widget-content ui-corner-all"), this._mouseDestroy()
      },
      _mouseCapture: function(event) {
        var position, normValue, distance, closestHandle, index, allowed, offset, mouseOverHandle, that = this,
          o = this.options;
        return o.disabled ? !1 : (this.elementSize = {
          width: this.element.outerWidth(),
          height: this.element.outerHeight()
        }, this.elementOffset = this.element.offset(), position = {
          x: event.pageX,
          y: event.pageY
        }, normValue = this._normValueFromMouse(position), distance = this._valueMax() - this._valueMin() + 1, this.handles.each(function(i) {
          var thisDistance = Math.abs(normValue - that.values(i));
          (distance > thisDistance || distance === thisDistance && (i === that._lastChangedValue || that.values(i) === o.min)) && (distance = thisDistance, closestHandle = $(this), index = i)
        }), allowed = this._start(event, index), allowed === !1 ? !1 : (this._mouseSliding = !0, this._handleIndex = index, closestHandle.addClass("ui-state-active").focus(), offset = closestHandle.offset(), mouseOverHandle = !$(event.target).parents().addBack().is(".ui-slider-handle"), this._clickOffset = mouseOverHandle ? {
          left: 0,
          top: 0
        } : {
          left: event.pageX - offset.left - closestHandle.width() / 2,
          top: event.pageY - offset.top - closestHandle.height() / 2 - (parseInt(closestHandle.css("borderTopWidth"), 10) || 0) - (parseInt(closestHandle.css("borderBottomWidth"), 10) || 0) + (parseInt(closestHandle.css("marginTop"), 10) || 0)
        }, this.handles.hasClass("ui-state-hover") || this._slide(event, index, normValue), this._animateOff = !0, !0))
      },
      _mouseStart: function() {
        return !0
      },
      _mouseDrag: function(event) {
        var position = {
            x: event.pageX,
            y: event.pageY
          },
          normValue = this._normValueFromMouse(position);
        return this._slide(event, this._handleIndex, normValue), !1
      },
      _mouseStop: function(event) {
        return this.handles.removeClass("ui-state-active"), this._mouseSliding = !1, this._stop(event, this._handleIndex), this._change(event, this._handleIndex), this._handleIndex = null, this._clickOffset = null, this._animateOff = !1, !1
      },
      _detectOrientation: function() {
        this.orientation = "vertical" === this.options.orientation ? "vertical" : "horizontal"
      },
      _normValueFromMouse: function(position) {
        var pixelTotal, pixelMouse, percentMouse, valueTotal, valueMouse;
        return "horizontal" === this.orientation ? (pixelTotal = this.elementSize.width, pixelMouse = position.x - this.elementOffset.left - (this._clickOffset ? this._clickOffset.left : 0)) : (pixelTotal = this.elementSize.height, pixelMouse = position.y - this.elementOffset.top - (this._clickOffset ? this._clickOffset.top : 0)), percentMouse = pixelMouse / pixelTotal, percentMouse > 1 && (percentMouse = 1), 0 > percentMouse && (percentMouse = 0), "vertical" === this.orientation && (percentMouse = 1 - percentMouse), valueTotal = this._valueMax() - this._valueMin(), valueMouse = this._valueMin() + percentMouse * valueTotal, this._trimAlignValue(valueMouse)
      },
      _start: function(event, index) {
        var uiHash = {
          handle: this.handles[index],
          value: this.value()
        };
        return this.options.values && this.options.values.length && (uiHash.value = this.values(index), uiHash.values = this.values()), this._trigger("start", event, uiHash)
      },
      _slide: function(event, index, newVal) {
        var otherVal, newValues, allowed;
        this.options.values && this.options.values.length ? (otherVal = this.values(index ? 0 : 1), 2 === this.options.values.length && this.options.range === !0 && (0 === index && newVal > otherVal || 1 === index && otherVal > newVal) && (newVal = otherVal), newVal !== this.values(index) && (newValues = this.values(), newValues[index] = newVal, allowed = this._trigger("slide", event, {
          handle: this.handles[index],
          value: newVal,
          values: newValues
        }), otherVal = this.values(index ? 0 : 1), allowed !== !1 && this.values(index, newVal))) : newVal !== this.value() && (allowed = this._trigger("slide", event, {
          handle: this.handles[index],
          value: newVal
        }), allowed !== !1 && this.value(newVal))
      },
      _stop: function(event, index) {
        var uiHash = {
          handle: this.handles[index],
          value: this.value()
        };
        this.options.values && this.options.values.length && (uiHash.value = this.values(index), uiHash.values = this.values()), this._trigger("stop", event, uiHash)
      },
      _change: function(event, index) {
        if (!this._keySliding && !this._mouseSliding) {
          var uiHash = {
            handle: this.handles[index],
            value: this.value()
          };
          this.options.values && this.options.values.length && (uiHash.value = this.values(index), uiHash.values = this.values()), this._lastChangedValue = index, this._trigger("change", event, uiHash)
        }
      },
      value: function(newValue) {
        return arguments.length ? (this.options.value = this._trimAlignValue(newValue), this._refreshValue(), void this._change(null, 0)) : this._value()
      },
      values: function(index, newValue) {
        var vals, newValues, i;
        if (arguments.length > 1) return this.options.values[index] = this._trimAlignValue(newValue), this._refreshValue(), void this._change(null, index);
        if (!arguments.length) return this._values();
        if (!$.isArray(arguments[0])) return this.options.values && this.options.values.length ? this._values(index) : this.value();
        for (vals = this.options.values, newValues = arguments[0], i = 0; i < vals.length; i += 1) vals[i] = this._trimAlignValue(newValues[i]), this._change(null, i);
        this._refreshValue()
      },
      _setOption: function(key, value) {
        var i, valsLength = 0;
        switch ("range" === key && this.options.range === !0 && ("min" === value ? (this.options.value = this._values(0), this.options.values = null) : "max" === value && (this.options.value = this._values(this.options.values.length - 1), this.options.values = null)), $.isArray(this.options.values) && (valsLength = this.options.values.length), "disabled" === key && this.element.toggleClass("ui-state-disabled", !!value), this._super(key, value), key) {
          case "orientation":
            this._detectOrientation(), this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-" + this.orientation), this._refreshValue();
            break;
          case "value":
            this._animateOff = !0, this._refreshValue(), this._change(null, 0), this._animateOff = !1;
            break;
          case "values":
            for (this._animateOff = !0, this._refreshValue(), i = 0; valsLength > i; i += 1) this._change(null, i);
            this._animateOff = !1;
            break;
          case "min":
          case "max":
            this._animateOff = !0, this._refreshValue(), this._animateOff = !1;
            break;
          case "range":
            this._animateOff = !0, this._refresh(), this._animateOff = !1
        }
      },
      _value: function() {
        var val = this.options.value;
        return val = this._trimAlignValue(val)
      },
      _values: function(index) {
        var val, vals, i;
        if (arguments.length) return val = this.options.values[index], val = this._trimAlignValue(val);
        if (this.options.values && this.options.values.length) {
          for (vals = this.options.values.slice(), i = 0; i < vals.length; i += 1) vals[i] = this._trimAlignValue(vals[i]);
          return vals
        }
        return []
      },
      _trimAlignValue: function(val) {
        if (val <= this._valueMin()) return this._valueMin();
        if (val >= this._valueMax()) return this._valueMax();
        var step = this.options.step > 0 ? this.options.step : 1,
          valModStep = (val - this._valueMin()) % step,
          alignValue = val - valModStep;
        return 2 * Math.abs(valModStep) >= step && (alignValue += valModStep > 0 ? step : -step), parseFloat(alignValue.toFixed(5))
      },
      _valueMin: function() {
        return this.options.min
      },
      _valueMax: function() {
        return this.options.max
      },
      _refreshValue: function() {
        var lastValPercent, valPercent, value, valueMin, valueMax, oRange = this.options.range,
          o = this.options,
          that = this,
          animate = this._animateOff ? !1 : o.animate,
          _set = {};
        this.options.values && this.options.values.length ? this.handles.each(function(i) {
          valPercent = (that.values(i) - that._valueMin()) / (that._valueMax() - that._valueMin()) * 100, _set["horizontal" === that.orientation ? "left" : "bottom"] = valPercent + "%", $(this).stop(1, 1)[animate ? "animate" : "css"](_set, o.animate), that.options.range === !0 && ("horizontal" === that.orientation ? (0 === i && that.range.stop(1, 1)[animate ? "animate" : "css"]({
            left: valPercent + "%"
          }, o.animate), 1 === i && that.range[animate ? "animate" : "css"]({
            width: valPercent - lastValPercent + "%"
          }, {
            queue: !1,
            duration: o.animate
          })) : (0 === i && that.range.stop(1, 1)[animate ? "animate" : "css"]({
            bottom: valPercent + "%"
          }, o.animate), 1 === i && that.range[animate ? "animate" : "css"]({
            height: valPercent - lastValPercent + "%"
          }, {
            queue: !1,
            duration: o.animate
          }))), lastValPercent = valPercent
        }) : (value = this.value(), valueMin = this._valueMin(), valueMax = this._valueMax(), valPercent = valueMax !== valueMin ? (value - valueMin) / (valueMax - valueMin) * 100 : 0, _set["horizontal" === this.orientation ? "left" : "bottom"] = valPercent + "%", this.handle.stop(1, 1)[animate ? "animate" : "css"](_set, o.animate), "min" === oRange && "horizontal" === this.orientation && this.range.stop(1, 1)[animate ? "animate" : "css"]({
          width: valPercent + "%"
        }, o.animate), "max" === oRange && "horizontal" === this.orientation && this.range[animate ? "animate" : "css"]({
          width: 100 - valPercent + "%"
        }, {
          queue: !1,
          duration: o.animate
        }), "min" === oRange && "vertical" === this.orientation && this.range.stop(1, 1)[animate ? "animate" : "css"]({
          height: valPercent + "%"
        }, o.animate), "max" === oRange && "vertical" === this.orientation && this.range[animate ? "animate" : "css"]({
          height: 100 - valPercent + "%"
        }, {
          queue: !1,
          duration: o.animate
        }))
      },
      _handleEvents: {
        keydown: function(event) {
          var allowed, curVal, newVal, step, index = $(event.target).data("ui-slider-handle-index");
          switch (event.keyCode) {
            case $.ui.keyCode.HOME:
            case $.ui.keyCode.END:
            case $.ui.keyCode.PAGE_UP:
            case $.ui.keyCode.PAGE_DOWN:
            case $.ui.keyCode.UP:
            case $.ui.keyCode.RIGHT:
            case $.ui.keyCode.DOWN:
            case $.ui.keyCode.LEFT:
              if (event.preventDefault(), !this._keySliding && (this._keySliding = !0, $(event.target).addClass("ui-state-active"), allowed = this._start(event, index), allowed === !1)) return
          }
          switch (step = this.options.step, curVal = newVal = this.options.values && this.options.values.length ? this.values(index) : this.value(), event.keyCode) {
            case $.ui.keyCode.HOME:
              newVal = this._valueMin();
              break;
            case $.ui.keyCode.END:
              newVal = this._valueMax();
              break;
            case $.ui.keyCode.PAGE_UP:
              newVal = this._trimAlignValue(curVal + (this._valueMax() - this._valueMin()) / this.numPages);
              break;
            case $.ui.keyCode.PAGE_DOWN:
              newVal = this._trimAlignValue(curVal - (this._valueMax() - this._valueMin()) / this.numPages);
              break;
            case $.ui.keyCode.UP:
            case $.ui.keyCode.RIGHT:
              if (curVal === this._valueMax()) return;
              newVal = this._trimAlignValue(curVal + step);
              break;
            case $.ui.keyCode.DOWN:
            case $.ui.keyCode.LEFT:
              if (curVal === this._valueMin()) return;
              newVal = this._trimAlignValue(curVal - step)
          }
          this._slide(event, index, newVal)
        },
        keyup: function(event) {
          var index = $(event.target).data("ui-slider-handle-index");
          this._keySliding && (this._keySliding = !1, this._stop(event, index), this._change(event, index), $(event.target).removeClass("ui-state-active"))
        }
      }
    }), $.widget("ui.spinner", {
      version: "1.11.0",
      defaultElement: "<input>",
      widgetEventPrefix: "spin",
      options: {
        culture: null,
        icons: {
          down: "ui-icon-triangle-1-s",
          up: "ui-icon-triangle-1-n"
        },
        incremental: !0,
        max: null,
        min: null,
        numberFormat: null,
        page: 10,
        step: 1,
        change: null,
        spin: null,
        start: null,
        stop: null
      },
      _create: function() {
        this._setOption("max", this.options.max), this._setOption("min", this.options.min), this._setOption("step", this.options.step), "" !== this.value() && this._value(this.element.val(), !0), this._draw(), this._on(this._events), this._refresh(), this._on(this.window, {
          beforeunload: function() {
            this.element.removeAttr("autocomplete")
          }
        })
      },
      _getCreateOptions: function() {
        var options = {},
          element = this.element;
        return $.each(["min", "max", "step"], function(i, option) {
          var value = element.attr(option);
          void 0 !== value && value.length && (options[option] = value)
        }), options
      },
      _events: {
        keydown: function(event) {
          this._start(event) && this._keydown(event) && event.preventDefault()
        },
        keyup: "_stop",
        focus: function() {
          this.previous = this.element.val()
        },
        blur: function(event) {
          return this.cancelBlur ? void delete this.cancelBlur : (this._stop(), this._refresh(), void(this.previous !== this.element.val() && this._trigger("change", event)))
        },
        mousewheel: function(event, delta) {
          if (delta) {
            if (!this.spinning && !this._start(event)) return !1;
            this._spin((delta > 0 ? 1 : -1) * this.options.step, event), clearTimeout(this.mousewheelTimer), this.mousewheelTimer = this._delay(function() {
              this.spinning && this._stop(event)
            }, 100), event.preventDefault()
          }
        },
        "mousedown .ui-spinner-button": function(event) {
          function checkFocus() {
            var isActive = this.element[0] === this.document[0].activeElement;
            isActive || (this.element.focus(), this.previous = previous, this._delay(function() {
              this.previous = previous
            }))
          }
          var previous;
          previous = this.element[0] === this.document[0].activeElement ? this.previous : this.element.val(), event.preventDefault(), checkFocus.call(this), this.cancelBlur = !0, this._delay(function() {
            delete this.cancelBlur, checkFocus.call(this)
          }), this._start(event) !== !1 && this._repeat(null, $(event.currentTarget).hasClass("ui-spinner-up") ? 1 : -1, event)
        },
        "mouseup .ui-spinner-button": "_stop",
        "mouseenter .ui-spinner-button": function(event) {
          return $(event.currentTarget).hasClass("ui-state-active") ? this._start(event) === !1 ? !1 : void this._repeat(null, $(event.currentTarget).hasClass("ui-spinner-up") ? 1 : -1, event) : void 0
        },
        "mouseleave .ui-spinner-button": "_stop"
      },
      _draw: function() {
        var uiSpinner = this.uiSpinner = this.element.addClass("ui-spinner-input").attr("autocomplete", "off").wrap(this._uiSpinnerHtml()).parent().append(this._buttonHtml());
        this.element.attr("role", "spinbutton"), this.buttons = uiSpinner.find(".ui-spinner-button").attr("tabIndex", -1).button().removeClass("ui-corner-all"), this.buttons.height() > Math.ceil(.5 * uiSpinner.height()) && uiSpinner.height() > 0 && uiSpinner.height(uiSpinner.height()), this.options.disabled && this.disable()
      },
      _keydown: function(event) {
        var options = this.options,
          keyCode = $.ui.keyCode;
        switch (event.keyCode) {
          case keyCode.UP:
            return this._repeat(null, 1, event), !0;
          case keyCode.DOWN:
            return this._repeat(null, -1, event), !0;
          case keyCode.PAGE_UP:
            return this._repeat(null, options.page, event), !0;
          case keyCode.PAGE_DOWN:
            return this._repeat(null, -options.page, event), !0
        }
        return !1
      },
      _uiSpinnerHtml: function() {
        return "<span class='ui-spinner ui-widget ui-widget-content ui-corner-all'></span>"
      },
      _buttonHtml: function() {
        return "<a class='ui-spinner-button ui-spinner-up ui-corner-tr'><span class='ui-icon " + this.options.icons.up + "'>&#9650;</span></a><a class='ui-spinner-button ui-spinner-down ui-corner-br'><span class='ui-icon " + this.options.icons.down + "'>&#9660;</span></a>"
      },
      _start: function(event) {
        return this.spinning || this._trigger("start", event) !== !1 ? (this.counter || (this.counter = 1), this.spinning = !0, !0) : !1
      },
      _repeat: function(i, steps, event) {
        i = i || 500, clearTimeout(this.timer), this.timer = this._delay(function() {
          this._repeat(40, steps, event)
        }, i), this._spin(steps * this.options.step, event)
      },
      _spin: function(step, event) {
        var value = this.value() || 0;
        this.counter || (this.counter = 1), value = this._adjustValue(value + step * this._increment(this.counter)), this.spinning && this._trigger("spin", event, {
          value: value
        }) === !1 || (this._value(value), this.counter++)
      },
      _increment: function(i) {
        var incremental = this.options.incremental;
        return incremental ? $.isFunction(incremental) ? incremental(i) : Math.floor(i * i * i / 5e4 - i * i / 500 + 17 * i / 200 + 1) : 1
      },
      _precision: function() {
        var precision = this._precisionOf(this.options.step);
        return null !== this.options.min && (precision = Math.max(precision, this._precisionOf(this.options.min))), precision
      },
      _precisionOf: function(num) {
        var str = num.toString(),
          decimal = str.indexOf(".");
        return -1 === decimal ? 0 : str.length - decimal - 1
      },
      _adjustValue: function(value) {
        var base, aboveMin, options = this.options;
        return base = null !== options.min ? options.min : 0, aboveMin = value - base, aboveMin = Math.round(aboveMin / options.step) * options.step, value = base + aboveMin, value = parseFloat(value.toFixed(this._precision())), null !== options.max && value > options.max ? options.max : null !== options.min && value < options.min ? options.min : value
      },
      _stop: function(event) {
        this.spinning && (clearTimeout(this.timer), clearTimeout(this.mousewheelTimer), this.counter = 0, this.spinning = !1, this._trigger("stop", event))
      },
      _setOption: function(key, value) {
        if ("culture" === key || "numberFormat" === key) {
          var prevValue = this._parse(this.element.val());
          return this.options[key] = value, void this.element.val(this._format(prevValue))
        }("max" === key || "min" === key || "step" === key) && "string" == typeof value && (value = this._parse(value)), "icons" === key && (this.buttons.first().find(".ui-icon").removeClass(this.options.icons.up).addClass(value.up), this.buttons.last().find(".ui-icon").removeClass(this.options.icons.down).addClass(value.down)), this._super(key, value), "disabled" === key && (this.widget().toggleClass("ui-state-disabled", !!value), this.element.prop("disabled", !!value), this.buttons.button(value ? "disable" : "enable"))
      },
      _setOptions: spinner_modifier(function(options) {
        this._super(options)
      }),
      _parse: function(val) {
        return "string" == typeof val && "" !== val && (val = window.Globalize && this.options.numberFormat ? Globalize.parseFloat(val, 10, this.options.culture) : +val), "" === val || isNaN(val) ? null : val
      },
      _format: function(value) {
        return "" === value ? "" : window.Globalize && this.options.numberFormat ? Globalize.format(value, this.options.numberFormat, this.options.culture) : value
      },
      _refresh: function() {
        this.element.attr({
          "aria-valuemin": this.options.min,
          "aria-valuemax": this.options.max,
          "aria-valuenow": this._parse(this.element.val())
        })
      },
      isValid: function() {
        var value = this.value();
        return null === value ? !1 : value === this._adjustValue(value)
      },
      _value: function(value, allowAny) {
        var parsed;
        "" !== value && (parsed = this._parse(value), null !== parsed && (allowAny || (parsed = this._adjustValue(parsed)), value = this._format(parsed))), this.element.val(value), this._refresh()
      },
      _destroy: function() {
        this.element.removeClass("ui-spinner-input").prop("disabled", !1).removeAttr("autocomplete").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow"), this.uiSpinner.replaceWith(this.element)
      },
      stepUp: spinner_modifier(function(steps) {
        this._stepUp(steps)
      }),
      _stepUp: function(steps) {
        this._start() && (this._spin((steps || 1) * this.options.step), this._stop())
      },
      stepDown: spinner_modifier(function(steps) {
        this._stepDown(steps)
      }),
      _stepDown: function(steps) {
        this._start() && (this._spin((steps || 1) * -this.options.step), this._stop())
      },
      pageUp: spinner_modifier(function(pages) {
        this._stepUp((pages || 1) * this.options.page)
      }),
      pageDown: spinner_modifier(function(pages) {
        this._stepDown((pages || 1) * this.options.page)
      }),
      value: function(newVal) {
        return arguments.length ? void spinner_modifier(this._value).call(this, newVal) : this._parse(this.element.val())
      },
      widget: function() {
        return this.uiSpinner
      }
    }), $.widget("ui.tabs", {
      version: "1.11.0",
      delay: 300,
      options: {
        active: null,
        collapsible: !1,
        event: "click",
        heightStyle: "content",
        hide: null,
        show: null,
        activate: null,
        beforeActivate: null,
        beforeLoad: null,
        load: null
      },
      _isLocal: function() {
        var rhash = /#.*$/;
        return function(anchor) {
          var anchorUrl, locationUrl;
          anchor = anchor.cloneNode(!1), anchorUrl = anchor.href.replace(rhash, ""), locationUrl = location.href.replace(rhash, "");
          try {
            anchorUrl = decodeURIComponent(anchorUrl)
          } catch (error) {}
          try {
            locationUrl = decodeURIComponent(locationUrl)
          } catch (error) {}
          return anchor.hash.length > 1 && anchorUrl === locationUrl
        }
      }(),
      _create: function() {
        var that = this,
          options = this.options;
        this.running = !1, this.element.addClass("ui-tabs ui-widget ui-widget-content ui-corner-all").toggleClass("ui-tabs-collapsible", options.collapsible).delegate(".ui-tabs-nav > li", "mousedown" + this.eventNamespace, function(event) {
            $(this).is(".ui-state-disabled") && event.preventDefault()
          }).delegate(".ui-tabs-anchor", "focus" + this.eventNamespace, function() {
            $(this).closest("li").is(".ui-state-disabled") && this.blur()
          }),
          this._processTabs(), options.active = this._initialActive(), $.isArray(options.disabled) && (options.disabled = $.unique(options.disabled.concat($.map(this.tabs.filter(".ui-state-disabled"), function(li) {
            return that.tabs.index(li)
          }))).sort()), this.options.active !== !1 && this.anchors.length ? this.active = this._findActive(options.active) : this.active = $(), this._refresh(), this.active.length && this.load(options.active)
      },
      _initialActive: function() {
        var active = this.options.active,
          collapsible = this.options.collapsible,
          locationHash = location.hash.substring(1);
        return null === active && (locationHash && this.tabs.each(function(i, tab) {
          return $(tab).attr("aria-controls") === locationHash ? (active = i, !1) : void 0
        }), null === active && (active = this.tabs.index(this.tabs.filter(".ui-tabs-active"))), (null === active || -1 === active) && (active = this.tabs.length ? 0 : !1)), active !== !1 && (active = this.tabs.index(this.tabs.eq(active)), -1 === active && (active = collapsible ? !1 : 0)), !collapsible && active === !1 && this.anchors.length && (active = 0), active
      },
      _getCreateEventData: function() {
        return {
          tab: this.active,
          panel: this.active.length ? this._getPanelForTab(this.active) : $()
        }
      },
      _tabKeydown: function(event) {
        var focusedTab = $(this.document[0].activeElement).closest("li"),
          selectedIndex = this.tabs.index(focusedTab),
          goingForward = !0;
        if (!this._handlePageNav(event)) {
          switch (event.keyCode) {
            case $.ui.keyCode.RIGHT:
            case $.ui.keyCode.DOWN:
              selectedIndex++;
              break;
            case $.ui.keyCode.UP:
            case $.ui.keyCode.LEFT:
              goingForward = !1, selectedIndex--;
              break;
            case $.ui.keyCode.END:
              selectedIndex = this.anchors.length - 1;
              break;
            case $.ui.keyCode.HOME:
              selectedIndex = 0;
              break;
            case $.ui.keyCode.SPACE:
              return event.preventDefault(), clearTimeout(this.activating), void this._activate(selectedIndex);
            case $.ui.keyCode.ENTER:
              return event.preventDefault(), clearTimeout(this.activating), void this._activate(selectedIndex === this.options.active ? !1 : selectedIndex);
            default:
              return
          }
          event.preventDefault(), clearTimeout(this.activating), selectedIndex = this._focusNextTab(selectedIndex, goingForward), event.ctrlKey || (focusedTab.attr("aria-selected", "false"), this.tabs.eq(selectedIndex).attr("aria-selected", "true"), this.activating = this._delay(function() {
            this.option("active", selectedIndex)
          }, this.delay))
        }
      },
      _panelKeydown: function(event) {
        this._handlePageNav(event) || event.ctrlKey && event.keyCode === $.ui.keyCode.UP && (event.preventDefault(), this.active.focus())
      },
      _handlePageNav: function(event) {
        return event.altKey && event.keyCode === $.ui.keyCode.PAGE_UP ? (this._activate(this._focusNextTab(this.options.active - 1, !1)), !0) : event.altKey && event.keyCode === $.ui.keyCode.PAGE_DOWN ? (this._activate(this._focusNextTab(this.options.active + 1, !0)), !0) : void 0
      },
      _findNextTab: function(index, goingForward) {
        function constrain() {
          return index > lastTabIndex && (index = 0), 0 > index && (index = lastTabIndex), index
        }
        for (var lastTabIndex = this.tabs.length - 1; - 1 !== $.inArray(constrain(), this.options.disabled);) index = goingForward ? index + 1 : index - 1;
        return index
      },
      _focusNextTab: function(index, goingForward) {
        return index = this._findNextTab(index, goingForward), this.tabs.eq(index).focus(), index
      },
      _setOption: function(key, value) {
        return "active" === key ? void this._activate(value) : "disabled" === key ? void this._setupDisabled(value) : (this._super(key, value), "collapsible" === key && (this.element.toggleClass("ui-tabs-collapsible", value), value || this.options.active !== !1 || this._activate(0)), "event" === key && this._setupEvents(value), void("heightStyle" === key && this._setupHeightStyle(value)))
      },
      _sanitizeSelector: function(hash) {
        return hash ? hash.replace(/[!"$%&'()*+,.\/:;<=>?@\[\]\^`{|}~]/g, "\\$&") : ""
      },
      refresh: function() {
        var options = this.options,
          lis = this.tablist.children(":has(a[href])");
        options.disabled = $.map(lis.filter(".ui-state-disabled"), function(tab) {
          return lis.index(tab)
        }), this._processTabs(), options.active !== !1 && this.anchors.length ? this.active.length && !$.contains(this.tablist[0], this.active[0]) ? this.tabs.length === options.disabled.length ? (options.active = !1, this.active = $()) : this._activate(this._findNextTab(Math.max(0, options.active - 1), !1)) : options.active = this.tabs.index(this.active) : (options.active = !1, this.active = $()), this._refresh()
      },
      _refresh: function() {
        this._setupDisabled(this.options.disabled), this._setupEvents(this.options.event), this._setupHeightStyle(this.options.heightStyle), this.tabs.not(this.active).attr({
          "aria-selected": "false",
          "aria-expanded": "false",
          tabIndex: -1
        }), this.panels.not(this._getPanelForTab(this.active)).hide().attr({
          "aria-hidden": "true"
        }), this.active.length ? (this.active.addClass("ui-tabs-active ui-state-active").attr({
          "aria-selected": "true",
          "aria-expanded": "true",
          tabIndex: 0
        }), this._getPanelForTab(this.active).show().attr({
          "aria-hidden": "false"
        })) : this.tabs.eq(0).attr("tabIndex", 0)
      },
      _processTabs: function() {
        var that = this;
        this.tablist = this._getList().addClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").attr("role", "tablist"), this.tabs = this.tablist.find("> li:has(a[href])").addClass("ui-state-default ui-corner-top").attr({
          role: "tab",
          tabIndex: -1
        }), this.anchors = this.tabs.map(function() {
          return $("a", this)[0]
        }).addClass("ui-tabs-anchor").attr({
          role: "presentation",
          tabIndex: -1
        }), this.panels = $(), this.anchors.each(function(i, anchor) {
          var selector, panel, panelId, anchorId = $(anchor).uniqueId().attr("id"),
            tab = $(anchor).closest("li"),
            originalAriaControls = tab.attr("aria-controls");
          that._isLocal(anchor) ? (selector = anchor.hash, panelId = selector.substring(1), panel = that.element.find(that._sanitizeSelector(selector))) : (panelId = tab.attr("aria-controls") || $({}).uniqueId()[0].id, selector = "#" + panelId, panel = that.element.find(selector), panel.length || (panel = that._createPanel(panelId), panel.insertAfter(that.panels[i - 1] || that.tablist)), panel.attr("aria-live", "polite")), panel.length && (that.panels = that.panels.add(panel)), originalAriaControls && tab.data("ui-tabs-aria-controls", originalAriaControls), tab.attr({
            "aria-controls": panelId,
            "aria-labelledby": anchorId
          }), panel.attr("aria-labelledby", anchorId)
        }), this.panels.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").attr("role", "tabpanel")
      },
      _getList: function() {
        return this.tablist || this.element.find("ol,ul").eq(0)
      },
      _createPanel: function(id) {
        return $("<div>").attr("id", id).addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").data("ui-tabs-destroy", !0)
      },
      _setupDisabled: function(disabled) {
        $.isArray(disabled) && (disabled.length ? disabled.length === this.anchors.length && (disabled = !0) : disabled = !1);
        for (var li, i = 0; li = this.tabs[i]; i++) disabled === !0 || -1 !== $.inArray(i, disabled) ? $(li).addClass("ui-state-disabled").attr("aria-disabled", "true") : $(li).removeClass("ui-state-disabled").removeAttr("aria-disabled");
        this.options.disabled = disabled
      },
      _setupEvents: function(event) {
        var events = {};
        event && $.each(event.split(" "), function(index, eventName) {
          events[eventName] = "_eventHandler"
        }), this._off(this.anchors.add(this.tabs).add(this.panels)), this._on(!0, this.anchors, {
          click: function(event) {
            event.preventDefault()
          }
        }), this._on(this.anchors, events), this._on(this.tabs, {
          keydown: "_tabKeydown"
        }), this._on(this.panels, {
          keydown: "_panelKeydown"
        }), this._focusable(this.tabs), this._hoverable(this.tabs)
      },
      _setupHeightStyle: function(heightStyle) {
        var maxHeight, parent = this.element.parent();
        "fill" === heightStyle ? (maxHeight = parent.height(), maxHeight -= this.element.outerHeight() - this.element.height(), this.element.siblings(":visible").each(function() {
          var elem = $(this),
            position = elem.css("position");
          "absolute" !== position && "fixed" !== position && (maxHeight -= elem.outerHeight(!0))
        }), this.element.children().not(this.panels).each(function() {
          maxHeight -= $(this).outerHeight(!0)
        }), this.panels.each(function() {
          $(this).height(Math.max(0, maxHeight - $(this).innerHeight() + $(this).height()))
        }).css("overflow", "auto")) : "auto" === heightStyle && (maxHeight = 0, this.panels.each(function() {
          maxHeight = Math.max(maxHeight, $(this).height("").height())
        }).height(maxHeight))
      },
      _eventHandler: function(event) {
        var options = this.options,
          active = this.active,
          anchor = $(event.currentTarget),
          tab = anchor.closest("li"),
          clickedIsActive = tab[0] === active[0],
          collapsing = clickedIsActive && options.collapsible,
          toShow = collapsing ? $() : this._getPanelForTab(tab),
          toHide = active.length ? this._getPanelForTab(active) : $(),
          eventData = {
            oldTab: active,
            oldPanel: toHide,
            newTab: collapsing ? $() : tab,
            newPanel: toShow
          };
        event.preventDefault(), tab.hasClass("ui-state-disabled") || tab.hasClass("ui-tabs-loading") || this.running || clickedIsActive && !options.collapsible || this._trigger("beforeActivate", event, eventData) === !1 || (options.active = collapsing ? !1 : this.tabs.index(tab), this.active = clickedIsActive ? $() : tab, this.xhr && this.xhr.abort(), toHide.length || toShow.length || $.error("jQuery UI Tabs: Mismatching fragment identifier."), toShow.length && this.load(this.tabs.index(tab), event), this._toggle(event, eventData))
      },
      _toggle: function(event, eventData) {
        function complete() {
          that.running = !1, that._trigger("activate", event, eventData)
        }

        function show() {
          eventData.newTab.closest("li").addClass("ui-tabs-active ui-state-active"), toShow.length && that.options.show ? that._show(toShow, that.options.show, complete) : (toShow.show(), complete())
        }
        var that = this,
          toShow = eventData.newPanel,
          toHide = eventData.oldPanel;
        this.running = !0, toHide.length && this.options.hide ? this._hide(toHide, this.options.hide, function() {
          eventData.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"), show()
        }) : (eventData.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"), toHide.hide(), show()), toHide.attr("aria-hidden", "true"), eventData.oldTab.attr({
          "aria-selected": "false",
          "aria-expanded": "false"
        }), toShow.length && toHide.length ? eventData.oldTab.attr("tabIndex", -1) : toShow.length && this.tabs.filter(function() {
          return 0 === $(this).attr("tabIndex")
        }).attr("tabIndex", -1), toShow.attr("aria-hidden", "false"), eventData.newTab.attr({
          "aria-selected": "true",
          "aria-expanded": "true",
          tabIndex: 0
        })
      },
      _activate: function(index) {
        var anchor, active = this._findActive(index);
        active[0] !== this.active[0] && (active.length || (active = this.active), anchor = active.find(".ui-tabs-anchor")[0], this._eventHandler({
          target: anchor,
          currentTarget: anchor,
          preventDefault: $.noop
        }))
      },
      _findActive: function(index) {
        return index === !1 ? $() : this.tabs.eq(index)
      },
      _getIndex: function(index) {
        return "string" == typeof index && (index = this.anchors.index(this.anchors.filter("[href$='" + index + "']"))), index
      },
      _destroy: function() {
        this.xhr && this.xhr.abort(), this.element.removeClass("ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-collapsible"), this.tablist.removeClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").removeAttr("role"), this.anchors.removeClass("ui-tabs-anchor").removeAttr("role").removeAttr("tabIndex").removeUniqueId(), this.tabs.add(this.panels).each(function() {
          $.data(this, "ui-tabs-destroy") ? $(this).remove() : $(this).removeClass("ui-state-default ui-state-active ui-state-disabled ui-corner-top ui-corner-bottom ui-widget-content ui-tabs-active ui-tabs-panel").removeAttr("tabIndex").removeAttr("aria-live").removeAttr("aria-busy").removeAttr("aria-selected").removeAttr("aria-labelledby").removeAttr("aria-hidden").removeAttr("aria-expanded").removeAttr("role")
        }), this.tabs.each(function() {
          var li = $(this),
            prev = li.data("ui-tabs-aria-controls");
          prev ? li.attr("aria-controls", prev).removeData("ui-tabs-aria-controls") : li.removeAttr("aria-controls")
        }), this.panels.show(), "content" !== this.options.heightStyle && this.panels.css("height", "")
      },
      enable: function(index) {
        var disabled = this.options.disabled;
        disabled !== !1 && (void 0 === index ? disabled = !1 : (index = this._getIndex(index), disabled = $.isArray(disabled) ? $.map(disabled, function(num) {
          return num !== index ? num : null
        }) : $.map(this.tabs, function(li, num) {
          return num !== index ? num : null
        })), this._setupDisabled(disabled))
      },
      disable: function(index) {
        var disabled = this.options.disabled;
        if (disabled !== !0) {
          if (void 0 === index) disabled = !0;
          else {
            if (index = this._getIndex(index), -1 !== $.inArray(index, disabled)) return;
            disabled = $.isArray(disabled) ? $.merge([index], disabled).sort() : [index]
          }
          this._setupDisabled(disabled)
        }
      },
      load: function(index, event) {
        index = this._getIndex(index);
        var that = this,
          tab = this.tabs.eq(index),
          anchor = tab.find(".ui-tabs-anchor"),
          panel = this._getPanelForTab(tab),
          eventData = {
            tab: tab,
            panel: panel
          };
        this._isLocal(anchor[0]) || (this.xhr = $.ajax(this._ajaxSettings(anchor, event, eventData)), this.xhr && "canceled" !== this.xhr.statusText && (tab.addClass("ui-tabs-loading"), panel.attr("aria-busy", "true"), this.xhr.success(function(response) {
          setTimeout(function() {
            panel.html(response), that._trigger("load", event, eventData)
          }, 1)
        }).complete(function(jqXHR, status) {
          setTimeout(function() {
            "abort" === status && that.panels.stop(!1, !0), tab.removeClass("ui-tabs-loading"), panel.removeAttr("aria-busy"), jqXHR === that.xhr && delete that.xhr
          }, 1)
        })))
      },
      _ajaxSettings: function(anchor, event, eventData) {
        var that = this;
        return {
          url: anchor.attr("href"),
          beforeSend: function(jqXHR, settings) {
            return that._trigger("beforeLoad", event, $.extend({
              jqXHR: jqXHR,
              ajaxSettings: settings
            }, eventData))
          }
        }
      },
      _getPanelForTab: function(tab) {
        var id = $(tab).attr("aria-controls");
        return this.element.find(this._sanitizeSelector("#" + id))
      }
    }), $.widget("ui.tooltip", {
      version: "1.11.0",
      options: {
        content: function() {
          var title = $(this).attr("title") || "";
          return $("<a>").text(title).html()
        },
        hide: !0,
        items: "[title]:not([disabled])",
        position: {
          my: "left top+15",
          at: "left bottom",
          collision: "flipfit flip"
        },
        show: !0,
        tooltipClass: null,
        track: !1,
        close: null,
        open: null
      },
      _addDescribedBy: function(elem, id) {
        var describedby = (elem.attr("aria-describedby") || "").split(/\s+/);
        describedby.push(id), elem.data("ui-tooltip-id", id).attr("aria-describedby", $.trim(describedby.join(" ")))
      },
      _removeDescribedBy: function(elem) {
        var id = elem.data("ui-tooltip-id"),
          describedby = (elem.attr("aria-describedby") || "").split(/\s+/),
          index = $.inArray(id, describedby); - 1 !== index && describedby.splice(index, 1), elem.removeData("ui-tooltip-id"), describedby = $.trim(describedby.join(" ")), describedby ? elem.attr("aria-describedby", describedby) : elem.removeAttr("aria-describedby")
      },
      _create: function() {
        this._on({
          mouseover: "open",
          focusin: "open"
        }), this.tooltips = {}, this.parents = {}, this.options.disabled && this._disable(), this.liveRegion = $("<div>").attr({
          role: "log",
          "aria-live": "assertive",
          "aria-relevant": "additions"
        }).addClass("ui-helper-hidden-accessible").appendTo(this.document[0].body)
      },
      _setOption: function(key, value) {
        var that = this;
        return "disabled" === key ? (this[value ? "_disable" : "_enable"](), void(this.options[key] = value)) : (this._super(key, value), void("content" === key && $.each(this.tooltips, function(id, element) {
          that._updateContent(element)
        })))
      },
      _disable: function() {
        var that = this;
        $.each(this.tooltips, function(id, element) {
          var event = $.Event("blur");
          event.target = event.currentTarget = element[0], that.close(event, !0)
        }), this.element.find(this.options.items).addBack().each(function() {
          var element = $(this);
          element.is("[title]") && element.data("ui-tooltip-title", element.attr("title")).removeAttr("title")
        })
      },
      _enable: function() {
        this.element.find(this.options.items).addBack().each(function() {
          var element = $(this);
          element.data("ui-tooltip-title") && element.attr("title", element.data("ui-tooltip-title"))
        })
      },
      open: function(event) {
        var that = this,
          target = $(event ? event.target : this.element).closest(this.options.items);
        target.length && !target.data("ui-tooltip-id") && (target.attr("title") && target.data("ui-tooltip-title", target.attr("title")), target.data("ui-tooltip-open", !0), event && "mouseover" === event.type && target.parents().each(function() {
          var blurEvent, parent = $(this);
          parent.data("ui-tooltip-open") && (blurEvent = $.Event("blur"), blurEvent.target = blurEvent.currentTarget = this, that.close(blurEvent, !0)), parent.attr("title") && (parent.uniqueId(), that.parents[this.id] = {
            element: this,
            title: parent.attr("title")
          }, parent.attr("title", ""))
        }), this._updateContent(target, event))
      },
      _updateContent: function(target, event) {
        var content, contentOption = this.options.content,
          that = this,
          eventType = event ? event.type : null;
        return "string" == typeof contentOption ? this._open(event, target, contentOption) : (content = contentOption.call(target[0], function(response) {
          target.data("ui-tooltip-open") && that._delay(function() {
            event && (event.type = eventType), this._open(event, target, response)
          })
        }), void(content && this._open(event, target, content)))
      },
      _open: function(event, target, content) {
        function position(event) {
          positionOption.of = event, tooltip.is(":hidden") || tooltip.position(positionOption)
        }
        var tooltip, events, delayedShow, a11yContent, positionOption = $.extend({}, this.options.position);
        if (content) {
          if (tooltip = this._find(target), tooltip.length) return void tooltip.find(".ui-tooltip-content").html(content);
          target.is("[title]") && (event && "mouseover" === event.type ? target.attr("title", "") : target.removeAttr("title")), tooltip = this._tooltip(target), this._addDescribedBy(target, tooltip.attr("id")), tooltip.find(".ui-tooltip-content").html(content), this.liveRegion.children().hide(), content.clone ? (a11yContent = content.clone(), a11yContent.removeAttr("id").find("[id]").removeAttr("id")) : a11yContent = content, $("<div>").html(a11yContent).appendTo(this.liveRegion), this.options.track && event && /^mouse/.test(event.type) ? (this._on(this.document, {
            mousemove: position
          }), position(event)) : tooltip.position($.extend({
            of: target
          }, this.options.position)), tooltip.hide(), this._show(tooltip, this.options.show), this.options.show && this.options.show.delay && (delayedShow = this.delayedShow = setInterval(function() {
            tooltip.is(":visible") && (position(positionOption.of), clearInterval(delayedShow))
          }, $.fx.interval)), this._trigger("open", event, {
            tooltip: tooltip
          }), events = {
            keyup: function(event) {
              if (event.keyCode === $.ui.keyCode.ESCAPE) {
                var fakeEvent = $.Event(event);
                fakeEvent.currentTarget = target[0], this.close(fakeEvent, !0)
              }
            }
          }, target[0] !== this.element[0] && (events.remove = function() {
            this._removeTooltip(tooltip)
          }), event && "mouseover" !== event.type || (events.mouseleave = "close"), event && "focusin" !== event.type || (events.focusout = "close"), this._on(!0, target, events)
        }
      },
      close: function(event) {
        var that = this,
          target = $(event ? event.currentTarget : this.element),
          tooltip = this._find(target);
        this.closing || (clearInterval(this.delayedShow), target.data("ui-tooltip-title") && !target.attr("title") && target.attr("title", target.data("ui-tooltip-title")), this._removeDescribedBy(target), tooltip.stop(!0), this._hide(tooltip, this.options.hide, function() {
          that._removeTooltip($(this))
        }), target.removeData("ui-tooltip-open"), this._off(target, "mouseleave focusout keyup"), target[0] !== this.element[0] && this._off(target, "remove"), this._off(this.document, "mousemove"), event && "mouseleave" === event.type && $.each(this.parents, function(id, parent) {
          $(parent.element).attr("title", parent.title), delete that.parents[id]
        }), this.closing = !0, this._trigger("close", event, {
          tooltip: tooltip
        }), this.closing = !1)
      },
      _tooltip: function(element) {
        var tooltip = $("<div>").attr("role", "tooltip").addClass("ui-tooltip ui-widget ui-corner-all ui-widget-content " + (this.options.tooltipClass || "")),
          id = tooltip.uniqueId().attr("id");
        return $("<div>").addClass("ui-tooltip-content").appendTo(tooltip), tooltip.appendTo(this.document[0].body), this.tooltips[id] = element, tooltip
      },
      _find: function(target) {
        var id = target.data("ui-tooltip-id");
        return id ? $("#" + id) : $()
      },
      _removeTooltip: function(tooltip) {
        tooltip.remove(), delete this.tooltips[tooltip.attr("id")]
      },
      _destroy: function() {
        var that = this;
        $.each(this.tooltips, function(id, element) {
          var event = $.Event("blur");
          event.target = event.currentTarget = element[0], that.close(event, !0), $("#" + id).remove(), element.data("ui-tooltip-title") && (element.attr("title") || element.attr("title", element.data("ui-tooltip-title")), element.removeData("ui-tooltip-title"))
        }), this.liveRegion.remove()
      }
    }), "ui-effects-");
    $.effects = {
        effect: {}
      },
      function(jQuery, undefined) {
        function clamp(value, prop, allowEmpty) {
          var type = propTypes[prop.type] || {};
          return null == value ? allowEmpty || !prop.def ? null : prop.def : (value = type.floor ? ~~value : parseFloat(value), isNaN(value) ? prop.def : type.mod ? (value + type.mod) % type.mod : 0 > value ? 0 : type.max < value ? type.max : value)
        }

        function stringParse(string) {
          var inst = color(),
            rgba = inst._rgba = [];
          return string = string.toLowerCase(), each(stringParsers, function(i, parser) {
            var parsed, match = parser.re.exec(string),
              values = match && parser.parse(match),
              spaceName = parser.space || "rgba";
            return values ? (parsed = inst[spaceName](values), inst[spaces[spaceName].cache] = parsed[spaces[spaceName].cache], rgba = inst._rgba = parsed._rgba, !1) : void 0
          }), rgba.length ? ("0,0,0,0" === rgba.join() && jQuery.extend(rgba, colors.transparent), inst) : colors[string]
        }

        function hue2rgb(p, q, h) {
          return h = (h + 1) % 1, 1 > 6 * h ? p + (q - p) * h * 6 : 1 > 2 * h ? q : 2 > 3 * h ? p + (q - p) * (2 / 3 - h) * 6 : p
        }
        var colors, stepHooks = "backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor",
          rplusequals = /^([\-+])=\s*(\d+\.?\d*)/,
          stringParsers = [{
            re: /rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
            parse: function(execResult) {
              return [execResult[1], execResult[2], execResult[3], execResult[4]]
            }
          }, {
            re: /rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
            parse: function(execResult) {
              return [2.55 * execResult[1], 2.55 * execResult[2], 2.55 * execResult[3], execResult[4]]
            }
          }, {
            re: /#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,
            parse: function(execResult) {
              return [parseInt(execResult[1], 16), parseInt(execResult[2], 16), parseInt(execResult[3], 16)]
            }
          }, {
            re: /#([a-f0-9])([a-f0-9])([a-f0-9])/,
            parse: function(execResult) {
              return [parseInt(execResult[1] + execResult[1], 16), parseInt(execResult[2] + execResult[2], 16), parseInt(execResult[3] + execResult[3], 16)]
            }
          }, {
            re: /hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
            space: "hsla",
            parse: function(execResult) {
              return [execResult[1], execResult[2] / 100, execResult[3] / 100, execResult[4]]
            }
          }],
          color = jQuery.Color = function(color, green, blue, alpha) {
            return new jQuery.Color.fn.parse(color, green, blue, alpha)
          },
          spaces = {
            rgba: {
              props: {
                red: {
                  idx: 0,
                  type: "byte"
                },
                green: {
                  idx: 1,
                  type: "byte"
                },
                blue: {
                  idx: 2,
                  type: "byte"
                }
              }
            },
            hsla: {
              props: {
                hue: {
                  idx: 0,
                  type: "degrees"
                },
                saturation: {
                  idx: 1,
                  type: "percent"
                },
                lightness: {
                  idx: 2,
                  type: "percent"
                }
              }
            }
          },
          propTypes = {
            "byte": {
              floor: !0,
              max: 255
            },
            percent: {
              max: 1
            },
            degrees: {
              mod: 360,
              floor: !0
            }
          },
          support = color.support = {},
          supportElem = jQuery("<p>")[0],
          each = jQuery.each;
        supportElem.style.cssText = "background-color:rgba(1,1,1,.5)", support.rgba = supportElem.style.backgroundColor.indexOf("rgba") > -1, each(spaces, function(spaceName, space) {
          space.cache = "_" + spaceName, space.props.alpha = {
            idx: 3,
            type: "percent",
            def: 1
          }
        }), color.fn = jQuery.extend(color.prototype, {
          parse: function(red, green, blue, alpha) {
            if (red === undefined) return this._rgba = [null, null, null, null], this;
            (red.jquery || red.nodeType) && (red = jQuery(red).css(green), green = undefined);
            var inst = this,
              type = jQuery.type(red),
              rgba = this._rgba = [];
            return green !== undefined && (red = [red, green, blue, alpha], type = "array"), "string" === type ? this.parse(stringParse(red) || colors._default) : "array" === type ? (each(spaces.rgba.props, function(key, prop) {
              rgba[prop.idx] = clamp(red[prop.idx], prop)
            }), this) : "object" === type ? (red instanceof color ? each(spaces, function(spaceName, space) {
              red[space.cache] && (inst[space.cache] = red[space.cache].slice())
            }) : each(spaces, function(spaceName, space) {
              var cache = space.cache;
              each(space.props, function(key, prop) {
                if (!inst[cache] && space.to) {
                  if ("alpha" === key || null == red[key]) return;
                  inst[cache] = space.to(inst._rgba)
                }
                inst[cache][prop.idx] = clamp(red[key], prop, !0)
              }), inst[cache] && jQuery.inArray(null, inst[cache].slice(0, 3)) < 0 && (inst[cache][3] = 1, space.from && (inst._rgba = space.from(inst[cache])))
            }), this) : void 0
          },
          is: function(compare) {
            var is = color(compare),
              same = !0,
              inst = this;
            return each(spaces, function(_, space) {
              var localCache, isCache = is[space.cache];
              return isCache && (localCache = inst[space.cache] || space.to && space.to(inst._rgba) || [], each(space.props, function(_, prop) {
                return null != isCache[prop.idx] ? same = isCache[prop.idx] === localCache[prop.idx] : void 0
              })), same
            }), same
          },
          _space: function() {
            var used = [],
              inst = this;
            return each(spaces, function(spaceName, space) {
              inst[space.cache] && used.push(spaceName)
            }), used.pop()
          },
          transition: function(other, distance) {
            var end = color(other),
              spaceName = end._space(),
              space = spaces[spaceName],
              startColor = 0 === this.alpha() ? color("transparent") : this,
              start = startColor[space.cache] || space.to(startColor._rgba),
              result = start.slice();
            return end = end[space.cache], each(space.props, function(key, prop) {
              var index = prop.idx,
                startValue = start[index],
                endValue = end[index],
                type = propTypes[prop.type] || {};
              null !== endValue && (null === startValue ? result[index] = endValue : (type.mod && (endValue - startValue > type.mod / 2 ? startValue += type.mod : startValue - endValue > type.mod / 2 && (startValue -= type.mod)), result[index] = clamp((endValue - startValue) * distance + startValue, prop)))
            }), this[spaceName](result)
          },
          blend: function(opaque) {
            if (1 === this._rgba[3]) return this;
            var rgb = this._rgba.slice(),
              a = rgb.pop(),
              blend = color(opaque)._rgba;
            return color(jQuery.map(rgb, function(v, i) {
              return (1 - a) * blend[i] + a * v
            }))
          },
          toRgbaString: function() {
            var prefix = "rgba(",
              rgba = jQuery.map(this._rgba, function(v, i) {
                return null == v ? i > 2 ? 1 : 0 : v
              });
            return 1 === rgba[3] && (rgba.pop(), prefix = "rgb("), prefix + rgba.join() + ")"
          },
          toHslaString: function() {
            var prefix = "hsla(",
              hsla = jQuery.map(this.hsla(), function(v, i) {
                return null == v && (v = i > 2 ? 1 : 0), i && 3 > i && (v = Math.round(100 * v) + "%"), v
              });
            return 1 === hsla[3] && (hsla.pop(), prefix = "hsl("), prefix + hsla.join() + ")"
          },
          toHexString: function(includeAlpha) {
            var rgba = this._rgba.slice(),
              alpha = rgba.pop();
            return includeAlpha && rgba.push(~~(255 * alpha)), "#" + jQuery.map(rgba, function(v) {
              return v = (v || 0).toString(16), 1 === v.length ? "0" + v : v
            }).join("")
          },
          toString: function() {
            return 0 === this._rgba[3] ? "transparent" : this.toRgbaString()
          }
        }), color.fn.parse.prototype = color.fn, spaces.hsla.to = function(rgba) {
          if (null == rgba[0] || null == rgba[1] || null == rgba[2]) return [null, null, null, rgba[3]];
          var h, s, r = rgba[0] / 255,
            g = rgba[1] / 255,
            b = rgba[2] / 255,
            a = rgba[3],
            max = Math.max(r, g, b),
            min = Math.min(r, g, b),
            diff = max - min,
            add = max + min,
            l = .5 * add;
          return h = min === max ? 0 : r === max ? 60 * (g - b) / diff + 360 : g === max ? 60 * (b - r) / diff + 120 : 60 * (r - g) / diff + 240, s = 0 === diff ? 0 : .5 >= l ? diff / add : diff / (2 - add), [Math.round(h) % 360, s, l, null == a ? 1 : a]
        }, spaces.hsla.from = function(hsla) {
          if (null == hsla[0] || null == hsla[1] || null == hsla[2]) return [null, null, null, hsla[3]];
          var h = hsla[0] / 360,
            s = hsla[1],
            l = hsla[2],
            a = hsla[3],
            q = .5 >= l ? l * (1 + s) : l + s - l * s,
            p = 2 * l - q;
          return [Math.round(255 * hue2rgb(p, q, h + 1 / 3)), Math.round(255 * hue2rgb(p, q, h)), Math.round(255 * hue2rgb(p, q, h - 1 / 3)), a]
        }, each(spaces, function(spaceName, space) {
          var props = space.props,
            cache = space.cache,
            to = space.to,
            from = space.from;
          color.fn[spaceName] = function(value) {
            if (to && !this[cache] && (this[cache] = to(this._rgba)), value === undefined) return this[cache].slice();
            var ret, type = jQuery.type(value),
              arr = "array" === type || "object" === type ? value : arguments,
              local = this[cache].slice();
            return each(props, function(key, prop) {
              var val = arr["object" === type ? key : prop.idx];
              null == val && (val = local[prop.idx]), local[prop.idx] = clamp(val, prop)
            }), from ? (ret = color(from(local)), ret[cache] = local, ret) : color(local)
          }, each(props, function(key, prop) {
            color.fn[key] || (color.fn[key] = function(value) {
              var match, vtype = jQuery.type(value),
                fn = "alpha" === key ? this._hsla ? "hsla" : "rgba" : spaceName,
                local = this[fn](),
                cur = local[prop.idx];
              return "undefined" === vtype ? cur : ("function" === vtype && (value = value.call(this, cur), vtype = jQuery.type(value)), null == value && prop.empty ? this : ("string" === vtype && (match = rplusequals.exec(value), match && (value = cur + parseFloat(match[2]) * ("+" === match[1] ? 1 : -1))), local[prop.idx] = value, this[fn](local)))
            })
          })
        }), color.hook = function(hook) {
          var hooks = hook.split(" ");
          each(hooks, function(i, hook) {
            jQuery.cssHooks[hook] = {
              set: function(elem, value) {
                var parsed, curElem, backgroundColor = "";
                if ("transparent" !== value && ("string" !== jQuery.type(value) || (parsed = stringParse(value)))) {
                  if (value = color(parsed || value), !support.rgba && 1 !== value._rgba[3]) {
                    for (curElem = "backgroundColor" === hook ? elem.parentNode : elem;
                      ("" === backgroundColor || "transparent" === backgroundColor) && curElem && curElem.style;) try {
                      backgroundColor = jQuery.css(curElem, "backgroundColor"), curElem = curElem.parentNode
                    } catch (e) {}
                    value = value.blend(backgroundColor && "transparent" !== backgroundColor ? backgroundColor : "_default")
                  }
                  value = value.toRgbaString()
                }
                try {
                  elem.style[hook] = value
                } catch (e) {}
              }
            }, jQuery.fx.step[hook] = function(fx) {
              fx.colorInit || (fx.start = color(fx.elem, hook), fx.end = color(fx.end), fx.colorInit = !0), jQuery.cssHooks[hook].set(fx.elem, fx.start.transition(fx.end, fx.pos))
            }
          })
        }, color.hook(stepHooks), jQuery.cssHooks.borderColor = {
          expand: function(value) {
            var expanded = {};
            return each(["Top", "Right", "Bottom", "Left"], function(i, part) {
              expanded["border" + part + "Color"] = value
            }), expanded
          }
        }, colors = jQuery.Color.names = {
          aqua: "#00ffff",
          black: "#000000",
          blue: "#0000ff",
          fuchsia: "#ff00ff",
          gray: "#808080",
          green: "#008000",
          lime: "#00ff00",
          maroon: "#800000",
          navy: "#000080",
          olive: "#808000",
          purple: "#800080",
          red: "#ff0000",
          silver: "#c0c0c0",
          teal: "#008080",
          white: "#ffffff",
          yellow: "#ffff00",
          transparent: [null, null, null, 0],
          _default: "#ffffff"
        }
      }(jQuery),
      function() {
        function getElementStyles(elem) {
          var key, len, style = elem.ownerDocument.defaultView ? elem.ownerDocument.defaultView.getComputedStyle(elem, null) : elem.currentStyle,
            styles = {};
          if (style && style.length && style[0] && style[style[0]])
            for (len = style.length; len--;) key = style[len], "string" == typeof style[key] && (styles[$.camelCase(key)] = style[key]);
          else
            for (key in style) "string" == typeof style[key] && (styles[key] = style[key]);
          return styles
        }

        function styleDifference(oldStyle, newStyle) {
          var name, value, diff = {};
          for (name in newStyle) value = newStyle[name], oldStyle[name] !== value && (shorthandStyles[name] || ($.fx.step[name] || !isNaN(parseFloat(value))) && (diff[name] = value));
          return diff
        }
        var classAnimationActions = ["add", "remove", "toggle"],
          shorthandStyles = {
            border: 1,
            borderBottom: 1,
            borderColor: 1,
            borderLeft: 1,
            borderRight: 1,
            borderTop: 1,
            borderWidth: 1,
            margin: 1,
            padding: 1
          };
        $.each(["borderLeftStyle", "borderRightStyle", "borderBottomStyle", "borderTopStyle"], function(_, prop) {
          $.fx.step[prop] = function(fx) {
            ("none" !== fx.end && !fx.setAttr || 1 === fx.pos && !fx.setAttr) && (jQuery.style(fx.elem, prop, fx.end), fx.setAttr = !0)
          }
        }), $.fn.addBack || ($.fn.addBack = function(selector) {
          return this.add(null == selector ? this.prevObject : this.prevObject.filter(selector))
        }), $.effects.animateClass = function(value, duration, easing, callback) {
          var o = $.speed(duration, easing, callback);
          return this.queue(function() {
            var applyClassChange, animated = $(this),
              baseClass = animated.attr("class") || "",
              allAnimations = o.children ? animated.find("*").addBack() : animated;
            allAnimations = allAnimations.map(function() {
              var el = $(this);
              return {
                el: el,
                start: getElementStyles(this)
              }
            }), applyClassChange = function() {
              $.each(classAnimationActions, function(i, action) {
                value[action] && animated[action + "Class"](value[action])
              })
            }, applyClassChange(), allAnimations = allAnimations.map(function() {
              return this.end = getElementStyles(this.el[0]), this.diff = styleDifference(this.start, this.end), this
            }), animated.attr("class", baseClass), allAnimations = allAnimations.map(function() {
              var styleInfo = this,
                dfd = $.Deferred(),
                opts = $.extend({}, o, {
                  queue: !1,
                  complete: function() {
                    dfd.resolve(styleInfo)
                  }
                });
              return this.el.animate(this.diff, opts), dfd.promise()
            }), $.when.apply($, allAnimations.get()).done(function() {
              applyClassChange(), $.each(arguments, function() {
                var el = this.el;
                $.each(this.diff, function(key) {
                  el.css(key, "")
                })
              }), o.complete.call(animated[0])
            })
          })
        }, $.fn.extend({
          addClass: function(orig) {
            return function(classNames, speed, easing, callback) {
              return speed ? $.effects.animateClass.call(this, {
                add: classNames
              }, speed, easing, callback) : orig.apply(this, arguments)
            }
          }($.fn.addClass),
          removeClass: function(orig) {
            return function(classNames, speed, easing, callback) {
              return arguments.length > 1 ? $.effects.animateClass.call(this, {
                remove: classNames
              }, speed, easing, callback) : orig.apply(this, arguments)
            }
          }($.fn.removeClass),
          toggleClass: function(orig) {
            return function(classNames, force, speed, easing, callback) {
              return "boolean" == typeof force || void 0 === force ? speed ? $.effects.animateClass.call(this, force ? {
                add: classNames
              } : {
                remove: classNames
              }, speed, easing, callback) : orig.apply(this, arguments) : $.effects.animateClass.call(this, {
                toggle: classNames
              }, force, speed, easing)
            }
          }($.fn.toggleClass),
          switchClass: function(remove, add, speed, easing, callback) {
            return $.effects.animateClass.call(this, {
              add: add,
              remove: remove
            }, speed, easing, callback)
          }
        })
      }(),
      function() {
        function _normalizeArguments(effect, options, speed, callback) {
          return $.isPlainObject(effect) && (options = effect, effect = effect.effect), effect = {
            effect: effect
          }, null == options && (options = {}), $.isFunction(options) && (callback = options,
            speed = null, options = {}), ("number" == typeof options || $.fx.speeds[options]) && (callback = speed, speed = options, options = {}), $.isFunction(speed) && (callback = speed, speed = null), options && $.extend(effect, options), speed = speed || options.duration, effect.duration = $.fx.off ? 0 : "number" == typeof speed ? speed : speed in $.fx.speeds ? $.fx.speeds[speed] : $.fx.speeds._default, effect.complete = callback || options.complete, effect
        }

        function standardAnimationOption(option) {
          return !option || "number" == typeof option || $.fx.speeds[option] ? !0 : "string" != typeof option || $.effects.effect[option] ? $.isFunction(option) ? !0 : "object" != typeof option || option.effect ? !1 : !0 : !0
        }
        $.extend($.effects, {
          version: "1.11.0",
          save: function(element, set) {
            for (var i = 0; i < set.length; i++) null !== set[i] && element.data(dataSpace + set[i], element[0].style[set[i]])
          },
          restore: function(element, set) {
            var val, i;
            for (i = 0; i < set.length; i++) null !== set[i] && (val = element.data(dataSpace + set[i]), void 0 === val && (val = ""), element.css(set[i], val))
          },
          setMode: function(el, mode) {
            return "toggle" === mode && (mode = el.is(":hidden") ? "show" : "hide"), mode
          },
          getBaseline: function(origin, original) {
            var y, x;
            switch (origin[0]) {
              case "top":
                y = 0;
                break;
              case "middle":
                y = .5;
                break;
              case "bottom":
                y = 1;
                break;
              default:
                y = origin[0] / original.height
            }
            switch (origin[1]) {
              case "left":
                x = 0;
                break;
              case "center":
                x = .5;
                break;
              case "right":
                x = 1;
                break;
              default:
                x = origin[1] / original.width
            }
            return {
              x: x,
              y: y
            }
          },
          createWrapper: function(element) {
            if (element.parent().is(".ui-effects-wrapper")) return element.parent();
            var props = {
                width: element.outerWidth(!0),
                height: element.outerHeight(!0),
                "float": element.css("float")
              },
              wrapper = $("<div></div>").addClass("ui-effects-wrapper").css({
                fontSize: "100%",
                background: "transparent",
                border: "none",
                margin: 0,
                padding: 0
              }),
              size = {
                width: element.width(),
                height: element.height()
              },
              active = document.activeElement;
            try {
              active.id
            } catch (e) {
              active = document.body
            }
            return element.wrap(wrapper), (element[0] === active || $.contains(element[0], active)) && $(active).focus(), wrapper = element.parent(), "static" === element.css("position") ? (wrapper.css({
              position: "relative"
            }), element.css({
              position: "relative"
            })) : ($.extend(props, {
              position: element.css("position"),
              zIndex: element.css("z-index")
            }), $.each(["top", "left", "bottom", "right"], function(i, pos) {
              props[pos] = element.css(pos), isNaN(parseInt(props[pos], 10)) && (props[pos] = "auto")
            }), element.css({
              position: "relative",
              top: 0,
              left: 0,
              right: "auto",
              bottom: "auto"
            })), element.css(size), wrapper.css(props).show()
          },
          removeWrapper: function(element) {
            var active = document.activeElement;
            return element.parent().is(".ui-effects-wrapper") && (element.parent().replaceWith(element), (element[0] === active || $.contains(element[0], active)) && $(active).focus()), element
          },
          setTransition: function(element, list, factor, value) {
            return value = value || {}, $.each(list, function(i, x) {
              var unit = element.cssUnit(x);
              unit[0] > 0 && (value[x] = unit[0] * factor + unit[1])
            }), value
          }
        }), $.fn.extend({
          effect: function() {
            function run(next) {
              function done() {
                $.isFunction(complete) && complete.call(elem[0]), $.isFunction(next) && next()
              }
              var elem = $(this),
                complete = args.complete,
                mode = args.mode;
              (elem.is(":hidden") ? "hide" === mode : "show" === mode) ? (elem[mode](), done()) : effectMethod.call(elem[0], args, done)
            }
            var args = _normalizeArguments.apply(this, arguments),
              mode = args.mode,
              queue = args.queue,
              effectMethod = $.effects.effect[args.effect];
            return $.fx.off || !effectMethod ? mode ? this[mode](args.duration, args.complete) : this.each(function() {
              args.complete && args.complete.call(this)
            }) : queue === !1 ? this.each(run) : this.queue(queue || "fx", run)
          },
          show: function(orig) {
            return function(option) {
              if (standardAnimationOption(option)) return orig.apply(this, arguments);
              var args = _normalizeArguments.apply(this, arguments);
              return args.mode = "show", this.effect.call(this, args)
            }
          }($.fn.show),
          hide: function(orig) {
            return function(option) {
              if (standardAnimationOption(option)) return orig.apply(this, arguments);
              var args = _normalizeArguments.apply(this, arguments);
              return args.mode = "hide", this.effect.call(this, args)
            }
          }($.fn.hide),
          toggle: function(orig) {
            return function(option) {
              if (standardAnimationOption(option) || "boolean" == typeof option) return orig.apply(this, arguments);
              var args = _normalizeArguments.apply(this, arguments);
              return args.mode = "toggle", this.effect.call(this, args)
            }
          }($.fn.toggle),
          cssUnit: function(key) {
            var style = this.css(key),
              val = [];
            return $.each(["em", "px", "%", "pt"], function(i, unit) {
              style.indexOf(unit) > 0 && (val = [parseFloat(style), unit])
            }), val
          }
        })
      }(),
      function() {
        var baseEasings = {};
        $.each(["Quad", "Cubic", "Quart", "Quint", "Expo"], function(i, name) {
          baseEasings[name] = function(p) {
            return Math.pow(p, i + 2)
          }
        }), $.extend(baseEasings, {
          Sine: function(p) {
            return 1 - Math.cos(p * Math.PI / 2)
          },
          Circ: function(p) {
            return 1 - Math.sqrt(1 - p * p)
          },
          Elastic: function(p) {
            return 0 === p || 1 === p ? p : -Math.pow(2, 8 * (p - 1)) * Math.sin((80 * (p - 1) - 7.5) * Math.PI / 15)
          },
          Back: function(p) {
            return p * p * (3 * p - 2)
          },
          Bounce: function(p) {
            for (var pow2, bounce = 4; p < ((pow2 = Math.pow(2, --bounce)) - 1) / 11;);
            return 1 / Math.pow(4, 3 - bounce) - 7.5625 * Math.pow((3 * pow2 - 2) / 22 - p, 2)
          }
        }), $.each(baseEasings, function(name, easeIn) {
          $.easing["easeIn" + name] = easeIn, $.easing["easeOut" + name] = function(p) {
            return 1 - easeIn(1 - p)
          }, $.easing["easeInOut" + name] = function(p) {
            return .5 > p ? easeIn(2 * p) / 2 : 1 - easeIn(-2 * p + 2) / 2
          }
        })
      }();
    $.effects, $.effects.effect.blind = function(o, done) {
      var wrapper, distance, margin, el = $(this),
        rvertical = /up|down|vertical/,
        rpositivemotion = /up|left|vertical|horizontal/,
        props = ["position", "top", "bottom", "left", "right", "height", "width"],
        mode = $.effects.setMode(el, o.mode || "hide"),
        direction = o.direction || "up",
        vertical = rvertical.test(direction),
        ref = vertical ? "height" : "width",
        ref2 = vertical ? "top" : "left",
        motion = rpositivemotion.test(direction),
        animation = {},
        show = "show" === mode;
      el.parent().is(".ui-effects-wrapper") ? $.effects.save(el.parent(), props) : $.effects.save(el, props), el.show(), wrapper = $.effects.createWrapper(el).css({
        overflow: "hidden"
      }), distance = wrapper[ref](), margin = parseFloat(wrapper.css(ref2)) || 0, animation[ref] = show ? distance : 0, motion || (el.css(vertical ? "bottom" : "right", 0).css(vertical ? "top" : "left", "auto").css({
        position: "absolute"
      }), animation[ref2] = show ? margin : distance + margin), show && (wrapper.css(ref, 0), motion || wrapper.css(ref2, margin + distance)), wrapper.animate(animation, {
        duration: o.duration,
        easing: o.easing,
        queue: !1,
        complete: function() {
          "hide" === mode && el.hide(), $.effects.restore(el, props), $.effects.removeWrapper(el), done()
        }
      })
    }, $.effects.effect.bounce = function(o, done) {
      var i, upAnim, downAnim, el = $(this),
        props = ["position", "top", "bottom", "left", "right", "height", "width"],
        mode = $.effects.setMode(el, o.mode || "effect"),
        hide = "hide" === mode,
        show = "show" === mode,
        direction = o.direction || "up",
        distance = o.distance,
        times = o.times || 5,
        anims = 2 * times + (show || hide ? 1 : 0),
        speed = o.duration / anims,
        easing = o.easing,
        ref = "up" === direction || "down" === direction ? "top" : "left",
        motion = "up" === direction || "left" === direction,
        queue = el.queue(),
        queuelen = queue.length;
      for ((show || hide) && props.push("opacity"), $.effects.save(el, props), el.show(), $.effects.createWrapper(el), distance || (distance = el["top" === ref ? "outerHeight" : "outerWidth"]() / 3), show && (downAnim = {
          opacity: 1
        }, downAnim[ref] = 0, el.css("opacity", 0).css(ref, motion ? 2 * -distance : 2 * distance).animate(downAnim, speed, easing)), hide && (distance /= Math.pow(2, times - 1)), downAnim = {}, downAnim[ref] = 0, i = 0; times > i; i++) upAnim = {}, upAnim[ref] = (motion ? "-=" : "+=") + distance, el.animate(upAnim, speed, easing).animate(downAnim, speed, easing), distance = hide ? 2 * distance : distance / 2;
      hide && (upAnim = {
        opacity: 0
      }, upAnim[ref] = (motion ? "-=" : "+=") + distance, el.animate(upAnim, speed, easing)), el.queue(function() {
        hide && el.hide(), $.effects.restore(el, props), $.effects.removeWrapper(el), done()
      }), queuelen > 1 && queue.splice.apply(queue, [1, 0].concat(queue.splice(queuelen, anims + 1))), el.dequeue()
    }, $.effects.effect.clip = function(o, done) {
      var wrapper, animate, distance, el = $(this),
        props = ["position", "top", "bottom", "left", "right", "height", "width"],
        mode = $.effects.setMode(el, o.mode || "hide"),
        show = "show" === mode,
        direction = o.direction || "vertical",
        vert = "vertical" === direction,
        size = vert ? "height" : "width",
        position = vert ? "top" : "left",
        animation = {};
      $.effects.save(el, props), el.show(), wrapper = $.effects.createWrapper(el).css({
        overflow: "hidden"
      }), animate = "IMG" === el[0].tagName ? wrapper : el, distance = animate[size](), show && (animate.css(size, 0), animate.css(position, distance / 2)), animation[size] = show ? distance : 0, animation[position] = show ? 0 : distance / 2, animate.animate(animation, {
        queue: !1,
        duration: o.duration,
        easing: o.easing,
        complete: function() {
          show || el.hide(), $.effects.restore(el, props), $.effects.removeWrapper(el), done()
        }
      })
    }, $.effects.effect.drop = function(o, done) {
      var distance, el = $(this),
        props = ["position", "top", "bottom", "left", "right", "opacity", "height", "width"],
        mode = $.effects.setMode(el, o.mode || "hide"),
        show = "show" === mode,
        direction = o.direction || "left",
        ref = "up" === direction || "down" === direction ? "top" : "left",
        motion = "up" === direction || "left" === direction ? "pos" : "neg",
        animation = {
          opacity: show ? 1 : 0
        };
      $.effects.save(el, props), el.show(), $.effects.createWrapper(el), distance = o.distance || el["top" === ref ? "outerHeight" : "outerWidth"](!0) / 2, show && el.css("opacity", 0).css(ref, "pos" === motion ? -distance : distance), animation[ref] = (show ? "pos" === motion ? "+=" : "-=" : "pos" === motion ? "-=" : "+=") + distance, el.animate(animation, {
        queue: !1,
        duration: o.duration,
        easing: o.easing,
        complete: function() {
          "hide" === mode && el.hide(), $.effects.restore(el, props), $.effects.removeWrapper(el), done()
        }
      })
    }, $.effects.effect.explode = function(o, done) {
      function childComplete() {
        pieces.push(this), pieces.length === rows * cells && animComplete()
      }

      function animComplete() {
        el.css({
          visibility: "visible"
        }), $(pieces).remove(), show || el.hide(), done()
      }
      var i, j, left, top, mx, my, rows = o.pieces ? Math.round(Math.sqrt(o.pieces)) : 3,
        cells = rows,
        el = $(this),
        mode = $.effects.setMode(el, o.mode || "hide"),
        show = "show" === mode,
        offset = el.show().css("visibility", "hidden").offset(),
        width = Math.ceil(el.outerWidth() / cells),
        height = Math.ceil(el.outerHeight() / rows),
        pieces = [];
      for (i = 0; rows > i; i++)
        for (top = offset.top + i * height, my = i - (rows - 1) / 2, j = 0; cells > j; j++) left = offset.left + j * width, mx = j - (cells - 1) / 2, el.clone().appendTo("body").wrap("<div></div>").css({
          position: "absolute",
          visibility: "visible",
          left: -j * width,
          top: -i * height
        }).parent().addClass("ui-effects-explode").css({
          position: "absolute",
          overflow: "hidden",
          width: width,
          height: height,
          left: left + (show ? mx * width : 0),
          top: top + (show ? my * height : 0),
          opacity: show ? 0 : 1
        }).animate({
          left: left + (show ? 0 : mx * width),
          top: top + (show ? 0 : my * height),
          opacity: show ? 1 : 0
        }, o.duration || 500, o.easing, childComplete)
    }, $.effects.effect.fade = function(o, done) {
      var el = $(this),
        mode = $.effects.setMode(el, o.mode || "toggle");
      el.animate({
        opacity: mode
      }, {
        queue: !1,
        duration: o.duration,
        easing: o.easing,
        complete: done
      })
    }, $.effects.effect.fold = function(o, done) {
      var wrapper, distance, el = $(this),
        props = ["position", "top", "bottom", "left", "right", "height", "width"],
        mode = $.effects.setMode(el, o.mode || "hide"),
        show = "show" === mode,
        hide = "hide" === mode,
        size = o.size || 15,
        percent = /([0-9]+)%/.exec(size),
        horizFirst = !!o.horizFirst,
        widthFirst = show !== horizFirst,
        ref = widthFirst ? ["width", "height"] : ["height", "width"],
        duration = o.duration / 2,
        animation1 = {},
        animation2 = {};
      $.effects.save(el, props), el.show(), wrapper = $.effects.createWrapper(el).css({
        overflow: "hidden"
      }), distance = widthFirst ? [wrapper.width(), wrapper.height()] : [wrapper.height(), wrapper.width()], percent && (size = parseInt(percent[1], 10) / 100 * distance[hide ? 0 : 1]), show && wrapper.css(horizFirst ? {
        height: 0,
        width: size
      } : {
        height: size,
        width: 0
      }), animation1[ref[0]] = show ? distance[0] : size, animation2[ref[1]] = show ? distance[1] : 0, wrapper.animate(animation1, duration, o.easing).animate(animation2, duration, o.easing, function() {
        hide && el.hide(), $.effects.restore(el, props), $.effects.removeWrapper(el), done()
      })
    }, $.effects.effect.highlight = function(o, done) {
      var elem = $(this),
        props = ["backgroundImage", "backgroundColor", "opacity"],
        mode = $.effects.setMode(elem, o.mode || "show"),
        animation = {
          backgroundColor: elem.css("backgroundColor")
        };
      "hide" === mode && (animation.opacity = 0), $.effects.save(elem, props), elem.show().css({
        backgroundImage: "none",
        backgroundColor: o.color || "#ffff99"
      }).animate(animation, {
        queue: !1,
        duration: o.duration,
        easing: o.easing,
        complete: function() {
          "hide" === mode && elem.hide(), $.effects.restore(elem, props), done()
        }
      })
    }, $.effects.effect.size = function(o, done) {
      var original, baseline, factor, el = $(this),
        props0 = ["position", "top", "bottom", "left", "right", "width", "height", "overflow", "opacity"],
        props1 = ["position", "top", "bottom", "left", "right", "overflow", "opacity"],
        props2 = ["width", "height", "overflow"],
        cProps = ["fontSize"],
        vProps = ["borderTopWidth", "borderBottomWidth", "paddingTop", "paddingBottom"],
        hProps = ["borderLeftWidth", "borderRightWidth", "paddingLeft", "paddingRight"],
        mode = $.effects.setMode(el, o.mode || "effect"),
        restore = o.restore || "effect" !== mode,
        scale = o.scale || "both",
        origin = o.origin || ["middle", "center"],
        position = el.css("position"),
        props = restore ? props0 : props1,
        zero = {
          height: 0,
          width: 0,
          outerHeight: 0,
          outerWidth: 0
        };
      "show" === mode && el.show(), original = {
        height: el.height(),
        width: el.width(),
        outerHeight: el.outerHeight(),
        outerWidth: el.outerWidth()
      }, "toggle" === o.mode && "show" === mode ? (el.from = o.to || zero, el.to = o.from || original) : (el.from = o.from || ("show" === mode ? zero : original), el.to = o.to || ("hide" === mode ? zero : original)), factor = {
        from: {
          y: el.from.height / original.height,
          x: el.from.width / original.width
        },
        to: {
          y: el.to.height / original.height,
          x: el.to.width / original.width
        }
      }, ("box" === scale || "both" === scale) && (factor.from.y !== factor.to.y && (props = props.concat(vProps), el.from = $.effects.setTransition(el, vProps, factor.from.y, el.from), el.to = $.effects.setTransition(el, vProps, factor.to.y, el.to)), factor.from.x !== factor.to.x && (props = props.concat(hProps), el.from = $.effects.setTransition(el, hProps, factor.from.x, el.from), el.to = $.effects.setTransition(el, hProps, factor.to.x, el.to))), ("content" === scale || "both" === scale) && factor.from.y !== factor.to.y && (props = props.concat(cProps).concat(props2), el.from = $.effects.setTransition(el, cProps, factor.from.y, el.from), el.to = $.effects.setTransition(el, cProps, factor.to.y, el.to)), $.effects.save(el, props), el.show(), $.effects.createWrapper(el), el.css("overflow", "hidden").css(el.from), origin && (baseline = $.effects.getBaseline(origin, original), el.from.top = (original.outerHeight - el.outerHeight()) * baseline.y, el.from.left = (original.outerWidth - el.outerWidth()) * baseline.x, el.to.top = (original.outerHeight - el.to.outerHeight) * baseline.y, el.to.left = (original.outerWidth - el.to.outerWidth) * baseline.x), el.css(el.from), ("content" === scale || "both" === scale) && (vProps = vProps.concat(["marginTop", "marginBottom"]).concat(cProps), hProps = hProps.concat(["marginLeft", "marginRight"]), props2 = props0.concat(vProps).concat(hProps), el.find("*[width]").each(function() {
        var child = $(this),
          c_original = {
            height: child.height(),
            width: child.width(),
            outerHeight: child.outerHeight(),
            outerWidth: child.outerWidth()
          };
        restore && $.effects.save(child, props2), child.from = {
          height: c_original.height * factor.from.y,
          width: c_original.width * factor.from.x,
          outerHeight: c_original.outerHeight * factor.from.y,
          outerWidth: c_original.outerWidth * factor.from.x
        }, child.to = {
          height: c_original.height * factor.to.y,
          width: c_original.width * factor.to.x,
          outerHeight: c_original.height * factor.to.y,
          outerWidth: c_original.width * factor.to.x
        }, factor.from.y !== factor.to.y && (child.from = $.effects.setTransition(child, vProps, factor.from.y, child.from), child.to = $.effects.setTransition(child, vProps, factor.to.y, child.to)), factor.from.x !== factor.to.x && (child.from = $.effects.setTransition(child, hProps, factor.from.x, child.from), child.to = $.effects.setTransition(child, hProps, factor.to.x, child.to)), child.css(child.from), child.animate(child.to, o.duration, o.easing, function() {
          restore && $.effects.restore(child, props2)
        })
      })), el.animate(el.to, {
        queue: !1,
        duration: o.duration,
        easing: o.easing,
        complete: function() {
          0 === el.to.opacity && el.css("opacity", el.from.opacity), "hide" === mode && el.hide(), $.effects.restore(el, props), restore || ("static" === position ? el.css({
            position: "relative",
            top: el.to.top,
            left: el.to.left
          }) : $.each(["top", "left"], function(idx, pos) {
            el.css(pos, function(_, str) {
              var val = parseInt(str, 10),
                toRef = idx ? el.to.left : el.to.top;
              return "auto" === str ? toRef + "px" : val + toRef + "px"
            })
          })), $.effects.removeWrapper(el), done()
        }
      })
    }, $.effects.effect.scale = function(o, done) {
      var el = $(this),
        options = $.extend(!0, {}, o),
        mode = $.effects.setMode(el, o.mode || "effect"),
        percent = parseInt(o.percent, 10) || (0 === parseInt(o.percent, 10) ? 0 : "hide" === mode ? 0 : 100),
        direction = o.direction || "both",
        origin = o.origin,
        original = {
          height: el.height(),
          width: el.width(),
          outerHeight: el.outerHeight(),
          outerWidth: el.outerWidth()
        },
        factor = {
          y: "horizontal" !== direction ? percent / 100 : 1,
          x: "vertical" !== direction ? percent / 100 : 1
        };
      options.effect = "size", options.queue = !1, options.complete = done, "effect" !== mode && (options.origin = origin || ["middle", "center"], options.restore = !0), options.from = o.from || ("show" === mode ? {
        height: 0,
        width: 0,
        outerHeight: 0,
        outerWidth: 0
      } : original), options.to = {
        height: original.height * factor.y,
        width: original.width * factor.x,
        outerHeight: original.outerHeight * factor.y,
        outerWidth: original.outerWidth * factor.x
      }, options.fade && ("show" === mode && (options.from.opacity = 0, options.to.opacity = 1), "hide" === mode && (options.from.opacity = 1, options.to.opacity = 0)), el.effect(options)
    }, $.effects.effect.puff = function(o, done) {
      var elem = $(this),
        mode = $.effects.setMode(elem, o.mode || "hide"),
        hide = "hide" === mode,
        percent = parseInt(o.percent, 10) || 150,
        factor = percent / 100,
        original = {
          height: elem.height(),
          width: elem.width(),
          outerHeight: elem.outerHeight(),
          outerWidth: elem.outerWidth()
        };
      $.extend(o, {
        effect: "scale",
        queue: !1,
        fade: !0,
        mode: mode,
        complete: done,
        percent: hide ? percent : 100,
        from: hide ? original : {
          height: original.height * factor,
          width: original.width * factor,
          outerHeight: original.outerHeight * factor,
          outerWidth: original.outerWidth * factor
        }
      }), elem.effect(o)
    }, $.effects.effect.pulsate = function(o, done) {
      var i, elem = $(this),
        mode = $.effects.setMode(elem, o.mode || "show"),
        show = "show" === mode,
        hide = "hide" === mode,
        showhide = show || "hide" === mode,
        anims = 2 * (o.times || 5) + (showhide ? 1 : 0),
        duration = o.duration / anims,
        animateTo = 0,
        queue = elem.queue(),
        queuelen = queue.length;
      for ((show || !elem.is(":visible")) && (elem.css("opacity", 0).show(), animateTo = 1), i = 1; anims > i; i++) elem.animate({
        opacity: animateTo
      }, duration, o.easing), animateTo = 1 - animateTo;
      elem.animate({
        opacity: animateTo
      }, duration, o.easing), elem.queue(function() {
        hide && elem.hide(), done()
      }), queuelen > 1 && queue.splice.apply(queue, [1, 0].concat(queue.splice(queuelen, anims + 1))), elem.dequeue()
    }, $.effects.effect.shake = function(o, done) {
      var i, el = $(this),
        props = ["position", "top", "bottom", "left", "right", "height", "width"],
        mode = $.effects.setMode(el, o.mode || "effect"),
        direction = o.direction || "left",
        distance = o.distance || 20,
        times = o.times || 3,
        anims = 2 * times + 1,
        speed = Math.round(o.duration / anims),
        ref = "up" === direction || "down" === direction ? "top" : "left",
        positiveMotion = "up" === direction || "left" === direction,
        animation = {},
        animation1 = {},
        animation2 = {},
        queue = el.queue(),
        queuelen = queue.length;
      for ($.effects.save(el, props), el.show(), $.effects.createWrapper(el), animation[ref] = (positiveMotion ? "-=" : "+=") + distance, animation1[ref] = (positiveMotion ? "+=" : "-=") + 2 * distance, animation2[ref] = (positiveMotion ? "-=" : "+=") + 2 * distance, el.animate(animation, speed, o.easing), i = 1; times > i; i++) el.animate(animation1, speed, o.easing).animate(animation2, speed, o.easing);
      el.animate(animation1, speed, o.easing).animate(animation, speed / 2, o.easing).queue(function() {
        "hide" === mode && el.hide(), $.effects.restore(el, props), $.effects.removeWrapper(el), done()
      }), queuelen > 1 && queue.splice.apply(queue, [1, 0].concat(queue.splice(queuelen, anims + 1))), el.dequeue()
    }, $.effects.effect.slide = function(o, done) {
      var distance, el = $(this),
        props = ["position", "top", "bottom", "left", "right", "width", "height"],
        mode = $.effects.setMode(el, o.mode || "show"),
        show = "show" === mode,
        direction = o.direction || "left",
        ref = "up" === direction || "down" === direction ? "top" : "left",
        positiveMotion = "up" === direction || "left" === direction,
        animation = {};
      $.effects.save(el, props), el.show(), distance = o.distance || el["top" === ref ? "outerHeight" : "outerWidth"](!0), $.effects.createWrapper(el).css({
        overflow: "hidden"
      }), show && el.css(ref, positiveMotion ? isNaN(distance) ? "-" + distance : -distance : distance), animation[ref] = (show ? positiveMotion ? "+=" : "-=" : positiveMotion ? "-=" : "+=") + distance, el.animate(animation, {
        queue: !1,
        duration: o.duration,
        easing: o.easing,
        complete: function() {
          "hide" === mode && el.hide(), $.effects.restore(el, props), $.effects.removeWrapper(el), done()
        }
      })
    }, $.effects.effect.transfer = function(o, done) {
      var elem = $(this),
        target = $(o.to),
        targetFixed = "fixed" === target.css("position"),
        body = $("body"),
        fixTop = targetFixed ? body.scrollTop() : 0,
        fixLeft = targetFixed ? body.scrollLeft() : 0,
        endPosition = target.offset(),
        animation = {
          top: endPosition.top - fixTop,
          left: endPosition.left - fixLeft,
          height: target.innerHeight(),
          width: target.innerWidth()
        },
        startPosition = elem.offset(),
        transfer = $("<div class='ui-effects-transfer'></div>").appendTo(document.body).addClass(o.className).css({
          top: startPosition.top - fixTop,
          left: startPosition.left - fixLeft,
          height: elem.innerHeight(),
          width: elem.innerWidth(),
          position: targetFixed ? "fixed" : "absolute"
        }).animate(animation, o.duration, o.easing, function() {
          transfer.remove(), done()
        })
    }
  }), function(factory) {
    if ("function" == typeof define && define.amd) define(["jquery"], factory);
    else if ("object" == typeof exports) {
      var jQuery = require("jquery");
      module.exports = factory(jQuery)
    } else factory(window.jQuery || window.Zepto || window.$)
  }(function($) {
    "use strict";
    $.fn.serializeJSON = function(options) {
      var f, $form, opts, formAsArray, serializedObject, name, value, _obj, nameWithNoType, type, keys;
      return f = $.serializeJSON, $form = this, opts = f.setupOpts(options), formAsArray = $form.serializeArray(), f.readCheckboxUncheckedValues(formAsArray, opts, $form), serializedObject = {}, $.each(formAsArray, function(i, obj) {
        name = obj.name, value = obj.value, _obj = f.extractTypeAndNameWithNoType(name), nameWithNoType = _obj.nameWithNoType, type = _obj.type, type || (type = f.tryToFindTypeFromDataAttr(name, $form)), f.validateType(name, type, opts), "skip" !== type && (keys = f.splitInputNameIntoKeysArray(nameWithNoType), value = f.parseValue(value, name, type, opts), f.deepSet(serializedObject, keys, value, opts))
      }), serializedObject
    }, $.serializeJSON = {
      defaultOptions: {
        checkboxUncheckedValue: void 0,
        parseNumbers: !1,
        parseBooleans: !1,
        parseNulls: !1,
        parseAll: !1,
        parseWithFunction: null,
        customTypes: {},
        defaultTypes: {
          string: function(str) {
            return String(str)
          },
          number: function(str) {
            return Number(str)
          },
          "boolean": function(str) {
            var falses = ["false", "null", "undefined", "", "0"];
            return -1 === falses.indexOf(str)
          },
          "null": function(str) {
            var falses = ["false", "null", "undefined", "", "0"];
            return -1 === falses.indexOf(str) ? str : null
          },
          array: function(str) {
            return JSON.parse(str)
          },
          object: function(str) {
            return JSON.parse(str)
          },
          auto: function(str) {
            return $.serializeJSON.parseValue(str, null, null, {
              parseNumbers: !0,
              parseBooleans: !0,
              parseNulls: !0
            })
          },
          skip: null
        },
        useIntKeysAsArrayIndex: !1
      },
      setupOpts: function(options) {
        var opt, validOpts, defaultOptions, optWithDefault, parseAll, f;
        f = $.serializeJSON, null == options && (options = {}), defaultOptions = f.defaultOptions || {}, validOpts = ["checkboxUncheckedValue", "parseNumbers", "parseBooleans", "parseNulls", "parseAll", "parseWithFunction", "customTypes", "defaultTypes", "useIntKeysAsArrayIndex"];
        for (opt in options)
          if (-1 === validOpts.indexOf(opt)) throw new Error("serializeJSON ERROR: invalid option '" + opt + "'. Please use one of " + validOpts.join(", "));
        return optWithDefault = function(key) {
          return options[key] !== !1 && "" !== options[key] && (options[key] || defaultOptions[key])
        }, parseAll = optWithDefault("parseAll"), {
          checkboxUncheckedValue: optWithDefault("checkboxUncheckedValue"),
          parseNumbers: parseAll || optWithDefault("parseNumbers"),
          parseBooleans: parseAll || optWithDefault("parseBooleans"),
          parseNulls: parseAll || optWithDefault("parseNulls"),
          parseWithFunction: optWithDefault("parseWithFunction"),
          typeFunctions: $.extend({}, optWithDefault("defaultTypes"), optWithDefault("customTypes")),
          useIntKeysAsArrayIndex: optWithDefault("useIntKeysAsArrayIndex")
        }
      },
      parseValue: function(valStr, inputName, type, opts) {
        var f, parsedVal;
        return f = $.serializeJSON, parsedVal = valStr, opts.typeFunctions && type && opts.typeFunctions[type] ? parsedVal = opts.typeFunctions[type](valStr) : opts.parseNumbers && f.isNumeric(valStr) ? parsedVal = Number(valStr) : !opts.parseBooleans || "true" !== valStr && "false" !== valStr ? opts.parseNulls && "null" == valStr && (parsedVal = null) : parsedVal = "true" === valStr, opts.parseWithFunction && !type && (parsedVal = opts.parseWithFunction(parsedVal, inputName)), parsedVal
      },
      isObject: function(obj) {
        return obj === Object(obj)
      },
      isUndefined: function(obj) {
        return void 0 === obj
      },
      isValidArrayIndex: function(val) {
        return /^[0-9]+$/.test(String(val))
      },
      isNumeric: function(obj) {
        return obj - parseFloat(obj) >= 0
      },
      optionKeys: function(obj) {
        if (Object.keys) return Object.keys(obj);
        var key, keys = [];
        for (key in obj) keys.push(key);
        return keys
      },
      readCheckboxUncheckedValues: function(formAsArray, opts, $form) {
        var selector, $uncheckedCheckboxes, $el, dataUncheckedValue, f;
        null == opts && (opts = {}), f = $.serializeJSON, selector = "input[type=checkbox][name]:not(:checked):not([disabled])", $uncheckedCheckboxes = $form.find(selector).add($form.filter(selector)), $uncheckedCheckboxes.each(function(i, el) {
          $el = $(el), dataUncheckedValue = $el.attr("data-unchecked-value"), dataUncheckedValue ? formAsArray.push({
            name: el.name,
            value: dataUncheckedValue
          }) : f.isUndefined(opts.checkboxUncheckedValue) || formAsArray.push({
            name: el.name,
            value: opts.checkboxUncheckedValue
          })
        })
      },
      extractTypeAndNameWithNoType: function(name) {
        var match;
        return (match = name.match(/(.*):([^:]+)$/)) ? {
          nameWithNoType: match[1],
          type: match[2]
        } : {
          nameWithNoType: name,
          type: null
        }
      },
      tryToFindTypeFromDataAttr: function(name, $form) {
        var escapedName, selector, $input, typeFromDataAttr;
        return escapedName = name.replace(/(:|\.|\[|\]|\s)/g, "\\$1"), selector = '[name="' + escapedName + '"]', $input = $form.find(selector).add($form.filter(selector)), typeFromDataAttr = $input.attr("data-value-type"), typeFromDataAttr || null
      },
      validateType: function(name, type, opts) {
        var validTypes, f;
        if (f = $.serializeJSON, validTypes = f.optionKeys(opts ? opts.typeFunctions : f.defaultOptions.defaultTypes), type && -1 === validTypes.indexOf(type)) throw new Error("serializeJSON ERROR: Invalid type " + type + " found in input name '" + name + "', please use one of " + validTypes.join(", "));
        return !0
      },
      splitInputNameIntoKeysArray: function(nameWithNoType) {
        var keys, f;
        return f = $.serializeJSON, keys = nameWithNoType.split("["), keys = $.map(keys, function(key) {
          return key.replace(/\]/g, "")
        }), "" === keys[0] && keys.shift(), keys
      },
      deepSet: function(o, keys, value, opts) {
        var key, nextKey, tail, lastIdx, lastVal, f;
        if (null == opts && (opts = {}), f = $.serializeJSON, f.isUndefined(o)) throw new Error("ArgumentError: param 'o' expected to be an object or array, found undefined");
        if (!keys || 0 === keys.length) throw new Error("ArgumentError: param 'keys' expected to be an array with least one element");
        key = keys[0], 1 === keys.length ? "" === key ? o.push(value) : o[key] = value : (nextKey = keys[1], "" === key && (lastIdx = o.length - 1, lastVal = o[lastIdx], key = f.isObject(lastVal) && (f.isUndefined(lastVal[nextKey]) || keys.length > 2) ? lastIdx : lastIdx + 1), "" === nextKey ? (f.isUndefined(o[key]) || !$.isArray(o[key])) && (o[key] = []) : opts.useIntKeysAsArrayIndex && f.isValidArrayIndex(nextKey) ? (f.isUndefined(o[key]) || !$.isArray(o[key])) && (o[key] = []) : (f.isUndefined(o[key]) || !f.isObject(o[key])) && (o[key] = {}), tail = keys.slice(1), f.deepSet(o[key], tail, value, opts))
      }
    }
  }), window.Modernizr = function(a, b, c) {
    function w(a) {
      j.cssText = a
    }

    function y(a, b) {
      return typeof a === b
    }
    var k, s, v, d = "2.6.2",
      e = {},
      f = !0,
      g = b.documentElement,
      h = "modernizr",
      i = b.createElement(h),
      j = i.style,
      m = ({}.toString, " -webkit- -moz- -o- -ms- ".split(" ")),
      n = {},
      q = [],
      r = q.slice,
      t = function(a, c, d, e) {
        var f, i, j, k, l = b.createElement("div"),
          m = b.body,
          n = m || b.createElement("body");
        if (parseInt(d, 10))
          for (; d--;) j = b.createElement("div"), j.id = e ? e[d] : h + (d + 1), l.appendChild(j);
        return f = ["&#173;", '<style id="s', h, '">', a, "</style>"].join(""), l.id = h, (m ? l : n).innerHTML += f, n.appendChild(l), m || (n.style.background = "", n.style.overflow = "hidden", k = g.style.overflow, g.style.overflow = "hidden", g.appendChild(n)), i = c(l, a), m ? l.parentNode.removeChild(l) : (n.parentNode.removeChild(n), g.style.overflow = k), !!i
      },
      u = {}.hasOwnProperty;
    v = y(u, "undefined") || y(u.call, "undefined") ? function(a, b) {
      return b in a && y(a.constructor.prototype[b], "undefined")
    } : function(a, b) {
      return u.call(a, b)
    }, Function.prototype.bind || (Function.prototype.bind = function(b) {
      var c = this;
      if ("function" != typeof c) throw new TypeError;
      var d = r.call(arguments, 1),
        e = function() {
          if (this instanceof e) {
            var a = function() {};
            a.prototype = c.prototype;
            var f = new a,
              g = c.apply(f, d.concat(r.call(arguments)));
            return Object(g) === g ? g : f
          }
          return c.apply(b, d.concat(r.call(arguments)))
        };
      return e
    }), n.touch = function() {
      var c;
      return "ontouchstart" in a || a.DocumentTouch && b instanceof DocumentTouch ? c = !0 : t(["@media (", m.join("touch-enabled),("), h, ")", "{#modernizr{top:9px;position:absolute}}"].join(""), function(a) {
        c = 9 === a.offsetTop
      }), c
    };
    for (var B in n) v(n, B) && (s = B.toLowerCase(), e[s] = n[B](), q.push((e[s] ? "" : "no-") + s));
    return e.addTest = function(a, b) {
      if ("object" == typeof a)
        for (var d in a) v(a, d) && e.addTest(d, a[d]);
      else {
        if (a = a.toLowerCase(), e[a] !== c) return e;
        b = "function" == typeof b ? b() : b, "undefined" != typeof f && f && (g.className += " " + (b ? "" : "no-") + a), e[a] = b
      }
      return e
    }, w(""), i = k = null, e._version = d, e._prefixes = m, e.testStyles = t, g.className = g.className.replace(/(^|\s)no-js(\s|$)/, "$1$2") + (f ? " js " + q.join(" ") : ""), e
  }(this, this.document), Modernizr.addTest("android", function() {
    return !!navigator.userAgent.match(/Android/i)
  }), Modernizr.addTest("chrome", function() {
    return !!navigator.userAgent.match(/Chrome/i)
  }), Modernizr.addTest("firefox", function() {
    return !!navigator.userAgent.match(/Firefox/i)
  }), Modernizr.addTest("iemobile", function() {
    return !!navigator.userAgent.match(/IEMobile/i)
  }), Modernizr.addTest("ie", function() {
    return !!navigator.userAgent.match(/MSIE/i)
  }), Modernizr.addTest("ie10", function() {
    return !!navigator.userAgent.match(/MSIE 10/i)
  }), Modernizr.addTest("ie11", function() {
    return !!navigator.userAgent.match(/Trident.*rv:11\./)
  }), Modernizr.addTest("ios", function() {
    return !!navigator.userAgent.match(/iPhone|iPad|iPod/i)
  }), "undefined" == typeof jQuery) throw new Error("Bootstrap requires jQuery");
if (+ function($) {
    "use strict";

    function transitionEnd() {
      var el = document.createElement("bootstrap"),
        transEndEventNames = {
          WebkitTransition: "webkitTransitionEnd",
          MozTransition: "transitionend",
          OTransition: "oTransitionEnd otransitionend",
          transition: "transitionend"
        };
      for (var name in transEndEventNames)
        if (void 0 !== el.style[name]) return {
          end: transEndEventNames[name]
        }
    }
    $.fn.emulateTransitionEnd = function(duration) {
      var called = !1,
        $el = this;
      $(this).one($.support.transition.end, function() {
        called = !0
      });
      var callback = function() {
        called || $($el).trigger($.support.transition.end)
      };
      return setTimeout(callback, duration), this
    }, $(function() {
      $.support.transition = transitionEnd()
    })
  }(jQuery), + function($) {
    "use strict";
    var dismiss = '[data-dismiss="alert"]',
      Alert = function(el) {
        $(el).on("click", dismiss, this.close)
      };
    Alert.prototype.close = function(e) {
      function removeElement() {
        $parent.trigger("closed.bs.alert").remove()
      }
      var $this = $(this),
        selector = $this.attr("data-target");
      selector || (selector = $this.attr("href"), selector = selector && selector.replace(/.*(?=#[^\s]*$)/, ""));
      var $parent = $(selector);
      e && e.preventDefault(), $parent.length || ($parent = $this.hasClass("alert") ? $this : $this.parent()), $parent.trigger(e = $.Event("close.bs.alert")), e.isDefaultPrevented() || ($parent.removeClass("in"), $.support.transition && $parent.hasClass("fade") ? $parent.one($.support.transition.end, removeElement).emulateTransitionEnd(150) : removeElement())
    };
    var old = $.fn.alert;
    $.fn.alert = function(option) {
      return this.each(function() {
        var $this = $(this),
          data = $this.data("bs.alert");
        data || $this.data("bs.alert", data = new Alert(this)), "string" == typeof option && data[option].call($this)
      })
    }, $.fn.alert.Constructor = Alert, $.fn.alert.noConflict = function() {
      return $.fn.alert = old, this
    }, $(document).on("click.bs.alert.data-api", dismiss, Alert.prototype.close)
  }(jQuery), + function($) {
    "use strict";
    var Button = function(element, options) {
      this.$element = $(element), this.options = $.extend({}, Button.DEFAULTS, options)
    };
    Button.DEFAULTS = {
      loadingText: "loading..."
    }, Button.prototype.setState = function(state) {
      var d = "disabled",
        $el = this.$element,
        val = $el.is("input") ? "val" : "html",
        data = $el.data();
      state += "Text", data.resetText || $el.data("resetText", $el[val]()), $el[val](data[state] || this.options[state]), setTimeout(function() {
        "loadingText" == state ? $el.addClass(d).attr(d, d) : $el.removeClass(d).removeAttr(d)
      }, 0)
    }, Button.prototype.toggle = function() {
      var $parent = this.$element.closest('[data-toggle="buttons"]');
      if ($parent.length) {
        var $input = this.$element.find("input").prop("checked", !this.$element.hasClass("active")).trigger("change");
        "radio" === $input.prop("type") && $parent.find(".active").removeClass("active")
      }
      this.$element.toggleClass("active")
    };
    var old = $.fn.button;
    $.fn.button = function(option) {
      return this.each(function() {
        var $this = $(this),
          data = $this.data("bs.button"),
          options = "object" == typeof option && option;
        data || $this.data("bs.button", data = new Button(this, options)), "toggle" == option ? data.toggle() : option && data.setState(option)
      })
    }, $.fn.button.Constructor = Button, $.fn.button.noConflict = function() {
      return $.fn.button = old, this
    }, $(document).on("click.bs.button.data-api", "[data-toggle^=button]", function(e) {
      var $btn = $(e.target);
      $btn.hasClass("btn") || ($btn = $btn.closest(".btn")), $btn.button("toggle"), e.preventDefault()
    })
  }(jQuery), + function($) {
    "use strict";
    var Carousel = function(element, options) {
      this.$element = $(element), this.$indicators = this.$element.find(".carousel-indicators"), this.options = options, this.paused = this.sliding = this.interval = this.$active = this.$items = null, "hover" == this.options.pause && this.$element.on("mouseenter", $.proxy(this.pause, this)).on("mouseleave", $.proxy(this.cycle, this))
    };
    Carousel.DEFAULTS = {
      interval: 5e3,
      pause: "hover",
      wrap: !0
    }, Carousel.prototype.cycle = function(e) {
      return e || (this.paused = !1), this.interval && clearInterval(this.interval), this.options.interval && !this.paused && (this.interval = setInterval($.proxy(this.next, this), this.options.interval)), this
    }, Carousel.prototype.getActiveIndex = function() {
      return this.$active = this.$element.find(".item.active"), this.$items = this.$active.parent().children(), this.$items.index(this.$active)
    }, Carousel.prototype.to = function(pos) {
      var that = this,
        activeIndex = this.getActiveIndex();
      return pos > this.$items.length - 1 || 0 > pos ? void 0 : this.sliding ? this.$element.one("slid", function() {
        that.to(pos)
      }) : activeIndex == pos ? this.pause().cycle() : this.slide(pos > activeIndex ? "next" : "prev", $(this.$items[pos]))
    }, Carousel.prototype.pause = function(e) {
      return e || (this.paused = !0), this.$element.find(".next, .prev").length && $.support.transition.end && (this.$element.trigger($.support.transition.end), this.cycle(!0)), this.interval = clearInterval(this.interval), this
    }, Carousel.prototype.next = function() {
      return this.sliding ? void 0 : this.slide("next")
    }, Carousel.prototype.prev = function() {
      return this.sliding ? void 0 : this.slide("prev")
    }, Carousel.prototype.slide = function(type, next) {
      var $active = this.$element.find(".item.active"),
        $next = next || $active[type](),
        isCycling = this.interval,
        direction = "next" == type ? "left" : "right",
        fallback = "next" == type ? "first" : "last",
        that = this;
      if (!$next.length) {
        if (!this.options.wrap) return;
        $next = this.$element.find(".item")[fallback]()
      }
      this.sliding = !0, isCycling && this.pause();
      var e = $.Event("slide.bs.carousel", {
        relatedTarget: $next[0],
        direction: direction
      });
      if (!$next.hasClass("active")) {
        if (this.$indicators.length && (this.$indicators.find(".active").removeClass("active"), this.$element.one("slid", function() {
            var $nextIndicator = $(that.$indicators.children()[that.getActiveIndex()]);
            $nextIndicator && $nextIndicator.addClass("active")
          })), $.support.transition && this.$element.hasClass("slide")) {
          if (this.$element.trigger(e), e.isDefaultPrevented()) return;
          $next.addClass(type), $next[0].offsetWidth, $active.addClass(direction), $next.addClass(direction), $active.one($.support.transition.end, function() {
            $next.removeClass([type, direction].join(" ")).addClass("active"), $active.removeClass(["active", direction].join(" ")), that.sliding = !1, setTimeout(function() {
              that.$element.trigger("slid")
            }, 0)
          }).emulateTransitionEnd(600)
        } else {
          if (this.$element.trigger(e), e.isDefaultPrevented()) return;
          $active.removeClass("active"), $next.addClass("active"), this.sliding = !1, this.$element.trigger("slid")
        }
        return isCycling && this.cycle(), this
      }
    };
    var old = $.fn.carousel;
    $.fn.carousel = function(option) {
      return this.each(function() {
        var $this = $(this),
          data = $this.data("bs.carousel"),
          options = $.extend({}, Carousel.DEFAULTS, $this.data(), "object" == typeof option && option),
          action = "string" == typeof option ? option : options.slide;
        data || $this.data("bs.carousel", data = new Carousel(this, options)), "number" == typeof option ? data.to(option) : action ? data[action]() : options.interval && data.pause().cycle()
      })
    }, $.fn.carousel.Constructor = Carousel, $.fn.carousel.noConflict = function() {
      return $.fn.carousel = old, this
    }, $(document).on("click.bs.carousel.data-api", "[data-slide], [data-slide-to]", function(e) {
      var href, $this = $(this),
        $target = $($this.attr("data-target") || (href = $this.attr("href")) && href.replace(/.*(?=#[^\s]+$)/, "")),
        options = $.extend({}, $target.data(), $this.data()),
        slideIndex = $this.attr("data-slide-to");
      slideIndex && (options.interval = !1), $target.carousel(options), (slideIndex = $this.attr("data-slide-to")) && $target.data("bs.carousel").to(slideIndex), e.preventDefault()
    }), $(window).on("load", function() {
      $('[data-ride="carousel"]').each(function() {
        var $carousel = $(this);
        $carousel.carousel($carousel.data())
      })
    })
  }(jQuery), + function($) {
    "use strict";
    var Collapse = function(element, options) {
      this.$element = $(element), this.options = $.extend({}, Collapse.DEFAULTS, options), this.transitioning = null, this.options.parent && (this.$parent = $(this.options.parent)), this.options.toggle && this.toggle()
    };
    Collapse.DEFAULTS = {
      toggle: !0
    }, Collapse.prototype.dimension = function() {
      var hasWidth = this.$element.hasClass("width");
      return hasWidth ? "width" : "height"
    }, Collapse.prototype.show = function() {
      if (!this.transitioning && !this.$element.hasClass("in")) {
        var startEvent = $.Event("show.bs.collapse");
        if (this.$element.trigger(startEvent), !startEvent.isDefaultPrevented()) {
          var actives = this.$parent && this.$parent.find("> .panel > .in");
          if (actives && actives.length) {
            var hasData = actives.data("bs.collapse");
            if (hasData && hasData.transitioning) return;
            actives.collapse("hide"), hasData || actives.data("bs.collapse", null)
          }
          var dimension = this.dimension();
          this.$element.removeClass("collapse").addClass("collapsing")[dimension](0), this.transitioning = 1;
          var complete = function() {
            this.$element.removeClass("collapsing").addClass("in")[dimension]("auto"), this.transitioning = 0, this.$element.trigger("shown.bs.collapse")
          };
          if (!$.support.transition) return complete.call(this);
          var scrollSize = $.camelCase(["scroll", dimension].join("-"));
          this.$element.one($.support.transition.end, $.proxy(complete, this)).emulateTransitionEnd(350)[dimension](this.$element[0][scrollSize])
        }
      }
    }, Collapse.prototype.hide = function() {
      if (!this.transitioning && this.$element.hasClass("in")) {
        var startEvent = $.Event("hide.bs.collapse");
        if (this.$element.trigger(startEvent), !startEvent.isDefaultPrevented()) {
          var dimension = this.dimension();
          this.$element[dimension](this.$element[dimension]())[0].offsetHeight, this.$element.addClass("collapsing").removeClass("collapse").removeClass("in"), this.transitioning = 1;
          var complete = function() {
            this.transitioning = 0, this.$element.trigger("hidden.bs.collapse").removeClass("collapsing").addClass("collapse")
          };
          return $.support.transition ? void this.$element[dimension](0).one($.support.transition.end, $.proxy(complete, this)).emulateTransitionEnd(350) : complete.call(this)
        }
      }
    }, Collapse.prototype.toggle = function() {
      this[this.$element.hasClass("in") ? "hide" : "show"]()
    };
    var old = $.fn.collapse;
    $.fn.collapse = function(option) {
      return this.each(function() {
        var $this = $(this),
          data = $this.data("bs.collapse"),
          options = $.extend({}, Collapse.DEFAULTS, $this.data(), "object" == typeof option && option);
        data || $this.data("bs.collapse", data = new Collapse(this, options)), "string" == typeof option && data[option]()
      })
    }, $.fn.collapse.Constructor = Collapse, $.fn.collapse.noConflict = function() {
      return $.fn.collapse = old, this
    }, $(document).on("click.bs.collapse.data-api", "[data-toggle=collapse]", function(e) {
      var href, $this = $(this),
        target = $this.attr("data-target") || e.preventDefault() || (href = $this.attr("href")) && href.replace(/.*(?=#[^\s]+$)/, ""),
        $target = $(target),
        data = $target.data("bs.collapse"),
        option = data ? "toggle" : $this.data(),
        parent = $this.attr("data-parent"),
        $parent = parent && $(parent);
      data && data.transitioning || ($parent && $parent.find('[data-toggle=collapse][data-parent="' + parent + '"]').not($this).addClass("collapsed"), $this[$target.hasClass("in") ? "addClass" : "removeClass"]("collapsed")), $target.collapse(option)
    })
  }(jQuery), + function($) {
    "use strict";

    function clearMenus() {
      $(backdrop).remove(), $(toggle).each(function(e) {
        var $parent = getParent($(this));
        $parent.hasClass("open") && ($parent.trigger(e = $.Event("hide.bs.dropdown")), e.isDefaultPrevented() || $parent.removeClass("open").trigger("hidden.bs.dropdown"))
      })
    }

    function getParent($this) {
      var selector = $this.attr("data-target");
      selector || (selector = $this.attr("href"), selector = selector && /#/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, ""));
      var $parent = selector && $(selector);
      return $parent && $parent.length ? $parent : $this.parent()
    }
    var backdrop = ".dropdown-backdrop",
      toggle = "[data-toggle=dropdown]",
      Dropdown = function(element) {
        $(element).on("click.bs.dropdown", this.toggle)
      };
    Dropdown.prototype.toggle = function(e) {
      var $this = $(this);
      if (!$this.is(".disabled, :disabled")) {
        var $parent = getParent($this),
          isActive = $parent.hasClass("open");
        if (clearMenus(), !isActive) {
          if ("ontouchstart" in document.documentElement && !$parent.closest(".navbar-nav").length && $('<div class="dropdown-backdrop"/>').insertAfter($(this)).on("click", clearMenus), $parent.trigger(e = $.Event("show.bs.dropdown")), e.isDefaultPrevented()) return;
          $parent.toggleClass("open").trigger("shown.bs.dropdown"), $this.focus()
        }
        return !1
      }
    }, Dropdown.prototype.keydown = function(e) {
      if (/(38|40|27)/.test(e.keyCode)) {
        var $this = $(this);
        if (e.preventDefault(), e.stopPropagation(), !$this.is(".disabled, :disabled")) {
          var $parent = getParent($this),
            isActive = $parent.hasClass("open");
          if (!isActive || isActive && 27 == e.keyCode) return 27 == e.which && $parent.find(toggle).focus(), $this.click();
          var $items = $("[role=menu] li:not(.divider):visible a", $parent);
          if ($items.length) {
            var index = $items.index($items.filter(":focus"));
            38 == e.keyCode && index > 0 && index--, 40 == e.keyCode && index < $items.length - 1 && index++, ~index || (index = 0), $items.eq(index).focus()
          }
        }
      }
    };
    var old = $.fn.dropdown;
    $.fn.dropdown = function(option) {
      return this.each(function() {
        var $this = $(this),
          data = $this.data("dropdown");
        data || $this.data("dropdown", data = new Dropdown(this)), "string" == typeof option && data[option].call($this)
      })
    }, $.fn.dropdown.Constructor = Dropdown, $.fn.dropdown.noConflict = function() {
      return $.fn.dropdown = old, this
    }, $(document).on("click.bs.dropdown.data-api", clearMenus).on("click.bs.dropdown.data-api", ".dropdown form", function(e) {
      e.stopPropagation()
    }).on("click.bs.dropdown.data-api", toggle, Dropdown.prototype.toggle).on("keydown.bs.dropdown.data-api", toggle + ", [role=menu]", Dropdown.prototype.keydown)
  }(jQuery), + function($) {
    "use strict";
    var Modal = function(element, options) {
      this.options = options, this.$element = $(element), this.$backdrop = this.isShown = null, this.options.remote && this.$element.load(this.options.remote)
    };
    Modal.DEFAULTS = {
      backdrop: !0,
      keyboard: !0,
      show: !0
    }, Modal.prototype.toggle = function(_relatedTarget) {
      return this[this.isShown ? "hide" : "show"](_relatedTarget)
    }, Modal.prototype.show = function(_relatedTarget) {
      var that = this,
        e = $.Event("show.bs.modal", {
          relatedTarget: _relatedTarget
        });
      this.$element.trigger(e), this.isShown || e.isDefaultPrevented() || (this.isShown = !0, this.escape(), this.$element.on("click.dismiss.modal", '[data-dismiss="modal"]', $.proxy(this.hide, this)), this.backdrop(function() {
        var transition = $.support.transition && that.$element.hasClass("fade");
        that.$element.parent().length || that.$element.appendTo(document.body), that.$element.show(), transition && that.$element[0].offsetWidth, that.$element.addClass("in").attr("aria-hidden", !1), that.enforceFocus();
        var e = $.Event("shown.bs.modal", {
          relatedTarget: _relatedTarget
        });
        transition ? that.$element.find(".modal-dialog").one($.support.transition.end, function() {
          that.$element.focus().trigger(e)
        }).emulateTransitionEnd(300) : that.$element.focus().trigger(e)
      }))
    }, Modal.prototype.hide = function(e) {
      e && e.preventDefault(), e = $.Event("hide.bs.modal"), this.$element.trigger(e), this.isShown && !e.isDefaultPrevented() && (this.isShown = !1, this.escape(), $(document).off("focusin.bs.modal"), this.$element.removeClass("in").attr("aria-hidden", !0).off("click.dismiss.modal"), $.support.transition && this.$element.hasClass("fade") ? this.$element.one($.support.transition.end, $.proxy(this.hideModal, this)).emulateTransitionEnd(300) : this.hideModal())
    }, Modal.prototype.enforceFocus = function() {
      $(document).off("focusin.bs.modal").on("focusin.bs.modal", $.proxy(function(e) {
        this.$element[0] === e.target || this.$element.has(e.target).length || this.$element.focus()
      }, this))
    }, Modal.prototype.escape = function() {
      this.isShown && this.options.keyboard ? this.$element.on("keyup.dismiss.bs.modal", $.proxy(function(e) {
        27 == e.which && this.hide()
      }, this)) : this.isShown || this.$element.off("keyup.dismiss.bs.modal")
    }, Modal.prototype.hideModal = function() {
      var that = this;
      this.$element.hide(), this.backdrop(function() {
        that.removeBackdrop(), that.$element.trigger("hidden.bs.modal")
      })
    }, Modal.prototype.removeBackdrop = function() {
      this.$backdrop && this.$backdrop.remove(), this.$backdrop = null
    }, Modal.prototype.backdrop = function(callback) {
      var animate = this.$element.hasClass("fade") ? "fade" : "";
      if (this.isShown && this.options.backdrop) {
        var doAnimate = $.support.transition && animate;
        if (this.$backdrop = $('<div class="modal-backdrop ' + animate + '" />').appendTo(document.body), this.$element.on("click.dismiss.modal", $.proxy(function(e) {
            e.target === e.currentTarget && ("static" == this.options.backdrop ? this.$element[0].focus.call(this.$element[0]) : this.hide.call(this))
          }, this)), doAnimate && this.$backdrop[0].offsetWidth, this.$backdrop.addClass("in"), !callback) return;
        doAnimate ? this.$backdrop.one($.support.transition.end, callback).emulateTransitionEnd(150) : callback()
      } else !this.isShown && this.$backdrop ? (this.$backdrop.removeClass("in"), $.support.transition && this.$element.hasClass("fade") ? this.$backdrop.one($.support.transition.end, callback).emulateTransitionEnd(150) : callback()) : callback && callback()
    };
    var old = $.fn.modal;
    $.fn.modal = function(option, _relatedTarget) {
      return this.each(function() {
        var $this = $(this),
          data = $this.data("bs.modal"),
          options = $.extend({}, Modal.DEFAULTS, $this.data(), "object" == typeof option && option);
        data || $this.data("bs.modal", data = new Modal(this, options)), "string" == typeof option ? data[option](_relatedTarget) : options.show && data.show(_relatedTarget)
      })
    }, $.fn.modal.Constructor = Modal, $.fn.modal.noConflict = function() {
      return $.fn.modal = old, this
    }, $(document).on("click.bs.modal.data-api", '[data-toggle="modal"]', function(e) {
      var $this = $(this),
        href = $this.attr("href"),
        $target = $($this.attr("data-target") || href && href.replace(/.*(?=#[^\s]+$)/, "")),
        option = $target.data("modal") ? "toggle" : $.extend({
          remote: !/#/.test(href) && href
        }, $target.data(), $this.data());
      e.preventDefault(), $target.modal(option, this).one("hide", function() {
        $this.is(":visible") && $this.focus()
      })
    }), $(document).on("show.bs.modal", ".modal", function() {
      $(document.body).addClass("modal-open")
    }).on("hidden.bs.modal", ".modal", function() {
      $(document.body).removeClass("modal-open")
    })
  }(jQuery), + function($) {
    "use strict";
    var Tooltip = function(element, options) {
      this.type = this.options = this.enabled = this.timeout = this.hoverState = this.$element = null, this.init("tooltip", element, options)
    };
    Tooltip.DEFAULTS = {
      animation: !0,
      placement: "top",
      selector: !1,
      template: '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
      trigger: "hover focus",
      title: "",
      delay: 0,
      html: !1,
      container: !1
    }, Tooltip.prototype.init = function(type, element, options) {
      this.enabled = !0, this.type = type, this.$element = $(element), this.options = this.getOptions(options);
      for (var triggers = this.options.trigger.split(" "), i = triggers.length; i--;) {
        var trigger = triggers[i];
        if ("click" == trigger) this.$element.on("click." + this.type, this.options.selector, $.proxy(this.toggle, this));
        else if ("manual" != trigger) {
          var eventIn = "hover" == trigger ? "mouseenter" : "focus",
            eventOut = "hover" == trigger ? "mouseleave" : "blur";
          this.$element.on(eventIn + "." + this.type, this.options.selector, $.proxy(this.enter, this)), this.$element.on(eventOut + "." + this.type, this.options.selector, $.proxy(this.leave, this))
        }
      }
      this.options.selector ? this._options = $.extend({}, this.options, {
        trigger: "manual",
        selector: ""
      }) : this.fixTitle()
    }, Tooltip.prototype.getDefaults = function() {
      return Tooltip.DEFAULTS
    }, Tooltip.prototype.getOptions = function(options) {
      return options = $.extend({}, this.getDefaults(), this.$element.data(), options), options.delay && "number" == typeof options.delay && (options.delay = {
        show: options.delay,
        hide: options.delay
      }), options
    }, Tooltip.prototype.getDelegateOptions = function() {
      var options = {},
        defaults = this.getDefaults();
      return this._options && $.each(this._options, function(key, value) {
        defaults[key] != value && (options[key] = value)
      }), options
    }, Tooltip.prototype.enter = function(obj) {
      var self = obj instanceof this.constructor ? obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data("bs." + this.type);
      return clearTimeout(self.timeout), self.hoverState = "in", self.options.delay && self.options.delay.show ? void(self.timeout = setTimeout(function() {
        "in" == self.hoverState && self.show()
      }, self.options.delay.show)) : self.show()
    }, Tooltip.prototype.leave = function(obj) {
      var self = obj instanceof this.constructor ? obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data("bs." + this.type);
      return clearTimeout(self.timeout), self.hoverState = "out", self.options.delay && self.options.delay.hide ? void(self.timeout = setTimeout(function() {
        "out" == self.hoverState && self.hide()
      }, self.options.delay.hide)) : self.hide()
    }, Tooltip.prototype.show = function() {
      var e = $.Event("show.bs." + this.type);
      if (this.hasContent() && this.enabled) {
        if (this.$element.trigger(e), e.isDefaultPrevented()) return;
        var $tip = this.tip();
        this.setContent(), this.options.animation && $tip.addClass("fade");
        var placement = "function" == typeof this.options.placement ? this.options.placement.call(this, $tip[0], this.$element[0]) : this.options.placement,
          autoToken = /\s?auto?\s?/i,
          autoPlace = autoToken.test(placement);
        autoPlace && (placement = placement.replace(autoToken, "") || "top"), $tip.detach().css({
          top: 0,
          left: 0,
          display: "block"
        }).addClass(placement), this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element);
        var pos = this.getPosition(),
          actualWidth = $tip[0].offsetWidth,
          actualHeight = $tip[0].offsetHeight;
        if (autoPlace) {
          var $parent = this.$element.parent(),
            orgPlacement = placement,
            docScroll = document.documentElement.scrollTop || document.body.scrollTop,
            parentWidth = "body" == this.options.container ? window.innerWidth : $parent.outerWidth(),
            parentHeight = "body" == this.options.container ? window.innerHeight : $parent.outerHeight(),
            parentLeft = "body" == this.options.container ? 0 : $parent.offset().left;
          placement = "bottom" == placement && pos.top + pos.height + actualHeight - docScroll > parentHeight ? "top" : "top" == placement && pos.top - docScroll - actualHeight < 0 ? "bottom" : "right" == placement && pos.right + actualWidth > parentWidth ? "left" : "left" == placement && pos.left - actualWidth < parentLeft ? "right" : placement, $tip.removeClass(orgPlacement).addClass(placement)
        }
        var calculatedOffset = this.getCalculatedOffset(placement, pos, actualWidth, actualHeight);
        this.applyPlacement(calculatedOffset, placement), this.$element.trigger("shown.bs." + this.type)
      }
    }, Tooltip.prototype.applyPlacement = function(offset, placement) {
      var replace, $tip = this.tip(),
        width = $tip[0].offsetWidth,
        height = $tip[0].offsetHeight,
        marginTop = parseInt($tip.css("margin-top"), 10),
        marginLeft = parseInt($tip.css("margin-left"), 10);
      isNaN(marginTop) && (marginTop = 0), isNaN(marginLeft) && (marginLeft = 0), offset.top = offset.top + marginTop, offset.left = offset.left + marginLeft, $tip.offset(offset).addClass("in");
      var actualWidth = $tip[0].offsetWidth,
        actualHeight = $tip[0].offsetHeight;
      if ("top" == placement && actualHeight != height && (replace = !0, offset.top = offset.top + height - actualHeight), /bottom|top/.test(placement)) {
        var delta = 0;
        offset.left < 0 && (delta = -2 * offset.left, offset.left = 0, $tip.offset(offset), actualWidth = $tip[0].offsetWidth, actualHeight = $tip[0].offsetHeight), this.replaceArrow(delta - width + actualWidth, actualWidth, "left")
      } else this.replaceArrow(actualHeight - height, actualHeight, "top");
      replace && $tip.offset(offset)
    }, Tooltip.prototype.replaceArrow = function(delta, dimension, position) {
      this.arrow().css(position, delta ? 50 * (1 - delta / dimension) + "%" : "")
    }, Tooltip.prototype.setContent = function() {
      var $tip = this.tip(),
        title = this.getTitle();
      $tip.find(".tooltip-inner")[this.options.html ? "html" : "text"](title), $tip.removeClass("fade in top bottom left right")
    }, Tooltip.prototype.hide = function() {
      function complete() {
        "in" != that.hoverState && $tip.detach()
      }
      var that = this,
        $tip = this.tip(),
        e = $.Event("hide.bs." + this.type);
      return this.$element.trigger(e), e.isDefaultPrevented() ? void 0 : ($tip.removeClass("in"), $.support.transition && this.$tip.hasClass("fade") ? $tip.one($.support.transition.end, complete).emulateTransitionEnd(150) : complete(), this.$element.trigger("hidden.bs." + this.type), this)
    }, Tooltip.prototype.fixTitle = function() {
      var $e = this.$element;
      ($e.attr("title") || "string" != typeof $e.attr("data-original-title")) && $e.attr("data-original-title", $e.attr("title") || "").attr("title", "")
    }, Tooltip.prototype.hasContent = function() {
      return this.getTitle()
    }, Tooltip.prototype.getPosition = function() {
      var el = this.$element[0];
      return $.extend({}, "function" == typeof el.getBoundingClientRect ? el.getBoundingClientRect() : {
        width: el.offsetWidth,
        height: el.offsetHeight
      }, this.$element.offset())
    }, Tooltip.prototype.getCalculatedOffset = function(placement, pos, actualWidth, actualHeight) {
      return "bottom" == placement ? {
        top: pos.top + pos.height,
        left: pos.left + pos.width / 2 - actualWidth / 2
      } : "top" == placement ? {
        top: pos.top - actualHeight,
        left: pos.left + pos.width / 2 - actualWidth / 2
      } : "left" == placement ? {
        top: pos.top + pos.height / 2 - actualHeight / 2,
        left: pos.left - actualWidth
      } : {
        top: pos.top + pos.height / 2 - actualHeight / 2,
        left: pos.left + pos.width
      }
    }, Tooltip.prototype.getTitle = function() {
      var title, $e = this.$element,
        o = this.options;
      return title = $e.attr("data-original-title") || ("function" == typeof o.title ? o.title.call($e[0]) : o.title)
    }, Tooltip.prototype.tip = function() {
      return this.$tip = this.$tip || $(this.options.template)
    }, Tooltip.prototype.arrow = function() {
      return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow")
    }, Tooltip.prototype.validate = function() {
      this.$element[0].parentNode || (this.hide(), this.$element = null, this.options = null)
    }, Tooltip.prototype.enable = function() {
      this.enabled = !0
    }, Tooltip.prototype.disable = function() {
      this.enabled = !1
    }, Tooltip.prototype.toggleEnabled = function() {
      this.enabled = !this.enabled
    }, Tooltip.prototype.toggle = function(e) {
      var self = e ? $(e.currentTarget)[this.type](this.getDelegateOptions()).data("bs." + this.type) : this;
      self.tip().hasClass("in") ? self.leave(self) : self.enter(self)
    }, Tooltip.prototype.destroy = function() {
      this.hide().$element.off("." + this.type).removeData("bs." + this.type)
    };
    var old = $.fn.tooltip;
    $.fn.tooltip = function(option) {
      return this.each(function() {
        var $this = $(this),
          data = $this.data("bs.tooltip"),
          options = "object" == typeof option && option;
        data || $this.data("bs.tooltip", data = new Tooltip(this, options)), "string" == typeof option && data[option]()
      })
    }, $.fn.tooltip.Constructor = Tooltip, $.fn.tooltip.noConflict = function() {
      return $.fn.tooltip = old, this
    }
  }(jQuery), + function($) {
    "use strict";
    var Popover = function(element, options) {
      this.init("popover", element, options)
    };
    if (!$.fn.tooltip) throw new Error("Popover requires tooltip.js");
    Popover.DEFAULTS = $.extend({}, $.fn.tooltip.Constructor.DEFAULTS, {
      placement: "right",
      trigger: "click",
      content: "",
      template: '<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    }), Popover.prototype = $.extend({}, $.fn.tooltip.Constructor.prototype), Popover.prototype.constructor = Popover, Popover.prototype.getDefaults = function() {
      return Popover.DEFAULTS
    }, Popover.prototype.setContent = function() {
      var $tip = this.tip(),
        title = this.getTitle(),
        content = this.getContent();
      $tip.find(".popover-title")[this.options.html ? "html" : "text"](title), $tip.find(".popover-content")[this.options.html ? "html" : "text"](content), $tip.removeClass("fade top bottom left right in"), $tip.find(".popover-title").html() || $tip.find(".popover-title").hide()
    }, Popover.prototype.hasContent = function() {
      return this.getTitle() || this.getContent()
    }, Popover.prototype.getContent = function() {
      var $e = this.$element,
        o = this.options;
      return $e.attr("data-content") || ("function" == typeof o.content ? o.content.call($e[0]) : o.content)
    }, Popover.prototype.arrow = function() {
      return this.$arrow = this.$arrow || this.tip().find(".arrow")
    }, Popover.prototype.tip = function() {
      return this.$tip || (this.$tip = $(this.options.template)), this.$tip
    };
    var old = $.fn.popover;
    $.fn.popover = function(option) {
      return this.each(function() {
        var $this = $(this),
          data = $this.data("bs.popover"),
          options = "object" == typeof option && option;
        data || $this.data("bs.popover", data = new Popover(this, options)), "string" == typeof option && data[option]()
      })
    }, $.fn.popover.Constructor = Popover, $.fn.popover.noConflict = function() {
      return $.fn.popover = old, this
    }
  }(jQuery), + function($) {
    "use strict";

    function ScrollSpy(element, options) {
      var href, process = $.proxy(this.process, this);
      this.$element = $($(element).is("body") ? window : element), this.$body = $("body"), this.$scrollElement = this.$element.on("scroll.bs.scroll-spy.data-api", process), this.options = $.extend({}, ScrollSpy.DEFAULTS, options), this.selector = (this.options.target || (href = $(element).attr("href")) && href.replace(/.*(?=#[^\s]+$)/, "") || "") + " .nav li > a", this.offsets = $([]), this.targets = $([]), this.activeTarget = null, this.refresh(), this.process()
    }
    ScrollSpy.DEFAULTS = {
      offset: 10
    }, ScrollSpy.prototype.refresh = function() {
      var offsetMethod = this.$element[0] == window ? "offset" : "position";
      this.offsets = $([]), this.targets = $([]);
      var self = this;
      this.$body.find(this.selector).map(function() {
        var $el = $(this),
          href = $el.data("target") || $el.attr("href"),
          $href = /^#\w/.test(href) && $(href);
        return $href && $href.length && [
          [$href[offsetMethod]().top + (!$.isWindow(self.$scrollElement.get(0)) && self.$scrollElement.scrollTop()), href]
        ] || null
      }).sort(function(a, b) {
        return a[0] - b[0]
      }).each(function() {
        self.offsets.push(this[0]), self.targets.push(this[1])
      })
    }, ScrollSpy.prototype.process = function() {
      var i, scrollTop = this.$scrollElement.scrollTop() + this.options.offset,
        scrollHeight = this.$scrollElement[0].scrollHeight || this.$body[0].scrollHeight,
        maxScroll = scrollHeight - this.$scrollElement.height(),
        offsets = this.offsets,
        targets = this.targets,
        activeTarget = this.activeTarget;
      if (scrollTop >= maxScroll) return activeTarget != (i = targets.last()[0]) && this.activate(i);
      for (i = offsets.length; i--;) activeTarget != targets[i] && scrollTop >= offsets[i] && (!offsets[i + 1] || scrollTop <= offsets[i + 1]) && this.activate(targets[i])
    }, ScrollSpy.prototype.activate = function(target) {
      this.activeTarget = target, $(this.selector).parents(".active").removeClass("active");
      var selector = this.selector + '[data-target="' + target + '"],' + this.selector + '[href="' + target + '"]',
        active = $(selector).parents("li").addClass("active");
      active.parent(".dropdown-menu").length && (active = active.closest("li.dropdown").addClass("active")), active.trigger("activate")
    };
    var old = $.fn.scrollspy;
    $.fn.scrollspy = function(option) {
      return this.each(function() {
        var $this = $(this),
          data = $this.data("bs.scrollspy"),
          options = "object" == typeof option && option;
        data || $this.data("bs.scrollspy", data = new ScrollSpy(this, options)), "string" == typeof option && data[option]()
      })
    }, $.fn.scrollspy.Constructor = ScrollSpy, $.fn.scrollspy.noConflict = function() {
      return $.fn.scrollspy = old, this
    }, $(window).on("load", function() {
      $('[data-spy="scroll"]').each(function() {
        var $spy = $(this);
        $spy.scrollspy($spy.data())
      })
    })
  }(jQuery), + function($) {
    "use strict";
    var Tab = function(element) {
      this.element = $(element)
    };
    Tab.prototype.show = function() {
      var $this = this.element,
        $ul = $this.closest("ul:not(.dropdown-menu)"),
        selector = $this.data("target");
      if (selector || (selector = $this.attr("href"), selector = selector && selector.replace(/.*(?=#[^\s]*$)/, "")), !$this.parent("li").hasClass("active")) {
        var previous = $ul.find(".active:last a")[0],
          e = $.Event("show.bs.tab", {
            relatedTarget: previous
          });
        if ($this.trigger(e), !e.isDefaultPrevented()) {
          var $target = $(selector);
          this.activate($this.parent("li"), $ul), this.activate($target, $target.parent(), function() {
            $this.trigger({
              type: "shown.bs.tab",
              relatedTarget: previous
            })
          })
        }
      }
    }, Tab.prototype.activate = function(element, container, callback) {
      function next() {
        $active.removeClass("active").find("> .dropdown-menu > .active").removeClass("active"), element.addClass("active"), transition ? (element[0].offsetWidth, element.addClass("in")) : element.removeClass("fade"), element.parent(".dropdown-menu") && element.closest("li.dropdown").addClass("active"), callback && callback()
      }
      var $active = container.find("> .active"),
        transition = callback && $.support.transition && $active.hasClass("fade");
      transition ? $active.one($.support.transition.end, next).emulateTransitionEnd(150) : next(), $active.removeClass("in")
    };
    var old = $.fn.tab;
    $.fn.tab = function(option) {
      return this.each(function() {
        var $this = $(this),
          data = $this.data("bs.tab");
        data || $this.data("bs.tab", data = new Tab(this)), "string" == typeof option && data[option]()
      })
    }, $.fn.tab.Constructor = Tab, $.fn.tab.noConflict = function() {
      return $.fn.tab = old, this
    }, $(document).on("click.bs.tab.data-api", '[data-toggle="tab"], [data-toggle="pill"]', function(e) {
      e.preventDefault(), $(this).tab("show")
    })
  }(jQuery), + function($) {
    "use strict";
    var Affix = function(element, options) {
      this.options = $.extend({}, Affix.DEFAULTS, options), this.$window = $(window).on("scroll.bs.affix.data-api", $.proxy(this.checkPosition, this)).on("click.bs.affix.data-api", $.proxy(this.checkPositionWithEventLoop, this)), this.$element = $(element), this.affixed = this.unpin = null, this.checkPosition()
    };
    Affix.RESET = "affix affix-top affix-bottom", Affix.DEFAULTS = {
      offset: 0
    }, Affix.prototype.checkPositionWithEventLoop = function() {
      setTimeout($.proxy(this.checkPosition, this), 1)
    }, Affix.prototype.checkPosition = function() {
      if (this.$element.is(":visible")) {
        var scrollHeight = $(document).height(),
          scrollTop = this.$window.scrollTop(),
          position = this.$element.offset(),
          offset = this.options.offset,
          offsetTop = offset.top,
          offsetBottom = offset.bottom;
        "object" != typeof offset && (offsetBottom = offsetTop = offset), "function" == typeof offsetTop && (offsetTop = offset.top()), "function" == typeof offsetBottom && (offsetBottom = offset.bottom());
        var affix = null != this.unpin && scrollTop + this.unpin <= position.top ? !1 : null != offsetBottom && position.top + this.$element.height() >= scrollHeight - offsetBottom ? "bottom" : null != offsetTop && offsetTop >= scrollTop ? "top" : !1;
        this.affixed !== affix && (this.unpin && this.$element.css("top", ""), this.affixed = affix, this.unpin = "bottom" == affix ? position.top - scrollTop : null, this.$element.removeClass(Affix.RESET).addClass("affix" + (affix ? "-" + affix : "")), "bottom" == affix && this.$element.offset({
          top: document.body.offsetHeight - offsetBottom - this.$element.height()
        }))
      }
    };
    var old = $.fn.affix;
    $.fn.affix = function(option) {
      return this.each(function() {
        var $this = $(this),
          data = $this.data("bs.affix"),
          options = "object" == typeof option && option;
        data || $this.data("bs.affix", data = new Affix(this, options)), "string" == typeof option && data[option]()
      })
    }, $.fn.affix.Constructor = Affix, $.fn.affix.noConflict = function() {
      return $.fn.affix = old, this
    }, $(window).on("load", function() {
      $('[data-spy="affix"]').each(function() {
        var $spy = $(this),
          data = $spy.data();
        data.offset = data.offset || {}, data.offsetBottom && (data.offset.bottom = data.offsetBottom), data.offsetTop && (data.offset.top = data.offsetTop), $spy.affix(data)
      })
    })
  }(jQuery), function($, undefined) {
    function UTCDate() {
      return new Date(Date.UTC.apply(Date, arguments))
    }

    function UTCToday() {
      var today = new Date;
      return UTCDate(today.getFullYear(), today.getMonth(), today.getDate())
    }

    function alias(method) {
      return function() {
        return this[method].apply(this, arguments)
      }
    }

    function opts_from_el(el, prefix) {
      function re_lower(_, a) {
        return a.toLowerCase()
      }
      var inkey, data = $(el).data(),
        out = {},
        replace = new RegExp("^" + prefix.toLowerCase() + "([A-Z])");
      prefix = new RegExp("^" + prefix.toLowerCase());
      for (var key in data) prefix.test(key) && (inkey = key.replace(replace, re_lower), out[inkey] = data[key]);
      return out
    }

    function opts_from_locale(lang) {
      var out = {};
      if (dates[lang] || (lang = lang.split("-")[0], dates[lang])) {
        var d = dates[lang];
        return $.each(locale_opts, function(i, k) {
          k in d && (out[k] = d[k])
        }), out
      }
    }
    var $window = $(window),
      DateArray = function() {
        var extras = {
          get: function(i) {
            return this.slice(i)[0]
          },
          contains: function(d) {
            for (var val = d && d.valueOf(), i = 0, l = this.length; l > i; i++)
              if (this[i].valueOf() === val) return i;
            return -1
          },
          remove: function(i) {
            this.splice(i, 1)
          },
          replace: function(new_array) {
            new_array && ($.isArray(new_array) || (new_array = [new_array]), this.clear(), this.push.apply(this, new_array))
          },
          clear: function() {
            this.length = 0
          },
          copy: function() {
            var a = new DateArray;
            return a.replace(this), a
          }
        };
        return function() {
          var a = [];
          return a.push.apply(a, arguments), $.extend(a, extras), a
        }
      }(),
      Datepicker = function(element, options) {
        this.dates = new DateArray, this.viewDate = UTCToday(), this.focusDate = null, this._process_options(options), this.element = $(element), this.isInline = !1, this.isInput = this.element.is("input"), this.component = this.element.is(".date") ? this.element.find(".add-on, .input-group-addon, .btn") : !1, this.hasInput = this.component && this.element.find("input").length, this.component && 0 === this.component.length && (this.component = !1), this.picker = $(DPGlobal.template), this._buildEvents(), this._attachEvents(), this.isInline ? this.picker.addClass("datepicker-inline").appendTo(this.element) : this.picker.addClass("datepicker-dropdown dropdown-menu"), this.o.rtl && this.picker.addClass("datepicker-rtl"), this.viewMode = this.o.startView, this.o.calendarWeeks && this.picker.find("tfoot th.today").attr("colspan", function(i, val) {
          return parseInt(val) + 1
        }), this._allow_update = !1, this.setStartDate(this._o.startDate), this.setEndDate(this._o.endDate), this.setDaysOfWeekDisabled(this.o.daysOfWeekDisabled), this.fillDow(), this.fillMonths(), this._allow_update = !0, this.update(), this.showMode(), this.isInline && this.show()
      };
    Datepicker.prototype = {
      constructor: Datepicker,
      _process_options: function(opts) {
        this._o = $.extend({}, this._o, opts);
        var o = this.o = $.extend({}, this._o),
          lang = o.language;
        switch (dates[lang] || (lang = lang.split("-")[0], dates[lang] || (lang = defaults.language)), o.language = lang, o.startView) {
          case 2:
          case "decade":
            o.startView = 2;
            break;
          case 1:
          case "year":
            o.startView = 1;
            break;
          default:
            o.startView = 0
        }
        switch (o.minViewMode) {
          case 1:
          case "months":
            o.minViewMode = 1;
            break;
          case 2:
          case "years":
            o.minViewMode = 2;
            break;
          default:
            o.minViewMode = 0
        }
        o.startView = Math.max(o.startView, o.minViewMode), o.multidate !== !0 && (o.multidate = Number(o.multidate) || !1, o.multidate !== !1 ? o.multidate = Math.max(0, o.multidate) : o.multidate = 1), o.multidateSeparator = String(o.multidateSeparator), o.weekStart %= 7, o.weekEnd = (o.weekStart + 6) % 7;
        var format = DPGlobal.parseFormat(o.format);
        o.startDate !== -(1 / 0) && (o.startDate ? o.startDate instanceof Date ? o.startDate = this._local_to_utc(this._zero_time(o.startDate)) : o.startDate = DPGlobal.parseDate(o.startDate, format, o.language) : o.startDate = -(1 / 0)), o.endDate !== 1 / 0 && (o.endDate ? o.endDate instanceof Date ? o.endDate = this._local_to_utc(this._zero_time(o.endDate)) : o.endDate = DPGlobal.parseDate(o.endDate, format, o.language) : o.endDate = 1 / 0), o.daysOfWeekDisabled = o.daysOfWeekDisabled || [], $.isArray(o.daysOfWeekDisabled) || (o.daysOfWeekDisabled = o.daysOfWeekDisabled.split(/[,\s]*/)), o.daysOfWeekDisabled = $.map(o.daysOfWeekDisabled, function(d) {
          return parseInt(d, 10)
        });
        var plc = String(o.orientation).toLowerCase().split(/\s+/g),
          _plc = o.orientation.toLowerCase();
        if (plc = $.grep(plc, function(word) {
            return /^auto|left|right|top|bottom$/.test(word)
          }), o.orientation = {
            x: "auto",
            y: "auto"
          }, _plc && "auto" !== _plc)
          if (1 === plc.length) switch (plc[0]) {
            case "top":
            case "bottom":
              o.orientation.y = plc[0];
              break;
            case "left":
            case "right":
              o.orientation.x = plc[0]
          } else _plc = $.grep(plc, function(word) {
            return /^left|right$/.test(word)
          }), o.orientation.x = _plc[0] || "auto", _plc = $.grep(plc, function(word) {
            return /^top|bottom$/.test(word)
          }), o.orientation.y = _plc[0] || "auto";
          else;
      },
      _events: [],
      _secondaryEvents: [],
      _applyEvents: function(evs) {
        for (var el, ch, ev, i = 0; i < evs.length; i++) el = evs[i][0], 2 === evs[i].length ? (ch = undefined, ev = evs[i][1]) : 3 === evs[i].length && (ch = evs[i][1], ev = evs[i][2]), el.on(ev, ch)
      },
      _unapplyEvents: function(evs) {
        for (var el, ev, ch, i = 0; i < evs.length; i++) el = evs[i][0], 2 === evs[i].length ? (ch = undefined, ev = evs[i][1]) : 3 === evs[i].length && (ch = evs[i][1], ev = evs[i][2]), el.off(ev, ch)
      },
      _buildEvents: function() {
        this.isInput ? this._events = [
          [this.element, {
            focus: $.proxy(this.show, this),
            keyup: $.proxy(function(e) {
              -1 === $.inArray(e.keyCode, [27, 37, 39, 38, 40, 32, 13, 9]) && this.update()
            }, this),
            keydown: $.proxy(this.keydown, this)
          }]
        ] : this.component && this.hasInput ? this._events = [
          [this.element.find("input"), {
            focus: $.proxy(this.show, this),
            keyup: $.proxy(function(e) {
              -1 === $.inArray(e.keyCode, [27, 37, 39, 38, 40, 32, 13, 9]) && this.update()
            }, this),
            keydown: $.proxy(this.keydown, this)
          }],
          [this.component, {
            click: $.proxy(this.show, this)
          }]
        ] : this.element.is("div") ? this.isInline = !0 : this._events = [
          [this.element, {
            click: $.proxy(this.show, this)
          }]
        ], this._events.push([this.element, "*", {
          blur: $.proxy(function(e) {
            this._focused_from = e.target
          }, this)
        }], [this.element, {
          blur: $.proxy(function(e) {
            this._focused_from = e.target
          }, this)
        }]), this._secondaryEvents = [
          [this.picker, {
            click: $.proxy(this.click, this)
          }],
          [$(window), {
            resize: $.proxy(this.place, this)
          }],
          [$(document), {
            "mousedown touchstart": $.proxy(function(e) {
              this.element.is(e.target) || this.element.find(e.target).length || this.picker.is(e.target) || this.picker.find(e.target).length || this.hide()
            }, this)
          }]
        ]
      },
      _attachEvents: function() {
        this._detachEvents(), this._applyEvents(this._events)
      },
      _detachEvents: function() {
        this._unapplyEvents(this._events)
      },
      _attachSecondaryEvents: function() {
        this._detachSecondaryEvents(), this._applyEvents(this._secondaryEvents)
      },
      _detachSecondaryEvents: function() {
        this._unapplyEvents(this._secondaryEvents)
      },
      _trigger: function(event, altdate) {
        var date = altdate || this.dates.get(-1),
          local_date = this._utc_to_local(date);
        this.element.trigger({
          type: event,
          date: local_date,
          dates: $.map(this.dates, this._utc_to_local),
          format: $.proxy(function(ix, format) {
            0 === arguments.length ? (ix = this.dates.length - 1, format = this.o.format) : "string" == typeof ix && (format = ix, ix = this.dates.length - 1), format = format || this.o.format;
            var date = this.dates.get(ix);
            return DPGlobal.formatDate(date, format, this.o.language)
          }, this)
        })
      },
      show: function() {
        this.isInline || this.picker.appendTo("body"), this.picker.show(), this.place(), this._attachSecondaryEvents(), this._trigger("show")
      },
      hide: function() {
        this.isInline || this.picker.is(":visible") && (this.focusDate = null, this.picker.hide().detach(), this._detachSecondaryEvents(), this.viewMode = this.o.startView, this.showMode(), this.o.forceParse && (this.isInput && this.element.val() || this.hasInput && this.element.find("input").val()) && this.setValue(), this._trigger("hide"))
      },
      remove: function() {
        this.hide(), this._detachEvents(), this._detachSecondaryEvents(), this.picker.remove(), delete this.element.data().datepicker, this.isInput || delete this.element.data().date
      },
      _utc_to_local: function(utc) {
        return utc && new Date(utc.getTime() + 6e4 * utc.getTimezoneOffset())
      },
      _local_to_utc: function(local) {
        return local && new Date(local.getTime() - 6e4 * local.getTimezoneOffset())
      },
      _zero_time: function(local) {
        return local && new Date(local.getFullYear(), local.getMonth(), local.getDate())
      },
      _zero_utc_time: function(utc) {
        return utc && new Date(Date.UTC(utc.getUTCFullYear(), utc.getUTCMonth(), utc.getUTCDate()))
      },
      getDates: function() {
        return $.map(this.dates, this._utc_to_local)
      },
      getUTCDates: function() {
        return $.map(this.dates, function(d) {
          return new Date(d)
        })
      },
      getDate: function() {
        return this._utc_to_local(this.getUTCDate())
      },
      getUTCDate: function() {
        return new Date(this.dates.get(-1))
      },
      setDates: function() {
        var args = $.isArray(arguments[0]) ? arguments[0] : arguments;
        this.update.apply(this, args), this._trigger("changeDate"), this.setValue()
      },
      setUTCDates: function() {
        var args = $.isArray(arguments[0]) ? arguments[0] : arguments;
        this.update.apply(this, $.map(args, this._utc_to_local)), this._trigger("changeDate"), this.setValue()
      },
      setDate: alias("setDates"),
      setUTCDate: alias("setUTCDates"),
      setValue: function() {
        var formatted = this.getFormattedDate();
        this.isInput ? this.element.val(formatted).change() : this.component && this.element.find("input").val(formatted).change()
      },
      getFormattedDate: function(format) {
        format === undefined && (format = this.o.format);
        var lang = this.o.language;
        return $.map(this.dates, function(d) {
          return DPGlobal.formatDate(d, format, lang)
        }).join(this.o.multidateSeparator)
      },
      setStartDate: function(startDate) {
        this._process_options({
          startDate: startDate
        }), this.update(), this.updateNavArrows()
      },
      setEndDate: function(endDate) {
        this._process_options({
          endDate: endDate
        }), this.update(), this.updateNavArrows()
      },
      setDaysOfWeekDisabled: function(daysOfWeekDisabled) {
        this._process_options({
          daysOfWeekDisabled: daysOfWeekDisabled
        }), this.update(), this.updateNavArrows()
      },
      place: function() {
        if (!this.isInline) {
          var calendarWidth = this.picker.outerWidth(),
            calendarHeight = this.picker.outerHeight(),
            visualPadding = 10,
            windowWidth = $window.width(),
            windowHeight = $window.height(),
            scrollTop = $window.scrollTop(),
            zIndex = parseInt(this.element.parents().filter(function() {
              return "auto" !== $(this).css("z-index")
            }).first().css("z-index")) + 10,
            offset = this.component ? this.component.parent().offset() : this.element.offset(),
            height = this.component ? this.component.outerHeight(!0) : this.element.outerHeight(!1),
            width = this.component ? this.component.outerWidth(!0) : this.element.outerWidth(!1),
            left = offset.left,
            top = offset.top;
          this.picker.removeClass("datepicker-orient-top datepicker-orient-bottom datepicker-orient-right datepicker-orient-left"), "auto" !== this.o.orientation.x ? (this.picker.addClass("datepicker-orient-" + this.o.orientation.x), "right" === this.o.orientation.x && (left -= calendarWidth - width)) : (this.picker.addClass("datepicker-orient-left"), offset.left < 0 ? left -= offset.left - visualPadding : offset.left + calendarWidth > windowWidth && (left = windowWidth - calendarWidth - visualPadding));
          var top_overflow, bottom_overflow, yorient = this.o.orientation.y;
          "auto" === yorient && (top_overflow = -scrollTop + offset.top - calendarHeight, bottom_overflow = scrollTop + windowHeight - (offset.top + height + calendarHeight), yorient = Math.max(top_overflow, bottom_overflow) === bottom_overflow ? "top" : "bottom"), this.picker.addClass("datepicker-orient-" + yorient), "top" === yorient ? top += height : top -= calendarHeight + parseInt(this.picker.css("padding-top")), this.picker.css({
            top: top,
            left: left,
            zIndex: zIndex
          })
        }
      },
      _allow_update: !0,
      update: function() {
        if (this._allow_update) {
          var oldDates = this.dates.copy(),
            dates = [],
            fromArgs = !1;
          arguments.length ? ($.each(arguments, $.proxy(function(i, date) {
            date instanceof Date && (date = this._local_to_utc(date)), dates.push(date)
          }, this)), fromArgs = !0) : (dates = this.isInput ? this.element.val() : this.element.data("date") || this.element.find("input").val(), dates = dates && this.o.multidate ? dates.split(this.o.multidateSeparator) : [dates], delete this.element.data().date), dates = $.map(dates, $.proxy(function(date) {
            return DPGlobal.parseDate(date, this.o.format, this.o.language)
          }, this)), dates = $.grep(dates, $.proxy(function(date) {
            return date < this.o.startDate || date > this.o.endDate || !date
          }, this), !0), this.dates.replace(dates), this.dates.length ? this.viewDate = new Date(this.dates.get(-1)) : this.viewDate < this.o.startDate ? this.viewDate = new Date(this.o.startDate) : this.viewDate > this.o.endDate && (this.viewDate = new Date(this.o.endDate)), fromArgs ? this.setValue() : dates.length && String(oldDates) !== String(this.dates) && this._trigger("changeDate"), !this.dates.length && oldDates.length && this._trigger("clearDate"), this.fill()
        }
      },
      fillDow: function() {
        var dowCnt = this.o.weekStart,
          html = "<tr>";
        if (this.o.calendarWeeks) {
          var cell = '<th class="cw">&nbsp;</th>';
          html += cell, this.picker.find(".datepicker-days thead tr:first-child").prepend(cell)
        }
        for (; dowCnt < this.o.weekStart + 7;) html += '<th class="dow">' + dates[this.o.language].daysMin[dowCnt++ % 7] + "</th>";
        html += "</tr>", this.picker.find(".datepicker-days thead").append(html)
      },
      fillMonths: function() {
        for (var html = "", i = 0; 12 > i;) html += '<span class="month">' + dates[this.o.language].monthsShort[i++] + "</span>";
        this.picker.find(".datepicker-months td").html(html)
      },
      setRange: function(range) {
        range && range.length ? this.range = $.map(range, function(d) {
          return d.valueOf()
        }) : delete this.range, this.fill()
      },
      getClassNames: function(date) {
        var cls = [],
          year = this.viewDate.getUTCFullYear(),
          month = this.viewDate.getUTCMonth(),
          today = new Date;
        return date.getUTCFullYear() < year || date.getUTCFullYear() === year && date.getUTCMonth() < month ? cls.push("old") : (date.getUTCFullYear() > year || date.getUTCFullYear() === year && date.getUTCMonth() > month) && cls.push("new"), this.focusDate && date.valueOf() === this.focusDate.valueOf() && cls.push("focused"), this.o.todayHighlight && date.getUTCFullYear() === today.getFullYear() && date.getUTCMonth() === today.getMonth() && date.getUTCDate() === today.getDate() && cls.push("today"), -1 !== this.dates.contains(date) && cls.push("active"), (date.valueOf() < this.o.startDate || date.valueOf() > this.o.endDate || -1 !== $.inArray(date.getUTCDay(), this.o.daysOfWeekDisabled)) && cls.push("disabled"), this.range && (date > this.range[0] && date < this.range[this.range.length - 1] && cls.push("range"), -1 !== $.inArray(date.valueOf(), this.range) && cls.push("selected")), cls
      },
      fill: function() {
        var tooltip, d = new Date(this.viewDate),
          year = d.getUTCFullYear(),
          month = d.getUTCMonth(),
          startYear = this.o.startDate !== -(1 / 0) ? this.o.startDate.getUTCFullYear() : -(1 / 0),
          startMonth = this.o.startDate !== -(1 / 0) ? this.o.startDate.getUTCMonth() : -(1 / 0),
          endYear = this.o.endDate !== 1 / 0 ? this.o.endDate.getUTCFullYear() : 1 / 0,
          endMonth = this.o.endDate !== 1 / 0 ? this.o.endDate.getUTCMonth() : 1 / 0,
          todaytxt = dates[this.o.language].today || dates.en.today || "",
          cleartxt = dates[this.o.language].clear || dates.en.clear || "";
        this.picker.find(".datepicker-days thead th.datepicker-switch").text(dates[this.o.language].months[month] + " " + year), this.picker.find("tfoot th.today").text(todaytxt).toggle(this.o.todayBtn !== !1), this.picker.find("tfoot th.clear").text(cleartxt).toggle(this.o.clearBtn !== !1), this.updateNavArrows(), this.fillMonths();
        var prevMonth = UTCDate(year, month - 1, 28),
          day = DPGlobal.getDaysInMonth(prevMonth.getUTCFullYear(), prevMonth.getUTCMonth());
        prevMonth.setUTCDate(day), prevMonth.setUTCDate(day - (prevMonth.getUTCDay() - this.o.weekStart + 7) % 7);
        var nextMonth = new Date(prevMonth);
        nextMonth.setUTCDate(nextMonth.getUTCDate() + 42), nextMonth = nextMonth.valueOf();
        for (var clsName, html = []; prevMonth.valueOf() < nextMonth;) {
          if (prevMonth.getUTCDay() === this.o.weekStart && (html.push("<tr>"), this.o.calendarWeeks)) {
            var ws = new Date(+prevMonth + (this.o.weekStart - prevMonth.getUTCDay() - 7) % 7 * 864e5),
              th = new Date(Number(ws) + (11 - ws.getUTCDay()) % 7 * 864e5),
              yth = new Date(Number(yth = UTCDate(th.getUTCFullYear(), 0, 1)) + (11 - yth.getUTCDay()) % 7 * 864e5),
              calWeek = (th - yth) / 864e5 / 7 + 1;
            html.push('<td class="cw">' + calWeek + "</td>")
          }
          if (clsName = this.getClassNames(prevMonth), clsName.push("day"), this.o.beforeShowDay !== $.noop) {
            var before = this.o.beforeShowDay(this._utc_to_local(prevMonth));
            before === undefined ? before = {} : "boolean" == typeof before ? before = {
              enabled: before
            } : "string" == typeof before && (before = {
              classes: before
            }), before.enabled === !1 && clsName.push("disabled"), before.classes && (clsName = clsName.concat(before.classes.split(/\s+/))), before.tooltip && (tooltip = before.tooltip)
          }
          clsName = $.unique(clsName), html.push('<td class="' + clsName.join(" ") + '"' + (tooltip ? ' title="' + tooltip + '"' : "") + ">" + prevMonth.getUTCDate() + "</td>"), prevMonth.getUTCDay() === this.o.weekEnd && html.push("</tr>"), prevMonth.setUTCDate(prevMonth.getUTCDate() + 1)
        }
        this.picker.find(".datepicker-days tbody").empty().append(html.join(""));
        var months = this.picker.find(".datepicker-months").find("th:eq(1)").text(year).end().find("span").removeClass("active");
        $.each(this.dates, function(i, d) {
          d.getUTCFullYear() === year && months.eq(d.getUTCMonth()).addClass("active")
        }), (startYear > year || year > endYear) && months.addClass("disabled"), year === startYear && months.slice(0, startMonth).addClass("disabled"), year === endYear && months.slice(endMonth + 1).addClass("disabled"), html = "", year = 10 * parseInt(year / 10, 10);
        var yearCont = this.picker.find(".datepicker-years").find("th:eq(1)").text(year + "-" + (year + 9)).end().find("td");
        year -= 1;
        for (var classes, years = $.map(this.dates, function(d) {
            return d.getUTCFullYear()
          }), i = -1; 11 > i; i++) classes = ["year"], -1 === i ? classes.push("old") : 10 === i && classes.push("new"), -1 !== $.inArray(year, years) && classes.push("active"), (startYear > year || year > endYear) && classes.push("disabled"), html += '<span class="' + classes.join(" ") + '">' + year + "</span>", year += 1;
        yearCont.html(html)
      },
      updateNavArrows: function() {
        if (this._allow_update) {
          var d = new Date(this.viewDate),
            year = d.getUTCFullYear(),
            month = d.getUTCMonth();
          switch (this.viewMode) {
            case 0:
              this.o.startDate !== -(1 / 0) && year <= this.o.startDate.getUTCFullYear() && month <= this.o.startDate.getUTCMonth() ? this.picker.find(".prev").css({
                visibility: "hidden"
              }) : this.picker.find(".prev").css({
                visibility: "visible"
              }), this.o.endDate !== 1 / 0 && year >= this.o.endDate.getUTCFullYear() && month >= this.o.endDate.getUTCMonth() ? this.picker.find(".next").css({
                visibility: "hidden"
              }) : this.picker.find(".next").css({
                visibility: "visible"
              });
              break;
            case 1:
            case 2:
              this.o.startDate !== -(1 / 0) && year <= this.o.startDate.getUTCFullYear() ? this.picker.find(".prev").css({
                visibility: "hidden"
              }) : this.picker.find(".prev").css({
                visibility: "visible"
              }), this.o.endDate !== 1 / 0 && year >= this.o.endDate.getUTCFullYear() ? this.picker.find(".next").css({
                visibility: "hidden"
              }) : this.picker.find(".next").css({
                visibility: "visible"
              })
          }
        }
      },
      click: function(e) {
        e.preventDefault();
        var year, month, day, target = $(e.target).closest("span, td, th");
        if (1 === target.length) switch (target[0].nodeName.toLowerCase()) {
          case "th":
            switch (target[0].className) {
              case "datepicker-switch":
                this.showMode(1);
                break;
              case "prev":
              case "next":
                var dir = DPGlobal.modes[this.viewMode].navStep * ("prev" === target[0].className ? -1 : 1);
                switch (this.viewMode) {
                  case 0:
                    this.viewDate = this.moveMonth(this.viewDate, dir), this._trigger("changeMonth", this.viewDate);
                    break;
                  case 1:
                  case 2:
                    this.viewDate = this.moveYear(this.viewDate, dir), 1 === this.viewMode && this._trigger("changeYear", this.viewDate)
                }
                this.fill();
                break;
              case "today":
                var date = new Date;
                date = UTCDate(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0), this.showMode(-2);
                var which = "linked" === this.o.todayBtn ? null : "view";
                this._setDate(date, which);
                break;
              case "clear":
                var element;
                this.isInput ? element = this.element : this.component && (element = this.element.find("input")), element && element.val("").change(), this.update(), this._trigger("changeDate"), this.o.autoclose && this.hide()
            }
            break;
          case "span":
            target.is(".disabled") || (this.viewDate.setUTCDate(1), target.is(".month") ? (day = 1, month = target.parent().find("span").index(target), year = this.viewDate.getUTCFullYear(), this.viewDate.setUTCMonth(month), this._trigger("changeMonth", this.viewDate), 1 === this.o.minViewMode && this._setDate(UTCDate(year, month, day))) : (day = 1, month = 0, year = parseInt(target.text(), 10) || 0, this.viewDate.setUTCFullYear(year), this._trigger("changeYear", this.viewDate), 2 === this.o.minViewMode && this._setDate(UTCDate(year, month, day))), this.showMode(-1), this.fill());
            break;
          case "td":
            target.is(".day") && !target.is(".disabled") && (day = parseInt(target.text(), 10) || 1, year = this.viewDate.getUTCFullYear(), month = this.viewDate.getUTCMonth(), target.is(".old") ? 0 === month ? (month = 11, year -= 1) : month -= 1 : target.is(".new") && (11 === month ? (month = 0, year += 1) : month += 1), this._setDate(UTCDate(year, month, day)))
        }
        this.picker.is(":visible") && this._focused_from && $(this._focused_from).focus(), delete this._focused_from
      },
      _toggle_multidate: function(date) {
        var ix = this.dates.contains(date);
        if (date ? -1 !== ix ? this.dates.remove(ix) : this.dates.push(date) : this.dates.clear(), "number" == typeof this.o.multidate)
          for (; this.dates.length > this.o.multidate;) this.dates.remove(0)
      },
      _setDate: function(date, which) {
        which && "date" !== which || this._toggle_multidate(date && new Date(date)), which && "view" !== which || (this.viewDate = date && new Date(date)), this.fill(), this.setValue(), this._trigger("changeDate");
        var element;
        this.isInput ? element = this.element : this.component && (element = this.element.find("input")), element && element.change(), !this.o.autoclose || which && "date" !== which || this.hide()
      },
      moveMonth: function(date, dir) {
        if (!date) return undefined;
        if (!dir) return date;
        var new_month, test, new_date = new Date(date.valueOf()),
          day = new_date.getUTCDate(),
          month = new_date.getUTCMonth(),
          mag = Math.abs(dir);
        if (dir = dir > 0 ? 1 : -1, 1 === mag) test = -1 === dir ? function() {
          return new_date.getUTCMonth() === month
        } : function() {
          return new_date.getUTCMonth() !== new_month
        }, new_month = month + dir, new_date.setUTCMonth(new_month), (0 > new_month || new_month > 11) && (new_month = (new_month + 12) % 12);
        else {
          for (var i = 0; mag > i; i++) new_date = this.moveMonth(new_date, dir);
          new_month = new_date.getUTCMonth(), new_date.setUTCDate(day), test = function() {
            return new_month !== new_date.getUTCMonth()
          }
        }
        for (; test();) new_date.setUTCDate(--day), new_date.setUTCMonth(new_month);
        return new_date
      },
      moveYear: function(date, dir) {
        return this.moveMonth(date, 12 * dir)
      },
      dateWithinRange: function(date) {
        return date >= this.o.startDate && date <= this.o.endDate
      },
      keydown: function(e) {
        if (this.picker.is(":not(:visible)")) return void(27 === e.keyCode && this.show());
        var dir, newDate, newViewDate, dateChanged = !1,
          focusDate = this.focusDate || this.viewDate;
        switch (e.keyCode) {
          case 27:
            this.focusDate ? (this.focusDate = null, this.viewDate = this.dates.get(-1) || this.viewDate, this.fill()) : this.hide(), e.preventDefault();
            break;
          case 37:
          case 39:
            if (!this.o.keyboardNavigation) break;
            dir = 37 === e.keyCode ? -1 : 1, e.ctrlKey ? (newDate = this.moveYear(this.dates.get(-1) || UTCToday(), dir), newViewDate = this.moveYear(focusDate, dir), this._trigger("changeYear", this.viewDate)) : e.shiftKey ? (newDate = this.moveMonth(this.dates.get(-1) || UTCToday(), dir), newViewDate = this.moveMonth(focusDate, dir), this._trigger("changeMonth", this.viewDate)) : (newDate = new Date(this.dates.get(-1) || UTCToday()), newDate.setUTCDate(newDate.getUTCDate() + dir), newViewDate = new Date(focusDate), newViewDate.setUTCDate(focusDate.getUTCDate() + dir)), this.dateWithinRange(newDate) && (this.focusDate = this.viewDate = newViewDate, this.setValue(), this.fill(), e.preventDefault());
            break;
          case 38:
          case 40:
            if (!this.o.keyboardNavigation) break;
            dir = 38 === e.keyCode ? -1 : 1, e.ctrlKey ? (newDate = this.moveYear(this.dates.get(-1) || UTCToday(), dir), newViewDate = this.moveYear(focusDate, dir), this._trigger("changeYear", this.viewDate)) : e.shiftKey ? (newDate = this.moveMonth(this.dates.get(-1) || UTCToday(), dir), newViewDate = this.moveMonth(focusDate, dir), this._trigger("changeMonth", this.viewDate)) : (newDate = new Date(this.dates.get(-1) || UTCToday()), newDate.setUTCDate(newDate.getUTCDate() + 7 * dir), newViewDate = new Date(focusDate), newViewDate.setUTCDate(focusDate.getUTCDate() + 7 * dir)), this.dateWithinRange(newDate) && (this.focusDate = this.viewDate = newViewDate, this.setValue(), this.fill(), e.preventDefault());
            break;
          case 32:
            break;
          case 13:
            focusDate = this.focusDate || this.dates.get(-1) || this.viewDate, this._toggle_multidate(focusDate), dateChanged = !0, this.focusDate = null, this.viewDate = this.dates.get(-1) || this.viewDate, this.setValue(), this.fill(), this.picker.is(":visible") && (e.preventDefault(), this.o.autoclose && this.hide());
            break;
          case 9:
            this.focusDate = null, this.viewDate = this.dates.get(-1) || this.viewDate, this.fill(), this.hide()
        }
        if (dateChanged) {
          this.dates.length ? this._trigger("changeDate") : this._trigger("clearDate");
          var element;
          this.isInput ? element = this.element : this.component && (element = this.element.find("input")), element && element.change()
        }
      },
      showMode: function(dir) {
        dir && (this.viewMode = Math.max(this.o.minViewMode, Math.min(2, this.viewMode + dir))), this.picker.find(">div").hide().filter(".datepicker-" + DPGlobal.modes[this.viewMode].clsName).css("display", "block"), this.updateNavArrows()
      }
    };
    var DateRangePicker = function(element, options) {
      this.element = $(element), this.inputs = $.map(options.inputs, function(i) {
        return i.jquery ? i[0] : i
      }), delete options.inputs, $(this.inputs).datepicker(options).bind("changeDate", $.proxy(this.dateUpdated, this)), this.pickers = $.map(this.inputs, function(i) {
        return $(i).data("datepicker")
      }), this.updateDates()
    };
    DateRangePicker.prototype = {
      updateDates: function() {
        this.dates = $.map(this.pickers, function(i) {
          return i.getUTCDate()
        }), this.updateRanges()
      },
      updateRanges: function() {
        var range = $.map(this.dates, function(d) {
          return d.valueOf()
        });
        $.each(this.pickers, function(i, p) {
          p.setRange(range)
        })
      },
      dateUpdated: function(e) {
        if (!this.updating) {
          this.updating = !0;
          var dp = $(e.target).data("datepicker"),
            new_date = dp.getUTCDate(),
            i = $.inArray(e.target, this.inputs),
            l = this.inputs.length;
          if (-1 !== i) {
            if ($.each(this.pickers, function(i, p) {
                p.getUTCDate() || p.setUTCDate(new_date)
              }), new_date < this.dates[i])
              for (; i >= 0 && new_date < this.dates[i];) this.pickers[i--].setUTCDate(new_date);
            else if (new_date > this.dates[i])
              for (; l > i && new_date > this.dates[i];) this.pickers[i++].setUTCDate(new_date);
            this.updateDates(), delete this.updating
          }
        }
      },
      remove: function() {
        $.map(this.pickers, function(p) {
          p.remove()
        }), delete this.element.data().datepicker
      }
    };
    var old = $.fn.datepicker;
    $.fn.datepicker = function(option) {
      var args = Array.apply(null, arguments);
      args.shift();
      var internal_return;
      return this.each(function() {
        var $this = $(this),
          data = $this.data("datepicker"),
          options = "object" == typeof option && option;
        if (!data) {
          var elopts = opts_from_el(this, "date"),
            xopts = $.extend({}, defaults, elopts, options),
            locopts = opts_from_locale(xopts.language),
            opts = $.extend({}, defaults, locopts, elopts, options);
          if ($this.is(".input-daterange") || opts.inputs) {
            var ropts = {
              inputs: opts.inputs || $this.find("input").toArray()
            };
            $this.data("datepicker", data = new DateRangePicker(this, $.extend(opts, ropts)))
          } else $this.data("datepicker", data = new Datepicker(this, opts))
        }
        return "string" == typeof option && "function" == typeof data[option] && (internal_return = data[option].apply(data, args), internal_return !== undefined) ? !1 : void 0
      }), internal_return !== undefined ? internal_return : this
    };
    var defaults = $.fn.datepicker.defaults = {
        autoclose: !1,
        beforeShowDay: $.noop,
        calendarWeeks: !1,
        clearBtn: !1,
        daysOfWeekDisabled: [],
        endDate: 1 / 0,
        forceParse: !0,
        format: "mm/dd/yyyy",
        keyboardNavigation: !0,
        language: "en",
        minViewMode: 0,
        multidate: !1,
        multidateSeparator: ",",
        orientation: "auto",
        rtl: !1,
        startDate: -(1 / 0),
        startView: 0,
        todayBtn: !1,
        todayHighlight: !1,
        weekStart: 0
      },
      locale_opts = $.fn.datepicker.locale_opts = ["format", "rtl", "weekStart"];
    $.fn.datepicker.Constructor = Datepicker;
    var dates = $.fn.datepicker.dates = {
        en: {
          days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
          daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
          daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"],
          months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
          monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
          today: "Today",
          clear: "Clear"
        }
      },
      DPGlobal = {
        modes: [{
          clsName: "days",
          navFnc: "Month",
          navStep: 1
        }, {
          clsName: "months",
          navFnc: "FullYear",
          navStep: 1
        }, {
          clsName: "years",
          navFnc: "FullYear",
          navStep: 10
        }],
        isLeapYear: function(year) {
          return year % 4 === 0 && year % 100 !== 0 || year % 400 === 0
        },
        getDaysInMonth: function(year, month) {
          return [31, DPGlobal.isLeapYear(year) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month]
        },
        validParts: /dd?|DD?|mm?|MM?|yy(?:yy)?/g,
        nonpunctuation: /[^ -\/:-@\[\u3400-\u9fff-`{-~\t\n\r]+/g,
        parseFormat: function(format) {
          var separators = format.replace(this.validParts, "\x00").split("\x00"),
            parts = format.match(this.validParts);
          if (!separators || !separators.length || !parts || 0 === parts.length) throw new Error("Invalid date format.");
          return {
            separators: separators,
            parts: parts
          }
        },
        parseDate: function(date, format, language) {
          function match_part() {
            var m = this.slice(0, parts[i].length),
              p = parts[i].slice(0, m.length);
            return m === p
          }
          if (!date) return undefined;
          if (date instanceof Date) return date;
          "string" == typeof format && (format = DPGlobal.parseFormat(format));
          var part, dir, i, part_re = /([\-+]\d+)([dmwy])/,
            parts = date.match(/([\-+]\d+)([dmwy])/g);
          if (/^[\-+]\d+[dmwy]([\s,]+[\-+]\d+[dmwy])*$/.test(date)) {
            for (date = new Date, i = 0; i < parts.length; i++) switch (part = part_re.exec(parts[i]), dir = parseInt(part[1]), part[2]) {
              case "d":
                date.setUTCDate(date.getUTCDate() + dir);
                break;
              case "m":
                date = Datepicker.prototype.moveMonth.call(Datepicker.prototype, date, dir);
                break;
              case "w":
                date.setUTCDate(date.getUTCDate() + 7 * dir);
                break;
              case "y":
                date = Datepicker.prototype.moveYear.call(Datepicker.prototype, date, dir)
            }
            return UTCDate(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), 0, 0, 0)
          }
          parts = date && date.match(this.nonpunctuation) || [], date = new Date;
          var val, filtered, parsed = {},
            setters_order = ["yyyy", "yy", "M", "MM", "m", "mm", "d", "dd"],
            setters_map = {
              yyyy: function(d, v) {
                return d.setUTCFullYear(v)
              },
              yy: function(d, v) {
                return d.setUTCFullYear(2e3 + v)
              },
              m: function(d, v) {
                if (isNaN(d)) return d;
                for (v -= 1; 0 > v;) v += 12;
                for (v %= 12, d.setUTCMonth(v); d.getUTCMonth() !== v;) d.setUTCDate(d.getUTCDate() - 1);
                return d
              },
              d: function(d, v) {
                return d.setUTCDate(v)
              }
            };
          setters_map.M = setters_map.MM = setters_map.mm = setters_map.m, setters_map.dd = setters_map.d, date = UTCDate(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0);
          var fparts = format.parts.slice();
          if (parts.length !== fparts.length && (fparts = $(fparts).filter(function(i, p) {
              return -1 !== $.inArray(p, setters_order)
            }).toArray()), parts.length === fparts.length) {
            var cnt;
            for (i = 0, cnt = fparts.length; cnt > i; i++) {
              if (val = parseInt(parts[i], 10), part = fparts[i], isNaN(val)) switch (part) {
                case "MM":
                  filtered = $(dates[language].months).filter(match_part), val = $.inArray(filtered[0], dates[language].months) + 1;
                  break;
                case "M":
                  filtered = $(dates[language].monthsShort).filter(match_part), val = $.inArray(filtered[0], dates[language].monthsShort) + 1
              }
              parsed[part] = val
            }
            var _date, s;
            for (i = 0; i < setters_order.length; i++) s = setters_order[i], s in parsed && !isNaN(parsed[s]) && (_date = new Date(date), setters_map[s](_date, parsed[s]), isNaN(_date) || (date = _date))
          }
          return date
        },
        formatDate: function(date, format, language) {
          if (!date) return "";
          "string" == typeof format && (format = DPGlobal.parseFormat(format));
          var val = {
            d: date.getUTCDate(),
            D: dates[language].daysShort[date.getUTCDay()],
            DD: dates[language].days[date.getUTCDay()],
            m: date.getUTCMonth() + 1,
            M: dates[language].monthsShort[date.getUTCMonth()],
            MM: dates[language].months[date.getUTCMonth()],
            yy: date.getUTCFullYear().toString().substring(2),
            yyyy: date.getUTCFullYear()
          };
          val.dd = (val.d < 10 ? "0" : "") + val.d, val.mm = (val.m < 10 ? "0" : "") + val.m, date = [];
          for (var seps = $.extend([], format.separators), i = 0, cnt = format.parts.length; cnt >= i; i++) seps.length && date.push(seps.shift()), date.push(val[format.parts[i]]);
          return date.join("")
        },
        headTemplate: '<thead><tr><th class="prev">&laquo;</th><th colspan="5" class="datepicker-switch"></th><th class="next">&raquo;</th></tr></thead>',
        contTemplate: '<tbody><tr><td colspan="7"></td></tr></tbody>',
        footTemplate: '<tfoot><tr><th colspan="7" class="today"></th></tr><tr><th colspan="7" class="clear"></th></tr></tfoot>'
      };
    DPGlobal.template = '<div class="datepicker"><div class="datepicker-days"><table class=" table-condensed">' + DPGlobal.headTemplate + "<tbody></tbody>" + DPGlobal.footTemplate + '</table></div><div class="datepicker-months"><table class="table-condensed">' + DPGlobal.headTemplate + DPGlobal.contTemplate + DPGlobal.footTemplate + '</table></div><div class="datepicker-years"><table class="table-condensed">' + DPGlobal.headTemplate + DPGlobal.contTemplate + DPGlobal.footTemplate + "</table></div></div>", $.fn.datepicker.DPGlobal = DPGlobal, $.fn.datepicker.noConflict = function() {
      return $.fn.datepicker = old, this
    }, $(document).on("focus.datepicker.data-api click.datepicker.data-api", '[data-provide="datepicker"]', function(e) {
      var $this = $(this);
      $this.data("datepicker") || (e.preventDefault(), $this.datepicker("show"))
    }), $(function() {
      $('[data-provide="datepicker-inline"]').datepicker()
    })
  }(window.jQuery), function(factory) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], factory) : window.jQuery && !window.jQuery.fn.colorpicker && factory(window.jQuery)
  }(function($) {
    "use strict";
    var Color = function(val) {
      this.value = {
        h: 0,
        s: 0,
        b: 0,
        a: 1
      }, this.origFormat = null, val && (void 0 !== val.toLowerCase ? (val += "", this.setColor(val)) : void 0 !== val.h && (this.value = val))
    };
    Color.prototype = {
      constructor: Color,
      colors: {
        aliceblue: "#f0f8ff",
        antiquewhite: "#faebd7",
        aqua: "#00ffff",
        aquamarine: "#7fffd4",
        azure: "#f0ffff",
        beige: "#f5f5dc",
        bisque: "#ffe4c4",
        black: "#000000",
        blanchedalmond: "#ffebcd",
        blue: "#0000ff",
        blueviolet: "#8a2be2",
        brown: "#a52a2a",
        burlywood: "#deb887",
        cadetblue: "#5f9ea0",
        chartreuse: "#7fff00",
        chocolate: "#d2691e",
        coral: "#ff7f50",
        cornflowerblue: "#6495ed",
        cornsilk: "#fff8dc",
        crimson: "#dc143c",
        cyan: "#00ffff",
        darkblue: "#00008b",
        darkcyan: "#008b8b",
        darkgoldenrod: "#b8860b",
        darkgray: "#a9a9a9",
        darkgreen: "#006400",
        darkkhaki: "#bdb76b",
        darkmagenta: "#8b008b",
        darkolivegreen: "#556b2f",
        darkorange: "#ff8c00",
        darkorchid: "#9932cc",
        darkred: "#8b0000",
        darksalmon: "#e9967a",
        darkseagreen: "#8fbc8f",
        darkslateblue: "#483d8b",
        darkslategray: "#2f4f4f",
        darkturquoise: "#00ced1",
        darkviolet: "#9400d3",
        deeppink: "#ff1493",
        deepskyblue: "#00bfff",
        dimgray: "#696969",
        dodgerblue: "#1e90ff",
        firebrick: "#b22222",
        floralwhite: "#fffaf0",
        forestgreen: "#228b22",
        fuchsia: "#ff00ff",
        gainsboro: "#dcdcdc",
        ghostwhite: "#f8f8ff",
        gold: "#ffd700",
        goldenrod: "#daa520",
        gray: "#808080",
        green: "#008000",
        greenyellow: "#adff2f",
        honeydew: "#f0fff0",
        hotpink: "#ff69b4",
        "indianred ": "#cd5c5c",
        "indigo ": "#4b0082",
        ivory: "#fffff0",
        khaki: "#f0e68c",
        lavender: "#e6e6fa",
        lavenderblush: "#fff0f5",
        lawngreen: "#7cfc00",
        lemonchiffon: "#fffacd",
        lightblue: "#add8e6",
        lightcoral: "#f08080",
        lightcyan: "#e0ffff",
        lightgoldenrodyellow: "#fafad2",
        lightgrey: "#d3d3d3",
        lightgreen: "#90ee90",
        lightpink: "#ffb6c1",
        lightsalmon: "#ffa07a",
        lightseagreen: "#20b2aa",
        lightskyblue: "#87cefa",
        lightslategray: "#778899",
        lightsteelblue: "#b0c4de",
        lightyellow: "#ffffe0",
        lime: "#00ff00",
        limegreen: "#32cd32",
        linen: "#faf0e6",
        magenta: "#ff00ff",
        maroon: "#800000",
        mediumaquamarine: "#66cdaa",
        mediumblue: "#0000cd",
        mediumorchid: "#ba55d3",
        mediumpurple: "#9370d8",
        mediumseagreen: "#3cb371",
        mediumslateblue: "#7b68ee",
        mediumspringgreen: "#00fa9a",
        mediumturquoise: "#48d1cc",
        mediumvioletred: "#c71585",
        midnightblue: "#191970",
        mintcream: "#f5fffa",
        mistyrose: "#ffe4e1",
        moccasin: "#ffe4b5",
        navajowhite: "#ffdead",
        navy: "#000080",
        oldlace: "#fdf5e6",
        olive: "#808000",
        olivedrab: "#6b8e23",
        orange: "#ffa500",
        orangered: "#ff4500",
        orchid: "#da70d6",
        palegoldenrod: "#eee8aa",
        palegreen: "#98fb98",
        paleturquoise: "#afeeee",
        palevioletred: "#d87093",
        papayawhip: "#ffefd5",
        peachpuff: "#ffdab9",
        peru: "#cd853f",
        pink: "#ffc0cb",
        plum: "#dda0dd",
        powderblue: "#b0e0e6",
        purple: "#800080",
        red: "#ff0000",
        rosybrown: "#bc8f8f",
        royalblue: "#4169e1",
        saddlebrown: "#8b4513",
        salmon: "#fa8072",
        sandybrown: "#f4a460",
        seagreen: "#2e8b57",
        seashell: "#fff5ee",
        sienna: "#a0522d",
        silver: "#c0c0c0",
        skyblue: "#87ceeb",
        slateblue: "#6a5acd",
        slategray: "#708090",
        snow: "#fffafa",
        springgreen: "#00ff7f",
        steelblue: "#4682b4",
        tan: "#d2b48c",
        teal: "#008080",
        thistle: "#d8bfd8",
        tomato: "#ff6347",
        turquoise: "#40e0d0",
        violet: "#ee82ee",
        wheat: "#f5deb3",
        white: "#ffffff",
        whitesmoke: "#f5f5f5",
        yellow: "#ffff00",
        yellowgreen: "#9acd32",
        transparent: "transparent"
      },
      _sanitizeNumber: function(val) {
        return "number" == typeof val ? val : isNaN(val) || null === val || "" === val || void 0 === val ? 1 : void 0 !== val.toLowerCase ? parseFloat(val) : 1
      },
      isTransparent: function(strVal) {
        return strVal ? (strVal = strVal.toLowerCase().trim(), "transparent" == strVal || strVal.match(/#?00000000/) || strVal.match(/(rgba|hsla)\(0,0,0,0?\.?0\)/)) : !1
      },
      rgbaIsTransparent: function(rgba) {
        return 0 == rgba.r && 0 == rgba.g && 0 == rgba.b && 0 == rgba.a
      },
      setColor: function(strVal) {
        strVal = strVal.toLowerCase().trim(), strVal && (this.isTransparent(strVal) ? this.value = {
          h: 0,
          s: 0,
          b: 0,
          a: 0
        } : this.value = this.stringToHSB(strVal) || {
          h: 0,
          s: 0,
          b: 0,
          a: 1
        })
      },
      stringToHSB: function(strVal) {
        strVal = strVal.toLowerCase();
        var that = this,
          result = !1;
        return $.each(this.stringParsers, function(i, parser) {
          var match = parser.re.exec(strVal),
            values = match && parser.parse.apply(that, [match]),
            format = parser.format || "rgba";
          return values ? (result = format.match(/hsla?/) ? that.RGBtoHSB.apply(that, that.HSLtoRGB.apply(that, values)) : that.RGBtoHSB.apply(that, values), that.origFormat = format, !1) : !0
        }), result
      },
      setHue: function(h) {
        this.value.h = 1 - h
      },
      setSaturation: function(s) {
        this.value.s = s
      },
      setBrightness: function(b) {
        this.value.b = 1 - b
      },
      setAlpha: function(a) {
        this.value.a = parseInt(100 * (1 - a), 10) / 100
      },
      toRGB: function(h, s, b, a) {
        h || (h = this.value.h, s = this.value.s, b = this.value.b), h *= 360;
        var R, G, B, X, C;
        return h = h % 360 / 60, C = b * s, X = C * (1 - Math.abs(h % 2 - 1)), R = G = B = b - C, h = ~~h, R += [C, X, 0, 0, X, C][h], G += [X, C, C, X, 0, 0][h], B += [0, 0, X, C, C, X][h], {
          r: Math.round(255 * R),
          g: Math.round(255 * G),
          b: Math.round(255 * B),
          a: a || this.value.a
        }
      },
      toHex: function(h, s, b, a) {
        var rgb = this.toRGB(h, s, b, a);
        return this.rgbaIsTransparent(rgb) ? "transparent" : "#" + (1 << 24 | parseInt(rgb.r) << 16 | parseInt(rgb.g) << 8 | parseInt(rgb.b)).toString(16).substr(1)
      },
      toHSL: function(h, s, b, a) {
        h = h || this.value.h, s = s || this.value.s, b = b || this.value.b, a = a || this.value.a;
        var H = h,
          L = (2 - s) * b,
          S = s * b;
        return S /= L > 0 && 1 >= L ? L : 2 - L, L /= 2, S > 1 && (S = 1), {
          h: isNaN(H) ? 0 : H,
          s: isNaN(S) ? 0 : S,
          l: isNaN(L) ? 0 : L,
          a: isNaN(a) ? 0 : a
        }
      },
      toAlias: function(r, g, b, a) {
        var rgb = this.toHex(r, g, b, a);
        for (var alias in this.colors)
          if (this.colors[alias] == rgb) return alias;
        return !1
      },
      RGBtoHSB: function(r, g, b, a) {
        r /= 255, g /= 255, b /= 255;
        var H, S, V, C;
        return V = Math.max(r, g, b), C = V - Math.min(r, g, b), H = 0 === C ? null : V === r ? (g - b) / C : V === g ? (b - r) / C + 2 : (r - g) / C + 4, H = (H + 360) % 6 * 60 / 360, S = 0 === C ? 0 : C / V, {
          h: this._sanitizeNumber(H),
          s: S,
          b: V,
          a: this._sanitizeNumber(a)
        }
      },
      HueToRGB: function(p, q, h) {
        return 0 > h ? h += 1 : h > 1 && (h -= 1), 1 > 6 * h ? p + (q - p) * h * 6 : 1 > 2 * h ? q : 2 > 3 * h ? p + (q - p) * (2 / 3 - h) * 6 : p
      },
      HSLtoRGB: function(h, s, l, a) {
        0 > s && (s = 0);
        var q;
        q = .5 >= l ? l * (1 + s) : l + s - l * s;
        var p = 2 * l - q,
          tr = h + 1 / 3,
          tg = h,
          tb = h - 1 / 3,
          r = Math.round(255 * this.HueToRGB(p, q, tr)),
          g = Math.round(255 * this.HueToRGB(p, q, tg)),
          b = Math.round(255 * this.HueToRGB(p, q, tb));
        return [r, g, b, this._sanitizeNumber(a)]
      },
      toString: function(format) {
        switch (format = format || "rgba") {
          case "rgb":
            var rgb = this.toRGB();
            return this.rgbaIsTransparent(rgb) ? "transparent" : "rgb(" + rgb.r + "," + rgb.g + "," + rgb.b + ")";
          case "rgba":
            var rgb = this.toRGB();
            return "rgba(" + rgb.r + "," + rgb.g + "," + rgb.b + "," + rgb.a + ")";
          case "hsl":
            var hsl = this.toHSL();
            return "hsl(" + Math.round(360 * hsl.h) + "," + Math.round(100 * hsl.s) + "%," + Math.round(100 * hsl.l) + "%)";
          case "hsla":
            var hsl = this.toHSL();
            return "hsla(" + Math.round(360 * hsl.h) + "," + Math.round(100 * hsl.s) + "%," + Math.round(100 * hsl.l) + "%," + hsl.a + ")";
          case "hex":
            return this.toHex();
          case "alias":
            return this.toAlias() || this.toHex();
          default:
            return !1
        }
      },
      stringParsers: [{
        re: /rgb\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*?\)/,
        format: "rgb",
        parse: function(execResult) {
          return [execResult[1], execResult[2], execResult[3], 1]
        }
      }, {
        re: /rgb\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*?\)/,
        format: "rgb",
        parse: function(execResult) {
          return [2.55 * execResult[1], 2.55 * execResult[2], 2.55 * execResult[3], 1]
        }
      }, {
        re: /rgba\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/,
        format: "rgba",
        parse: function(execResult) {
          return [execResult[1], execResult[2], execResult[3], execResult[4]]
        }
      }, {
        re: /rgba\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/,
        format: "rgba",
        parse: function(execResult) {
          return [2.55 * execResult[1], 2.55 * execResult[2], 2.55 * execResult[3], execResult[4]]
        }
      }, {
        re: /hsl\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*?\)/,
        format: "hsl",
        parse: function(execResult) {
          return [execResult[1] / 360, execResult[2] / 100, execResult[3] / 100, execResult[4]]
        }
      }, {
        re: /hsla\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/,
        format: "hsla",
        parse: function(execResult) {
          return [execResult[1] / 360, execResult[2] / 100, execResult[3] / 100, execResult[4]]
        }
      }, {
        re: /#?([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/,
        format: "hex",
        parse: function(execResult) {
          return [parseInt(execResult[1], 16), parseInt(execResult[2], 16), parseInt(execResult[3], 16), 1]
        }
      }, {
        re: /#?([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])/,
        format: "hex",
        parse: function(execResult) {
          return [parseInt(execResult[1] + execResult[1], 16), parseInt(execResult[2] + execResult[2], 16), parseInt(execResult[3] + execResult[3], 16), 1]
        }
      }, {
        re: /^([a-z]{3,})$/,
        format: "alias",
        parse: function(execResult) {
          var hexval = this.colorNameToHex(execResult[0]) || "#000000",
            match = this.stringParsers[6].re.exec(hexval),
            values = match && this.stringParsers[6].parse.apply(this, [match]);
          return values
        }
      }],
      colorNameToHex: function(name) {
        return "undefined" != typeof this.colors[name.toLowerCase()] ? this.colors[name.toLowerCase()] : !1
      }
    };
    var defaults = {
        horizontal: !1,
        inline: !1,
        color: !1,
        format: !1,
        input: "input",
        container: !1,
        component: ".add-on, .input-group-addon",
        sliders: {
          saturation: {
            maxLeft: 100,
            maxTop: 100,
            callLeft: "setSaturation",
            callTop: "setBrightness"
          },
          hue: {
            maxLeft: 0,
            maxTop: 100,
            callLeft: !1,
            callTop: "setHue"
          },
          alpha: {
            maxLeft: 0,
            maxTop: 100,
            callLeft: !1,
            callTop: "setAlpha"
          }
        },
        slidersHorz: {
          saturation: {
            maxLeft: 100,
            maxTop: 100,
            callLeft: "setSaturation",
            callTop: "setBrightness"
          },
          hue: {
            maxLeft: 100,
            maxTop: 0,
            callLeft: "setHue",
            callTop: !1
          },
          alpha: {
            maxLeft: 100,
            maxTop: 0,
            callLeft: "setAlpha",
            callTop: !1
          }
        },
        template: '<div class="colorpicker dropdown-menu"><div class="colorpicker-saturation"><i><b></b></i></div><div class="colorpicker-hue"><i></i></div><div class="colorpicker-alpha"><i></i></div><div class="colorpicker-color"><div /></div></div>'
      },
      Colorpicker = function(element, options) {
        this.element = $(element).addClass("colorpicker-element"), this.options = $.extend({}, defaults, this.element.data(), options), this.component = this.options.component, this.component = this.component !== !1 ? this.element.find(this.component) : !1, this.component && 0 === this.component.length && (this.component = !1), this.container = this.options.container === !0 ? this.element : this.options.container, this.container = this.container !== !1 ? $(this.container) : !1, this.input = this.element.is("input") ? this.element : this.options.input ? this.element.find(this.options.input) : !1, this.input && 0 === this.input.length && (this.input = !1), this.color = new Color(this.options.color !== !1 ? this.options.color : this.getValue()), this.format = this.options.format !== !1 ? this.options.format : this.color.origFormat, this.picker = $(this.options.template), this.options.inline ? this.picker.addClass("colorpicker-inline colorpicker-visible") : this.picker.addClass("colorpicker-hidden"), this.options.horizontal && this.picker.addClass("colorpicker-horizontal"), ("rgba" === this.format || "hsla" === this.format) && this.picker.addClass("colorpicker-with-alpha"), "right" === this.options.align && this.picker.addClass("colorpicker-right"), this.picker.on("mousedown.colorpicker touchstart.colorpicker", $.proxy(this.mousedown, this)), this.picker.appendTo(this.container ? this.container : $("body")), this.input !== !1 && (this.input.on({
          "keyup.colorpicker": $.proxy(this.keyup, this)
        }), this.component === !1 && this.element.on({
          "focus.colorpicker": $.proxy(this.show, this)
        }), this.options.inline === !1 && this.element.on({
          "focusout.colorpicker": $.proxy(this.hide, this)
        })), this.component !== !1 && this.component.on({
          "click.colorpicker": $.proxy(this.show, this)
        }), this.input === !1 && this.component === !1 && this.element.on({
          "click.colorpicker": $.proxy(this.show, this)
        }), this.input !== !1 && this.component !== !1 && "color" === this.input.attr("type") && this.input.on({
          "click.colorpicker": $.proxy(this.show, this),
          "focus.colorpicker": $.proxy(this.show, this)
        }), this.update(), $($.proxy(function() {
          this.element.trigger("create")
        }, this))
      };
    Colorpicker.Color = Color, Colorpicker.prototype = {
      constructor: Colorpicker,
      destroy: function() {
        this.picker.remove(), this.element.removeData("colorpicker").off(".colorpicker"), this.input !== !1 && this.input.off(".colorpicker"), this.component !== !1 && this.component.off(".colorpicker"), this.element.removeClass("colorpicker-element"), this.element.trigger({
          type: "destroy"
        })
      },
      reposition: function() {
        if (this.options.inline !== !1 || this.options.container) return !1;
        var type = this.container && this.container[0] !== document.body ? "position" : "offset",
          element = this.component || this.element,
          offset = element[type]();
        "right" === this.options.align && (offset.left -= this.picker.outerWidth() - element.outerWidth()), this.picker.css({
          top: offset.top + element.outerHeight(),
          left: offset.left
        })
      },
      show: function(e) {
        return this.isDisabled() ? !1 : (this.picker.addClass("colorpicker-visible").removeClass("colorpicker-hidden"), this.reposition(), $(window).on("resize.colorpicker", $.proxy(this.reposition, this)), !e || this.hasInput() && "color" !== this.input.attr("type") || e.stopPropagation && e.preventDefault && (e.stopPropagation(), e.preventDefault()), this.options.inline === !1 && $(window.document).on({
          "mousedown.colorpicker": $.proxy(this.hide, this)
        }), void this.element.trigger({
          type: "showPicker",
          color: this.color
        }))
      },
      hide: function() {
        this.picker.addClass("colorpicker-hidden").removeClass("colorpicker-visible"), $(window).off("resize.colorpicker", this.reposition), $(document).off({
          "mousedown.colorpicker": this.hide
        }), this.update(), this.element.trigger({
          type: "hidePicker",
          color: this.color
        })
      },
      updateData: function(val) {
        return val = val || this.color.toString(this.format), this.element.data("color", val), val
      },
      updateInput: function(val) {
        return val = val || this.color.toString(this.format), this.input !== !1 && this.input.prop("value", val), val
      },
      updatePicker: function(val) {
        void 0 !== val && (this.color = new Color(val));
        var sl = this.options.horizontal === !1 ? this.options.sliders : this.options.slidersHorz,
          icns = this.picker.find("i");
        return 0 !== icns.length ? (this.options.horizontal === !1 ? (sl = this.options.sliders, icns.eq(1).css("top", sl.hue.maxTop * (1 - this.color.value.h)).end().eq(2).css("top", sl.alpha.maxTop * (1 - this.color.value.a))) : (sl = this.options.slidersHorz, icns.eq(1).css("left", sl.hue.maxLeft * (1 - this.color.value.h)).end().eq(2).css("left", sl.alpha.maxLeft * (1 - this.color.value.a))), icns.eq(0).css({
          top: sl.saturation.maxTop - this.color.value.b * sl.saturation.maxTop,
          left: this.color.value.s * sl.saturation.maxLeft
        }), this.picker.find(".colorpicker-saturation").css("backgroundColor", this.color.toHex(this.color.value.h, 1, 1, 1)), this.picker.find(".colorpicker-alpha").css("backgroundColor", this.color.toHex()), this.picker.find(".colorpicker-color, .colorpicker-color div").css("backgroundColor", this.color.toString(this.format)), val) : void 0
      },
      updateComponent: function(val) {
        if (val = val || this.color.toString(this.format), this.component !== !1) {
          var icn = this.component.find("i").eq(0);
          icn.length > 0 ? icn.css({
            backgroundColor: val
          }) : this.component.css({
            backgroundColor: val
          })
        }
        return val
      },
      update: function(force) {
        var val;
        return (this.getValue(!1) !== !1 || force === !0) && (val = this.updateComponent(), this.updateInput(val), this.updateData(val), this.updatePicker()), val
      },
      setValue: function(val) {
        this.color = new Color(val), this.update(), this.element.trigger({
          type: "changeColor",
          color: this.color,
          value: val
        })
      },
      getValue: function(defaultValue) {
        defaultValue = void 0 === defaultValue ? "#000000" : defaultValue;
        var val;
        return val = this.hasInput() ? this.input.val() : this.element.data("color"), (void 0 === val || "" === val || null === val) && (val = defaultValue), val
      },
      hasInput: function() {
        return this.input !== !1
      },
      isDisabled: function() {
        return this.hasInput() ? this.input.prop("disabled") === !0 : !1
      },
      disable: function() {
        return this.hasInput() ? (this.input.prop("disabled", !0), this.element.trigger({
          type: "disable",
          color: this.color,
          value: this.getValue()
        }), !0) : !1
      },
      enable: function() {
        return this.hasInput() ? (this.input.prop("disabled", !1), this.element.trigger({
          type: "enable",
          color: this.color,
          value: this.getValue()
        }), !0) : !1
      },
      currentSlider: null,
      mousePointer: {
        left: 0,
        top: 0
      },
      mousedown: function(e) {
        e.pageX || e.pageY || !e.originalEvent || (e.pageX = e.originalEvent.touches[0].pageX, e.pageY = e.originalEvent.touches[0].pageY), e.stopPropagation(), e.preventDefault();
        var target = $(e.target),
          zone = target.closest("div"),
          sl = this.options.horizontal ? this.options.slidersHorz : this.options.sliders;
        if (!zone.is(".colorpicker")) {
          if (zone.is(".colorpicker-saturation")) this.currentSlider = $.extend({}, sl.saturation);
          else if (zone.is(".colorpicker-hue")) this.currentSlider = $.extend({}, sl.hue);
          else {
            if (!zone.is(".colorpicker-alpha")) return !1;
            this.currentSlider = $.extend({}, sl.alpha)
          }
          var offset = zone.offset();
          this.currentSlider.guide = zone.find("i")[0].style, this.currentSlider.left = e.pageX - offset.left, this.currentSlider.top = e.pageY - offset.top, this.mousePointer = {
            left: e.pageX,
            top: e.pageY
          }, $(document).on({
            "mousemove.colorpicker": $.proxy(this.mousemove, this),
            "touchmove.colorpicker": $.proxy(this.mousemove, this),
            "mouseup.colorpicker": $.proxy(this.mouseup, this),
            "touchend.colorpicker": $.proxy(this.mouseup, this)
          }).trigger("mousemove")
        }
        return !1
      },
      mousemove: function(e) {
        e.pageX || e.pageY || !e.originalEvent || (e.pageX = e.originalEvent.touches[0].pageX, e.pageY = e.originalEvent.touches[0].pageY), e.stopPropagation(), e.preventDefault();
        var left = Math.max(0, Math.min(this.currentSlider.maxLeft, this.currentSlider.left + ((e.pageX || this.mousePointer.left) - this.mousePointer.left))),
          top = Math.max(0, Math.min(this.currentSlider.maxTop, this.currentSlider.top + ((e.pageY || this.mousePointer.top) - this.mousePointer.top)));
        return this.currentSlider.guide.left = left + "px", this.currentSlider.guide.top = top + "px", this.currentSlider.callLeft && this.color[this.currentSlider.callLeft].call(this.color, left / this.currentSlider.maxLeft), this.currentSlider.callTop && this.color[this.currentSlider.callTop].call(this.color, top / this.currentSlider.maxTop), this.update(!0), this.element.trigger({
          type: "changeColor",
          color: this.color
        }), !1
      },
      mouseup: function(e) {
        return e.stopPropagation(), e.preventDefault(), $(document).off({
          "mousemove.colorpicker": this.mousemove,
          "touchmove.colorpicker": this.mousemove,
          "mouseup.colorpicker": this.mouseup,
          "touchend.colorpicker": this.mouseup
        }), !1
      },
      keyup: function(e) {
        if (38 === e.keyCode) this.color.value.a < 1 && (this.color.value.a = Math.round(100 * (this.color.value.a + .01)) / 100), this.update(!0);
        else if (40 === e.keyCode) this.color.value.a > 0 && (this.color.value.a = Math.round(100 * (this.color.value.a - .01)) / 100), this.update(!0);
        else {
          var val = this.input.val();
          this.color = new Color(val), this.getValue(!1) !== !1 && (this.updateData(), this.updateComponent(), this.updatePicker())
        }
        this.element.trigger({
          type: "changeColor",
          color: this.color,
          value: val
        })
      }
    }, $.colorpicker = Colorpicker, $.fn.colorpicker = function(option) {
      var rv, pickerArgs = arguments,
        $returnValue = this.each(function() {
          var $this = $(this),
            inst = $this.data("colorpicker"),
            options = "object" == typeof option ? option : {};
          inst || "string" == typeof option ? "string" == typeof option && (rv = inst[option].apply(inst, Array.prototype.slice.call(pickerArgs, 1))) : $this.data("colorpicker", new Colorpicker(this, options))
        });
      return "getValue" === option ? rv : $returnValue
    }, $.fn.colorpicker.constructor = Colorpicker
  }), function($) {
    "use strict";
    var Filestyle = function(element, options) {
      this.options = options, this.$elementFilestyle = [], this.$element = $(element)
    };
    Filestyle.prototype = {
      clear: function() {
        this.$element.val(""), this.$elementFilestyle.find(":text").val(""), this.$elementFilestyle.find(".badge").remove()
      },
      destroy: function() {
        this.$element.removeAttr("style").removeData("filestyle").val(""), this.$elementFilestyle.remove()
      },
      disabled: function(value) {
        if (value === !0) this.options.disabled || (this.$element.attr("disabled", "true"), this.$elementFilestyle.find("label").attr("disabled", "true"), this.options.disabled = !0);
        else {
          if (value !== !1) return this.options.disabled;
          this.options.disabled && (this.$element.removeAttr("disabled"), this.$elementFilestyle.find("label").removeAttr("disabled"), this.options.disabled = !1)
        }
      },
      buttonBefore: function(value) {
        if (value === !0) this.options.buttonBefore || (this.options.buttonBefore = !0, this.options.input && (this.$elementFilestyle.remove(), this.constructor(), this.pushNameFiles()));
        else {
          if (value !== !1) return this.options.buttonBefore;
          this.options.buttonBefore && (this.options.buttonBefore = !1, this.options.input && (this.$elementFilestyle.remove(), this.constructor(), this.pushNameFiles()))
        }
      },
      icon: function(value) {
        if (value === !0) this.options.icon || (this.options.icon = !0, this.$elementFilestyle.find("label").prepend(this.htmlIcon()));
        else {
          if (value !== !1) return this.options.icon;
          this.options.icon && (this.options.icon = !1, this.$elementFilestyle.find(".glyphicon").remove())
        }
      },
      input: function(value) {
        if (value === !0) this.options.input || (this.options.input = !0, this.options.buttonBefore ? this.$elementFilestyle.append(this.htmlInput()) : this.$elementFilestyle.prepend(this.htmlInput()), this.$elementFilestyle.find(".badge").remove(), this.pushNameFiles(), this.$elementFilestyle.find(".group-span-filestyle").addClass("input-group-btn"));
        else {
          if (value !== !1) return this.options.input;
          if (this.options.input) {
            this.options.input = !1, this.$elementFilestyle.find(":text").remove();
            var files = this.pushNameFiles();
            files.length > 0 && this.options.badge && this.$elementFilestyle.find("label").append(' <span class="badge">' + files.length + "</span>"), this.$elementFilestyle.find(".group-span-filestyle").removeClass("input-group-btn")
          }
        }
      },
      size: function(value) {
        if (void 0 === value) return this.options.size;
        var btn = this.$elementFilestyle.find("label"),
          input = this.$elementFilestyle.find("input");
        btn.removeClass("btn-lg btn-sm"), input.removeClass("input-lg input-sm"), "nr" != value && (btn.addClass("btn-" + value), input.addClass("input-" + value))
      },
      buttonText: function(value) {
        return void 0 === value ? this.options.buttonText : (this.options.buttonText = value, void this.$elementFilestyle.find("label span").html(this.options.buttonText))
      },
      buttonName: function(value) {
        return void 0 === value ? this.options.buttonName : (this.options.buttonName = value, void this.$elementFilestyle.find("label").attr({
          "class": "btn " + this.options.buttonName
        }))
      },
      iconName: function(value) {
        return void 0 === value ? this.options.iconName : void this.$elementFilestyle.find(".glyphicon").attr({
          "class": this.options.iconName
        })
      },
      htmlIcon: function() {
        return this.options.icon ? '<span class="' + this.options.iconName + '"></span> ' : ""
      },
      htmlInput: function() {
        return this.options.input ? '<input type="text" class="form-control ' + ("nr" == this.options.size ? "" : "input-" + this.options.size) + '" disabled> ' : ""
      },
      pushNameFiles: function() {
        var content = "",
          files = [];
        void 0 === this.$element[0].files ? files[0] = {
          name: this.$element[0] && this.$element[0].value
        } : files = this.$element[0].files;
        for (var i = 0; i < files.length; i++) content += files[i].name.split("\\").pop() + ", ";
        return "" !== content ? this.$elementFilestyle.find(":text").val(content.replace(/\, $/g, "")) : this.$elementFilestyle.find(":text").val(""), files
      },
      constructor: function() {
        var _self = this,
          html = "",
          id = _self.$element.attr("id"),
          btn = "";
        "" !== id && id || (id = "filestyle-" + $(".bootstrap-filestyle").length, _self.$element.attr({
          id: id
        })), btn = '<span class="group-span-filestyle ' + (_self.options.input ? "input-group-btn" : "") + '"><label for="' + id + '" class="btn ' + _self.options.buttonName + " " + ("nr" == _self.options.size ? "" : "btn-" + _self.options.size) + '" ' + (_self.options.disabled ? 'disabled="true"' : "") + ">" + _self.htmlIcon() + _self.options.buttonText + "</label></span>", html = _self.options.buttonBefore ? btn + _self.htmlInput() : _self.htmlInput() + btn, _self.$elementFilestyle = $('<div class="bootstrap-filestyle input-group">' + html + "</div>"), _self.$elementFilestyle.find(".group-span-filestyle").attr("tabindex", "0").keypress(function(e) {
          return 13 === e.keyCode || 32 === e.charCode ? (_self.$elementFilestyle.find("label").click(), !1) : void 0
        }), _self.$element.css({
          position: "absolute",
          clip: "rect(0px 0px 0px 0px)"
        }).attr("tabindex", "-1").after(_self.$elementFilestyle), _self.options.disabled && _self.$element.attr("disabled", "true"), _self.$element.change(function() {
          var files = _self.pushNameFiles();
          0 == _self.options.input && _self.options.badge ? 0 == _self.$elementFilestyle.find(".badge").length ? _self.$elementFilestyle.find("label").append(' <span class="badge">' + files.length + "</span>") : 0 == files.length ? _self.$elementFilestyle.find(".badge").remove() : _self.$elementFilestyle.find(".badge").html(files.length) : _self.$elementFilestyle.find(".badge").remove()
        })
      }
    };
    var old = $.fn.filestyle;
    $.fn.filestyle = function(option, value) {
      var get = "",
        element = this.each(function() {
          if ("file" === $(this).attr("type")) {
            var $this = $(this),
              data = $this.data("filestyle"),
              options = $.extend({}, $.fn.filestyle.defaults, option, "object" == typeof option && option);
            data || ($this.data("filestyle", data = new Filestyle(this, options)), data.constructor()), "string" == typeof option && (get = data[option](value))
          }
        });
      return void 0 !== typeof get ? get : element
    }, $.fn.filestyle.defaults = {
      buttonText: "Choose file",
      iconName: "glyphicon-folder-open",
      buttonName: "btn-default",
      size: "nr",
      input: !0,
      badge: !0,
      icon: !0,
      buttonBefore: !1,
      disabled: !1
    }, $.fn.filestyle.noConflict = function() {
      return $.fn.filestyle = old, this
    }, $(function() {
      $(".filestyle").each(function() {
        var $this = $(this),
          options = {
            input: "false" === $this.attr("data-input") ? !1 : !0,
            icon: "false" === $this.attr("data-icon") ? !1 : !0,
            buttonBefore: "true" === $this.attr("data-buttonBefore") ? !0 : !1,
            disabled: "true" === $this.attr("data-disabled") ? !0 : !1,
            size: $this.attr("data-size"),
            buttonText: $this.attr("data-buttonText"),
            buttonName: $this.attr("data-buttonName"),
            iconName: $this.attr("data-iconName"),
            badge: "false" === $this.attr("data-badge") ? !1 : !0
          };
        $this.filestyle(options)
      })
    })
  }(window.jQuery), function(window, document, $) {
    function args(elem) {
      var newAttrs = {},
        rinlinejQuery = /^jQuery\d+$/;
      return $.each(elem.attributes, function(i, attr) {
        attr.specified && !rinlinejQuery.test(attr.name) && (newAttrs[attr.name] = attr.value)
      }), newAttrs
    }

    function clearPlaceholder(event, value) {
      var input = this,
        $input = $(input);
      if (input.value == $input.attr("placeholder") && $input.hasClass("placeholder"))
        if ($input.data("placeholder-password")) {
          if ($input = $input.hide().next().show().attr("id", $input.removeAttr("id").data("placeholder-id")), event === !0) return $input[0].value = value;
          $input.focus()
        } else input.value = "", $input.removeClass("placeholder"), input == safeActiveElement() && input.select()
    }

    function setPlaceholder() {
      var $replacement, input = this,
        $input = $(input),
        id = this.id;
      if ("" == input.value) {
        if ("password" == input.type) {
          if (!$input.data("placeholder-textinput")) {
            try {
              $replacement = $input.clone().attr({
                type: "text"
              })
            } catch (e) {
              $replacement = $("<input>").attr($.extend(args(this), {
                type: "text"
              }))
            }
            $replacement.removeAttr("name").data({
              "placeholder-password": $input,
              "placeholder-id": id
            }).bind("focus.placeholder", clearPlaceholder), $input.data({
              "placeholder-textinput": $replacement,
              "placeholder-id": id
            }).before($replacement)
          }
          $input = $input.removeAttr("id").hide().prev().attr("id", id).show()
        }
        $input.addClass("placeholder"), $input[0].value = $input.attr("placeholder")
      } else $input.removeClass("placeholder")
    }

    function safeActiveElement() {
      try {
        return document.activeElement
      } catch (exception) {}
    }
    var hooks, placeholder, isOperaMini = "[object OperaMini]" == Object.prototype.toString.call(window.operamini),
      isInputSupported = "placeholder" in document.createElement("input") && !isOperaMini,
      isTextareaSupported = "placeholder" in document.createElement("textarea") && !isOperaMini,
      prototype = $.fn,
      valHooks = $.valHooks,
      propHooks = $.propHooks;
    isInputSupported && isTextareaSupported ? (placeholder = prototype.placeholder = function() {
      return this
    }, placeholder.input = placeholder.textarea = !0) : (placeholder = prototype.placeholder = function() {
      var $this = this;
      return $this.filter((isInputSupported ? "textarea" : ":input") + "[placeholder]").not(".placeholder").bind({
        "focus.placeholder": clearPlaceholder,
        "blur.placeholder": setPlaceholder
      }).data("placeholder-enabled", !0).trigger("blur.placeholder"), $this
    }, placeholder.input = isInputSupported, placeholder.textarea = isTextareaSupported, hooks = {
      get: function(element) {
        var $element = $(element),
          $passwordInput = $element.data("placeholder-password");
        return $passwordInput ? $passwordInput[0].value : $element.data("placeholder-enabled") && $element.hasClass("placeholder") ? "" : element.value
      },
      set: function(element, value) {
        var $element = $(element),
          $passwordInput = $element.data("placeholder-password");
        return $passwordInput ? $passwordInput[0].value = value : $element.data("placeholder-enabled") ? ("" == value ? (element.value = value, element != safeActiveElement() && setPlaceholder.call(element)) : $element.hasClass("placeholder") ? clearPlaceholder.call(element, !0, value) || (element.value = value) : element.value = value, $element) : element.value = value
      }
    }, isInputSupported || (valHooks.input = hooks, propHooks.value = hooks), isTextareaSupported || (valHooks.textarea = hooks, propHooks.value = hooks), $(function() {
      $(document).delegate("form", "submit.placeholder", function() {
        var $inputs = $(".placeholder", this).each(clearPlaceholder);
        setTimeout(function() {
          $inputs.each(setPlaceholder)
        }, 10)
      })
    }), $(window).bind("beforeunload.placeholder", function() {
      $(".placeholder").each(function() {
        this.value = ""
      })
    }))
  }(this, document, jQuery), function(window, undefined) {
    "$:nomunge";
    var jq_throttle, $ = window.jQuery || window.Cowboy || (window.Cowboy = {});
    $.throttle = jq_throttle = function(delay, no_trailing, callback, debounce_mode) {
      function wrapper() {
        function exec() {
          last_exec = +new Date, callback.apply(that, args)
        }

        function clear() {
          timeout_id = undefined
        }
        var that = this,
          elapsed = +new Date - last_exec,
          args = arguments;
        debounce_mode && !timeout_id && exec(), timeout_id && clearTimeout(timeout_id), debounce_mode === undefined && elapsed > delay ? exec() : no_trailing !== !0 && (timeout_id = setTimeout(debounce_mode ? clear : exec, debounce_mode === undefined ? delay - elapsed : delay))
      }
      var timeout_id, last_exec = 0;
      return "boolean" != typeof no_trailing && (debounce_mode = callback, callback = no_trailing, no_trailing = undefined), $.guid, wrapper
    }, $.debounce = function(delay, at_begin, callback) {
      return callback === undefined ? jq_throttle(delay, at_begin, !1) : jq_throttle(delay, callback, at_begin !== !1)
    }
  }(this), function() {
    ! function($) {
      var Selectorator, clean, contains, escapeSelector, extend, inArray, map, unique;
      return map = $.map, extend = $.extend, inArray = $.inArray, contains = function(item, array) {
        return -1 !== inArray(item, array)
      }, escapeSelector = function(selector) {
        return selector.replace(/([\!\"\#\$\%\&'\(\)\*\+\,\.\/\:\;<\=>\?\@\[\\\]\^\`\{\|\}\~])/g, "\\$1")
      }, clean = function(arr, reject) {
        return map(arr, function(item) {
          return item === reject ? null : item
        })
      }, unique = function(arr) {
        return map(arr, function(item, index) {
          return parseInt(index, 10) === parseInt(arr.indexOf(item), 10) ? item : null
        })
      }, Selectorator = function() {
        function Selectorator(element, options) {
          this.element = element, this.options = extend(extend({}, $.selectorator.options), options), this.cachedResults = {}
        }
        return Selectorator.prototype.query = function(selector) {
          var _base;
          return (_base = this.cachedResults)[selector] || (_base[selector] = $(selector.replace(/#([^\s]+)/g, "[id='$1']")))
        }, Selectorator.prototype.getProperTagName = function() {
          return this.element[0] ? this.element[0].tagName.toLowerCase() : null
        }, Selectorator.prototype.hasParent = function() {
          return this.element && 0 < this.element.parent().size()
        }, Selectorator.prototype.isElement = function() {
          var node;
          return node = this.element[0], node && node.nodeType === node.ELEMENT_NODE
        }, Selectorator.prototype.validate = function(selector, parentSelector, single, isFirst) {
          var delimiter, element;
          if (null == single && (single = !0), null == isFirst && (isFirst = !1), element = this.query(selector), single && 1 < element.size() || !single && 0 === element.size()) {
            if (!parentSelector || -1 !== selector.indexOf(":")) return null;
            if (delimiter = isFirst ? " > " : " ", selector = parentSelector + delimiter + selector, element = this.query(selector), single && 1 < element.size() || !single && 0 === element.size()) return null
          }
          return contains(this.element[0], element.get()) ? selector : null
        }, Selectorator.prototype.generate = function() {
          var fn, res, _i, _len, _ref;
          if (!(this.element && this.hasParent() && this.isElement())) return [""];
          for (res = [], _ref = [this.generateSimple, this.generateAncestor, this.generateRecursive], _i = 0, _len = _ref.length; _len > _i; _i++)
            if (fn = _ref[_i], res = unique(clean(fn.call(this))), res && res.length > 0) return res;
          return unique(res)
        }, Selectorator.prototype.generateAncestor = function() {
          var isFirst, parent, parentSelector, parentSelectors, results, selector, selectors, _i, _j, _k, _len, _len1, _len2, _ref;
          for (results = [], _ref = this.element.parents(), _i = 0, _len = _ref.length; _len > _i; _i++) {
            for (parent = _ref[_i], isFirst = !0, selectors = this.generateSimple(null, !1), _j = 0, _len1 = selectors.length; _len1 > _j; _j++)
              for (selector = selectors[_j], parentSelectors = new Selectorator($(parent), this.options).generateSimple(null, !1), _k = 0, _len2 = parentSelectors.length; _len2 > _k; _k++) parentSelector = parentSelectors[_k], $.merge(results, this.generateSimple(parentSelector, !0, isFirst));
            isFirst = !1
          }
          return results
        }, Selectorator.prototype.generateSimple = function(parentSelector, single, isFirst) {
          var fn, res, self, tagName, validate, _i, _len, _ref;
          for (self = this, tagName = self.getProperTagName(), validate = function(selector) {
              return self.validate(selector, parentSelector, single, isFirst)
            }, _ref = [
              [self.getIdSelector],
              [self.getClassSelector],
              [self.getIdSelector, !0],
              [self.getClassSelector, !0],
              [self.getNameSelector],
              [function() {
                return [self.getProperTagName()]
              }]
            ], _i = 0, _len = _ref.length; _len > _i; _i++)
            if (fn = _ref[_i], res = fn[0].call(self, fn[1]) || [], res = clean(map(res, validate)), res.length > 0) return res;
          return []
        }, Selectorator.prototype.generateRecursive = function() {
          var index, parent, parentSelector, selector;
          return selector = this.getProperTagName(), -1 !== selector.indexOf(":") && (selector = "*"), parent = this.element.parent(), parentSelector = new Selectorator(parent).generate()[0], index = parent.children(selector).index(this.element), selector = "" + selector + ":eq(" + index + ")", "" !== parentSelector && (selector = parentSelector + " > " + selector), [selector]
        }, Selectorator.prototype.getIdSelector = function(tagName) {
          var id;
          return null == tagName && (tagName = !1), tagName = tagName ? this.getProperTagName() : "", id = this.element.attr("id"), "string" != typeof id || contains(id, this.getIgnore("id")) ? null : ["" + tagName + "#" + escapeSelector(id)]
        }, Selectorator.prototype.getClassSelector = function(tagName) {
          var classes, invalidClasses, tn;
          return null == tagName && (tagName = !1), tn = this.getProperTagName(), /^(body|html)$/.test(tn) ? null : (tagName = tagName ? tn : "",
            invalidClasses = this.getIgnore("class"), classes = (this.element.attr("class") || "").replace(/\{.*\}/, "").split(/\s/), map(classes, function(klazz) {
              return klazz && !contains(klazz, invalidClasses) ? "" + tagName + "." + escapeSelector(klazz) : null
            }))
        }, Selectorator.prototype.getNameSelector = function() {
          var name, tagName;
          return tagName = this.getProperTagName(), name = this.element.attr("name"), name && !contains(name, this.getIgnore("name")) ? ["" + tagName + "[name='" + name + "']"] : null
        }, Selectorator.prototype.getIgnore = function(key) {
          var mulkey, opts, vals;
          return opts = this.options.ignore || {}, mulkey = "class" === key ? "classes" : "" + key + "s", vals = opts[key] || opts[mulkey], "string" == typeof vals ? [vals] : vals
        }, Selectorator
      }(), $.selectorator = {
        options: {},
        unique: unique,
        clean: clean,
        escapeSelector: escapeSelector
      }, $.fn.selectorator = function(options) {
        return new Selectorator($(this), options)
      }, $.fn.getSelector = function(options) {
        return this.selectorator(options).generate()
      }, this
    }(jQuery)
  }.call(this), function(factory) {
    "function" == typeof define && define.amd ? define(["jquery"], factory) : factory("object" == typeof exports ? require("jquery") : jQuery)
  }(function($) {
    function encode(s) {
      return config.raw ? s : encodeURIComponent(s)
    }

    function decode(s) {
      return config.raw ? s : decodeURIComponent(s)
    }

    function stringifyCookieValue(value) {
      return encode(config.json ? JSON.stringify(value) : String(value))
    }

    function parseCookieValue(s) {
      0 === s.indexOf('"') && (s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, "\\"));
      try {
        return s = decodeURIComponent(s.replace(pluses, " ")), config.json ? JSON.parse(s) : s
      } catch (e) {}
    }

    function read(s, converter) {
      var value = config.raw ? s : parseCookieValue(s);
      return $.isFunction(converter) ? converter(value) : value
    }
    var pluses = /\+/g,
      config = $.cookie = function(key, value, options) {
        if (void 0 !== value && !$.isFunction(value)) {
          if (options = $.extend({}, config.defaults, options), "number" == typeof options.expires) {
            var days = options.expires,
              t = options.expires = new Date;
            t.setTime(+t + 864e5 * days)
          }
          return document.cookie = [encode(key), "=", stringifyCookieValue(value), options.expires ? "; expires=" + options.expires.toUTCString() : "", options.path ? "; path=" + options.path : "", options.domain ? "; domain=" + options.domain : "", options.secure ? "; secure" : ""].join("")
        }
        for (var result = key ? void 0 : {}, cookies = document.cookie ? document.cookie.split("; ") : [], i = 0, l = cookies.length; l > i; i++) {
          var parts = cookies[i].split("="),
            name = decode(parts.shift()),
            cookie = parts.join("=");
          if (key && key === name) {
            result = read(cookie, value);
            break
          }
          key || void 0 === (cookie = read(cookie)) || (result[name] = cookie)
        }
        return result
      };
    config.defaults = {}, $.removeCookie = function(key, options) {
      return void 0 === $.cookie(key) ? !1 : ($.cookie(key, "", $.extend({}, options, {
        expires: -1
      })), !$.cookie(key))
    }
  }), function($) {
    function isDOMAttrModifiedSupported() {
      var p = document.createElement("p"),
        flag = !1;
      if (p.addEventListener) p.addEventListener("DOMAttrModified", function() {
        flag = !0
      }, !1);
      else {
        if (!p.attachEvent) return !1;
        p.attachEvent("onDOMAttrModified", function() {
          flag = !0
        })
      }
      return p.setAttribute("id", "target"), flag
    }

    function checkAttributes(chkAttr, e) {
      if (chkAttr) {
        var attributes = this.data("attr-old-value");
        if (e.attributeName.indexOf("style") >= 0) {
          attributes.style || (attributes.style = {});
          var keys = e.attributeName.split(".");
          e.attributeName = keys[0], e.oldValue = attributes.style[keys[1]], e.newValue = keys[1] + ":" + this.prop("style")[$.camelCase(keys[1])], attributes.style[keys[1]] = e.newValue
        } else e.oldValue = attributes[e.attributeName], e.newValue = this.attr(e.attributeName), attributes[e.attributeName] = e.newValue;
        this.data("attr-old-value", attributes)
      }
    }
    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
    $.fn.attrchange = function(a, b) {
      return "object" == typeof a ? attrchangeFx._core.call(this, a) : "string" == typeof a ? attrchangeFx._ext.call(this, a, b) : void 0
    };
    var attrchangeFx = {
      _core: function(o) {
        var cfg = {
          trackValues: !1,
          callback: $.noop
        };
        if ("function" == typeof o ? cfg.callback = o : $.extend(cfg, o), cfg.trackValues && this.each(function(i, el) {
            for (var attr, attributes = {}, i = 0, attrs = el.attributes, l = attrs.length; l > i; i++) attr = attrs.item(i), attributes[attr.nodeName] = attr.value;
            $(this).data("attr-old-value", attributes)
          }), MutationObserver) {
          var mOptions = {
              subtree: !1,
              attributes: !0,
              attributeOldValue: cfg.trackValues
            },
            observer = new MutationObserver(function(mutations) {
              mutations.forEach(function(e) {
                var _this = e.target;
                cfg.trackValues && (e.newValue = $(_this).attr(e.attributeName)), cfg.callback.call(_this, e)
              })
            });
          return this.data("attrchange-method", "Mutation Observer").data("attrchange-obs", observer).each(function() {
            observer.observe(this, mOptions)
          })
        }
        return isDOMAttrModifiedSupported() ? this.data("attrchange-method", "DOMAttrModified").on("DOMAttrModified", function(event) {
          event.originalEvent && (event = event.originalEvent), event.attributeName = event.attrName, event.oldValue = event.prevValue, cfg.callback.call(this, event)
        }) : "onpropertychange" in document.body ? this.data("attrchange-method", "propertychange").on("propertychange", function(e) {
          e.attributeName = window.event.propertyName, checkAttributes.call($(this), cfg.trackValues, e), cfg.callback.call(this, e)
        }) : this
      },
      _ext: function(s, o) {
        switch (s) {
          case "disconnect":
            return this.each(function() {
              var attrchangeMethod = $(this).data("attrchange-method");
              "propertychange" == attrchangeMethod || "DOMAttrModified" == attrchangeMethod ? $(this).off(attrchangeMethod) : "Mutation Observer" == attrchangeMethod && $(this).data("attrchange-obs").disconnect()
            }).removeData("attrchange-method")
        }
      }
    }
  }(jQuery), function(a, b) {
    "use strict";
    var c = "undefined" != typeof Element && "ALLOW_KEYBOARD_INPUT" in Element,
      d = function() {
        for (var a, c, d = [
            ["requestFullscreen", "exitFullscreen", "fullscreenElement", "fullscreenEnabled", "fullscreenchange", "fullscreenerror"],
            ["webkitRequestFullscreen", "webkitExitFullscreen", "webkitFullscreenElement", "webkitFullscreenEnabled", "webkitfullscreenchange", "webkitfullscreenerror"],
            ["webkitRequestFullScreen", "webkitCancelFullScreen", "webkitCurrentFullScreenElement", "webkitCancelFullScreen", "webkitfullscreenchange", "webkitfullscreenerror"],
            ["mozRequestFullScreen", "mozCancelFullScreen", "mozFullScreenElement", "mozFullScreenEnabled", "mozfullscreenchange", "mozfullscreenerror"]
          ], e = 0, f = d.length, g = {}; f > e; e++)
          if (a = d[e], a && a[1] in b) {
            for (e = 0, c = a.length; c > e; e++) g[d[0][e]] = a[e];
            return g
          } return !1
      }(),
      e = {
        request: function(a) {
          var e = d.requestFullscreen;
          a = a || b.documentElement, /5\.1[\.\d]* Safari/.test(navigator.userAgent) ? a[e]() : a[e](c && Element.ALLOW_KEYBOARD_INPUT)
        },
        exit: function() {
          b[d.exitFullscreen]()
        },
        toggle: function(a) {
          this.isFullscreen ? this.exit() : this.request(a)
        },
        onchange: function() {},
        onerror: function() {},
        raw: d
      };
    return d ? (Object.defineProperties(e, {
      isFullscreen: {
        get: function() {
          return !!b[d.fullscreenElement]
        }
      },
      element: {
        enumerable: !0,
        get: function() {
          return b[d.fullscreenElement]
        }
      },
      enabled: {
        enumerable: !0,
        get: function() {
          return !!b[d.fullscreenEnabled]
        }
      }
    }), b.addEventListener(d.fullscreenchange, function(a) {
      e.onchange.call(e, a)
    }), b.addEventListener(d.fullscreenerror, function(a) {
      e.onerror.call(e, a)
    }), void(a.screenfull = e)) : a.screenfull = !1
  }(window, document), function(f) {
    jQuery.fn.extend({
      slimScroll: function(h) {
        var a = f.extend({
          width: "auto",
          height: "250px",
          size: "7px",
          color: "#000",
          position: "right",
          distance: "1px",
          start: "top",
          opacity: .4,
          alwaysVisible: !1,
          disableFadeOut: !1,
          railVisible: !1,
          railColor: "#333",
          railOpacity: .2,
          railDraggable: !0,
          railClass: "slimScrollRail",
          barClass: "slimScrollBar",
          wrapperClass: "slimScrollDiv",
          allowPageScroll: !1,
          wheelStep: 20,
          touchScrollStep: 200,
          borderRadius: "7px",
          railBorderRadius: "7px"
        }, h);
        return this.each(function() {
          function r(d) {
            if (s) {
              d = d || window.event;
              var c = 0;
              d.wheelDelta && (c = -d.wheelDelta / 120), d.detail && (c = d.detail / 3), f(d.target || d.srcTarget || d.srcElement).closest("." + a.wrapperClass).is(b.parent()) && m(c, !0), d.preventDefault && !k && d.preventDefault(), k || (d.returnValue = !1)
            }
          }

          function m(d, f, h) {
            k = !1;
            var e = d,
              g = b.outerHeight() - c.outerHeight();
            f && (e = parseInt(c.css("top")) + d * parseInt(a.wheelStep) / 100 * c.outerHeight(), e = Math.min(Math.max(e, 0), g), e = d > 0 ? Math.ceil(e) : Math.floor(e), c.css({
              top: e + "px"
            })), l = parseInt(c.css("top")) / (b.outerHeight() - c.outerHeight()), e = l * (b[0].scrollHeight - b.outerHeight()), h && (e = d, d = e / b[0].scrollHeight * b.outerHeight(), d = Math.min(Math.max(d, 0), g), c.css({
              top: d + "px"
            })), b.scrollTop(e), b.trigger("slimscrolling", ~~e), v(), p()
          }

          function C() {
            window.addEventListener ? (this.addEventListener("DOMMouseScroll", r, !1), this.addEventListener("mousewheel", r, !1), this.addEventListener("MozMousePixelScroll", r, !1)) : document.attachEvent("onmousewheel", r)
          }

          function w() {
            u = Math.max(b.outerHeight() / b[0].scrollHeight * b.outerHeight(), D), c.css({
              height: u + "px"
            });
            var a = u == b.outerHeight() ? "none" : "block";
            c.css({
              display: a
            })
          }

          function v() {
            w(), clearTimeout(A), l == ~~l ? (k = a.allowPageScroll, B != l && b.trigger("slimscroll", 0 == ~~l ? "top" : "bottom")) : k = !1, B = l, u >= b.outerHeight() ? k = !0 : (c.stop(!0, !0).fadeIn("fast"), a.railVisible && g.stop(!0, !0).fadeIn("fast"))
          }

          function p() {
            a.alwaysVisible || (A = setTimeout(function() {
              a.disableFadeOut && s || x || y || (c.fadeOut("slow"), g.fadeOut("slow"))
            }, 1e3))
          }
          var s, x, y, A, z, u, l, B, D = 30,
            k = !1,
            b = f(this);
          if (b.parent().hasClass(a.wrapperClass)) {
            var n = b.scrollTop(),
              c = b.parent().find("." + a.barClass),
              g = b.parent().find("." + a.railClass);
            if (w(), f.isPlainObject(h)) {
              if ("height" in h && "auto" == h.height) {
                b.parent().css("height", "auto"), b.css("height", "auto");
                var q = b.parent().parent().height();
                b.parent().css("height", q), b.css("height", q)
              }
              if ("scrollTo" in h) n = parseInt(a.scrollTo);
              else if ("scrollBy" in h) n += parseInt(a.scrollBy);
              else if ("destroy" in h) return c.remove(), g.remove(), void b.unwrap();
              m(n, !1, !0)
            }
          } else {
            a.height = "auto" == a.height ? b.parent().height() : a.height, n = f("<div></div>").addClass(a.wrapperClass).css({
              position: "relative",
              overflow: "hidden",
              width: a.width,
              height: a.height
            }), b.css({
              overflow: "hidden",
              width: a.width,
              height: a.height
            });
            var g = f("<div></div>").addClass(a.railClass).css({
                width: a.size,
                height: "100%",
                position: "absolute",
                top: 0,
                display: a.alwaysVisible && a.railVisible ? "block" : "none",
                "border-radius": a.railBorderRadius,
                background: a.railColor,
                opacity: a.railOpacity,
                zIndex: 90
              }),
              c = f("<div></div>").addClass(a.barClass).css({
                background: a.color,
                width: a.size,
                position: "absolute",
                top: 0,
                opacity: a.opacity,
                display: a.alwaysVisible ? "block" : "none",
                "border-radius": a.borderRadius,
                BorderRadius: a.borderRadius,
                MozBorderRadius: a.borderRadius,
                WebkitBorderRadius: a.borderRadius,
                zIndex: 99
              }),
              q = "right" == a.position ? {
                right: a.distance
              } : {
                left: a.distance
              };
            g.css(q), c.css(q), b.wrap(n), b.parent().append(c), b.parent().append(g), a.railDraggable && c.bind("mousedown", function(a) {
              var b = f(document);
              return y = !0, t = parseFloat(c.css("top")), pageY = a.pageY, b.bind("mousemove.slimscroll", function(a) {
                currTop = t + a.pageY - pageY, c.css("top", currTop), m(0, c.position().top, !1)
              }), b.bind("mouseup.slimscroll", function(a) {
                y = !1, p(), b.unbind(".slimscroll")
              }), !1
            }).bind("selectstart.slimscroll", function(a) {
              return a.stopPropagation(), a.preventDefault(), !1
            }), g.hover(function() {
              v()
            }, function() {
              p()
            }), c.hover(function() {
              x = !0
            }, function() {
              x = !1
            }), b.hover(function() {
              s = !0, v(), p()
            }, function() {
              s = !1, p()
            }), b.bind("touchstart", function(a, b) {
              a.originalEvent.touches.length && (z = a.originalEvent.touches[0].pageY)
            }), b.bind("touchmove", function(b) {
              k || b.originalEvent.preventDefault(), b.originalEvent.touches.length && (m((z - b.originalEvent.touches[0].pageY) / a.touchScrollStep, !0), z = b.originalEvent.touches[0].pageY)
            }), w(), "bottom" === a.start ? (c.css({
              top: b.outerHeight() - c.outerHeight()
            }), m(0, !0)) : "top" !== a.start && (m(f(a.start).position().top, null, !0), a.alwaysVisible || c.hide()), C()
          }
        }), this
      }
    }), jQuery.fn.extend({
      slimscroll: jQuery.fn.slimScroll
    })
  }(jQuery), ! function(a) {
    function b() {
      return "Markdown.mk_block( " + uneval(this.toString()) + ", " + uneval(this.trailing) + ", " + uneval(this.lineNumber) + " )"
    }

    function c() {
      var a = require("util");
      return "Markdown.mk_block( " + a.inspect(this.toString()) + ", " + a.inspect(this.trailing) + ", " + a.inspect(this.lineNumber) + " )"
    }

    function d(a) {
      for (var b = 0, c = -1; - 1 !== (c = a.indexOf("\n", c + 1));) b++;
      return b
    }

    function e(a) {
      return a.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#39;")
    }

    function f(a) {
      if ("string" == typeof a) return e(a);
      var b = a.shift(),
        c = {},
        d = [];
      for (!a.length || "object" != typeof a[0] || a[0] instanceof Array || (c = a.shift()); a.length;) d.push(f(a.shift()));
      var g = "";
      for (var h in c) g += " " + h + '="' + e(c[h]) + '"';
      return "img" === b || "br" === b || "hr" === b ? "<" + b + g + "/>" : "<" + b + g + ">" + d.join("") + "</" + b + ">"
    }

    function g(a, b, c) {
      var d;
      c = c || {};
      var e = a.slice(0);
      "function" == typeof c.preprocessTreeNode && (e = c.preprocessTreeNode(e, b));
      var f = o(e);
      if (f) {
        e[1] = {};
        for (d in f) e[1][d] = f[d];
        f = e[1]
      }
      if ("string" == typeof e) return e;
      switch (e[0]) {
        case "header":
          e[0] = "h" + e[1].level, delete e[1].level;
          break;
        case "bulletlist":
          e[0] = "ul";
          break;
        case "numberlist":
          e[0] = "ol";
          break;
        case "listitem":
          e[0] = "li";
          break;
        case "para":
          e[0] = "p";
          break;
        case "markdown":
          e[0] = "html", f && delete f.references;
          break;
        case "code_block":
          e[0] = "pre", d = f ? 2 : 1;
          var h = ["code"];
          h.push.apply(h, e.splice(d, e.length - d)), e[d] = h;
          break;
        case "inlinecode":
          e[0] = "code";
          break;
        case "img":
          e[1].src = e[1].href, delete e[1].href;
          break;
        case "linebreak":
          e[0] = "br";
          break;
        case "link":
          e[0] = "a";
          break;
        case "link_ref":
          e[0] = "a";
          var i = b[f.ref];
          if (!i) return f.original;
          delete f.ref, f.href = i.href, i.title && (f.title = i.title), delete f.original;
          break;
        case "img_ref":
          e[0] = "img";
          var i = b[f.ref];
          if (!i) return f.original;
          delete f.ref, f.src = i.href, i.title && (f.title = i.title), delete f.original
      }
      if (d = 1, f) {
        for (var j in e[1]) {
          d = 2;
          break
        }
        1 === d && e.splice(d, 1)
      }
      for (; d < e.length; ++d) e[d] = g(e[d], b, c);
      return e
    }

    function h(a) {
      for (var b = o(a) ? 2 : 1; b < a.length;) "string" == typeof a[b] ? b + 1 < a.length && "string" == typeof a[b + 1] ? a[b] += a.splice(b + 1, 1)[0] : ++b : (h(a[b]), ++b)
    }

    function i(a, b) {
      function c(a) {
        this.len_after = a, this.name = "close_" + b
      }
      var d = a + "_state",
        e = "strong" === a ? "em_state" : "strong_state";
      return function(f) {
        if (this[d][0] === b) return this[d].shift(), [f.length, new c(f.length - b.length)];
        var g = this[e].slice(),
          h = this[d].slice();
        this[d].unshift(b);
        var i = this.processInline(f.substr(b.length)),
          j = i[i.length - 1];
        if (this[d].shift(), j instanceof c) {
          i.pop();
          var k = f.length - j.len_after;
          return [k, [a].concat(i)]
        }
        return this[e] = g, this[d] = h, [b.length, b]
      }
    }

    function j(a) {
      for (var b = a.split(""), c = [""], d = !1; b.length;) {
        var e = b.shift();
        switch (e) {
          case " ":
            d ? c[c.length - 1] += e : c.push("");
            break;
          case "'":
          case '"':
            d = !d;
            break;
          case "\\":
            e = b.shift();
          default:
            c[c.length - 1] += e
        }
      }
      return c
    }
    var k = {};
    k.mk_block = function(a, d, e) {
      1 === arguments.length && (d = "\n\n");
      var f = new String(a);
      return f.trailing = d, f.inspect = c, f.toSource = b, void 0 !== e && (f.lineNumber = e), f
    };
    var l = k.isArray = Array.isArray || function(a) {
      return "[object Array]" === Object.prototype.toString.call(a)
    };
    k.forEach = Array.prototype.forEach ? function(a, b, c) {
      return a.forEach(b, c)
    } : function(a, b, c) {
      for (var d = 0; d < a.length; d++) b.call(c || a, a[d], d, a)
    }, k.isEmpty = function(a) {
      for (var b in a)
        if (hasOwnProperty.call(a, b)) return !1;
      return !0
    }, k.extract_attr = function(a) {
      return l(a) && a.length > 1 && "object" == typeof a[1] && !l(a[1]) ? a[1] : void 0
    };
    var m = function(a) {
      switch (typeof a) {
        case "undefined":
          this.dialect = m.dialects.Gruber;
          break;
        case "object":
          this.dialect = a;
          break;
        default:
          if (!(a in m.dialects)) throw new Error("Unknown Markdown dialect '" + String(a) + "'");
          this.dialect = m.dialects[a]
      }
      this.em_state = [], this.strong_state = [], this.debug_indent = ""
    };
    m.dialects = {};
    var n = m.mk_block = k.mk_block,
      l = k.isArray;
    m.parse = function(a, b) {
      var c = new m(b);
      return c.toTree(a)
    }, m.prototype.split_blocks = function(a) {
      a = a.replace(/(\r\n|\n|\r)/g, "\n");
      var b, c = /([\s\S]+?)($|\n#|\n(?:\s*\n|$)+)/g,
        e = [],
        f = 1;
      for (null !== (b = /^(\s*\n)/.exec(a)) && (f += d(b[0]), c.lastIndex = b[0].length); null !== (b = c.exec(a));) "\n#" === b[2] && (b[2] = "\n", c.lastIndex--), e.push(n(b[1], b[2], f)), f += d(b[0]);
      return e
    }, m.prototype.processBlock = function(a, b) {
      var c = this.dialect.block,
        d = c.__order__;
      if ("__call__" in c) return c.__call__.call(this, a, b);
      for (var e = 0; e < d.length; e++) {
        var f = c[d[e]].call(this, a, b);
        if (f) return (!l(f) || f.length > 0 && !l(f[0])) && this.debug(d[e], "didn't return a proper array"), f
      }
      return []
    }, m.prototype.processInline = function(a) {
      return this.dialect.inline.__call__.call(this, String(a))
    }, m.prototype.toTree = function(a, b) {
      var c = a instanceof Array ? a : this.split_blocks(a),
        d = this.tree;
      try {
        for (this.tree = b || this.tree || ["markdown"]; c.length;) {
          var e = this.processBlock(c.shift(), c);
          e.length && this.tree.push.apply(this.tree, e)
        }
        return this.tree
      } finally {
        b && (this.tree = d)
      }
    }, m.prototype.debug = function() {
      var a = Array.prototype.slice.call(arguments);
      a.unshift(this.debug_indent), "undefined" != typeof print && print.apply(print, a), "undefined" != typeof console && "undefined" != typeof console.log && console.log.apply(null, a)
    }, m.prototype.loop_re_over_block = function(a, b, c) {
      for (var d, e = b.valueOf(); e.length && null !== (d = a.exec(e));) e = e.substr(d[0].length), c.call(this, d);
      return e
    }, m.buildBlockOrder = function(a) {
      var b = [];
      for (var c in a) "__order__" !== c && "__call__" !== c && b.push(c);
      a.__order__ = b
    }, m.buildInlinePatterns = function(a) {
      var b = [];
      for (var c in a)
        if (!c.match(/^__.*__$/)) {
          var d = c.replace(/([\\.*+?|()\[\]{}])/g, "\\$1").replace(/\n/, "\\n");
          b.push(1 === c.length ? d : "(?:" + d + ")")
        } b = b.join("|"), a.__patterns__ = b;
      var e = a.__call__;
      a.__call__ = function(a, c) {
        return void 0 !== c ? e.call(this, a, c) : e.call(this, a, b)
      }
    };
    var o = k.extract_attr;
    m.renderJsonML = function(a, b) {
      b = b || {}, b.root = b.root || !1;
      var c = [];
      if (b.root) c.push(f(a));
      else
        for (a.shift(), !a.length || "object" != typeof a[0] || a[0] instanceof Array || a.shift(); a.length;) c.push(f(a.shift()));
      return c.join("\n\n")
    }, m.toHTMLTree = function(a, b, c) {
      "string" == typeof a && (a = this.parse(a, b));
      var d = o(a),
        e = {};
      d && d.references && (e = d.references);
      var f = g(a, e, c);
      return h(f), f
    }, m.toHTML = function(a, b, c) {
      var d = this.toHTMLTree(a, b, c);
      return this.renderJsonML(d)
    };
    var p = {};
    p.inline_until_char = function(a, b) {
      for (var c = 0, d = [];;) {
        if (a.charAt(c) === b) return c++, [c, d];
        if (c >= a.length) return null;
        var e = this.dialect.inline.__oneElement__.call(this, a.substr(c));
        c += e[0], d.push.apply(d, e.slice(1))
      }
    }, p.subclassDialect = function(a) {
      function b() {}

      function c() {}
      return b.prototype = a.block, c.prototype = a.inline, {
        block: new b,
        inline: new c
      }
    };
    var q = k.forEach,
      o = k.extract_attr,
      n = k.mk_block,
      r = k.isEmpty,
      s = p.inline_until_char,
      t = {
        block: {
          atxHeader: function(a, b) {
            var c = a.match(/^(#{1,6})\s*(.*?)\s*#*\s*(?:\n|$)/);
            if (!c) return void 0;
            var d = ["header", {
              level: c[1].length
            }];
            return Array.prototype.push.apply(d, this.processInline(c[2])), c[0].length < a.length && b.unshift(n(a.substr(c[0].length), a.trailing, a.lineNumber + 2)), [d]
          },
          setextHeader: function(a, b) {
            var c = a.match(/^(.*)\n([-=])\2\2+(?:\n|$)/);
            if (!c) return void 0;
            var d = "=" === c[2] ? 1 : 2,
              e = ["header", {
                level: d
              }, c[1]];
            return c[0].length < a.length && b.unshift(n(a.substr(c[0].length), a.trailing, a.lineNumber + 2)), [e]
          },
          code: function(a, b) {
            var c = [],
              d = /^(?: {0,3}\t| {4})(.*)\n?/;
            if (!a.match(d)) return void 0;
            a: for (;;) {
              var e = this.loop_re_over_block(d, a.valueOf(), function(a) {
                c.push(a[1])
              });
              if (e.length) {
                b.unshift(n(e, a.trailing));
                break a
              }
              if (!b.length) break a;
              if (!b[0].match(d)) break a;
              c.push(a.trailing.replace(/[^\n]/g, "").substring(2)), a = b.shift()
            }
            return [
              ["code_block", c.join("\n")]
            ]
          },
          horizRule: function(a, b) {
            var c = a.match(/^(?:([\s\S]*?)\n)?[ \t]*([-_*])(?:[ \t]*\2){2,}[ \t]*(?:\n([\s\S]*))?$/);
            if (!c) return void 0;
            var d = [
              ["hr"]
            ];
            if (c[1]) {
              var e = n(c[1], "", a.lineNumber);
              d.unshift.apply(d, this.toTree(e, []))
            }
            return c[3] && b.unshift(n(c[3], a.trailing, a.lineNumber + 1)), d
          },
          lists: function() {
            function a(a) {
              return new RegExp("(?:^(" + i + "{0," + a + "} {0,3})(" + f + ")\\s+)|(^" + i + "{0," + (a - 1) + "}[ ]{0,4})")
            }

            function b(a) {
              return a.replace(/ {0,3}\t/g, "    ")
            }

            function c(a, b, c, d) {
              if (b) return void a.push(["para"].concat(c));
              var e = a[a.length - 1] instanceof Array && "para" === a[a.length - 1][0] ? a[a.length - 1] : a;
              d && a.length > 1 && c.unshift(d);
              for (var f = 0; f < c.length; f++) {
                var g = c[f],
                  h = "string" == typeof g;
                h && e.length > 1 && "string" == typeof e[e.length - 1] ? e[e.length - 1] += g : e.push(g)
              }
            }

            function d(a, b) {
              for (var c = new RegExp("^(" + i + "{" + a + "}.*?\\n?)*$"), d = new RegExp("^" + i + "{" + a + "}", "gm"), e = []; b.length > 0 && c.exec(b[0]);) {
                var f = b.shift(),
                  g = f.replace(d, "");
                e.push(n(g, f.trailing, f.lineNumber))
              }
              return e
            }

            function e(a, b, c) {
              var d = a.list,
                e = d[d.length - 1];
              if (!(e[1] instanceof Array && "para" === e[1][0]))
                if (b + 1 === c.length) e.push(["para"].concat(e.splice(1, e.length - 1)));
                else {
                  var f = e.pop();
                  e.push(["para"].concat(e.splice(1, e.length - 1)), f)
                }
            }
            var f = "[*+-]|\\d+\\.",
              g = /[*+-]/,
              h = new RegExp("^( {0,3})(" + f + ")[ 	]+"),
              i = "(?: {0,3}\\t| {4})";
            return function(f, i) {
              function j(a) {
                var b = g.exec(a[2]) ? ["bulletlist"] : ["numberlist"];
                return n.push({
                  list: b,
                  indent: a[1]
                }), b
              }
              var k = f.match(h);
              if (!k) return void 0;
              for (var l, m, n = [], o = j(k), p = !1, r = [n[0].list];;) {
                for (var s = f.split(/(?=\n)/), t = "", u = "", v = 0; v < s.length; v++) {
                  u = "";
                  var w = s[v].replace(/^\n/, function(a) {
                      return u = a, ""
                    }),
                    x = a(n.length);
                  if (k = w.match(x), void 0 !== k[1]) {
                    t.length && (c(l, p, this.processInline(t), u), p = !1, t = ""), k[1] = b(k[1]);
                    var y = Math.floor(k[1].length / 4) + 1;
                    if (y > n.length) o = j(k), l.push(o), l = o[1] = ["listitem"];
                    else {
                      var z = !1;
                      for (m = 0; m < n.length; m++)
                        if (n[m].indent === k[1]) {
                          o = n[m].list, n.splice(m + 1, n.length - (m + 1)), z = !0;
                          break
                        } z || (y++, y <= n.length ? (n.splice(y, n.length - y), o = n[y - 1].list) : (o = j(k), l.push(o))), l = ["listitem"], o.push(l)
                    }
                    u = ""
                  }
                  w.length > k[0].length && (t += u + w.substr(k[0].length))
                }
                t.length && (c(l, p, this.processInline(t), u), p = !1, t = "");
                var A = d(n.length, i);
                A.length > 0 && (q(n, e, this), l.push.apply(l, this.toTree(A, [])));
                var B = i[0] && i[0].valueOf() || "";
                if (!B.match(h) && !B.match(/^ /)) break;
                f = i.shift();
                var C = this.dialect.block.horizRule(f, i);
                if (C) {
                  r.push.apply(r, C);
                  break
                }
                q(n, e, this), p = !0
              }
              return r
            }
          }(),
          blockquote: function(a, b) {
            if (!a.match(/^>/m)) return void 0;
            var c = [];
            if (">" !== a[0]) {
              for (var d = a.split(/\n/), e = [], f = a.lineNumber; d.length && ">" !== d[0][0];) e.push(d.shift()), f++;
              var g = n(e.join("\n"), "\n", a.lineNumber);
              c.push.apply(c, this.processBlock(g, [])), a = n(d.join("\n"), a.trailing, f)
            }
            for (; b.length && ">" === b[0][0];) {
              var h = b.shift();
              a = n(a + a.trailing + h, h.trailing, a.lineNumber)
            }
            var i = a.replace(/^> ?/gm, ""),
              j = (this.tree, this.toTree(i, ["blockquote"])),
              k = o(j);
            return k && k.references && (delete k.references, r(k) && j.splice(1, 1)), c.push(j), c
          },
          referenceDefn: function(a, b) {
            var c = /^\s*\[(.*?)\]:\s*(\S+)(?:\s+(?:(['"])(.*?)\3|\((.*?)\)))?\n?/;
            if (!a.match(c)) return void 0;
            o(this.tree) || this.tree.splice(1, 0, {});
            var d = o(this.tree);
            void 0 === d.references && (d.references = {});
            var e = this.loop_re_over_block(c, a, function(a) {
              a[2] && "<" === a[2][0] && ">" === a[2][a[2].length - 1] && (a[2] = a[2].substring(1, a[2].length - 1));
              var b = d.references[a[1].toLowerCase()] = {
                href: a[2]
              };
              void 0 !== a[4] ? b.title = a[4] : void 0 !== a[5] && (b.title = a[5])
            });
            return e.length && b.unshift(n(e, a.trailing)), []
          },
          para: function(a) {
            return [
              ["para"].concat(this.processInline(a))
            ]
          }
        },
        inline: {
          __oneElement__: function(a, b, c) {
            var d, e;
            b = b || this.dialect.inline.__patterns__;
            var f = new RegExp("([\\s\\S]*?)(" + (b.source || b) + ")");
            if (d = f.exec(a), !d) return [a.length, a];
            if (d[1]) return [d[1].length, d[1]];
            var e;
            return d[2] in this.dialect.inline && (e = this.dialect.inline[d[2]].call(this, a.substr(d.index), d, c || [])), e = e || [d[2].length, d[2]]
          },
          __call__: function(a, b) {
            function c(a) {
              "string" == typeof a && "string" == typeof e[e.length - 1] ? e[e.length - 1] += a : e.push(a)
            }
            for (var d, e = []; a.length > 0;) d = this.dialect.inline.__oneElement__.call(this, a, b, e), a = a.substr(d.shift()), q(d, c);
            return e
          },
          "]": function() {},
          "}": function() {},
          __escape__: /^\\[\\`\*_{}\[\]()#\+.!\-]/,
          "\\": function(a) {
            return this.dialect.inline.__escape__.exec(a) ? [2, a.charAt(1)] : [1, "\\"]
          },
          "![": function(a) {
            var b = a.match(/^!\[(.*?)\][ \t]*\([ \t]*([^")]*?)(?:[ \t]+(["'])(.*?)\3)?[ \t]*\)/);
            if (b) {
              b[2] && "<" === b[2][0] && ">" === b[2][b[2].length - 1] && (b[2] = b[2].substring(1, b[2].length - 1)), b[2] = this.dialect.inline.__call__.call(this, b[2], /\\/)[0];
              var c = {
                alt: b[1],
                href: b[2] || ""
              };
              return void 0 !== b[4] && (c.title = b[4]), [b[0].length, ["img", c]]
            }
            return b = a.match(/^!\[(.*?)\][ \t]*\[(.*?)\]/), b ? [b[0].length, ["img_ref", {
              alt: b[1],
              ref: b[2].toLowerCase(),
              original: b[0]
            }]] : [2, "!["]
          },
          "[": function v(a) {
            var b = String(a),
              c = s.call(this, a.substr(1), "]");
            if (!c) return [1, "["];
            var v, d, e = 1 + c[0],
              f = c[1];
            a = a.substr(e);
            var g = a.match(/^\s*\([ \t]*([^"']*)(?:[ \t]+(["'])(.*?)\2)?[ \t]*\)/);
            if (g) {
              var h = g[1];
              if (e += g[0].length, h && "<" === h[0] && ">" === h[h.length - 1] && (h = h.substring(1, h.length - 1)), !g[3])
                for (var i = 1, j = 0; j < h.length; j++) switch (h[j]) {
                  case "(":
                    i++;
                    break;
                  case ")":
                    0 === --i && (e -= h.length - j, h = h.substring(0, j))
                }
              return h = this.dialect.inline.__call__.call(this, h, /\\/)[0], d = {
                href: h || ""
              }, void 0 !== g[3] && (d.title = g[3]), v = ["link", d].concat(f), [e, v]
            }
            return g = a.match(/^\s*\[(.*?)\]/), g ? (e += g[0].length, d = {
              ref: (g[1] || String(f)).toLowerCase(),
              original: b.substr(0, e)
            }, v = ["link_ref", d].concat(f), [e, v]) : 1 === f.length && "string" == typeof f[0] ? (d = {
              ref: f[0].toLowerCase(),
              original: b.substr(0, e)
            }, v = ["link_ref", d, f[0]], [e, v]) : [1, "["]
          },
          "<": function(a) {
            var b;
            return null !== (b = a.match(/^<(?:((https?|ftp|mailto):[^>]+)|(.*?@.*?\.[a-zA-Z]+))>/)) ? b[3] ? [b[0].length, ["link", {
              href: "mailto:" + b[3]
            }, b[3]]] : "mailto" === b[2] ? [b[0].length, ["link", {
              href: b[1]
            }, b[1].substr("mailto:".length)]] : [b[0].length, ["link", {
              href: b[1]
            }, b[1]]] : [1, "<"]
          },
          "`": function(a) {
            var b = a.match(/(`+)(([\s\S]*?)\1)/);
            return b && b[2] ? [b[1].length + b[2].length, ["inlinecode", b[3]]] : [1, "`"]
          },
          "  \n": function() {
            return [3, ["linebreak"]]
          }
        }
      };
    t.inline["**"] = i("strong", "**"), t.inline.__ = i("strong", "__"), t.inline["*"] = i("em", "*"), t.inline._ = i("em", "_"), m.dialects.Gruber = t, m.buildBlockOrder(m.dialects.Gruber.block), m.buildInlinePatterns(m.dialects.Gruber.inline);
    var u = p.subclassDialect(t),
      o = k.extract_attr,
      q = k.forEach;
    u.processMetaHash = function(a) {
      for (var b = j(a), c = {}, d = 0; d < b.length; ++d)
        if (/^#/.test(b[d])) c.id = b[d].substring(1);
        else if (/^\./.test(b[d])) c["class"] = c["class"] ? c["class"] + b[d].replace(/./, " ") : b[d].substring(1);
      else if (/\=/.test(b[d])) {
        var e = b[d].split(/\=/);
        c[e[0]] = e[1]
      }
      return c
    }, u.block.document_meta = function(a) {
      if (a.lineNumber > 1) return void 0;
      if (!a.match(/^(?:\w+:.*\n)*\w+:.*$/)) return void 0;
      o(this.tree) || this.tree.splice(1, 0, {});
      var b = a.split(/\n/);
      for (var c in b) {
        var d = b[c].match(/(\w+):\s*(.*)$/),
          e = d[1].toLowerCase(),
          f = d[2];
        this.tree[1][e] = f
      }
      return []
    }, u.block.block_meta = function(a) {
      var b = a.match(/(^|\n) {0,3}\{:\s*((?:\\\}|[^\}])*)\s*\}$/);
      if (!b) return void 0;
      var c, d = this.dialect.processMetaHash(b[2]);
      if ("" === b[1]) {
        var e = this.tree[this.tree.length - 1];
        if (c = o(e), "string" == typeof e) return void 0;
        c || (c = {}, e.splice(1, 0, c));
        for (var f in d) c[f] = d[f];
        return []
      }
      var g = a.replace(/\n.*$/, ""),
        h = this.processBlock(g, []);
      c = o(h[0]), c || (c = {}, h[0].splice(1, 0, c));
      for (var f in d) c[f] = d[f];
      return h
    }, u.block.definition_list = function(a, b) {
      var c, d, e = /^((?:[^\s:].*\n)+):\s+([\s\S]+)$/,
        f = ["dl"];
      if (!(d = a.match(e))) return void 0;
      for (var g = [a]; b.length && e.exec(b[0]);) g.push(b.shift());
      for (var h = 0; h < g.length; ++h) {
        var d = g[h].match(e),
          i = d[1].replace(/\n$/, "").split(/\n/),
          j = d[2].split(/\n:\s+/);
        for (c = 0; c < i.length; ++c) f.push(["dt", i[c]]);
        for (c = 0; c < j.length; ++c) f.push(["dd"].concat(this.processInline(j[c].replace(/(\n)\s+/, "$1"))))
      }
      return [f]
    }, u.block.table = function w(a) {
      var b, c, d = function(a, b) {
          b = b || "\\s", b.match(/^[\\|\[\]{}?*.+^$]$/) && (b = "\\" + b);
          for (var c, d = [], e = new RegExp("^((?:\\\\.|[^\\\\" + b + "])*)" + b + "(.*)"); c = a.match(e);) d.push(c[1]), a = c[2];
          return d.push(a), d
        },
        e = /^ {0,3}\|(.+)\n {0,3}\|\s*([\-:]+[\-| :]*)\n((?:\s*\|.*(?:\n|$))*)(?=\n|$)/,
        f = /^ {0,3}(\S(?:\\.|[^\\|])*\|.*)\n {0,3}([\-:]+\s*\|[\-| :]*)\n((?:(?:\\.|[^\\|])*\|.*(?:\n|$))*)(?=\n|$)/;
      if (c = a.match(e)) c[3] = c[3].replace(/^\s*\|/gm, "");
      else if (!(c = a.match(f))) return void 0;
      var w = ["table", ["thead", ["tr"]],
        ["tbody"]
      ];
      c[2] = c[2].replace(/\|\s*$/, "").split("|");
      var g = [];
      for (q(c[2], function(a) {
          a.match(/^\s*-+:\s*$/) ? g.push({
            align: "right"
          }) : a.match(/^\s*:-+\s*$/) ? g.push({
            align: "left"
          }) : a.match(/^\s*:-+:\s*$/) ? g.push({
            align: "center"
          }) : g.push({})
        }), c[1] = d(c[1].replace(/\|\s*$/, ""), "|"), b = 0; b < c[1].length; b++) w[1][1].push(["th", g[b] || {}].concat(this.processInline(c[1][b].trim())));
      return q(c[3].replace(/\|\s*$/gm, "").split("\n"), function(a) {
        var c = ["tr"];
        for (a = d(a, "|"), b = 0; b < a.length; b++) c.push(["td", g[b] || {}].concat(this.processInline(a[b].trim())));
        w[2].push(c)
      }, this), [w]
    }, u.inline["{:"] = function(a, b, c) {
      if (!c.length) return [2, "{:"];
      var d = c[c.length - 1];
      if ("string" == typeof d) return [2, "{:"];
      var e = a.match(/^\{:\s*((?:\\\}|[^\}])*)\s*\}/);
      if (!e) return [2, "{:"];
      var f = this.dialect.processMetaHash(e[1]),
        g = o(d);
      g || (g = {}, d.splice(1, 0, g));
      for (var h in f) g[h] = f[h];
      return [e[0].length, ""]
    }, m.dialects.Maruku = u, m.dialects.Maruku.inline.__escape__ = /^\\[\\`\*_{}\[\]()#\+.!\-|:]/, m.buildBlockOrder(m.dialects.Maruku.block), m.buildInlinePatterns(m.dialects.Maruku.inline), a.Markdown = m, a.parse = m.parse, a.toHTML = m.toHTML, a.toHTMLTree = m.toHTMLTree, a.renderJsonML = m.renderJsonML
  }(function() {
    return window.markdown = {}, window.markdown
  }()), Date.CultureInfo = {
    name: "en-US",
    englishName: "English (United States)",
    nativeName: "English (United States)",
    dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    abbreviatedDayNames: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    shortestDayNames: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
    firstLetterDayNames: ["S", "M", "T", "W", "T", "F", "S"],
    monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    abbreviatedMonthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    amDesignator: "AM",
    pmDesignator: "PM",
    firstDayOfWeek: 0,
    twoDigitYearMax: 2029,
    dateElementOrder: "mdy",
    formatPatterns: {
      shortDate: "M/d/yyyy",
      longDate: "dddd, MMMM dd, yyyy",
      shortTime: "h:mm tt",
      longTime: "h:mm:ss tt",
      fullDateTime: "dddd, MMMM dd, yyyy h:mm:ss tt",
      sortableDateTime: "yyyy-MM-ddTHH:mm:ss",
      universalSortableDateTime: "yyyy-MM-dd HH:mm:ssZ",
      rfc1123: "ddd, dd MMM yyyy HH:mm:ss GMT",
      monthDay: "MMMM dd",
      yearMonth: "MMMM, yyyy"
    },
    regexPatterns: {
      jan: /^jan(uary)?/i,
      feb: /^feb(ruary)?/i,
      mar: /^mar(ch)?/i,
      apr: /^apr(il)?/i,
      may: /^may/i,
      jun: /^jun(e)?/i,
      jul: /^jul(y)?/i,
      aug: /^aug(ust)?/i,
      sep: /^sep(t(ember)?)?/i,
      oct: /^oct(ober)?/i,
      nov: /^nov(ember)?/i,
      dec: /^dec(ember)?/i,
      sun: /^su(n(day)?)?/i,
      mon: /^mo(n(day)?)?/i,
      tue: /^tu(e(s(day)?)?)?/i,
      wed: /^we(d(nesday)?)?/i,
      thu: /^th(u(r(s(day)?)?)?)?/i,
      fri: /^fr(i(day)?)?/i,
      sat: /^sa(t(urday)?)?/i,
      future: /^next/i,
      past: /^last|past|prev(ious)?/i,
      add: /^(\+|after|from)/i,
      subtract: /^(\-|before|ago)/i,
      yesterday: /^yesterday/i,
      today: /^t(oday)?/i,
      tomorrow: /^tomorrow/i,
      now: /^n(ow)?/i,
      millisecond: /^ms|milli(second)?s?/i,
      second: /^sec(ond)?s?/i,
      minute: /^min(ute)?s?/i,
      hour: /^h(ou)?rs?/i,
      week: /^w(ee)?k/i,
      month: /^m(o(nth)?s?)?/i,
      day: /^d(ays?)?/i,
      year: /^y((ea)?rs?)?/i,
      shortMeridian: /^(a|p)/i,
      longMeridian: /^(a\.?m?\.?|p\.?m?\.?)/i,
      timezone: /^((e(s|d)t|c(s|d)t|m(s|d)t|p(s|d)t)|((gmt)?\s*(\+|\-)\s*\d\d\d\d?)|gmt)/i,
      ordinalSuffix: /^\s*(st|nd|rd|th)/i,
      timeContext: /^\s*(\:|a|p)/i
    },
    abbreviatedTimeZoneStandard: {
      GMT: "-000",
      EST: "-0400",
      CST: "-0500",
      MST: "-0600",
      PST: "-0700"
    },
    abbreviatedTimeZoneDST: {
      GMT: "-000",
      EDT: "-0500",
      CDT: "-0600",
      MDT: "-0700",
      PDT: "-0800"
    }
  }, Date.getMonthNumberFromName = function(name) {
    for (var n = Date.CultureInfo.monthNames, m = Date.CultureInfo.abbreviatedMonthNames, s = name.toLowerCase(), i = 0; i < n.length; i++)
      if (n[i].toLowerCase() == s || m[i].toLowerCase() == s) return i;
    return -1
  }, Date.getDayNumberFromName = function(name) {
    for (var n = Date.CultureInfo.dayNames, m = Date.CultureInfo.abbreviatedDayNames, s = (Date.CultureInfo.shortestDayNames, name.toLowerCase()), i = 0; i < n.length; i++)
      if (n[i].toLowerCase() == s || m[i].toLowerCase() == s) return i;
    return -1
  }, Date.isLeapYear = function(year) {
    return year % 4 === 0 && year % 100 !== 0 || year % 400 === 0
  }, Date.getDaysInMonth = function(year, month) {
    return [31, Date.isLeapYear(year) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month]
  }, Date.getTimezoneOffset = function(s, dst) {
    return dst ? Date.CultureInfo.abbreviatedTimeZoneDST[s.toUpperCase()] : Date.CultureInfo.abbreviatedTimeZoneStandard[s.toUpperCase()]
  }, Date.getTimezoneAbbreviation = function(offset, dst) {
    var p, n = dst ? Date.CultureInfo.abbreviatedTimeZoneDST : Date.CultureInfo.abbreviatedTimeZoneStandard;
    for (p in n)
      if (n[p] === offset) return p;
    return null
  }, Date.prototype.clone = function() {
    return new Date(this.getTime())
  }, Date.prototype.compareTo = function(date) {
    if (isNaN(this)) throw new Error(this);
    if (date instanceof Date && !isNaN(date)) return this > date ? 1 : date > this ? -1 : 0;
    throw new TypeError(date)
  }, Date.prototype.equals = function(date) {
    return 0 === this.compareTo(date)
  }, Date.prototype.between = function(start, end) {
    var t = this.getTime();
    return t >= start.getTime() && t <= end.getTime()
  }, Date.prototype.addMilliseconds = function(value) {
    return this.setMilliseconds(this.getMilliseconds() + value), this
  }, Date.prototype.addSeconds = function(value) {
    return this.addMilliseconds(1e3 * value)
  }, Date.prototype.addMinutes = function(value) {
    return this.addMilliseconds(6e4 * value)
  }, Date.prototype.addHours = function(value) {
    return this.addMilliseconds(36e5 * value)
  }, Date.prototype.addDays = function(value) {
    return this.addMilliseconds(864e5 * value)
  }, Date.prototype.addWeeks = function(value) {
    return this.addMilliseconds(6048e5 * value)
  }, Date.prototype.addMonths = function(value) {
    var n = this.getDate();
    return this.setDate(1), this.setMonth(this.getMonth() + value), this.setDate(Math.min(n, this.getDaysInMonth())), this
  }, Date.prototype.addYears = function(value) {
    return this.addMonths(12 * value)
  }, Date.prototype.add = function(config) {
    if ("number" == typeof config) return this._orient = config, this;
    var x = config;
    return (x.millisecond || x.milliseconds) && this.addMilliseconds(x.millisecond || x.milliseconds), (x.second || x.seconds) && this.addSeconds(x.second || x.seconds), (x.minute || x.minutes) && this.addMinutes(x.minute || x.minutes), (x.hour || x.hours) && this.addHours(x.hour || x.hours), (x.month || x.months) && this.addMonths(x.month || x.months), (x.year || x.years) && this.addYears(x.year || x.years), (x.day || x.days) && this.addDays(x.day || x.days), this
  }, Date._validate = function(value, min, max, name) {
    if ("number" != typeof value) throw new TypeError(value + " is not a Number.");
    if (min > value || value > max) throw new RangeError(value + " is not a valid value for " + name + ".");
    return !0
  }, Date.validateMillisecond = function(n) {
    return Date._validate(n, 0, 999, "milliseconds")
  }, Date.validateSecond = function(n) {
    return Date._validate(n, 0, 59, "seconds")
  }, Date.validateMinute = function(n) {
    return Date._validate(n, 0, 59, "minutes")
  }, Date.validateHour = function(n) {
    return Date._validate(n, 0, 23, "hours")
  }, Date.validateDay = function(n, year, month) {
    return Date._validate(n, 1, Date.getDaysInMonth(year, month), "days")
  }, Date.validateMonth = function(n) {
    return Date._validate(n, 0, 11, "months")
  }, Date.validateYear = function(n) {
    return Date._validate(n, 1, 9999, "seconds")
  }, Date.prototype.set = function(config) {
    var x = config;
    return x.millisecond || 0 === x.millisecond || (x.millisecond = -1), x.second || 0 === x.second || (x.second = -1), x.minute || 0 === x.minute || (x.minute = -1), x.hour || 0 === x.hour || (x.hour = -1), x.day || 0 === x.day || (x.day = -1), x.month || 0 === x.month || (x.month = -1), x.year || 0 === x.year || (x.year = -1), -1 != x.millisecond && Date.validateMillisecond(x.millisecond) && this.addMilliseconds(x.millisecond - this.getMilliseconds()), -1 != x.second && Date.validateSecond(x.second) && this.addSeconds(x.second - this.getSeconds()), -1 != x.minute && Date.validateMinute(x.minute) && this.addMinutes(x.minute - this.getMinutes()), -1 != x.hour && Date.validateHour(x.hour) && this.addHours(x.hour - this.getHours()), -1 !== x.month && Date.validateMonth(x.month) && this.addMonths(x.month - this.getMonth()), -1 != x.year && Date.validateYear(x.year) && this.addYears(x.year - this.getFullYear()), -1 != x.day && Date.validateDay(x.day, this.getFullYear(), this.getMonth()) && this.addDays(x.day - this.getDate()), x.timezone && this.setTimezone(x.timezone), x.timezoneOffset && this.setTimezoneOffset(x.timezoneOffset), this
  }, Date.prototype.clearTime = function() {
    return this.setHours(0), this.setMinutes(0), this.setSeconds(0), this.setMilliseconds(0), this
  }, Date.prototype.isLeapYear = function() {
    var y = this.getFullYear();
    return y % 4 === 0 && y % 100 !== 0 || y % 400 === 0
  }, Date.prototype.isWeekday = function() {
    return !(this.is().sat() || this.is().sun())
  }, Date.prototype.getDaysInMonth = function() {
    return Date.getDaysInMonth(this.getFullYear(), this.getMonth())
  }, Date.prototype.moveToFirstDayOfMonth = function() {
    return this.set({
      day: 1
    })
  }, Date.prototype.moveToLastDayOfMonth = function() {
    return this.set({
      day: this.getDaysInMonth()
    })
  }, Date.prototype.moveToDayOfWeek = function(day, orient) {
    var diff = (day - this.getDay() + 7 * (orient || 1)) % 7;
    return this.addDays(0 === diff ? diff += 7 * (orient || 1) : diff)
  }, Date.prototype.moveToMonth = function(month, orient) {
    var diff = (month - this.getMonth() + 12 * (orient || 1)) % 12;
    return this.addMonths(0 === diff ? diff += 12 * (orient || 1) : diff)
  }, Date.prototype.getDayOfYear = function() {
    return Math.floor((this - new Date(this.getFullYear(), 0, 1)) / 864e5)
  }, Date.prototype.getWeekOfYear = function(firstDayOfWeek) {
    var y = this.getFullYear(),
      m = this.getMonth(),
      d = this.getDate(),
      dow = firstDayOfWeek || Date.CultureInfo.firstDayOfWeek,
      offset = 8 - new Date(y, 0, 1).getDay();
    8 == offset && (offset = 1);
    var daynum = (Date.UTC(y, m, d, 0, 0, 0) - Date.UTC(y, 0, 1, 0, 0, 0)) / 864e5 + 1,
      w = Math.floor((daynum - offset + 7) / 7);
    if (w === dow) {
      y--;
      var prevOffset = 8 - new Date(y, 0, 1).getDay();
      w = 2 == prevOffset || 8 == prevOffset ? 53 : 52
    }
    return w
  }, Date.prototype.isDST = function() {
    return console.log("isDST"), "D" == this.toString().match(/(E|C|M|P)(S|D)T/)[2]
  }, Date.prototype.getTimezone = function() {
    return Date.getTimezoneAbbreviation(this.getUTCOffset, this.isDST())
  }, Date.prototype.setTimezoneOffset = function(s) {
    var here = this.getTimezoneOffset(),
      there = -6 * Number(s) / 10;
    return this.addMinutes(there - here), this
  }, Date.prototype.setTimezone = function(s) {
    return this.setTimezoneOffset(Date.getTimezoneOffset(s))
  }, Date.prototype.getUTCOffset = function() {
    var r, n = -10 * this.getTimezoneOffset() / 6;
    return 0 > n ? (r = (n - 1e4).toString(), r[0] + r.substr(2)) : (r = (n + 1e4).toString(), "+" + r.substr(1))
  }, Date.prototype.getDayName = function(abbrev) {
    return abbrev ? Date.CultureInfo.abbreviatedDayNames[this.getDay()] : Date.CultureInfo.dayNames[this.getDay()]
  }, Date.prototype.getMonthName = function(abbrev) {
    return abbrev ? Date.CultureInfo.abbreviatedMonthNames[this.getMonth()] : Date.CultureInfo.monthNames[this.getMonth()]
  }, Date.prototype._toString = Date.prototype.toString, Date.prototype.toString = function(format) {
    var self = this,
      p = function(s) {
        return 1 == s.toString().length ? "0" + s : s
      };
    return format ? format.replace(/dd?d?d?|MM?M?M?|yy?y?y?|hh?|HH?|mm?|ss?|tt?|zz?z?/g, function(format) {
      switch (format) {
        case "hh":
          return p(self.getHours() < 13 ? self.getHours() : self.getHours() - 12);
        case "h":
          return self.getHours() < 13 ? self.getHours() : self.getHours() - 12;
        case "HH":
          return p(self.getHours());
        case "H":
          return self.getHours();
        case "mm":
          return p(self.getMinutes());
        case "m":
          return self.getMinutes();
        case "ss":
          return p(self.getSeconds());
        case "s":
          return self.getSeconds();
        case "yyyy":
          return self.getFullYear();
        case "yy":
          return self.getFullYear().toString().substring(2, 4);
        case "dddd":
          return self.getDayName();
        case "ddd":
          return self.getDayName(!0);
        case "dd":
          return p(self.getDate());
        case "d":
          return self.getDate().toString();
        case "MMMM":
          return self.getMonthName();
        case "MMM":
          return self.getMonthName(!0);
        case "MM":
          return p(self.getMonth() + 1);
        case "M":
          return self.getMonth() + 1;
        case "t":
          return self.getHours() < 12 ? Date.CultureInfo.amDesignator.substring(0, 1) : Date.CultureInfo.pmDesignator.substring(0, 1);
        case "tt":
          return self.getHours() < 12 ? Date.CultureInfo.amDesignator : Date.CultureInfo.pmDesignator;
        case "zzz":
        case "zz":
        case "z":
          return ""
      }
    }) : this._toString()
  }, Date.now = function() {
    return new Date
  }, Date.today = function() {
    return Date.now().clearTime()
  }, Date.prototype._orient = 1, Date.prototype.next = function() {
    return this._orient = 1, this
  }, Date.prototype.last = Date.prototype.prev = Date.prototype.previous = function() {
    return this._orient = -1, this
  }, Date.prototype._is = !1, Date.prototype.is = function() {
    return this._is = !0, this
  }, Number.prototype._dateElement = "day", Number.prototype.fromNow = function() {
    var c = {};
    return c[this._dateElement] = this, Date.now().add(c)
  }, Number.prototype.ago = function() {
    var c = {};
    return c[this._dateElement] = -1 * this, Date.now().add(c)
  }, function() {
    for (var de, $D = Date.prototype, $N = Number.prototype, dx = "sunday monday tuesday wednesday thursday friday saturday".split(/\s/), mx = "january february march april may june july august september october november december".split(/\s/), px = "Millisecond Second Minute Hour Day Week Month Year".split(/\s/), df = function(n) {
        return function() {
          return this._is ? (this._is = !1, this.getDay() == n) : this.moveToDayOfWeek(n, this._orient)
        }
      }, i = 0; i < dx.length; i++) $D[dx[i]] = $D[dx[i].substring(0, 3)] = df(i);
    for (var mf = function(n) {
        return function() {
          return this._is ? (this._is = !1, this.getMonth() === n) : this.moveToMonth(n, this._orient)
        }
      }, j = 0; j < mx.length; j++) $D[mx[j]] = $D[mx[j].substring(0, 3)] = mf(j);
    for (var ef = function(j) {
        return function() {
          return "s" != j.substring(j.length - 1) && (j += "s"), this["add" + j](this._orient)
        }
      }, nf = function(n) {
        return function() {
          return this._dateElement = n, this
        }
      }, k = 0; k < px.length; k++) de = px[k].toLowerCase(), $D[de] = $D[de + "s"] = ef(px[k]), $N[de] = $N[de + "s"] = nf(de)
  }(), Date.prototype.toJSONString = function() {
    return this.toString("yyyy-MM-ddThh:mm:ssZ")
  }, Date.prototype.toShortDateString = function() {
    return this.toString(Date.CultureInfo.formatPatterns.shortDatePattern)
  }, Date.prototype.toLongDateString = function() {
    return this.toString(Date.CultureInfo.formatPatterns.longDatePattern)
  }, Date.prototype.toShortTimeString = function() {
    return this.toString(Date.CultureInfo.formatPatterns.shortTimePattern)
  }, Date.prototype.toLongTimeString = function() {
    return this.toString(Date.CultureInfo.formatPatterns.longTimePattern)
  }, Date.prototype.getOrdinal = function() {
    switch (this.getDate()) {
      case 1:
      case 21:
      case 31:
        return "st";
      case 2:
      case 22:
        return "nd";
      case 3:
      case 23:
        return "rd";
      default:
        return "th"
    }
  }, function() {
    Date.Parsing = {
      Exception: function(s) {
        this.message = "Parse error at '" + s.substring(0, 10) + " ...'"
      }
    };
    for (var $P = Date.Parsing, _ = $P.Operators = {
        rtoken: function(r) {
          return function(s) {
            var mx = s.match(r);
            if (mx) return [mx[0], s.substring(mx[0].length)];
            throw new $P.Exception(s)
          }
        },
        token: function(s) {
          return function(s) {
            return _.rtoken(new RegExp("^s*" + s + "s*"))(s)
          }
        },
        stoken: function(s) {
          return _.rtoken(new RegExp("^" + s))
        },
        until: function(p) {
          return function(s) {
            for (var qx = [], rx = null; s.length;) {
              try {
                rx = p.call(this, s)
              } catch (e) {
                qx.push(rx[0]), s = rx[1];
                continue
              }
              break
            }
            return [qx, s]
          }
        },
        many: function(p) {
          return function(s) {
            for (var rx = [], r = null; s.length;) {
              try {
                r = p.call(this, s)
              } catch (e) {
                return [rx, s]
              }
              rx.push(r[0]), s = r[1]
            }
            return [rx, s]
          }
        },
        optional: function(p) {
          return function(s) {
            var r = null;
            try {
              r = p.call(this, s)
            } catch (e) {
              return [null, s]
            }
            return [r[0], r[1]]
          }
        },
        not: function(p) {
          return function(s) {
            try {
              p.call(this, s)
            } catch (e) {
              return [null, s]
            }
            throw new $P.Exception(s)
          }
        },
        ignore: function(p) {
          return p ? function(s) {
            var r = null;
            return r = p.call(this, s), [null, r[1]]
          } : null
        },
        product: function() {
          for (var px = arguments[0], qx = Array.prototype.slice.call(arguments, 1), rx = [], i = 0; i < px.length; i++) rx.push(_.each(px[i], qx));
          return rx
        },
        cache: function(rule) {
          var cache = {},
            r = null;
          return function(s) {
            try {
              r = cache[s] = cache[s] || rule.call(this, s)
            } catch (e) {
              r = cache[s] = e
            }
            if (r instanceof $P.Exception) throw r;
            return r
          }
        },
        any: function() {
          var px = arguments;
          return function(s) {
            for (var r = null, i = 0; i < px.length; i++)
              if (null != px[i]) {
                try {
                  r = px[i].call(this, s)
                } catch (e) {
                  r = null
                }
                if (r) return r
              } throw new $P.Exception(s)
          }
        },
        each: function() {
          var px = arguments;
          return function(s) {
            for (var rx = [], r = null, i = 0; i < px.length; i++)
              if (null != px[i]) {
                try {
                  r = px[i].call(this, s)
                } catch (e) {
                  throw new $P.Exception(s)
                }
                rx.push(r[0]), s = r[1]
              } return [rx, s]
          }
        },
        all: function() {
          var px = arguments,
            _ = _;
          return _.each(_.optional(px))
        },
        sequence: function(px, d, c) {
          return d = d || _.rtoken(/^\s*/), c = c || null, 1 == px.length ? px[0] : function(s) {
            for (var r = null, q = null, rx = [], i = 0; i < px.length; i++) {
              try {
                r = px[i].call(this, s)
              } catch (e) {
                break
              }
              rx.push(r[0]);
              try {
                q = d.call(this, r[1])
              } catch (ex) {
                q = null;
                break
              }
              s = q[1]
            }
            if (!r) throw new $P.Exception(s);
            if (q) throw new $P.Exception(q[1]);
            if (c) try {
              r = c.call(this, r[1])
            } catch (ey) {
              throw new $P.Exception(r[1])
            }
            return [rx, r ? r[1] : s]
          }
        },
        between: function(d1, p, d2) {
          d2 = d2 || d1;
          var _fn = _.each(_.ignore(d1), p, _.ignore(d2));
          return function(s) {
            var rx = _fn.call(this, s);
            return [
              [rx[0][0], r[0][2]], rx[1]
            ]
          }
        },
        list: function(p, d, c) {
          return d = d || _.rtoken(/^\s*/), c = c || null, p instanceof Array ? _.each(_.product(p.slice(0, -1), _.ignore(d)), p.slice(-1), _.ignore(c)) : _.each(_.many(_.each(p, _.ignore(d))), px, _.ignore(c))
        },
        set: function(px, d, c) {
          return d = d || _.rtoken(/^\s*/), c = c || null,
            function(s) {
              for (var r = null, p = null, q = null, rx = null, best = [
                  [], s
                ], last = !1, i = 0; i < px.length; i++) {
                q = null, p = null, r = null, last = 1 == px.length;
                try {
                  r = px[i].call(this, s)
                } catch (e) {
                  continue
                }
                if (rx = [
                    [r[0]], r[1]
                  ], r[1].length > 0 && !last) try {
                  q = d.call(this, r[1])
                } catch (ex) {
                  last = !0
                } else last = !0;
                if (last || 0 !== q[1].length || (last = !0), !last) {
                  for (var qx = [], j = 0; j < px.length; j++) i != j && qx.push(px[j]);
                  p = _.set(qx, d).call(this, q[1]), p[0].length > 0 && (rx[0] = rx[0].concat(p[0]), rx[1] = p[1])
                }
                if (rx[1].length < best[1].length && (best = rx), 0 === best[1].length) break
              }
              if (0 === best[0].length) return best;
              if (c) {
                try {
                  q = c.call(this, best[1])
                } catch (ey) {
                  throw new $P.Exception(best[1])
                }
                best[1] = q[1]
              }
              return best
            }
        },
        forward: function(gr, fname) {
          return function(s) {
            return gr[fname].call(this, s)
          }
        },
        replace: function(rule, repl) {
          return function(s) {
            var r = rule.call(this, s);
            return [repl, r[1]]
          }
        },
        process: function(rule, fn) {
          return function(s) {
            var r = rule.call(this, s);
            return [fn.call(this, r[0]), r[1]]
          }
        },
        min: function(min, rule) {
          return function(s) {
            var rx = rule.call(this, s);
            if (rx[0].length < min) throw new $P.Exception(s);
            return rx
          }
        }
      }, _generator = function(op) {
        return function() {
          var args = null,
            rx = [];
          if (arguments.length > 1 ? args = Array.prototype.slice.call(arguments) : arguments[0] instanceof Array && (args = arguments[0]), !args) return op.apply(null, arguments);
          for (var i = 0, px = args.shift(); i < px.length; i++) return args.unshift(px[i]), rx.push(op.apply(null, args)), args.shift(), rx
        }
      }, gx = "optional not ignore cache".split(/\s/), i = 0; i < gx.length; i++) _[gx[i]] = _generator(_[gx[i]]);
    for (var _vector = function(op) {
        return function() {
          return arguments[0] instanceof Array ? op.apply(null, arguments[0]) : op.apply(null, arguments)
        }
      }, vx = "each any all".split(/\s/), j = 0; j < vx.length; j++) _[vx[j]] = _vector(_[vx[j]])
  }(), function() {
    var flattenAndCompact = function(ax) {
      for (var rx = [], i = 0; i < ax.length; i++) ax[i] instanceof Array ? rx = rx.concat(flattenAndCompact(ax[i])) : ax[i] && rx.push(ax[i]);
      return rx
    };
    Date.Grammar = {}, Date.Translator = {
      hour: function(s) {
        return function() {
          this.hour = Number(s)
        }
      },
      minute: function(s) {
        return function() {
          this.minute = Number(s)
        }
      },
      second: function(s) {
        return function() {
          this.second = Number(s)
        }
      },
      meridian: function(s) {
        return function() {
          this.meridian = s.slice(0, 1).toLowerCase()
        }
      },
      timezone: function(s) {
        return function() {
          var n = s.replace(/[^\d\+\-]/g, "");
          n.length ? this.timezoneOffset = Number(n) : this.timezone = s.toLowerCase()
        }
      },
      day: function(x) {
        var s = x[0];
        return function() {
          this.day = Number(s.match(/\d+/)[0])
        }
      },
      month: function(s) {
        return function() {
          this.month = 3 == s.length ? Date.getMonthNumberFromName(s) : Number(s) - 1
        }
      },
      year: function(s) {
        return function() {
          var n = Number(s);
          this.year = s.length > 2 ? n : n + (n + 2e3 < Date.CultureInfo.twoDigitYearMax ? 2e3 : 1900)
        }
      },
      rday: function(s) {
        return function() {
          switch (s) {
            case "yesterday":
              this.days = -1;
              break;
            case "tomorrow":
              this.days = 1;
              break;
            case "today":
              this.days = 0;
              break;
            case "now":
              this.days = 0, this.now = !0
          }
        }
      },
      finishExact: function(x) {
        x = x instanceof Array ? x : [x];
        var now = new Date;
        this.year = now.getFullYear(), this.month = now.getMonth(), this.day = 1, this.hour = 0, this.minute = 0, this.second = 0;
        for (var i = 0; i < x.length; i++) x[i] && x[i].call(this);
        if (this.hour = "p" == this.meridian && this.hour < 13 ? this.hour + 12 : this.hour, this.day > Date.getDaysInMonth(this.year, this.month)) throw new RangeError(this.day + " is not a valid value for days.");
        var r = new Date(this.year, this.month, this.day, this.hour, this.minute, this.second);
        return this.timezone ? r.set({
          timezone: this.timezone
        }) : this.timezoneOffset && r.set({
          timezoneOffset: this.timezoneOffset
        }), r
      },
      finish: function(x) {
        if (x = x instanceof Array ? flattenAndCompact(x) : [x], 0 === x.length) return null;
        for (var i = 0; i < x.length; i++) "function" == typeof x[i] && x[i].call(this);
        if (this.now) return new Date;
        var today = Date.today(),
          expression = !(null == this.days && !this.orient && !this.operator);
        if (expression) {
          var gap, mod, orient;
          return orient = "past" == this.orient || "subtract" == this.operator ? -1 : 1, this.weekday && (this.unit = "day", gap = Date.getDayNumberFromName(this.weekday) - today.getDay(), mod = 7, this.days = gap ? (gap + orient * mod) % mod : orient * mod), this.month && (this.unit = "month", gap = this.month - today.getMonth(), mod = 12, this.months = gap ? (gap + orient * mod) % mod : orient * mod, this.month = null), this.unit || (this.unit = "day"), (null == this[this.unit + "s"] || null != this.operator) && (this.value || (this.value = 1), "week" == this.unit && (this.unit = "day", this.value = 7 * this.value), this[this.unit + "s"] = this.value * orient), today.add(this)
        }
        return this.meridian && this.hour && (this.hour = this.hour < 13 && "p" == this.meridian ? this.hour + 12 : this.hour), this.weekday && !this.day && (this.day = today.addDays(Date.getDayNumberFromName(this.weekday) - today.getDay()).getDate()), this.month && !this.day && (this.day = 1), today.set(this)
      }
    };
    var _fn, _ = Date.Parsing.Operators,
      g = Date.Grammar,
      t = Date.Translator;
    g.datePartDelimiter = _.rtoken(/^([\s\-\.\,\/\x27]+)/), g.timePartDelimiter = _.stoken(":"), g.whiteSpace = _.rtoken(/^\s*/), g.generalDelimiter = _.rtoken(/^(([\s\,]|at|on)+)/);
    var _C = {};
    g.ctoken = function(keys) {
      var fn = _C[keys];
      if (!fn) {
        for (var c = Date.CultureInfo.regexPatterns, kx = keys.split(/\s+/), px = [], i = 0; i < kx.length; i++) px.push(_.replace(_.rtoken(c[kx[i]]), kx[i]));
        fn = _C[keys] = _.any.apply(null, px)
      }
      return fn
    }, g.ctoken2 = function(key) {
      return _.rtoken(Date.CultureInfo.regexPatterns[key])
    }, g.h = _.cache(_.process(_.rtoken(/^(0[0-9]|1[0-2]|[1-9])/), t.hour)), g.hh = _.cache(_.process(_.rtoken(/^(0[0-9]|1[0-2])/), t.hour)), g.H = _.cache(_.process(_.rtoken(/^([0-1][0-9]|2[0-3]|[0-9])/), t.hour)), g.HH = _.cache(_.process(_.rtoken(/^([0-1][0-9]|2[0-3])/), t.hour)), g.m = _.cache(_.process(_.rtoken(/^([0-5][0-9]|[0-9])/), t.minute)), g.mm = _.cache(_.process(_.rtoken(/^[0-5][0-9]/), t.minute)), g.s = _.cache(_.process(_.rtoken(/^([0-5][0-9]|[0-9])/), t.second)), g.ss = _.cache(_.process(_.rtoken(/^[0-5][0-9]/), t.second)), g.hms = _.cache(_.sequence([g.H, g.mm, g.ss], g.timePartDelimiter)), g.t = _.cache(_.process(g.ctoken2("shortMeridian"), t.meridian)), g.tt = _.cache(_.process(g.ctoken2("longMeridian"), t.meridian)), g.z = _.cache(_.process(_.rtoken(/^(\+|\-)?\s*\d\d\d\d?/), t.timezone)), g.zz = _.cache(_.process(_.rtoken(/^(\+|\-)\s*\d\d\d\d/), t.timezone)), g.zzz = _.cache(_.process(g.ctoken2("timezone"), t.timezone)), g.timeSuffix = _.each(_.ignore(g.whiteSpace), _.set([g.tt, g.zzz])), g.time = _.each(_.optional(_.ignore(_.stoken("T"))), g.hms, g.timeSuffix), g.d = _.cache(_.process(_.each(_.rtoken(/^([0-2]\d|3[0-1]|\d)/), _.optional(g.ctoken2("ordinalSuffix"))), t.day)), g.dd = _.cache(_.process(_.each(_.rtoken(/^([0-2]\d|3[0-1])/), _.optional(g.ctoken2("ordinalSuffix"))), t.day)), g.ddd = g.dddd = _.cache(_.process(g.ctoken("sun mon tue wed thu fri sat"), function(s) {
      return function() {
        this.weekday = s
      }
    })), g.M = _.cache(_.process(_.rtoken(/^(1[0-2]|0\d|\d)/), t.month)), g.MM = _.cache(_.process(_.rtoken(/^(1[0-2]|0\d)/), t.month)), g.MMM = g.MMMM = _.cache(_.process(g.ctoken("jan feb mar apr may jun jul aug sep oct nov dec"), t.month)), g.y = _.cache(_.process(_.rtoken(/^(\d\d?)/), t.year)), g.yy = _.cache(_.process(_.rtoken(/^(\d\d)/), t.year)), g.yyy = _.cache(_.process(_.rtoken(/^(\d\d?\d?\d?)/), t.year)), g.yyyy = _.cache(_.process(_.rtoken(/^(\d\d\d\d)/), t.year)), _fn = function() {
      return _.each(_.any.apply(null, arguments), _.not(g.ctoken2("timeContext")))
    }, g.day = _fn(g.d, g.dd), g.month = _fn(g.M, g.MMM), g.year = _fn(g.yyyy, g.yy), g.orientation = _.process(g.ctoken("past future"), function(s) {
      return function() {
        this.orient = s
      }
    }), g.operator = _.process(g.ctoken("add subtract"), function(s) {
      return function() {
        this.operator = s
      }
    }), g.rday = _.process(g.ctoken("yesterday tomorrow today now"), t.rday), g.unit = _.process(g.ctoken("minute hour day week month year"), function(s) {
      return function() {
        this.unit = s
      }
    }), g.value = _.process(_.rtoken(/^\d\d?(st|nd|rd|th)?/), function(s) {
      return function() {
        this.value = s.replace(/\D/g, "")
      }
    }), g.expression = _.set([g.rday, g.operator, g.value, g.unit, g.orientation, g.ddd, g.MMM]), _fn = function() {
      return _.set(arguments, g.datePartDelimiter)
    }, g.mdy = _fn(g.ddd, g.month, g.day, g.year), g.ymd = _fn(g.ddd, g.year, g.month, g.day), g.dmy = _fn(g.ddd, g.day, g.month, g.year), g.date = function(s) {
      return (g[Date.CultureInfo.dateElementOrder] || g.mdy).call(this, s)
    }, g.format = _.process(_.many(_.any(_.process(_.rtoken(/^(dd?d?d?|MM?M?M?|yy?y?y?|hh?|HH?|mm?|ss?|tt?|zz?z?)/), function(fmt) {
      if (g[fmt]) return g[fmt];
      throw Date.Parsing.Exception(fmt)
    }), _.process(_.rtoken(/^[^dMyhHmstz]+/), function(s) {
      return _.ignore(_.stoken(s))
    }))), function(rules) {
      return _.process(_.each.apply(null, rules), t.finishExact)
    });
    var _F = {},
      _get = function(f) {
        return _F[f] = _F[f] || g.format(f)[0]
      };
    g.formats = function(fx) {
      if (fx instanceof Array) {
        for (var rx = [], i = 0; i < fx.length; i++) rx.push(_get(fx[i]));
        return _.any.apply(null, rx)
      }
      return _get(fx)
    }, g._formats = g.formats(["yyyy-MM-ddTHH:mm:ss", "ddd, MMM dd, yyyy H:mm:ss tt", "ddd MMM d yyyy HH:mm:ss zzz", "d"]), g._start = _.process(_.set([g.date, g.time, g.expression], g.generalDelimiter, g.whiteSpace), t.finish), g.start = function(s) {
      try {
        var r = g._formats.call({}, s);
        if (0 === r[1].length) return r
      } catch (e) {}
      return g._start.call({}, s)
    }
  }(), Date._parse = Date.parse, Date.parse = function(s) {
    var r = null;
    if (!s) return null;
    try {
      r = Date.Grammar.start.call({}, s)
    } catch (e) {
      return null
    }
    return 0 === r[1].length ? r[0] : null
  }, Date.getParseFunction = function(fx) {
    var fn = Date.Grammar.formats(fx);
    return function(s) {
      var r = null;
      try {
        r = fn.call({}, s)
      } catch (e) {
        return null
      }
      return 0 === r[1].length ? r[0] : null
    }
  }, Date.parseExact = function(s, fx) {
    return Date.getParseFunction(fx)(s)
  }, + function($) {
    "use strict";
    var Checkbox = function(element, options) {
      this.$element = $(element), this.options = $.extend({}, $.fn.checkbox.defaults, options), this.$label = this.$element.parent(), this.$icon = this.$label.find("i"), this.$chk = this.$label.find("input[type=checkbox]"), this.setState(this.$chk), this.$chk.on("change", $.proxy(this.itemchecked, this))
    };
    Checkbox.prototype = {
      constructor: Checkbox,
      setState: function($chk) {
        var checked = $chk.is(":checked"),
          disabled = $chk.is(":disabled");
        this.$icon.removeClass("checked").removeClass("disabled"), checked === !0 && this.$icon.addClass("checked"), disabled === !0 && this.$icon.addClass("disabled")
      },
      enable: function() {
        this.$chk.attr("disabled", !1), this.$icon.removeClass("disabled")
      },
      disable: function() {
        this.$chk.attr("disabled", !0), this.$icon.addClass("disabled")
      },
      toggle: function() {
        this.$chk.click()
      },
      itemchecked: function(e) {
        var chk = $(e.target);
        this.setState(chk)
      }
    }, $.fn.checkbox = function(option, value) {
      var methodReturn, $set = this.each(function() {
        var $this = $(this),
          data = $this.data("checkbox"),
          options = "object" == typeof option && option;
        data || $this.data("checkbox", data = new Checkbox(this, options)), "string" == typeof option && (methodReturn = data[option](value))
      });
      return void 0 === methodReturn ? $set : methodReturn
    }, $.fn.checkbox.defaults = {}, $.fn.checkbox.Constructor = Checkbox, $(function() {
      $(window).on("load", function() {
        $(".checkbox-custom > input[type=checkbox]").each(function() {
          var $this = $(this);
          $this.data("checkbox") || $this.checkbox($this.data())
        })
      })
    })
  }(window.jQuery), + function($) {
    "use strict";

    function fuelTextExactCI(elem, text) {
      return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase() === (text || "").toLowerCase()
    }
    $.expr[":"].fuelTextExactCI = $.expr.createPseudo ? $.expr.createPseudo(function(text) {
      return function(elem) {
        return fuelTextExactCI(elem, text)
      }
    }) : function(elem, i, match) {
      return fuelTextExactCI(elem, match[3])
    }
  }(window.jQuery), + function($) {
    "use strict";
    var Combobox = function(element, options) {
      this.$element = $(element), this.options = $.extend({}, $.fn.combobox.defaults, options), this.$element.on("click", "a", $.proxy(this.itemclicked, this)), this.$element.on("change", "input", $.proxy(this.inputchanged, this)), this.$input = this.$element.find("input"), this.$button = this.$element.find(".btn"), this.setDefaultSelection()
    };
    Combobox.prototype = {
      constructor: Combobox,
      selectedItem: function() {
        var item = this.$selectedItem,
          data = {};
        if (item) {
          var txt = this.$selectedItem.text();
          data = $.extend({
            text: txt
          }, this.$selectedItem.data())
        } else data = {
          text: this.$input.val()
        };
        return data
      },
      selectByText: function(text) {
        var selector = "li:fuelTextExactCI(" + text + ")";
        this.selectBySelector(selector)
      },
      selectByValue: function(value) {
        var selector = 'li[data-value="' + value + '"]';
        this.selectBySelector(selector)
      },
      selectByIndex: function(index) {
        var selector = "li:eq(" + index + ")";
        this.selectBySelector(selector)
      },
      selectBySelector: function(selector) {
        var $item = this.$element.find(selector);
        "undefined" != typeof $item[0] ? (this.$selectedItem = $item, this.$input.val(this.$selectedItem.text())) : this.$selectedItem = null
      },
      setDefaultSelection: function() {
        var selector = "li[data-selected=true]:first",
          item = this.$element.find(selector);
        item.length > 0 && (this.selectBySelector(selector), item.removeData("selected"), item.removeAttr("data-selected"))
      },
      enable: function() {
        this.$input.removeAttr("disabled"), this.$button.removeClass("disabled")
      },
      disable: function() {
        this.$input.attr("disabled", !0), this.$button.addClass("disabled")
      },
      itemclicked: function(e) {
        this.$selectedItem = $(e.target).parent(), this.$input.val(this.$selectedItem.text()).trigger("change", {
          synthetic: !0
        });
        var data = this.selectedItem();
        this.$element.trigger("changed", data), e.preventDefault()
      },
      inputchanged: function(e, extra) {
        if (!extra || !extra.synthetic) {
          var val = $(e.target).val();
          this.selectByText(val);
          var data = this.selectedItem();
          0 === data.text.length && (data = {
            text: val
          }), this.$element.trigger("changed", data)
        }
      }
    }, $.fn.combobox = function(option, value) {
      var methodReturn, $set = this.each(function() {
        var $this = $(this),
          data = $this.data("combobox"),
          options = "object" == typeof option && option;
        data || $this.data("combobox", data = new Combobox(this, options)), "string" == typeof option && (methodReturn = data[option](value))
      });
      return void 0 === methodReturn ? $set : methodReturn
    }, $.fn.combobox.defaults = {}, $.fn.combobox.Constructor = Combobox, $(function() {
      $(window).on("load", function() {
        $(".combobox").each(function() {
          var $this = $(this);
          $this.data("combobox") || $this.combobox($this.data())
        })
      }), $("body").on("mousedown.combobox.data-api", ".combobox", function(e) {
        var $this = $(this);
        $this.data("combobox") || $this.combobox($this.data())
      })
    })
  }(window.jQuery), + function($) {
    "use strict";
    var SORTED_HEADER_OFFSET = 22,
      Datagrid = function(element, options) {
        this.$element = $(element), this.$thead = this.$element.find("thead"), this.$tfoot = this.$element.find("tfoot"), this.$footer = this.$element.find("tfoot th"), this.$footerchildren = this.$footer.children().show().css("visibility", "hidden"), this.$topheader = this.$element.find("thead th"), this.$searchcontrol = this.$element.find(".datagrid-search"), this.$filtercontrol = this.$element.find(".filter"), this.$pagesize = this.$element.find(".grid-pagesize"), this.$pageinput = this.$element.find(".grid-pager input"), this.$pagedropdown = this.$element.find(".grid-pager .dropdown-menu"), this.$prevpagebtn = this.$element.find(".grid-prevpage"), this.$nextpagebtn = this.$element.find(".grid-nextpage"), this.$pageslabel = this.$element.find(".grid-pages"), this.$countlabel = this.$element.find(".grid-count"), this.$startlabel = this.$element.find(".grid-start"), this.$endlabel = this.$element.find(".grid-end"), this.$tbody = $("<tbody>").insertAfter(this.$thead), this.$colheader = $("<tr>").appendTo(this.$thead), this.options = $.extend(!0, {}, $.fn.datagrid.defaults, options), this.$pagesize.hasClass("select") ? this.options.dataOptions.pageSize = parseInt(this.$pagesize.select("selectedItem").value, 10) : this.options.dataOptions.pageSize = parseInt(this.$pagesize.val(), 10), this.$searchcontrol.length <= 0 && (this.$searchcontrol = this.$element.find(".search")), this.columns = this.options.dataSource.columns(), this.$nextpagebtn.on("click", $.proxy(this.next, this)), this.$prevpagebtn.on("click", $.proxy(this.previous, this)), this.$searchcontrol.on("searched cleared", $.proxy(this.searchChanged, this)), this.$filtercontrol.on("changed", $.proxy(this.filterChanged, this)), this.$colheader.on("click", "th", $.proxy(this.headerClicked, this)), this.$pagesize.hasClass("select") ? this.$pagesize.on("changed", $.proxy(this.pagesizeChanged, this)) : this.$pagesize.on("change", $.proxy(this.pagesizeChanged, this)), this.$pageinput.on("change", $.proxy(this.pageChanged, this)), this.renderColumns(), this.options.stretchHeight && this.initStretchHeight(), this.renderData()
      };
    Datagrid.prototype = {
      constructor: Datagrid,
      renderColumns: function() {
        var self = this;
        this.$footer.attr("colspan", this.columns.length), this.$topheader.attr("colspan", this.columns.length);
        var colHTML = "";
        $.each(this.columns, function(index, column) {
          colHTML += '<th data-property="' + column.property + '"', column.sortable && (colHTML += ' class="sortable"'), colHTML += ">" + column.label + "</th>"
        }), self.$colheader.append(colHTML)
      },
      updateColumns: function($target, direction) {
        this._updateColumns(this.$colheader, $target, direction), this.$sizingHeader && this._updateColumns(this.$sizingHeader, this.$sizingHeader.find("th").eq($target.index()), direction)
      },
      _updateColumns: function($header, $target, direction) {
        var className = "asc" === direction ? "fa fa-caret-up" : "fa fa-caret-down";
        $header.find("i.datagrid-sort").remove(), $header.find("th").removeClass("sorted"), $("<i>").addClass(className + " datagrid-sort").appendTo($target), $target.addClass("sorted")
      },
      updatePageDropdown: function(data) {
        for (var pageHTML = "", i = 1; i <= data.pages; i++) pageHTML += "<li><a>" + i + "</a></li>";
        this.$pagedropdown.html(pageHTML)
      },
      updatePageButtons: function(data) {
        1 === data.page ? this.$prevpagebtn.attr("disabled", "disabled") : this.$prevpagebtn.removeAttr("disabled"), data.page === data.pages ? this.$nextpagebtn.attr("disabled", "disabled") : this.$nextpagebtn.removeAttr("disabled")
      },
      renderData: function() {
        var self = this;
        this.$tbody.html(this.placeholderRowHTML(this.options.loadingHTML)), this.options.dataSource.data(this.options.dataOptions, function(data) {
          var itemdesc = 1 === data.count ? self.options.itemText : self.options.itemsText,
            rowHTML = "";
          self.$footerchildren.css("visibility", function() {
            return data.count > 0 ? "visible" : "hidden"
          }), self.$pageinput.val(data.page), self.$pageslabel.text(data.pages), self.$countlabel.text(data.count + " " + itemdesc), self.$startlabel.text(data.start), self.$endlabel.text(data.end), self.updatePageDropdown(data), self.updatePageButtons(data), $.each(data.data, function(index, row) {
            rowHTML += "<tr>", $.each(self.columns, function(index, column) {
              rowHTML += "<td>" + row[column.property] + "</td>"
            }), rowHTML += "</tr>"
          }), rowHTML || (rowHTML = self.placeholderRowHTML("0 " + self.options.itemsText)), self.$tbody.html(rowHTML), self.stretchHeight(), self.$element.trigger("loaded")
        })
      },
      placeholderRowHTML: function(content) {
        return '<tr><td style="text-align:center;padding:20px;border-bottom:none;" colspan="' + this.columns.length + '">' + content + "</td></tr>"
      },
      headerClicked: function(e) {
        var $target = $(e.target);
        if ($target.hasClass("sortable")) {
          var direction = this.options.dataOptions.sortDirection,
            sort = this.options.dataOptions.sortProperty,
            property = $target.data("property");
          sort === property ? this.options.dataOptions.sortDirection = "asc" === direction ? "desc" : "asc" : (this.options.dataOptions.sortDirection = "asc", this.options.dataOptions.sortProperty = property), this.options.dataOptions.pageIndex = 0, this.updateColumns($target, this.options.dataOptions.sortDirection), this.renderData()
        }
      },
      pagesizeChanged: function(e, pageSize) {
        pageSize ? this.options.dataOptions.pageSize = parseInt(pageSize.value, 10) : this.options.dataOptions.pageSize = parseInt($(e.target).val(), 10), this.options.dataOptions.pageIndex = 0, this.renderData()
      },
      pageChanged: function(e) {
        var pageRequested = parseInt($(e.target).val(), 10);
        pageRequested = isNaN(pageRequested) ? 1 : pageRequested;
        var maxPages = this.$pageslabel.text();
        this.options.dataOptions.pageIndex = pageRequested > maxPages ? maxPages - 1 : pageRequested - 1, this.renderData()
      },
      searchChanged: function(e, search) {
        this.options.dataOptions.search = search, this.options.dataOptions.pageIndex = 0, this.renderData()
      },
      filterChanged: function(e, filter) {
        this.options.dataOptions.filter = filter, this.options.dataOptions.pageIndex = 0, this.renderData()
      },
      previous: function() {
        this.options.dataOptions.pageIndex--, this.renderData()
      },
      next: function() {
        this.options.dataOptions.pageIndex++, this.renderData()
      },
      reload: function() {
        this.options.dataOptions.pageIndex = 0, this.renderData()
      },
      initStretchHeight: function() {
        this.$gridContainer = this.$element.parent(), this.$element.wrap('<div class="datagrid-stretch-wrapper">'), this.$stretchWrapper = this.$element.parent(), this.$headerTable = $("<table>").attr("class", this.$element.attr("class")), this.$footerTable = this.$headerTable.clone(), this.$headerTable.prependTo(this.$gridContainer).addClass("datagrid-stretch-header"), this.$thead.detach().appendTo(this.$headerTable), this.$sizingHeader = this.$thead.clone(), this.$sizingHeader.find("tr:first").remove(), this.$footerTable.appendTo(this.$gridContainer).addClass("datagrid-stretch-footer"), this.$tfoot.detach().appendTo(this.$footerTable)
      },
      stretchHeight: function() {
        if (this.$gridContainer) {
          this.setColumnWidths();
          var targetHeight = this.$gridContainer.height(),
            headerHeight = this.$headerTable.outerHeight(),
            footerHeight = this.$footerTable.outerHeight(),
            overhead = headerHeight + footerHeight;
          this.$stretchWrapper.height(targetHeight - overhead);
        }
      },
      setColumnWidths: function() {
        function matchSizingCellWidth(i, el) {
          if (i !== columnCount - 1) {
            var $el = $(el),
              $sourceCell = $sizingCells.eq(i),
              width = $sourceCell.width();
            $sourceCell.hasClass("sorted") && "TD" === $el.prop("tagName") && (width += SORTED_HEADER_OFFSET), $el.width(width)
          }
        }
        if (this.$sizingHeader) {
          this.$element.prepend(this.$sizingHeader);
          var $sizingCells = this.$sizingHeader.find("th"),
            columnCount = $sizingCells.length;
          this.$colheader.find("th").each(matchSizingCellWidth), this.$tbody.find("tr:first > td").each(matchSizingCellWidth), this.$sizingHeader.detach()
        }
      }
    }, $.fn.datagrid = function(option) {
      return this.each(function() {
        var $this = $(this),
          data = $this.data("datagrid"),
          options = "object" == typeof option && option;
        data || $this.data("datagrid", data = new Datagrid(this, options)), "string" == typeof option && data[option]()
      })
    }, $.fn.datagrid.defaults = {
      dataOptions: {
        pageIndex: 0,
        pageSize: 10
      },
      loadingHTML: '<div class="progress progress-striped active" style="width:50%;margin:auto;"><div class="bar" style="width:100%;"></div></div>',
      itemsText: "items",
      itemText: "item"
    }, $.fn.datagrid.Constructor = Datagrid
  }(window.jQuery), + function($) {
    "use strict";
    var Pillbox = function(element, options) {
      this.$element = $(element), this.options = $.extend({}, $.fn.pillbox.defaults, options), this.$element.on("click", "li", $.proxy(this.itemclicked, this))
    };
    Pillbox.prototype = {
      constructor: Pillbox,
      items: function() {
        return this.$element.find("li").map(function() {
          var $this = $(this);
          return $.extend({
            text: $this.text()
          }, $this.data())
        }).get()
      },
      itemclicked: function(e) {
        $(e.currentTarget).remove(), e.preventDefault()
      }
    }, $.fn.pillbox = function(option) {
      var methodReturn, $set = this.each(function() {
        var $this = $(this),
          data = $this.data("pillbox"),
          options = "object" == typeof option && option;
        data || $this.data("pillbox", data = new Pillbox(this, options)), "string" == typeof option && (methodReturn = data[option]())
      });
      return void 0 === methodReturn ? $set : methodReturn
    }, $.fn.pillbox.defaults = {}, $.fn.pillbox.Constructor = Pillbox, $(function() {
      $("body").on("mousedown.pillbox.data-api", ".pillbox", function(e) {
        var $this = $(this);
        $this.data("pillbox") || $this.pillbox($this.data())
      })
    })
  }(window.jQuery), + function($) {
    "use strict";
    var Radio = function(element, options) {
      this.$element = $(element), this.options = $.extend({}, $.fn.radio.defaults, options), this.$label = this.$element.parent(), this.$icon = this.$label.find("i"), this.$radio = this.$label.find("input[type=radio]"), this.groupName = this.$radio.attr("name"), this.setState(this.$radio), this.$radio.on("change", $.proxy(this.itemchecked, this))
    };
    Radio.prototype = {
      constructor: Radio,
      setState: function($radio, resetGroupState) {
        var checked = $radio.is(":checked"),
          disabled = $radio.is(":disabled");
        checked === !0 && this.$icon.addClass("checked"), disabled === !0 && this.$icon.addClass("disabled")
      },
      resetGroup: function() {
        $("input[name=" + this.groupName + "]").next().removeClass("checked")
      },
      enable: function() {
        this.$radio.attr("disabled", !1), this.$icon.removeClass("disabled")
      },
      disable: function() {
        this.$radio.attr("disabled", !0), this.$icon.addClass("disabled")
      },
      itemchecked: function(e) {
        var radio = $(e.target);
        this.resetGroup(), this.setState(radio)
      }
    }, $.fn.radio = function(option, value) {
      var methodReturn, $set = this.each(function() {
        var $this = $(this),
          data = $this.data("radio"),
          options = "object" == typeof option && option;
        data || $this.data("radio", data = new Radio(this, options)), "string" == typeof option && (methodReturn = data[option](value))
      });
      return void 0 === methodReturn ? $set : methodReturn
    }, $.fn.radio.defaults = {}, $.fn.radio.Constructor = Radio, $(function() {
      $(window).on("load", function() {
        $(".radio-custom > input[type=radio]").each(function() {
          var $this = $(this);
          $this.data("radio") || $this.radio($this.data())
        })
      })
    })
  }(window.jQuery), + function($) {
    "use strict";
    var Search = function(element, options) {
      this.$element = $(element), this.options = $.extend({}, $.fn.search.defaults, options), this.$button = this.$element.find("button").on("click", $.proxy(this.buttonclicked, this)), this.$input = this.$element.find("input").on("keydown", $.proxy(this.keypress, this)).on("keyup", $.proxy(this.keypressed, this)), this.$icon = this.$element.find("i"), this.activeSearch = ""
    };
    Search.prototype = {
      constructor: Search,
      search: function(searchText) {
        this.$icon.attr("class", "fa fa-times"), this.activeSearch = searchText, this.$element.trigger("searched", searchText)
      },
      clear: function() {
        this.$icon.attr("class", "fa fa-search"), this.activeSearch = "", this.$input.val(""), this.$element.trigger("cleared")
      },
      action: function() {
        var val = this.$input.val(),
          inputEmptyOrUnchanged = "" === val || val === this.activeSearch;
        this.activeSearch && inputEmptyOrUnchanged ? this.clear() : val && this.search(val)
      },
      buttonclicked: function(e) {
        e.preventDefault(), $(e.currentTarget).is(".disabled, :disabled") || this.action()
      },
      keypress: function(e) {
        13 === e.which && e.preventDefault()
      },
      keypressed: function(e) {
        var val, inputPresentAndUnchanged;
        13 === e.which ? (e.preventDefault(), this.action()) : (val = this.$input.val(), inputPresentAndUnchanged = val && val === this.activeSearch, this.$icon.attr("class", inputPresentAndUnchanged ? "fa fa-times" : "fa fa-search"))
      },
      disable: function() {
        this.$input.attr("disabled", "disabled"), this.$button.addClass("disabled")
      },
      enable: function() {
        this.$input.removeAttr("disabled"), this.$button.removeClass("disabled")
      }
    }, $.fn.search = function(option) {
      return this.each(function() {
        var $this = $(this),
          data = $this.data("search"),
          options = "object" == typeof option && option;
        data || $this.data("search", data = new Search(this, options)), "string" == typeof option && data[option]()
      })
    }, $.fn.search.defaults = {}, $.fn.search.Constructor = Search, $(function() {
      $("body").on("mousedown.search.data-api", ".search", function() {
        var $this = $(this);
        $this.data("search") || $this.search($this.data())
      })
    })
  }(window.jQuery), + function($) {
    "use strict";
    var Spinner = function(element, options) {
      this.$element = $(element), this.options = $.extend({}, $.fn.spinner.defaults, options), this.$input = this.$element.find(".spinner-input"), this.$element.on("keyup", this.$input, $.proxy(this.change, this)), this.options.hold ? (this.$element.on("mousedown", ".spinner-up", $.proxy(function() {
        this.startSpin(!0)
      }, this)), this.$element.on("mouseup", ".spinner-up, .spinner-down", $.proxy(this.stopSpin, this)), this.$element.on("mouseout", ".spinner-up, .spinner-down", $.proxy(this.stopSpin, this)), this.$element.on("mousedown", ".spinner-down", $.proxy(function() {
        this.startSpin(!1)
      }, this))) : (this.$element.on("click", ".spinner-up", $.proxy(function() {
        this.step(!0)
      }, this)), this.$element.on("click", ".spinner-down", $.proxy(function() {
        this.step(!1)
      }, this))), this.switches = {
        count: 1,
        enabled: !0
      }, "medium" === this.options.speed ? this.switches.speed = 300 : "fast" === this.options.speed ? this.switches.speed = 100 : this.switches.speed = 500, this.lastValue = null, this.render(), this.options.disabled && this.disable()
    };
    Spinner.prototype = {
      constructor: Spinner,
      render: function() {
        this.$input.val(this.options.value), this.$input.attr("maxlength", (this.options.max + "").split("").length)
      },
      change: function() {
        var newVal = this.$input.val();
        newVal / 1 ? this.options.value = newVal / 1 : (newVal = newVal.replace(/[^0-9]/g, ""), this.$input.val(newVal), this.options.value = newVal / 1), this.triggerChangedEvent()
      },
      stopSpin: function() {
        clearTimeout(this.switches.timeout), this.switches.count = 1, this.triggerChangedEvent()
      },
      triggerChangedEvent: function() {
        var currentValue = this.value();
        currentValue !== this.lastValue && (this.lastValue = currentValue, this.$element.trigger("changed", currentValue), this.$element.trigger("change"))
      },
      startSpin: function(type) {
        if (!this.options.disabled) {
          var divisor = this.switches.count;
          1 === divisor ? (this.step(type), divisor = 1) : divisor = 3 > divisor ? 1.5 : 8 > divisor ? 2.5 : 4, this.switches.timeout = setTimeout($.proxy(function() {
            this.iterator(type)
          }, this), this.switches.speed / divisor), this.switches.count++
        }
      },
      iterator: function(type) {
        this.step(type), this.startSpin(type)
      },
      step: function(dir) {
        var curValue = this.options.value,
          limValue = dir ? this.options.max : this.options.min;
        if (dir ? limValue > curValue : curValue > limValue) {
          var newVal = curValue + (dir ? 1 : -1) * this.options.step;
          (dir ? newVal > limValue : limValue > newVal) ? this.value(limValue): this.value(newVal)
        }
      },
      value: function(value) {
        return !isNaN(parseFloat(value)) && isFinite(value) ? (value = parseFloat(value), this.options.value = value, this.$input.val(value), this) : this.options.value
      },
      disable: function() {
        this.options.disabled = !0, this.$input.attr("disabled", ""), this.$element.find("button").addClass("disabled")
      },
      enable: function() {
        this.options.disabled = !1, this.$input.removeAttr("disabled"), this.$element.find("button").removeClass("disabled")
      }
    }, $.fn.spinner = function(option, value) {
      var methodReturn, $set = this.each(function() {
        var $this = $(this),
          data = $this.data("spinner"),
          options = "object" == typeof option && option;
        data || $this.data("spinner", data = new Spinner(this, options)), "string" == typeof option && (methodReturn = data[option](value))
      });
      return void 0 === methodReturn ? $set : methodReturn
    }, $.fn.spinner.defaults = {
      value: 1,
      min: 1,
      max: 999,
      step: 1,
      hold: !0,
      speed: "medium",
      disabled: !1
    }, $.fn.spinner.Constructor = Spinner, $(function() {
      $("body").on("mousedown.spinner.data-api", ".spinner", function(e) {
        var $this = $(this);
        $this.data("spinner") || $this.spinner($this.data())
      })
    })
  }(window.jQuery), + function($) {
    "use strict";
    var Select = function(element, options) {
      this.$element = $(element), this.options = $.extend({}, $.fn.select.defaults, options), this.$element.on("click", "a", $.proxy(this.itemclicked, this)), this.$button = this.$element.find(".btn"), this.$label = this.$element.find(".dropdown-label"), this.setDefaultSelection(), "auto" === options.resize && this.resize()
    };
    Select.prototype = {
      constructor: Select,
      itemclicked: function(e) {
        this.$selectedItem = $(e.target).parent(), this.$label.text(this.$selectedItem.text());
        var data = this.selectedItem();
        this.$element.trigger("changed", data), e.preventDefault()
      },
      resize: function() {
        var el = $("#selectTextSize")[0];
        el || $("<div/>").attr({
          id: "selectTextSize"
        }).appendTo("body");
        var width = 0,
          newWidth = 0;
        this.$element.find("a").each(function() {
          var $this = $(this),
            txt = $this.text(),
            $txtSize = $("#selectTextSize");
          $txtSize.text(txt), newWidth = $txtSize.outerWidth(), newWidth > width && (width = newWidth)
        }), this.$label.width(width)
      },
      selectedItem: function() {
        var txt = this.$selectedItem.text();
        return $.extend({
          text: txt
        }, this.$selectedItem.data())
      },
      selectByText: function(text) {
        var selector = "li a:fuelTextExactCI(" + text + ")";
        this.selectBySelector(selector)
      },
      selectByValue: function(value) {
        var selector = 'li[data-value="' + value + '"]';
        this.selectBySelector(selector)
      },
      selectByIndex: function(index) {
        var selector = "li:eq(" + index + ")";
        this.selectBySelector(selector)
      },
      selectBySelector: function(selector) {
        var item = this.$element.find(selector);
        this.$selectedItem = item, this.$label.text(this.$selectedItem.text())
      },
      setDefaultSelection: function() {
        var selector = "li[data-selected=true]:first",
          item = this.$element.find(selector);
        0 === item.length ? this.selectByIndex(0) : (this.selectBySelector(selector), item.removeData("selected"), item.removeAttr("data-selected"))
      },
      enable: function() {
        this.$button.removeClass("disabled")
      },
      disable: function() {
        this.$button.addClass("disabled")
      }
    }, $.fn.select = function(option, value) {
      var methodReturn, $set = this.each(function() {
        var $this = $(this),
          data = $this.data("select"),
          options = "object" == typeof option && option;
        data || $this.data("select", data = new Select(this, options)), "string" == typeof option && (methodReturn = data[option](value))
      });
      return void 0 === methodReturn ? $set : methodReturn
    }, $.fn.select.defaults = {}, $.fn.select.Constructor = Select, $(function() {
      $(window).on("load", function() {
        $(".select").each(function() {
          var $this = $(this);
          $this.data("select") || $this.select($this.data())
        })
      }), $("body").on("mousedown.select.data-api", ".select", function(e) {
        var $this = $(this);
        $this.data("select") || $this.select($this.data())
      })
    })
  }(window.jQuery), + function($) {
    "use strict";
    var Tree = function(element, options) {
      this.$element = $(element), this.options = $.extend({}, $.fn.tree.defaults, options), this.$element.on("click", ".tree-item", $.proxy(function(ev) {
        this.selectItem(ev.currentTarget)
      }, this)), this.$element.on("click", ".tree-folder-header", $.proxy(function(ev) {
        this.selectFolder(ev.currentTarget)
      }, this)), this.render()
    };
    Tree.prototype = {
      constructor: Tree,
      render: function() {
        this.populate(this.$element)
      },
      populate: function($el) {
        var self = this,
          loader = $el.parent().find(".tree-loader:eq(0)");
        loader.show(), this.options.dataSource.data($el.data(), function(items) {
          loader.hide(), $.each(items.data, function(index, value) {
            var $entity;
            "folder" === value.type ? ($entity = self.$element.find(".tree-folder:eq(0)").clone().show(), $entity.find(".tree-folder-name").html(value.name), $entity.find(".tree-loader").html(self.options.loadingHTML), $entity.find(".tree-folder-header").data(value)) : "item" === value.type && ($entity = self.$element.find(".tree-item:eq(0)").clone().show(), $entity.find(".tree-item-name").html(value.name), $entity.data(value)), $el.hasClass("tree-folder-header") ? $el.parent().find(".tree-folder-content:eq(0)").append($entity) : $el.append($entity)
          }), self.$element.trigger("loaded")
        })
      },
      selectItem: function(el) {
        var $el = $(el),
          $all = this.$element.find(".tree-selected"),
          data = [];
        this.options.multiSelect ? $.each($all, function(index, value) {
          var $val = $(value);
          $val[0] !== $el[0] && data.push($(value).data())
        }) : $all[0] !== $el[0] && ($all.removeClass("tree-selected").find("i").removeClass("icon-ok").addClass("tree-dot"), data.push($el.data())), $el.hasClass("tree-selected") ? ($el.removeClass("tree-selected"), $el.find("i").removeClass("icon-ok").addClass("tree-dot")) : ($el.addClass("tree-selected"), $el.find("i").removeClass("tree-dot").addClass("icon-ok"), this.options.multiSelect && data.push($el.data())), data.length && this.$element.trigger("selected", {
          info: data
        })
      },
      selectFolder: function(el) {
        var $el = $(el),
          $par = $el.parent();
        $el.find(".icon-folder-close").length ? ($par.find(".tree-folder-content").children().length ? $par.find(".tree-folder-content:eq(0)").show() : this.populate($el), $par.find(".icon-folder-close:eq(0)").removeClass("icon-folder-close").addClass("icon-folder-open"), this.$element.trigger("opened", $el.data())) : (this.options.cacheItems ? $par.find(".tree-folder-content:eq(0)").hide() : $par.find(".tree-folder-content:eq(0)").empty(), $par.find(".icon-folder-open:eq(0)").removeClass("icon-folder-open").addClass("icon-folder-close"), this.$element.trigger("closed", $el.data()))
      },
      selectedItems: function() {
        var $sel = this.$element.find(".tree-selected"),
          data = [];
        return $.each($sel, function(index, value) {
          data.push($(value).data())
        }), data
      }
    }, $.fn.tree = function(option, value) {
      var methodReturn, $set = this.each(function() {
        var $this = $(this),
          data = $this.data("tree"),
          options = "object" == typeof option && option;
        data || $this.data("tree", data = new Tree(this, options)), "string" == typeof option && (methodReturn = data[option](value))
      });
      return void 0 === methodReturn ? $set : methodReturn
    }, $.fn.tree.defaults = {
      multiSelect: !1,
      loadingHTML: "<div>Loading...</div>",
      cacheItems: !0
    }, $.fn.tree.Constructor = Tree
  }(window.jQuery), + function($) {
    "use strict";
    var Wizard = function(element, options) {
      var kids;
      this.$element = $(element), this.options = $.extend({}, $.fn.wizard.defaults, options), this.currentStep = 1, this.numSteps = this.$element.find("li").length, this.$prevBtn = this.$element.find("button.btn-prev"), this.$nextBtn = this.$element.find("button.btn-next"), kids = this.$nextBtn.children().detach(), this.nextText = $.trim(this.$nextBtn.text()), this.$nextBtn.append(kids), this.$prevBtn.on("click", $.proxy(this.previous, this)), this.$nextBtn.on("click", $.proxy(this.next, this)), this.$element.on("click", "li.complete", $.proxy(this.stepclicked, this))
    };
    Wizard.prototype = {
      constructor: Wizard,
      setState: function() {
        var canMovePrev = this.currentStep > 1,
          firstStep = 1 === this.currentStep,
          lastStep = this.currentStep === this.numSteps;
        this.$prevBtn.attr("disabled", firstStep === !0 || canMovePrev === !1);
        var data = this.$nextBtn.data();
        if (data && data.last && (this.lastText = data.last, "undefined" != typeof this.lastText)) {
          var text = lastStep !== !0 ? this.nextText : this.lastText,
            kids = this.$nextBtn.children().detach();
          this.$nextBtn.text(text).append(kids)
        }
        var $steps = this.$element.find("li");
        $steps.removeClass("active").removeClass("complete"), $steps.find("span.badge").removeClass("badge-info").removeClass("badge-success");
        var prevSelector = "li:lt(" + (this.currentStep - 1) + ")",
          $prevSteps = this.$element.find(prevSelector);
        $prevSteps.addClass("complete"), $prevSteps.find("span.badge").addClass("badge-success");
        var currentSelector = "li:eq(" + (this.currentStep - 1) + ")",
          $currentStep = this.$element.find(currentSelector);
        $currentStep.addClass("active"), $currentStep.find("span.badge").addClass("badge-info");
        var target = $currentStep.data().target;
        $(".step-pane", $(target).parent()).removeClass("active"), $(target).addClass("active"), this.$element.trigger("changed")
      },
      stepclicked: function(e) {
        var li = $(e.currentTarget),
          index = li.parent().find("li").index(li),
          evt = $.Event("stepclick");
        this.$element.trigger(evt, {
          step: index + 1
        }), evt.isDefaultPrevented() || (this.currentStep = index + 1, this.setState())
      },
      previous: function() {
        var canMovePrev = this.currentStep > 1;
        if (canMovePrev) {
          var e = $.Event("change");
          if (this.$element.trigger(e, {
              step: this.currentStep,
              direction: "previous"
            }), e.isDefaultPrevented()) return;
          this.currentStep -= 1, this.setState()
        }
      },
      next: function() {
        var canMoveNext = this.currentStep + 1 <= this.numSteps,
          lastStep = this.currentStep === this.numSteps;
        if (canMoveNext) {
          var e = $.Event("change");
          if (this.$element.trigger(e, {
              step: this.currentStep,
              direction: "next"
            }), e.isDefaultPrevented()) return;
          this.currentStep += 1, this.setState()
        } else lastStep && this.$element.trigger("finished")
      },
      selectedItem: function(val) {
        return {
          step: this.currentStep
        }
      }
    }, $.fn.wizard = function(option, value) {
      var methodReturn, $set = this.each(function() {
        var $this = $(this),
          data = $this.data("wizard"),
          options = "object" == typeof option && option;
        data || $this.data("wizard", data = new Wizard(this, options)), "string" == typeof option && (methodReturn = data[option](value))
      });
      return void 0 === methodReturn ? $set : methodReturn
    }, $.fn.wizard.defaults = {}, $.fn.wizard.Constructor = Wizard
  }(window.jQuery), Kernel = function() {
    function strToArray(str) {
      return isString(str) && (str = [str]), str
    }

    function isString(str) {
      return "[object String]" == Object.prototype.toString.call(str)
    }

    function decorateMethods(obj, proto) {
      var key, method;
      if (obj)
        for (key in obj)
          if (obj.hasOwnProperty(key) || proto) {
            if ("decorateMethod" === key || "decorateMethods" === key) return;
            obj._decoratedMethods || (obj._decoratedMethods = {}), obj._decoratedMethods[key] || (method = obj[key], "function" == typeof method && (obj[key] = core.decorateMethod(obj, key, method), obj._decoratedMethods[key] = !0))
          }
    }

    function mergeParents(module) {
      var parents, parent, merged = {};
      if (module.extend)
        for (parents = strToArray(module.extend), i = 0; i < parents.length; i += 1) parent = parents[i], parent = modules[parent], parent && Kernel.extend(merged, parent, !0);
      return Kernel.extend(merged, module, !0)
    }

    function defineModule(name, Definition) {
      modules[name] = mergeParents(Definition)
    }

    function defineHub(name, Definition) {
      var broadcastCount = {
          event: 0
        },
        callbackCount = {
          event: 0
        },
        totalElapseTime = {
          event: 0
        },
        h = Definition;
      h._internals = {
        type: "hub"
      }, h.id = name, h.broadcast = function(type, data, callback, id) {
        var i, l, size, eventData, start, diff, mutate, e = listeners.event,
          elapseTime = 0,
          listenerData = [];
        if (h.beforeBroadcast && (mutate = h.beforeBroadcast(type, data, id), mutate.type && (type = mutate.type), mutate.data && (data = mutate.data)), l = listeners[type], broadcastCount[type] = broadcastCount[type] || 0, broadcastCount[type] += 1, broadcastCount.event += 1, callbackCount[type] = callbackCount[type] || 0, totalElapseTime[type] = totalElapseTime[type] || 0, l)
          for (i = 0, size = l.length; size > i; i += 1) start = (new Date).getTime(), listeners[type][i].callback(data, id), diff = (new Date).getTime() - start, elapseTime += diff, totalElapseTime[type] += diff, totalElapseTime.event += diff, callbackCount[type] += 1, callbackCount.event += 1, listenerData.push({
            id: listeners[type][i].id,
            elapseTime: diff,
            callback: listeners[type][i].callback
          });
        if (e)
          for (eventData = {
              type: type,
              data: data,
              time: new Date,
              listeners: listenerData,
              broadcastCount: broadcastCount[type],
              callbackCount: callbackCount[type],
              elapseTime: elapseTime,
              totalElapseTime: totalElapseTime[type],
              all: {
                broadcastCount: broadcastCount.event,
                callbackCount: callbackCount.event,
                totalElapseTime: totalElapseTime.event
              }
            }, i = 0, size = e.length; size > i; i += 1) listeners.event[i].callback(eventData);
        callback && callback()
      }, h.authorized = h.authorized || function(type, moduleId) {
        return !0
      }, h.listen = function(type, callback, moduleId) {
        moduleId = moduleId || "hub-" + h.id;
        var i, size, t;
        for (type = strToArray(type), i = 0, size = type.length; size > i; i += 1) t = type[i], h.authorized(type, moduleId) && (listeners[t] = listeners[t] || [], listeners[t].push({
          callback: callback,
          id: moduleId
        }))
      }, h.getStats = function() {
        return {
          broadcastCount: broadcastCount,
          callbackCount: callbackCount,
          totalElapseTime: totalElapseTime
        }
      }, h.share = function(obj) {
        Kernel.extend(h, obj)
      }, decorateMethods(h), hubs[name] = h
    }

    function skipDeepCopy(obj) {
      try {
        if (obj instanceof RegExp) return !0;
        if (obj instanceof Node) return !0;
        if (obj instanceof jQuery) return !0
      } catch (e) {}
      return !1
    }

    function extend(obj1, obj2, deep, proto, mergeArrays) {
      var key, i, l;
      if (null === obj2) return obj1;
      "object" == typeof obj1 && obj1 || (obj1 = obj2 instanceof Array ? [] : {}), !obj1._internals || "module" !== obj1._internals.type && "hub" !== obj1._internals.type || (deep = !0, decorateMethods(obj1, !0)), obj1 instanceof Array && obj2 instanceof Array && !mergeArrays && (obj1 = []);
      for (key in obj2)
        if (obj2.hasOwnProperty(key) || proto) {
          if (obj1[key] === obj2[key]) continue;
          if ("_internals" === key) continue;
          if ("_parent" === key) continue;
          if ("_children" === key) continue;
          if (obj1 === obj2[key]) continue;
          if ("hub" === key && obj2[key]._internals && "hub" === obj2[key]._internals.type) continue;
          if (skipDeepCopy(obj2[key])) {
            obj1[key] = obj2[key];
            continue
          }
          if (deep && "object" == typeof obj2[key]) obj1[key] = Kernel.extend(obj1[key], obj2[key], deep, proto, mergeArrays);
          else {
            if (obj1._internals && "Kernel" === obj1._internals.type) switch (key) {
              case "extend":
              case "decorateMethods":
              case "module":
              case "register":
              case "hub":
              case "start":
              case "stop":
              case "version":
              case "_internals":
                throw "You can't extend '" + key + "', it's part of Kernel's base functionality."
            } else if (obj1._internals && "module" === obj1._internals.type && "id" === key) throw "You can't overwrite a module instance id. [" + obj1.id + "]";
            if (obj1 instanceof Array && obj2 instanceof Array && mergeArrays) {
              for (i = 0, l = obj1.length; l > i; i += 1) obj1[i] !== obj2[key];
              obj1.push(obj2[key])
            } else obj1[key] = obj2[key]
          }
        } return obj1
    }

    function registerModule(id, type, hub, config, core) {
      var instance, hub = hub || defaultHub;
      registered[id] = {
        hub: hubs[hub],
        started: !1,
        Definition: modules[type]
      };
      try {
        instance = Kernel.extend(mergeParents(registered[id].Definition), registered[id].Definition, !0)
      } catch (e) {
        throw "Couldn't register module: [" + id + "] - missing or broken Definition: " + e.message
      }
      config && Kernel.extend(instance, config, !0), instance._internals = {
        type: "module",
        moduleType: type
      }, instance.kill = instance.kill || function() {}, instance.id = id;
      var H = function() {};
      H.prototype = registered[id].hub, instance.hub = new H, instance.hub.broadcast = function(type, data, callback) {
        registered[id].hub.broadcast(type, data, callback, id)
      }, instance.hub.listen = function(type, callback) {
        registered[id].hub.listen(type, callback, id)
      }, instance.hub._internals = Kernel.extend(registered[id].hub._internals, {
        moduleId: id,
        moduleType: type
      }), decorateMethods(instance, !0), registered[id].instance = instance, core.onRegister && core.onRegister(registered[id].instance), registered[id].instance.onRegister && registered[id].instance.onRegister()
    }

    function autoRegisterModules(fn) {
      var type;
      fn = fn || function(type) {
        return type.toString().replace(/([a-z])([A-Z])/g, "$1_$2").toLowerCase()
      };
      for (type in core._internals.modules) core.register(fn(type), type)
    }

    function unregisterModule(id) {
      registered[id].instance.started && Kernel.stop(id), delete registered[id]
    }

    function startModule(id, config, core) {
      return core.module.isStarted(id) ? void 0 : (config && extend(registered[id].instance, config, !0), registered[id].started = !0, core.onStart(registered[id].instance))
    }
    var core, hubs = {},
      modules = {},
      registered = {},
      listeners = {},
      defaultHub = "main";
    return core = {
      extend: extend,
      decorateMethods: decorateMethods,
      module: {
        define: defineModule,
        get: function(id) {
          if (!registered[id]) throw "Couldn't get instance for: " + id + ", is it registered?";
          return registered[id].instance
        },
        getDefinition: function(type) {
          return modules[type]
        },
        isStarted: function(id) {
          return registered[id].started
        }
      },
      hub: {
        define: defineHub,
        get: function(id) {
          return hubs[id]
        }
      },
      register: function(id, type, hub, config) {
        var i;
        if (isString(id)) registerModule(id, type, hub, config, this);
        else
          for (i = 0; i < id.length; i += 1) registerModule(id[i].id, id[i].type, id[i].hub, id[i].config, this)
      },
      unregister: unregisterModule,
      autoRegisterModules: autoRegisterModules,
      start: function(id, config) {
        var i;
        if (isString(id)) return startModule(id, config, this);
        for (i = 0; i < id.length; i += 1) startModule(id[i].id, id[i].config, this)
      },
      startAll: function() {
        var key;
        for (key in registered) startModule(key, null, this)
      },
      onStart: function(instance, callback) {
        instance.init(), callback && callback()
      },
      decorateMethod: function(instance, name, method) {
        return function() {
          return method.apply(instance, arguments)
        }
      },
      stop: function(id) {
        var key, i, size, listener;
        this.onStop(registered[id].instance);
        for (key in listeners)
          for (i = 0, size = listeners[key].length; size > i; i += 1) listener = listeners[key][i], listener.id === id && listeners[key].splice(i, 1);
        registered[id].started = !1
      },
      onStop: function(instance) {
        instance.kill()
      },
      version: "2.7.5",
      _internals: {
        PRIVATE: "FOR DEBUGGING ONLY",
        type: "Kernel",
        hubs: hubs,
        modules: modules,
        registered: registered,
        listeners: listeners
      }
    }
  }(), Kernel.hub.define("main", {}), "undefined" == typeof Aquarius) var Aquarius = Kernel.extend(Kernel, {});
Aquarius = Aquarius.extend(Aquarius, {
  accents: {
    table: {
      A: "[AaªÀ-Åà-åĀ-ąǍǎȀ-ȃȦȧᴬᵃḀḁẚẠ-ảₐ℀℁℻⒜Ⓐⓐ㍱-㍴㎀-㎄㎈㎉㎩-㎯㏂㏊㏟㏿Ａａ]",
      B: "[BbᴮᵇḂ-ḇℬ⒝Ⓑⓑ㍴㎅-㎇㏃㏈㏔㏝Ｂｂ]",
      C: "[CcÇçĆ-čᶜ℀ℂ℃℅℆ℭⅭⅽ⒞Ⓒⓒ㍶㎈㎉㎝㎠㎤㏄-㏇Ｃｃ]",
      D: "[DdĎďǄ-ǆǱ-ǳᴰᵈḊ-ḓⅅⅆⅮⅾ⒟Ⓓⓓ㋏㍲㍷-㍹㎗㎭-㎯㏅㏈Ｄｄ]",
      E: "[EeÈ-Ëè-ëĒ-ěȄ-ȇȨȩᴱᵉḘ-ḛẸ-ẽₑ℡ℯℰⅇ⒠Ⓔⓔ㉐㋍㋎Ｅｅ]",
      F: "[FfᶠḞḟ℉ℱ℻⒡Ⓕⓕ㎊-㎌㎙ﬀ-ﬄＦｆ]",
      G: "[GgĜ-ģǦǧǴǵᴳᵍḠḡℊ⒢Ⓖⓖ㋌㋍㎇㎍-㎏㎓㎬㏆㏉㏒㏿Ｇｇ]",
      H: "[HhĤĥȞȟʰᴴḢ-ḫẖℋ-ℎ⒣Ⓗⓗ㋌㍱㎐-㎔㏊㏋㏗Ｈｈ]",
      I: "[IiÌ-Ïì-ïĨ-İĲĳǏǐȈ-ȋᴵᵢḬḭỈ-ịⁱℐℑℹⅈⅠ-ⅣⅥ-ⅨⅪⅫⅰ-ⅳⅵ-ⅸⅺⅻ⒤Ⓘⓘ㍺㏌㏕ﬁﬃＩｉ]",
      J: "[JjĲ-ĵǇ-ǌǰʲᴶⅉ⒥ⒿⓙⱼＪｊ]",
      K: "[KkĶķǨǩᴷᵏḰ-ḵK⒦Ⓚⓚ㎄㎅㎉㎏㎑㎘㎞㎢㎦㎪㎸㎾㏀㏆㏍-㏏Ｋｋ]",
      L: "[LlĹ-ŀǇ-ǉˡᴸḶḷḺ-ḽℒℓ℡Ⅼⅼ⒧Ⓛⓛ㋏㎈㎉㏐-㏓㏕㏖㏿ﬂﬄＬｌ]",
      M: "[MmᴹᵐḾ-ṃ℠™ℳⅯⅿ⒨Ⓜⓜ㍷-㍹㎃㎆㎎㎒㎖㎙-㎨㎫㎳㎷㎹㎽㎿㏁㏂㏎㏐㏔-㏖㏘㏙㏞㏟Ｍｍ]",
      N: "[NnÑñŃ-ŉǊ-ǌǸǹᴺṄ-ṋⁿℕ№⒩Ⓝⓝ㎁㎋㎚㎱㎵㎻㏌㏑Ｎｎ]",
      O: "[OoºÒ-Öò-öŌ-őƠơǑǒǪǫȌ-ȏȮȯᴼᵒỌ-ỏₒ℅№ℴ⒪Ⓞⓞ㍵㏇㏒㏖Ｏｏ]",
      P: "[PpᴾᵖṔ-ṗℙ⒫Ⓟⓟ㉐㍱㍶㎀㎊㎩-㎬㎰㎴㎺㏋㏗-㏚Ｐｐ]",
      Q: "[Qqℚ⒬Ⓠⓠ㏃Ｑｑ]",
      R: "[RrŔ-řȐ-ȓʳᴿᵣṘ-ṛṞṟ₨ℛ-ℝ⒭Ⓡⓡ㋍㍴㎭-㎯㏚㏛Ｒｒ]",
      S: "[SsŚ-šſȘșˢṠ-ṣ₨℁℠⒮Ⓢⓢ㎧㎨㎮-㎳㏛㏜ﬆＳｓ]",
      T: "[TtŢ-ťȚțᵀᵗṪ-ṱẗ℡™⒯Ⓣⓣ㉐㋏㎔㏏ﬅﬆＴｔ]",
      U: "[UuÙ-Üù-üŨ-ųƯưǓǔȔ-ȗᵁᵘᵤṲ-ṷỤ-ủ℆⒰Ⓤⓤ㍳㍺Ｕｕ]",
      V: "[VvᵛᵥṼ-ṿⅣ-Ⅷⅳ-ⅷ⒱Ⓥⓥⱽ㋎㍵㎴-㎹㏜㏞Ｖｖ]",
      W: "[WwŴŵʷᵂẀ-ẉẘ⒲Ⓦⓦ㎺-㎿㏝Ｗｗ]",
      X: "[XxˣẊ-ẍₓ℻Ⅸ-Ⅻⅸ-ⅻ⒳Ⓧⓧ㏓Ｘｘ]",
      Y: "[YyÝýÿŶ-ŸȲȳʸẎẏẙỲ-ỹ⒴Ⓨⓨ㏉Ｙｙ]",
      Z: "[ZzŹ-žǱ-ǳᶻẐ-ẕℤℨ⒵Ⓩⓩ㎐-㎔Ｚｚ]"
    },
    replace: function(string) {
      var self = this;
      return string.replace(/\S/g, function(character) {
        return self.table[character.toUpperCase()] || character
      })
    },
    sanitize: function(string) {
      var self = this;
      return string.replace(/\S/g, function(character) {
        var isUppercase = character === character.toUpperCase(),
          replaced = character;
        return $.each(self.table, function(letter, regExpPattern) {
          var regExp = new RegExp(regExpPattern);
          return regExp.test(character) ? (replaced = letter, !1) : void 0
        }), isUppercase ? replaced : replaced.toLowerCase()
      })
    }
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  ajax: function() {
    var self = this,
      hub = this.hub.get("main"),
      jqXHR = $.ajax.apply(this, arguments),
      longTimer = setTimeout(function() {
        self.notify("info", {
          type: "Ajax",
          text: Aquarius.translate("The request takes more time than the usual. Please be patient."),
          status: "",
          response: ""
        })
      }, 15e3);
    return hub.broadcast(["ajax.load:before", "ajax.load"], {
      xhr: jqXHR
    }), jqXHR.fail(function(jqXHR, textStatus, errorThrown) {
      var response = jqXHR.responseJSON,
        message = Aquarius.translate(errorThrown);
      response && response.error && response.error.message && message != response.error.message && (message = "**" + Aquarius.translate(response.error.message) + "**"), response && response.error && response.error.comment && (message += " - " + response.error.comment), "abort" !== errorThrown && self.notify("error", {
        type: "Ajax",
        text: message,
        status: textStatus,
        response: jqXHR.responseText
      })
    }), jqXHR.always(function(jqXHR) {
      clearTimeout(longTimer), hub.broadcast("ajax.load:after", {
        xhr: jqXHR
      })
    }), jqXHR
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  alert: function(title, options) {
    return options || (options = {}), options.buttons = [{
      value: !0,
      label: this.translate("Ok"),
      "class": "btn btn-primary"
    }], Aquarius.confirm(title, function() {}, options)
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  breadcrumbs: {
    add: function(title, url, icon) {
      var breadCrumbs = Aquarius.module.get("navigation.breadcrumbs");
      return breadCrumbs && breadCrumbs.append(title, url, icon), this
    },
    removeLast: function() {
      var breadCrumbs = Aquarius.module.get("navigation.breadcrumbs");
      return breadCrumbs && breadCrumbs.removeLast(), this
    }
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  component: {
    _promises: {},
    _components: {},
    _helpers: {},
    _dependencies: {},
    dataId: "componentObject",
    loadEventId: "component.loaded",
    define: function(id, obj) {
      if (!obj instanceof Object) throw "Components must be objects";
      return this._components[id] = obj, this.getHelper(id)
    },
    getDefinition: function(id) {
      if (this.has(id)) return this._components[id];
      throw "Unknown component: " + id
    },
    getExtending: function(id) {
      return this._dependencies[id] ? this._dependencies[id]["extends"] : !1
    },
    getComponentDependencies: function(id) {
      return this._dependencies[id] ? this._dependencies[id].components : []
    },
    getRemoteDependencies: function(id) {
      return this._dependencies[id] ? this._dependencies[id].remote : []
    },
    getPromisedDependencies: function(id) {
      return this._dependencies[id] ? this._dependencies[id].promises : []
    },
    getHelper: function(id) {
      var self = this;
      return this._helpers[id] || (self._dependencies[id] = {
        "extends": !1,
        components: [],
        remote: [],
        promises: []
      }, this._helpers[id] = {
        "extends": function(componentName) {
          if (self._dependencies[id]["extends"]) throw "A component can only extend one component!";
          return self._dependencies[id]["extends"] = componentName, this.dependsOnComponent(componentName)
        },
        dependsOnComponent: function(dependency) {
          return self._dependencies[id].components.push(dependency), this
        },
        dependsOnRemote: function(url) {
          return self._dependencies[id].remote.push(url), this
        },
        dependsOnPromise: function(promise) {
          return self._dependencies[id].promises.push(promise), this
        }
      }), this._helpers[id]
    },
    loadDefinition: function(id) {
      var self = this,
        deferred = $.Deferred();
      if (this._promises[id]) return this._promises[id];
      if (this.has(id)) deferred.resolve(self.getDefinition(id));
      else {
        var errorMessage, componentJsFileName = id.replace(/\.?([A-Z])/g, function(x, y) {
            return "-" + y.toLowerCase()
          }).replace(/^-/, "").replace(/(\/\-)/g, "/") + ".js",
          url = Aquarius.scripts.getPath("components", componentJsFileName);
        Aquarius.scripts.load(url).done(function() {
          self.has(id) ? deferred.resolve(self.getDefinition(id)) : (errorMessage = Aquarius.translate("Invalid component definition loaded: ** %id% **", {
            id: id
          }), Aquarius.notify("warning", errorMessage), deferred.fail(errorMessage))
        }).fail(function() {
          errorMessage = Aquarius.translate("Error while loading component definition: ** %id% **", {
            id: id
          }), Aquarius.notify("error", errorMessage), deferred.fail(errorMessage)
        })
      }
      return deferred.done(function() {
        delete self._promises[id]
      }), this._promises[id] = deferred.promise()
    },
    getParentPrototypeFacade: function(object, parentPrototype) {
      var parentPrototypeFacade = {};
      return $.each(parentPrototype, function(functionName, closure) {
        parentPrototypeFacade[functionName] = function() {
          return parentPrototype[functionName].apply(object, arguments)
        }
      }), parentPrototypeFacade
    },
    has: function(id) {
      return "undefined" != typeof this._components[id] && this._components[id] instanceof Object
    },
    getClass: function(id) {
      if (!this.has(id)) throw "Unknown component: " + id;
      var self = this,
        component = this._components[id],
        deferred = $.Deferred(),
        args = [];
      return Array.prototype.push.apply(args, arguments), args.shift(), this.loadDependencies(id, function() {
        var parentId, componentClass = component.apply(component, args);
        if ("object" != typeof componentClass && "object" == typeof componentClass.prototype && (parentId = self.getExtending(id))) {
          args.unshift(parentId);
          var parentComponentPromise = self.getClass.apply(self, args);
          parentComponentPromise.done(function(parentComponentClass) {
            if ("object" != typeof parentComponentClass.prototype) throw "Components without prototypes cannot be extended!";
            var parentPrototype = Kernel.extend({}, parentComponentClass.prototype);
            componentClass.prototype = Kernel.extend(parentComponentClass.prototype, componentClass.prototype), componentClass.prototype.__initParent = function(componentObject) {
              componentObject.parent = self.getParentPrototypeFacade(componentObject, parentPrototype)
            }, deferred.resolve(componentClass, parentComponentClass.prototype);
          })
        } else deferred.resolve(componentClass)
      }), deferred.promise()
    },
    get: function(id) {
      var componentObject, deferred = $.Deferred(),
        componentClassPromise = this.getClass.apply(this, arguments);
      return componentClassPromise.done(function(componentClass, parentPrototype) {
        if (componentObject = "object" == typeof componentClass ? componentClass : "object" == typeof componentClass.prototype ? Object.create(componentClass.prototype) : Object.create(componentClass), "function" == typeof componentObject.__initParent && (componentObject.__initParent(componentObject), delete componentObject.__initParent), "function" == typeof componentObject.init) {
          var result = componentObject.init();
          result && "function" == typeof result.then ? result.done(function() {
            deferred.resolve(componentObject)
          }) : deferred.resolve(componentObject)
        } else deferred.resolve(componentObject)
      }), deferred.promise()
    },
    loadDependencies: function(id, callback) {
      var self = this,
        dependencyPromises = [],
        fileExtensionPattern = /\.([0-9a-z]+)(?=[?#])|(\.)(?:[\w]+)$/gim;
      $.each(this.getComponentDependencies(id), function(i, componentId) {
        dependencyPromises.push(self.loadDefinition(componentId))
      });
      try {
        $.each(this.getRemoteDependencies(id), function(i, url) {
          var loadRemoteDependencies = function(remoteUrl) {
            var extension = remoteUrl.match(fileExtensionPattern)[0];
            switch (extension) {
              case ".css":
                dependencyPromises.push(Aquarius.styles.load(url));
                break;
              case ".js":
                dependencyPromises.push(Aquarius.scripts.load(url));
                break;
              default:
                throw "Unknown extension: " + extension
            }
          };
          "object" == typeof url ? $.each(url, function(i, remoteUrl) {
            loadRemoteDependencies(remoteUrl)
          }) : loadRemoteDependencies(url)
        })
      } catch (error) {
        Aquarius.notify("error", error)
      }
      $.each(this.getPromisedDependencies(id), function(i, promise) {
        dependencyPromises.push(promise)
      }), $.when.apply(this, dependencyPromises).done(callback)
    },
    load: function(id) {
      var self = this,
        args = arguments,
        deferred = $.Deferred(),
        definitionPromise = self.loadDefinition(id);
      return definitionPromise.done(function() {
        self.get.apply(self, args).done(function(componentObject) {
          deferred.resolve(componentObject)
        })
      }), definitionPromise.fail(function(message) {
        deferred.fail(message)
      }), deferred.promise()
    },
    loadFromElement: function(element) {
      var component, $element = $(element),
        deferred = $.Deferred();
      return (component = $element.data(this.dataId)) ? deferred.resolve(component) : $element.on(this.loadEventId, function(event, component) {
        deferred.resolve(component)
      }), deferred.promise()
    }
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  confirm: function(title, callback, options) {
    var defaults = {
      title: "",
      text: "",
      target: "Target/Modal",
      mode: "dialog",
      buttons: [{
        value: !0,
        label: this.translate("Ok"),
        "class": "btn btn-primary"
      }, {
        value: !1,
        label: this.translate("Cancel"),
        "class": "btn btn-default"
      }]
    };
    options = Aquarius.extend(defaults, Aquarius.extend(options || {}, {
      title: title
    }));
    var target = this.registry.get(options.target);
    if (!target) throw "Target **" + options.target + "** is not found.";
    var $targetElement = target.getElement().empty();
    $targetElement.append($('<div class="m-b-md"></div>').html(options.text));
    var value = null;
    $(options.buttons).each(function(i, button) {
      var $button = $('<button class="m-r-md"></button>').addClass(button["class"]).html(button.label).val(button.value);
      $button.on("click", function() {
        value = button.value, target.close()
      }), $button.appendTo($targetElement)
    });
    var clickFirst = function(event) {
      13 == event.keyCode && $targetElement.find("button:first").trigger("click")
    };
    return $(document).on("keydown", clickFirst), target.namespace.listenOnce("close:after", function() {
      if ($(document).off("keydown", clickFirst), !callback) throw "Callback function is needed for confirm.";
      callback.apply(window, [value, options])
    }), target.open({
      title: options.title,
      mode: options.mode
    }), this
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  cookie: function() {
    return $.cookie.apply(this, arguments)
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  date: Kernel.extend(Date, {
    parse: function(value) {
      var date = Date.parse(value);
      return date || (date = new Date(value)), date
    }
  })
}), Aquarius = Aquarius.extend(Aquarius, {
  debounce: function() {
    return $.debounce.apply(this, arguments)
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  facade: function(object, properties) {
    var facade = {};
    return $.each(properties, function(i, property) {
      switch (typeof object[property]) {
        case "function":
          facade[property] = function() {
            return object[property].apply(object, arguments)
          };
          break;
        default:
          facade[property] = object[property]
      }
    }), facade
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  get: function(url, callback, cacheable) {
    var cacheData, self = this,
      cache = Aquarius.storage.bag("content-cache", url.hashCode());
    return cacheable && (cacheData = cache.load()) && cacheData && cacheData.eTag && setTimeout(function() {
      "function" == typeof callback && callback.apply(self, [cacheData.content])
    }, 1), Aquarius.ajax(url, {
      type: "GET",
      cache: !1,
      beforeSend: function(xhr) {
        cacheable && cacheData && cacheData.eTag && xhr.setRequestHeader("If-None-Match", cacheData.eTag)
      },
      success: function(html, status, xhr) {
        304 !== xhr.status && (cacheable && cacheData && cacheData.eTag && Aquarius.notify("warning", "Ajax ETag mismatch (Stored: " + cacheData.eTag + " New: " + xhr.getResponseHeader("ETag") + "), fresh content loaded.."), cacheable && cache.save({
          content: html,
          eTag: xhr.getResponseHeader("ETag")
        }), "function" == typeof callback && callback.apply(self, [html, status, xhr]))
      }
    }), self
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  load: function(element, url, callback, cacheable) {
    var self = this,
      $element = $(element);
    return this.get(url, function(result) {
      $element.html(result), "function" == typeof callback && callback.apply(self, arguments)
    }, cacheable), self
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  markdown: markdown
}), Aquarius = Aquarius.extend(Aquarius, {
  isRegex: function(string) {
    return /^\/.*\/[gimy]*$/.test(string)
  },
  addToFormData: function(key, value, formData) {
    var self = this;
    switch (typeof value) {
      case "object":
      case "array":
        if (null !== value) {
          $.each(value, function(i, v) {
            self.addToFormData(key.length ? key + "[" + i + "]" : i, v, formData)
          });
          break
        }
      default:
        formData.append(key, value)
    }
    return formData
  },
  checkCondition: function(condition, value, and) {
    var valid = and,
      operator = "==";
    if ("string" == typeof condition && this.isRegex(condition)) {
      var flags = condition.replace(/.*\/([gimy]*)$/, "$1") + "",
        pattern = condition.replace(new RegExp("^/(.*?)/" + flags + "$"), "$1") + "";
      return new RegExp(pattern, flags).test(value)
    }
    var parts = "string" != typeof condition ? [condition] : condition.split(","),
      result = !1;
    return $.each(parts, function(i, part) {
      if ("string" == typeof part) {
        var matches = part.match(/^(!==|!=|<=|<|===|==|>=|>)?\s?(.)*/);
        matches[1] && (part = part.replace(matches[1], "").trim(), operator = matches[1]), part = part.replace(/^["']|["']$/g, "")
      }
      var expression = (null === value || "null" === value ? "null" : '"' + value + '"') + " " + operator + " " + (null === part || "null" === part ? "null" : '"' + part + '"');
      return result = eval(expression), and ? void(valid = result && valid) : (valid = result || valid, !valid)
    }), !!valid
  },
  checkConditions: function(conditions, data, or) {
    var self = this;
    if (!data) return !1;
    var value, matches, valid = !or,
      result = valid,
      groupValid = !1;
    return $.each(conditions, function(key, condition) {
      return value = Aquarius.findValue(key, data), self.isRegex(condition) || (condition = Aquarius.bindContent(condition, data), groupValid = !1, "string" == typeof condition && (matches = condition.match(/\((.*?)\)/g)) && ($.each(matches, function(i, match) {
        condition = condition.replace(match, ""), groupValid = groupValid || self.checkCondition(match.replace(/^\((.*)\)$/, "$1"), value, !0)
      }), condition = condition.replace(/^\,*|\,*$/g, ""))), result = "string" != typeof condition || condition.length ? groupValid || self.checkCondition(condition, value) : groupValid, or ? (valid = result || valid, !valid) : void(valid = result && valid)
    }), !!valid
  },
  filterValue: function(value, filters) {
    var self = this;
    if ($.isArray(filters)) $.each(filters, function(i, filter) {
      value = self.filterValue(value, filter)
    });
    else {
      var filterParams = this.getFilterParams(filters);
      switch (filters = filters.replace(/\[.*\]/gi, "")) {
        case "length":
          value = "string" == typeof value ? value.length : 0;
          break;
        case "numberFormat":
          filterParams.unshift(value), value = self.numberFormat.apply(self, filterParams)
      }
    }
    return value
  },
  getFilterParams: function(filter) {
    var params = filter.match(/\[(.+)\]/gi);
    return params && params.length ? $.map(params, function(params) {
      return JSON.parse(params)
    }) : []
  },
  findValue: function(name, data, defaultValue) {
    var value;
    value = "undefined" != typeof defaultValue ? defaultValue : null;
    var cData = data,
      filters = [];
    if (-1 != name.indexOf("|") && (filters = name.split("|"), name = filters.shift()), data && "undefined" != typeof data[name]) value = data[name];
    else {
      var nameParts = name.split("/");
      $.each(nameParts, function(i, key) {
        if (!cData || "undefined" == typeof cData[key]) return value = "undefined" != typeof defaultValue ? defaultValue : null, !1;
        switch (value = cData[key], typeof cData[key]) {
          case "object":
            return cData = value, !0;
          default:
            return !1
        }
      })
    }
    return this.filterValue(value, filters)
  },
  flattenData: function(data, keyPrefix) {
    var self = this,
      flattenedData = {};
    return keyPrefix ? keyPrefix += "/" : keyPrefix = "", $.each(data, function(key, value) {
      /boolean|number|string/.test(typeof value) || null == value ? flattenedData[keyPrefix + key] = value : flattenedData = Aquarius.extend(flattenedData, self.flattenData(value, keyPrefix + key))
    }), flattenedData
  },
  writeValue: function(object, key, value) {
    object = Aquarius.extend({}, object);
    var keyParts = key.split("/"),
      subject = object;
    return $.each(keyParts, function(i, keyPart) {
      "undefined" != typeof subject[keyPart] ? subject = subject[keyPart] : keyParts.length > i + 1 ? (subject[keyPart] = {}, subject = subject[keyPart]) : subject[keyPart] = value
    }), object
  },
  getUrl: function(url, bind) {
    return bind ? this.bindContent(url, bind, !0) : url
  },
  bindContent: function(content, bind, encode) {
    if (content += "", bind) {
      var keys = this.getBindContentVariables(content);
      keys && keys.length && $.each(keys, function(i, key) {
        var value = Aquarius.findValue(key.replace(/\[\]$/, ""), bind);
        encode ? value = encodeURIComponent(value) : null === value && (value = ""), content = content.replace("{" + key + "}", value)
      })
    }
    return content
  },
  getBindContentVariables: function(string) {
    var keys = string.match(/\{([a-zA-Z0-9\|\[\]\,\s\"\-\/]+)\}/gi);
    return keys && keys.length ? $.map(keys, function(field) {
      return field.replace("{", "").replace("}", "")
    }) : []
  },
  getBindContentKeys: function(string) {
    var keys = this.getBindContentVariables(string);
    return keys && keys.length ? $.map(keys, function(field) {
      return field.split("|")[0]
    }) : []
  },
  inArray: function(elem, array, fromIndex, strict) {
    for (var i = fromIndex || 0, length = array.length; length > i; i++)
      if (array[i] == elem && (!strict || array[i] === elem)) return i;
    return -1
  },
  isMobile: function() {
    return Aquarius.registry.has("isMobile") || Aquarius.registry.set("isMobile", function(agent) {
      return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(agent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(agent.substr(0, 4))
    }(navigator.userAgent || navigator.vendor || window.opera)), Aquarius.registry.get("isMobile")
  },
  numberFormat: function(number, decimals, dec_point, thousands_sep) {
    number = (number + "").replace(/[^0-9+\-Ee.]/g, "");
    var n = isFinite(+number) ? +number : 0,
      prec = isFinite(+decimals) ? Math.abs(decimals) : 0,
      sep = "undefined" == typeof thousands_sep ? "," : thousands_sep,
      dec = "undefined" == typeof dec_point ? "." : dec_point,
      s = "",
      toFixedFix = function(n, prec) {
        var k = Math.pow(10, prec);
        return "" + Math.round(n * k) / k
      };
    return s = (prec ? toFixedFix(n, prec) : "" + Math.round(n)).split("."), s[0].length > 3 && (s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)), (s[1] || "").length < prec && (s[1] = s[1] || "", s[1] += new Array(prec - s[1].length + 1).join("0")), s.join(dec)
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  notify: function(level, details) {
    try {
      return this.module.get("notifications").activeLevels[level] ? (this.hub.get("main").broadcast(level, details), !0) : !1
    } catch (error) {
      return !1
    }
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  proxy: {
    namespaces: {},
    getNode: function(element, hub) {
      return hub || (hub = Aquarius.hub.get("main")), {
        listen: function(channels, callback) {
          var self = this;
          hub.listen(channels, function(data) {
            (!element || $.contains(document, $(element).get()[0])) && callback.apply(self, [data])
          })
        },
        listenOnce: function(channels, callback) {
          var self = this,
            run = !1;
          this.listen(channels, function() {
            run === !1 && (run = !0, callback.apply(self, arguments))
          })
        },
        broadcast: function() {
          hub.broadcast.apply(this, arguments)
        },
        authorize: function() {
          hub.authorize.apply(this, arguments)
        }
      }
    },
    getNamespace: function(hub) {
      return hub || (hub = Aquarius.hub.get("main")), {
        id: Aquarius.uniqueId.generate(),
        listen: function(channels, callback) {
          var self = this;
          if (registerListener = function(id, channel, callback) {
              hub.listen(id + channel, function() {
                callback.apply(self, arguments)
              })
            }, $.isArray(channels))
            for (var i in channels) registerListener(this.id, channels[i], callback);
          else registerListener(this.id, channels, callback)
        },
        listenOnce: function(channels, callback) {
          var self = this,
            run = !1;
          this.listen(channels, function() {
            run === !1 && (run = !0, callback.apply(self, arguments))
          })
        },
        broadcast: function(channel, data) {
          hub.broadcast(this.id + channel, data)
        },
        authorize: function(channel, data) {
          hub.authorize(this.id + channel, data)
        }
      }
    },
    attachAll: function(object, element, hub) {
      return {
        hub: this.attachHub(object, hub),
        node: this.attachNode(object, element, hub),
        namespace: this.attachNamespace(object, object.node)
      }
    },
    attachHub: function(object, hub) {
      return object.hub = hub, object.hub
    },
    attachNode: function(object, element, hub) {
      return object.node = this.getNode(element, hub), object.node
    },
    attachNamespace: function(object, hub) {
      return object.namespace = this.getNamespace(hub), object.namespace
    },
    attachEvents: function(object, events, hub) {
      object.namespace || this.attachNamespace(object, hub), $.each(events, function(method, event) {
        var temp = object[method];
        "string" == typeof event ? object[method] = function() {
          var result = temp.apply(object, arguments);
          return object.namespace.broadcast(event), result
        } : object[method] = function() {
          event.before && object.namespace.broadcast(event.before);
          var result = temp.apply(object, arguments);
          return event.after && object.namespace.broadcast(event.after), result
        }
      })
    }
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  query: function(apiUrl, host, callback) {
    this.component.load("Query", apiUrl, host).done(function(query) {
      "function" == typeof callback && callback.apply(query, [query])
    })
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  registry: {
    _registry: {},
    set: function(key, value) {
      return this._registry[key.toLowerCase()] = value, this
    },
    get: function(key) {
      return this.has(key) ? this._registry[key.toLowerCase()] : null
    },
    has: function(key) {
      return "undefined" != typeof this._registry[key.toLowerCase()]
    }
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  scripts: {
    _scripts: [],
    _detected: !1,
    getPath: function(type, fileName) {
      if (!Aquarius.settings.system.paths[type]) throw "Invalid path type: " + type;
      var parts = [Aquarius.settings.system.host, Aquarius.settings.system.basePath, Aquarius.settings.system.paths[type], fileName],
        path = "/";
      return $.each(parts, function(i, part) {
        path += "/" + part.replace(/\/$/, "")
      }), path
    },
    detect: function() {
      var self = this;
      $("script").each(function() {
        self._scripts.push($(this).attr("src"))
      })
    },
    has: function(url) {
      return this._detected || this.detect(), -1 !== $.inArray(url, this._scripts)
    },
    load: function(url) {
      var self = this,
        deferred = $.Deferred();
      return "object" == typeof url ? self.loadUrls(url) : (this.has(url) ? deferred.resolve() : Aquarius.ajax({
        dataType: "script",
        cache: !0,
        url: url + (Aquarius.settings.system.version ? (-1 !== url.indexOf("?") ? "&" : "?") + "v=" + Aquarius.settings.system.version : ""),
        success: function() {
          self._scripts.push(url), deferred.resolve()
        },
        error: function() {
          deferred.fail("Unable to load script form url: " + url)
        }
      }), deferred.done(function() {}), deferred.promise())
    },
    loadUrls: function(urls, i, deferred) {
      var self = this;
      return "undefined" == typeof i && (i = 0), "undefined" == typeof deferred && (deferred = $.Deferred()), urls[i] ? self.load(urls[i]).done(function() {
        self.loadUrls(urls, i + 1, deferred)
      }) : deferred.resolve(), deferred.promise()
    }
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  settings: {
    system: {
      host: "https://aquarius.wponline.hu",
      basePath: "assets/backend/js/",
      paths: {
        components: "app/components",
        hubs: "app/hubs",
        library: "library",
        modules: "app/modules",
        misc: "misc",
        vendor: "vendor"
      },
      version: "1.22"
    },
    get: function(key) {
      var setting = $("aquarius-settings").data(key);
      return "undefined" == typeof setting ? null : setting
    }
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  storage: {
    _namespace: "aquarius",
    isAvailable: function() {
      try {
        return "localStorage" in window && null !== window.localStorage
      } catch (e) {
        return !1
      }
    },
    bag: function() {
      var storage = this,
        key = "bag." + Array.prototype.join.call(arguments, ["."]);
      return {
        getKey: function() {
          return key
        },
        load: function() {
          return storage.get(key)
        },
        save: function(data) {
          return storage.set(key, data), this
        },
        "delete": function(key) {
          return storage.remove(key), this
        }
      }
    },
    set: function(key, value) {
      if (!key) return this;
      var envelope = {
        time: (new Date).getTime(),
        data: value
      };
      try {
        localStorage.setItem(this._namespace + "." + key, JSON.stringify(envelope))
      } catch (exception) {
        return this.removeOldest(10), this.set(key, value)
      }
      return this
    },
    getAll: function(regexp) {
      var values = {},
        keys = this.getAllKey(regexp);
      for (var key in keys) values[keys[key]] = this.get(keys[key]);
      return values
    },
    getAllKey: function(regexp) {
      var keys = [],
        isRegexp = regexp && regexp.test;
      if (this.isAvailable())
        for (var i = 0, len = localStorage.length; len > i; ++i) {
          var key = localStorage.key(i).replace(/^aquarius\./, "");
          isRegexp ? regexp.test(key) && keys.push(key) : keys.push(key)
        }
      return keys
    },
    has: function(key) {
      try {
        return null !== localStorage.getItem(this._namespace + "." + key)
      } catch (exception) {
        return !1
      }
    },
    get: function(key, returnEnvelope) {
      try {
        var storedValue = localStorage.getItem(this._namespace + "." + key)
      } catch (exception) {
        return !1
      }
      if (!storedValue) return !1;
      try {
        var envelope = JSON.parse(storedValue);
        return envelope.time ? returnEnvelope ? envelope : envelope.data : !1
      } catch (exception) {
        return !1
      }
    },
    remove: function(key) {
      try {
        localStorage.removeItem(this._namespace + "." + key)
      } catch (exception) {}
      return this
    },
    removeAll: function(regexp) {
      var keys = this.getAllKey(regexp);
      for (var key in keys) this.remove(keys[key]);
      return this
    },
    removeOldest: function(count) {
      for (var self = this, times = self.getAllKey().reduce(function(collection, key) {
          var data = self.get(key, !0);
          return data && data.time && (collection[data.time] = key), collection
        }, {}), timeData = Object.keys(times).map(function(item) {
          return parseInt(item)
        }).sort(), i = 0; count > i; i++) this.remove(times[timeData[i]])
    }
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  stream: {
    _namespace: "stream",
    _glue: ":",
    _initialized: null,
    _socket: null,
    _muted: !1,
    _getCallback: function(callback, force) {
      var stream = this;
      return function(data) {
        (!stream.isMuted() || force) && callback.apply(this, [JSON.parse(data)])
      }
    },
    init: function() {
      return null !== this._initialized ? this._initialized : "undefined" == typeof io ? this._initialized = !1 : (this._namespace = Aquarius.settings.get("streamNamespace"), this._glue = Aquarius.settings.get("streamGlue"), this._socket = io.connect(Aquarius.settings.get("streamSocket")), this._initialized = !0)
    },
    isEnabled: function() {
      return this.init()
    },
    mute: function() {
      return this._muted = !0
    },
    unmute: function() {
      return this._muted = !1
    },
    isMuted: function() {
      return this._muted
    },
    subscribe: function(channel, callback, force) {
      return this.init() ? this._socket.on(this._namespace + this._glue + channel, this._getCallback(callback, force)) : !1
    },
    unsubscribe: function(channel, callback) {
      callback ? this._socket.removeListener(this._namespace + this._glue + channel, this._getCallback(callback)) : this._socket.removeAllListeners(this._namespace + this._glue + channel)
    },
    publish: function(channel, data) {
      return this.init() ? this._socket.emit(this._namespace + this._glue + channel, JSON.stringify(data)) : !1
    }
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  styles: {
    _styles: [],
    _detected: !1,
    getPath: function(type, fileName) {
      if (!Aquarius.settings.system.paths[type]) throw "Invalid path type: " + type;
      var parts = [Aquarius.settings.system.host, Aquarius.settings.system.basePath, Aquarius.settings.system.paths[type], fileName],
        path = "/";
      return $.each(parts, function(i, part) {
        path += "/" + part.replace(/\/$/, "")
      }), path
    },
    detect: function() {
      var self = this;
      $('link [rel="stylesheet"]').each(function() {
        self._styles.push($(this).attr("href"))
      })
    },
    has: function(url) {
      return this._detected || this.detect(), -1 !== $.inArray(url, this._styles)
    },
    load: function(url) {
      var self = this,
        deferred = $.Deferred(),
        $head = $("head");
      if ("object" == typeof url) return self.loadUrls(url);
      if (!this.has(url)) {
        var $link = $('<link rel="stylesheet" type="text/css" />').attr("href", url + (Aquarius.settings.system.version ? "?v=" + Aquarius.settings.system.version : ""));
        $head.append($link), self._styles.push(url)
      }
      return deferred.resolve(), deferred.promise()
    },
    loadUrls: function(urls, i, deferred) {
      var self = this;
      return "undefined" == typeof i && (i = 0), "undefined" == typeof deferred && (deferred = $.Deferred()), urls[i] ? self.load(urls[i]).done(function() {
        self.loadUrls(urls, i + 1, deferred)
      }) : deferred.resolve(), deferred.promise()
    }
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  throttle: function() {
    return $.throttle.apply(this, arguments)
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  translations: {
    _data: {},
    initialized: !1,
    categoryId: null,
    uploadDisabled: !1,
    getLocale: function() {
      return $("html").attr("lang")
    },
    get: function(text) {
      var self = this;
      return this.initialized || (self._data = Aquarius.settings.get("translations"), self.uploadDisabled = Aquarius.settings.get("translationsUploadDisabled"), self.categoryId = Aquarius.settings.get("translationsCategoryId"), this.initialized = !0), this._data ? this._data[text] ? this._data[text] : null : text
    }
  },
  translate: function(text, placeholders) {
    if (!text || !text.length) return "";
    var translation = this.translations.get(text);
    if (null === translation) {
      this.notify("warning", "Missing translation for: <strong>" + text + "</strong>");
      var sentKeys = Aquarius.storage.get("translate.sent-keys") || {};
      this.translations.uploadDisabled || sentKeys[text] || (sentKeys[text] = 1, Aquarius.storage.set("translate.sent-keys", sentKeys), Aquarius.ajax({
        url: "/api/v1/system/translate/articles/external",
        type: "POST",
        data: {
          content: text,
          categories: this.translations.categoryId
        }
      })), translation = text
    }
    if (placeholders) {
      $.each(placeholders, function(placeholder, value) {
        translation = translation.replace("%" + placeholder + "%", value)
      });
      for (var matches; matches = /\{([a-zA-Z0-9\-\_]+)\:\s*([^\{\}]+)\}/g.exec(translation);) {
        var placeholder = matches[1],
          strings = matches[2].split(";"),
          string = "";
        if (strings.length)
          if (placeholders[placeholder]) {
            var number = placeholders[placeholder];
            string = "undefined" != typeof strings[number - 1] ? strings[number - 1] : strings[strings.length - 1]
          } else string = strings[0];
        translation = translation.replace(matches[0], string)
      }
    }
    return translation
  }
}), Aquarius = Aquarius.extend(Aquarius, {
  uniqueId: {
    _list: {},
    generate: function() {
      var id = "";
      do id = (Math.random() + 1).toString(36).substring(7); while (this._list[id]);
      return this._list[id] = !0, id
    }
  }
}), Aquarius.hub.define("Navigation", {
  setState: function(state) {
    var self = this;
    return self.broadcast("state.change", {
      state: state
    }), self.setUrl(state.data.url, !state.data.silent), self.state = state, self
  },
  getState: function() {
    return this.state
  },
  setUrl: function(url, broadcast) {
    var self = this;
    return self.url !== url && (self.url = url, broadcast !== !1 && self.broadcast("url.change", {
      url: self.url
    })), self
  }
}), Aquarius.module.define("Analytics", {
  init: function() {
    this.bindElements()
  },
  bindElements: function() {
    var self = this;
    $(document).on("click", "[data-track]", function() {
      self.send(self.getParams(this))
    })
  },
  send: function(params) {
    return "function" == typeof ga ? (ga("send", params), !0) : !1
  },
  getParams: function(element) {
    var $element = $(element);
    return params = {
      hitType: "event",
      eventAction: $element.data("track"),
      eventCategory: $element.data("track-category"),
      eventLabel: $element.data("track-label"),
      eventValue: $element.data("track-value")
    }
  }
}), Aquarius.module.define("Loader", {
  dataId: Aquarius.component.dataId,
  loadEventId: Aquarius.component.loadEventId,
  loaderDataId: "loader-" + Aquarius.component.dataId,
  events: [],
  factories: {},
  init: function() {
    var self = this;
    this.initFactories().done(function() {
      self.initListener(), self.load()
    })
  },
  initFactories: function() {
    var self = this,
      promises = [],
      deferred = $.Deferred();
    return self.factoryList = {}, $.each(this.factories, function(factoryId, componenentIds) {
      promises.push(Aquarius.component.load(factoryId, self.hub).done(function(factory) {
        $.each(componenentIds, function(i, componentId) {
          self.factoryList[componentId] = factory
        })
      }))
    }), $.when.apply($, promises).done(function() {
      deferred.resolve()
    }), deferred.promise()
  },
  initListener: function() {
    var self = this;
    self.hub.listen(self.events, Aquarius.debounce(10, function() {
      self.load()
    }))
  },
  load: function() {
    var self = this;
    $("[data-component]").sort(function(a, b) {
      var priorityA = $(a).data("component-priority"),
        priorityB = $(b).data("component-priority");
      return "undefined" == typeof priorityA ? "undefined" != typeof priorityB ? 1 : -1 : priorityB > priorityA ? 1 : -1
    }).each(function(i, element) {
      var $element = $(this),
        componentId = $element.data("component");
      if ($element.data(self.loaderDataId)) return !0;
      try {
        if (self.factoryList[componentId]) {
          var factory = self.factoryList[componentId];
          $element.data(self.loaderDataId, !0), factory.create({
            type: componentId,
            element: $element,
            options: $element.data("config")
          }).done(function(component) {
            $element.data(self.dataId, component), $element.trigger(self.loadEventId, [component])
          })
        } else $element.data(self.loaderDataId, !0), Aquarius.component.load(componentId, $element, $element.data("options")).done(function(component) {
          $element.data(self.dataId, component), $element.trigger(self.loadEventId, [component])
        })
      } catch (error) {
        throw Aquarius.notify("error", Aquarius.translate("Error while initiating component: **%name%**", {
          name: componentId
        })), error
      }
    })
  }
}), Aquarius.module.define("Navigation/Breadcrumbs", {
  holderSelector: "#content[data-breadcrumbs]",
  navigationSelector: ".nav-primary",
  breadcrumbs: [],
  init: function() {
    var self = this;
    return self.$navigation = $(self.navigationSelector), self.$holder = $(self.holderSelector), self.$element = self.findElement(), self.$element.length ? (self.setUri(self.$holder.data("uri")), self.backendUri = self.$holder.data("backend-uri"), self.append(Aquarius.translate("Home"), self.backendUri, "home"), self.hub.listen("content.load:after", function(data) {
      self.$element = self.findElement(), self.setUri(data.url), self.process(data.url)
    }), self.hub.listen("state.change", function(data) {
      data.state.data && self.isValid(data.state.data.breadcrumbs) && (self.breadcrumbs = data.state.data.breadcrumbs)
    }), void self.hub.listen("breadcrumbs.change", function() {
      Aquarius.storage.set(self.storageId, self.breadcrumbs)
    })) : !1
  },
  findElement: function() {
    return this.$holder.find("> .breadcrumb:first")
  },
  append: function(title, url, icon) {
    return this.breadcrumbs.push({
      title: title,
      url: url,
      icon: icon
    }), this
  },
  removeLast: function() {
    return this.breadcrumbs.pop(), this
  },
  prepend: function(title, url, icon) {
    return this.breadcrumbs.splice(1, 0, {
      title: title,
      url: url,
      icon: icon
    }), this
  },
  empty: function() {
    return this.breadcrumbs.splice(1, Number.MAX_VALUE), this
  },
  getItem: function(title, url, icon) {
    var $item = $('<a data-target="content"> ' + title + " </a>");
    return url && $item.attr("href", url), icon && $('<i class="fa fa-' + icon + '"></i>').prependTo($item), $("<li></li>").html($item)
  },
  setUri: function(uri) {
    return this.uri = uri, this.storageId = "navigation-state." + this.uri.hashCode() + ".breadcrumbs", this
  },
  isValid: function(breadcrumbs) {
    return breadcrumbs && breadcrumbs.length && breadcrumbs[breadcrumbs.length - 1].url === this.uri
  },
  process: function() {
    var self = this;
    if (!this.isValid(this.breadcrumbs)) {
      var $navLink = self.$navigation.find('a[href="' + this.uri + '"]:first');
      $navLink.length ? (self.empty(), $navLink.parents("li.active").find("> a").add($navLink).each(function() {
        var $link = $(this),
          icon = null,
          iconClass = $link.find(".icon").attr("class");
        if (iconClass) {
          var matches = iconClass.match(/fa-([a-zA-Z-]+)/);
          icon = matches && matches[1] ? matches[1] : null
        }
        self.append($link.find("span:last").text(), $link.attr("href"), icon)
      })) : Aquarius.storage.isAvailable() && Aquarius.storage.get(this.storageId) && (this.breadcrumbs = Aquarius.storage.get(this.storageId))
    }
    return this.build(), this
  },
  build: function() {
    var self = this;
    return this.$element.empty(), $.each(this.breadcrumbs, function(i, breadcrumb) {
      self.$element.append(self.getItem(breadcrumb.title, breadcrumb.url, breadcrumb.icon))
    }), this.hub.broadcast("breadcrumbs.change", {
      breadcrumbs: this.breadcrumbs
    }), this
  }
}), Aquarius.module.define("Navigation/Content", {
  cacheEnabled: !1,
  elementSelector: "#content",
  element: {},
  init: function() {
    var self = this;
    return self.$element = $(self.elementSelector), self.$element.length ? (self.url = self.$element.data("uri"), self.hub.setUrl(self.url, !1), self.hub.listen("url.change", function(data) {
      self.load(data.url)
    }), self.hub.listen(["ajax.load:after", "content.load:after"], function() {
      self.triggerEvent("load:after")
    }), self.hub.broadcast("content.load:after", {
      url: self.url,
      xhr: null,
      element: self.$element
    }), void(0 != self.$element.data("refresh") && $(document).on("keydown", function(event) {
      82 == event.keyCode && event.ctrlKey && (event.preventDefault(), self.refresh())
    }))) : !1
  },
  load: function(url, callback) {
    var cacheData, self = this,
      namespace = Aquarius.settings.get("content-cache-namespace"),
      $html = $("html"),
      companyId = void 0 !== $html.data("company-id") ? $html.data("company-id") : "",
      cache = Aquarius.storage.bag("content-cache", url.hashCode(), namespace + Aquarius.translations.getLocale(), companyId);
    self.cacheEnabled && (cacheData = cache.load()) && setTimeout(function() {
      self.handleLoad(url, cacheData.content, "success", null)
    }, 1);
    var xhr = Aquarius.ajax(url, {
      type: "GET",
      cache: !1,
      beforeSend: function(xhr) {
        cacheData && cacheData.eTag && xhr.setRequestHeader("If-None-Match", cacheData.eTag)
      },
      error: function(xhr, status, error) {
        self.hub.broadcast("content.load:after", {
          url: self.url,
          xhr: xhr,
          element: self.$element
        }), "function" == typeof callback && callback(url, null)
      },
      success: function(html, status, xhr) {
        304 !== xhr.status && (cacheData && cacheData.eTag && Aquarius.notify("warning", "Content ETag mismatch (Stored: " + cacheData.eTag + " New: " + xhr.getResponseHeader("ETag") + "), reloaded.."), self.handleLoad(url, html, status, xhr), cache.save({
          content: html,
          eTag: xhr.getResponseHeader("ETag")
        }))
      }
    });
    return self.hub.broadcast("content.load:before", {
      url: url,
      xhr: xhr,
      element: self.$element
    }), self
  },
  handleLoad: function(url, html, status, xhr) {
    var self = this;
    self.url = url, self.$element.data("uri", self.url), self.setContent(html), self.hub.broadcast("content.load:after", {
      url: self.url,
      xhr: xhr,
      element: self.$element
    }), "function" == typeof callback && callback(url, html)
  },
  triggerEvent: function(url) {
    var self = this;
    self.$element.trigger("load:after", url)
  },
  refresh: function(callback) {
    var self = this;
    return self.load(self.url, callback)
  },
  setContent: function(html) {
    var self = this;
    return self.$element.html(html), self
  }
}), Aquarius.module.define("Navigation/Cursor", {
  events: [],
  init: function() {
    var self = this;
    this.$overlayElement = $("<div>").css({
      position: "fixed",
      "z-index": 9999,
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      display: "none"
    }).appendTo("body"), $(document).on("keyup", function(event) {
      27 == event.which && self.close()
    }), $.each(this.events, function(i, event) {
      self.addEvent(event)
    })
  },
  addEvent: function(event) {
    var self = this;
    this.hub.listen(event.start, function() {
      self.open(event.cursor)
    }), this.hub.listen(event.end, function() {
      self.close()
    })
  },
  open: function(cursor) {
    this.$overlayElement.css("cursor", cursor).show()
  },
  close: function(cursor) {
    this.$overlayElement.css("cursor", "default").hide()
  }
}), Aquarius.module.define("Navigation/History", {
  init: function() {
    var self = this;
    Aquarius.scripts.load(Aquarius.scripts.getPath("vendor", "jquery-history/jquery.history.js")).done(function() {
      self.api = History, self.adapter = self.api.Adapter, self.api.replaceState({
        type: "init",
        silent: !1,
        url: location.href,
        breadcrumbs: []
      }, document.title, location.href), self.bind()
    })
  },
  bind: function() {
    var self = this;
    return self.adapter.bind(window, "statechange", function(event) {
      self.hub.setState(self.api.getState())
    }), self.hub.listen(["link.click", "link.open"], function(data) {
      self.api.pushState({
        type: "content",
        silent: data.silent ? !0 : !1,
        url: data.url
      }, document.title, data.url)
    }), self.hub.listen(["breadcrumbs.change"], function(data) {
      var state = self.hub.getState();
      state && (self.api.replaceState({
        type: state.data.type,
        silent: state.data.silent,
        url: state.data.url,
        breadcrumbs: data.breadcrumbs
      }, state.title, state.data.url), self.hub.setState(self.api.getState()))
    }), self
  }
}), Aquarius.module.define("Navigation/Link", {
  element: '[href][data-target="content"]',
  init: function() {
    var self = this;
    return self.bind(), self
  },
  bind: function() {
    var self = this;
    return $(document).on("click", self.element, function(event) {
      return event.ctrlKey || event.shiftKey || 2 === event.which ? !0 : (event.preventDefault(), self.hub.broadcast("link.click", {
        link: this,
        url: $(this).attr("href")
      }), !1)
    }), self
  }
}), Aquarius.module.define("Navigation/Menubar", {
  element: ".nav-primary",
  activeClass: "active",
  init: function() {
    var self = this;
    this.$elements = $(this.element).find("a[href]"), this.urls = [], this.$elements.each(function() {
      self.urls.push($(this).attr("href"))
    }), this.urls.sort(function(a, b) {
      return b.length - a.length
    }), self.hub.listen(["url.change", "content.load:after"], function(data) {
      var $element = self.findElement(data.url);
      self.closeAll(), $element.length && self.open($element)
    })
  },
  findElement: function(href) {
    var self = this,
      maxSimilarity = 0,
      $match = this.$elements.filter('[href="' + href + '"]').first();
    return $match.length || $.each(this.urls, function(i, url) {
      if (href.indexOf(url.replace(/(s)?\/$/, "")) >= 0) return $match = self.$elements.filter('[href="' + url + '"]').first(), !1;
      var similarity = self.getSimilarity(url, href);
      similarity > maxSimilarity && (maxSimilarity = similarity, $match = self.$elements.filter('[href="' + url + '"]').first())
    }), $match
  },
  getSimilarity: function(a, b) {
    for (var equivalency = 0, i = 0; i < Math.min(a.length, b.length); i++) a[i] == b[i] && equivalency++;
    return equivalency
  },
  closeAll: function() {
    return $(this.element).find("." + this.activeClass).removeClass(this.activeClass), this
  },
  open: function(element) {
    var self = this,
      $element = $(element),
      $li = $element.parents("li"),
      $a = $li.find("> a"),
      $ul = $a.next("ul");
    return $ul.length ? $ul.slideDown(300, function() {
      $li.addClass(self.activeClass), $a.addClass(self.activeClass)
    }) : ($li.addClass(self.activeClass), $a.addClass(self.activeClass)), this
  }
}), Aquarius.module.define("Navigation/State", {
  selector: '[data-stateful]:not([data-stateful-auto="false"])',
  dataKey: {
    type: "stateful",
    event: "stateful-event",
    id: "stateful-id",
    selector: "stateful-selector",
    state: "state"
  },
  baseId: "navigation-state",
  defaultEvent: "change",
  refreshEvent: "state.refresh",
  debounceDelay: 250,
  init: function() {
    var self = this;
    Aquarius.storage.isAvailable() && (this.$elements = $(), this.$content = null, this.url = null, this.urlHashCode = null, this.length = 0, self.hub.listen("content.load:after", function(data) {
      self.$content = $(data.element), self.url = data.url, self.urlHashCode = self.url.hashCode(), self.$elements = self.$content.find(self.selector), self.length = self.$elements.length, self.checkPage(), self.initElements()
    }), $(document).on(self.refreshEvent, self.selector, function(event) {
      event.stopPropagation(), $(this).each(function() {
        self.initElement(this)
      })
    }), $(window).on("keydown", function(event) {
      switch (event.ctrlKey && event.keyCode) {
        case 116:
          self.deletePage()
      }
    }), self.hub.listen("state.reset", function() {
      self.deletePage()
    }))
  },
  addEvent: function(event, element, callback) {
    var self = this,
      internalCallback = Aquarius.debounce(self.debounceDelay, function() {
        callback.apply(element, arguments)
      });
    switch (event) {
      case "attrchange":
        $(element).attrchange({
          callback: internalCallback
        });
        break;
      default:
        $(element).on(event, internalCallback)
    }
    return this
  },
  checkPage: function() {
    var self = this,
      idList = [],
      hashId = this.baseId + "." + this.urlHashCode + ".checkSum";
    this.$elements.each(function(i, element) {
      switch ($(element).data(self.dataKey.type)) {
        case "observer":
          break;
        default:
          idList.push(self.getElementId(element))
      }
    });
    var oldHash = Aquarius.storage.get(hashId),
      newHash = idList.join("|").hashCode();
    return oldHash !== newHash && this.deletePage(), Aquarius.storage.set(hashId, newHash), this
  },
  deletePage: function() {
    return Aquarius.storage.removeAll(new RegExp("^" + this.baseId + "." + this.urlHashCode + "\\.(.)*")), this.$elements.trigger("state.reset"), this
  },
  initElements: function() {
    var self = this;
    return this.$elements.each(function() {
      self.initElement(this)
    }), this
  },
  initElement: function(element) {
    var $element = $(element);
    this.initEvents($element), this.loadState($element)
  },
  initEvents: function($element) {
    var self = this,
      type = $element.data(this.dataKey.type),
      event = $element.data(this.dataKey.event) ? $element.data(this.dataKey.event) : this.defaultEvent;
    switch (type) {
      case "class":
        this.addEvent("attrchange", $element, function(event) {
          "class" == event.attributeName && self.save($element, $element.attr("class"))
        });
        break;
      case "observer":
        break;
      case "value":
      default:
        this.addEvent(event, $element, function() {
          self.save($element, $element.val())
        })
    }
  },
  loadState: function($element) {
    var self = this,
      type = $element.data(this.dataKey.type);
    switch (type) {
      case "class":
        self.load($element, function(value) {
          $element.attr("class", value.length ? value : "")
        });
        break;
      case "observer":
        break;
      case "value":
      default:
        self.load($element, function(value) {
          $element.val(value)
        })
    }
  },
  getElementSelector: function(element) {
    var self = this,
      $element = $(element),
      index = this.$elements.index($element);
    return $element.data(this.dataKey.selector) || (-1 === index && (this.$elements = this.$elements.add($element), index = this.length++), $element.data(this.dataKey.selector, self.selector + ":eq(" + index + ")")), $element.data(this.dataKey.selector)
  },
  getElementId: function(element) {
    var $element = $(element);
    if (!$element.data(this.dataKey.id)) {
      var html = $("<div>").append($element.clone().removeAttr("data-unique-id")).html();
      $element.data(this.dataKey.id, (this.getElementSelector($element) + html).hashCode())
    }
    return this.baseId + "." + this.urlHashCode + "." + $element.data(this.dataKey.id)
  },
  save: function(element, value) {
    var $element = $(element),
      elementId = this.getElementId($element);
    return Aquarius.storage.set(elementId, value), $element.data("state", value).trigger("state.set"), this.loadState($('[data-stateful-id="' + $element.data("stateful-id") + '"]').not($element)), this
  },
  load: function(element, callback) {
    var $element = $(element),
      elementId = this.getElementId($element);
    if (Aquarius.storage.has(elementId)) {
      var value = Aquarius.storage.get(elementId);
      return callback && callback.apply(this, [value]), $element.data("state", value).trigger("state.load"), value
    }
    return !1
  }
}), Aquarius.module.define("Notifications", {
  methods: {
    console: {
      enabled: !0,
      levels: {
        warning: !0,
        error: !0,
        info: !0,
        log: !0
      },
      functions: {
        error: "error",
        warning: "warn",
        info: "info",
        log: "log"
      }
    },
    flash: {
      enabled: !0,
      timeout: 5e3,
      levels: {
        warning: !1,
        error: !0,
        info: !0,
        success: !0
      },
      classes: {
        error: "alert-danger",
        warning: "alert-warning",
        info: "alert-info",
        success: "alert-success"
      }
    }
  },
  init: function() {
    var self = this;
    this.activeLevels = this.getActiveLevels(), this.hub.listen("notification", function(details) {
      activeLevels[details.level] && self.hub.broadcast(details.level, details)
    }), $.each(this.activeLevels, function(level) {
      self.hub.listen(level, function(details) {
        self.handle(level, details)
      })
    })
  },
  getActiveLevels: function() {
    var levels = {};
    return $.each(this.methods, function(methodName, method) {
      method.enabled && $.each(method.levels, function(level, enabled) {
        enabled && (levels[level] = !0)
      })
    }), levels
  },
  handle: function(level, data) {
    var self = this;
    return $.each(this.methods, function(methodName, method) {
      method.enabled && method.levels[level] && self.notification(methodName, level, data)
    }), this
  },
  notification: function(method, level, data) {
    if (!this.methods[method] || !this.methods[method].enabled || !this.methods[method].levels[level]) return this;
    var text = "";
    return "string" == typeof data ? text = data : "string" == typeof data.text && (text = data.text), text.length ? this[method](level, text) : this
  },
  console: function(level, text) {
    var functions = this.methods.console.functions;
    return "undefined" != typeof console && functions[level] && console[functions[level]] && (text = Aquarius.markdown.toHTML(text), console[functions[level]](text.replace(/(<([^>]+)>)/gi, ""))), this
  },
  flash: function(level, text, timeout) {
    var self = this;
    getElement = function(level, text) {
      var alert = $("<div>").addClass("alert " + self.methods.flash.classes[level]).attr("data-hash", (level + text).hashCode());
      return $("<button>").attr({
        type: "button",
        "class": "close",
        "data-dismiss": "alert"
      }).text("×").appendTo(alert), $("<span>").html($("<textarea></textarea>").html(Aquarius.markdown.toHTML(text)).val()).appendTo(alert), alert
    };
    var $element = getElement(level, text),
      hash = $element.data("hash"),
      $target = $(".alert-holder:visible");
    return $target.find('[data-hash="' + hash + '"]').remove(), timeout || (timeout = this.methods.flash.timeout), $target.each(function(i, alertHolder) {
      var $alert = $element.clone();
      $alert.hide().appendTo(alertHolder).fadeIn(500), $alert.data("timeout", setTimeout(function() {
        $alert.fadeOut(500, function() {
          $(this).remove()
        })
      }, timeout))
    }), this
  }
}), Aquarius.component.define("Target/Modal", function(holder, config) {
  function Target() {}
  var defaults = {
    backdrop: .8,
    keyboard: !0,
    mode: "dialog",
    title: null,
    animationSpeed: 250
  };
  return Target.prototype = {
    setup: function() {
      this.setOptions(defaults).setOptions(config), this.$holder = $(holder), this.active = !1, this.initTarget().initDismiss().initKeyboard()
    },
    initTarget: function() {
      var self = this;
      return $(document).on("click", '[data-target="modal"]', function(event) {
        return event.preventDefault(), self.load($(this).attr("href"), function() {
          self.hideLoader()
        }).open().showLoader(), !1
      }), this
    },
    initDismiss: function() {
      var self = this;
      return this.$holder.on("click", '[data-dismiss="modal"]', self.close), this
    },
    initKeyboard: function() {
      var self = this;
      return this.options.keyboard && $(document).on("keyup", function(event) {
        self.active && 27 === event.which && self.close()
      }), this
    },
    getModal: function() {
      return this.$holder.find('[data-modal-type="' + this.options.mode + '"]')
    },
    open: function(options) {
      return this.setOptions(options).setActive(!0), this.setTitle(this.options.title), this.getModal().stop().fadeIn(this.options.animationSpeed), this.getBackdrop().stop().fadeTo(this.options.animationSpeed, parseFloat(this.options.backdrop)), this
    },
    close: function() {
      return this.setActive(!1), $(this.getBackdrop()).add(this.getModal()).stop().fadeOut(this.options.animationSpeed), this
    },
    showLoader: function() {
      return this.getLoader().stop().fadeIn(100), this
    },
    hideLoader: function() {
      return this.getLoader().stop().fadeOut(100), this
    },
    setActive: function(active) {
      return this.active = Boolean(active), this
    },
    setOptions: function(options) {
      return this.options = Aquarius.extend(this.options, options), this
    },
    getElement: function() {
      return this.$holder.find('[data-modal-type="' + this.options.mode + '"] .modal-body')
    },
    getBackdrop: function() {
      return this.$holder.find(".modal-backdrop")
    },
    getLoader: function() {
      return this.getModal().find(".modal-loader .loader")
    },
    setTitle: function(title) {
      return this.getModal().find(".modal-title").html(title), this
    }
  }, Target
}), Aquarius.component.define("Target/Row", function(table, config) {
  function Target() {}
  var defaults = {},
    $element = $('<tr class="aquarius-target-row"><td colspan="6"><div class="target-element"></div></td></tr>');
  return Target.prototype = {
    setup: function() {
      this.setOptions(defaults).setOptions(config), this.$table = $(table)
    },
    initDismiss: function() {
      var self = this;
      return this.$table.on("select:after deselect:after draw.dt", function() {
        self.close()
      }), this
    },
    open: function(options) {
      return this.setOptions(options), $element.insertAfter(this.$table.find("> tbody > tr.selected")), this.initDismiss(), this
    },
    close: function() {
      return $element.remove(), this
    },
    showLoader: function() {
      return this
    },
    hideLoader: function() {
      return this
    },
    setOptions: function(options) {
      return this.options = Aquarius.extend(this.options, options), this
    },
    getElement: function() {
      return $(".target-element", $element)
    }
  }, Target
}), Aquarius.component.define("Target/Side", function(holder, config) {
  function Target() {}
  var defaults = {
    title: null
  };
  return Target.prototype = {
    setup: function() {
      this.setOptions(defaults).setOptions(config), this.$holder = $(holder), this.getWrapper().hide(), this.initActionHolder()
    },
    initActionHolder: function() {
      var self = this,
        $naButton = $('<a class="list-group-item disabled list-group-item-text">' + Aquarius.translate("no-available-actions") + "</a>");
      this.$actionHolder = this.$holder.find(".target-action-holder"), this.node.listen("action.refresh", Aquarius.debounce(10, function(data) {
        var $dropDown = $(data.element).clone(!0);
        self.$holder.find(".row-counter").html(data.itemCount ? data.itemCount : ""), self.$actionHolder.empty(), $dropDown.children().each(function(i, listItem) {
          var $button = $(listItem).find("[data-action]").addClass("list-group-item");
          $button && self.$actionHolder.append($button)
        }), self.$actionHolder.find("[data-action]:hidden").remove(), self.$actionHolder.find("[data-action]:visible").length && data.element || self.$actionHolder.empty().append($naButton), self.close()
      })), self.$actionHolder.empty().append($naButton)
    },
    open: function(options) {
      return this.setOptions(options), this.setTitle(this.options.title), this.getWrapper().stop().show(), this
    },
    close: function() {
      return this.getWrapper().stop().hide(), this
    },
    showLoader: function() {
      return this.getLoader().stop().fadeIn(300), this
    },
    hideLoader: function() {
      return this.getLoader().stop().fadeOut(300), this
    },
    getLoader: function() {
      return this.$holder.find(".loader")
    },
    setOptions: function(options) {
      return this.options = Aquarius.extend(this.options, options), this
    },
    getElement: function() {
      return $(".target-element", this.$holder)
    },
    getWrapper: function() {
      return this.getElement().parents(".target-element-wrapper")
    },
    setTitle: function(title) {
      return this.getWrapper().find(".target-title").html(title), this
    }
  }, Target
}), Aquarius.component.define("Target/Window", function(holder, config) {
  function Target() {}
  var defaults = {
    animationSpeed: 250
  };
  return Target.prototype = {
    setup: function() {
      this.setOptions(defaults).setOptions(config), this.$holder = $(holder), this.active = !1, this.holderScrollTop = null, this.initTarget(), this.initDismiss()
    },
    initTarget: function() {
      var self = this;
      return $(document).on("click", '[data-target="window"]', function(event) {
        return event.preventDefault(), self.load($(this).attr("href"), function() {
          self.hideLoader()
        }).open().showLoader(), !1
      }), this
    },
    initDismiss: function() {
      var self = this;
      return this.$holder.on("click", '[data-dismiss="window"]', self.close), this
    },
    open: function(options) {
      return this.setOptions(options).setActive(!0), this.holderScrollTop = this.$holder.scrollParent().scrollTop(), this.$holder.stop().fadeIn(this.options.animationSpeed).siblings().hide(), this
    },
    close: function() {
      return this.setActive(!1), this.$holder.stop().fadeOut(this.options.animationSpeed).siblings().show(), this.holderScrollTop && this.$holder.scrollParent().scrollTop(this.holderScrollTop).trigger("resize"), this
    },
    showLoader: function() {
      return this.getLoader().stop().fadeIn(100), this
    },
    hideLoader: function() {
      return this.getLoader().stop().fadeOut(100), this
    },
    setActive: function(active) {
      return this.active = Boolean(active), this
    },
    setOptions: function(options) {
      return this.options = Aquarius.extend(this.options, options), this
    },
    getElement: function() {
      return this.$holder.find(".window-body")
    },
    getLoader: function() {
      return this.$holder.find("> .loader")
    }
  }, Target
}), $(document).ready(function() {
  $(this).on("click.button.data-api", "[data-loading-text]", function(e) {
    var $this = $(e.target);
    $this.is("i") && ($this = $this.parent()), $this.button("loading")
  })
}), $(document).ready(function() {
  $("#content").on("load:after", function() {
    $(".carousel.auto").carousel()
  })
}), $(document).ready(function() {
  $(this).on("click", '[data-toggle^="class"]', function(e) {
    e && e.preventDefault();
    var $class, $target, $tmp, $classes, $targets, $this = $(e.target);
    !$this.data("toggle") && ($this = $this.closest('[data-toggle^="class"]')), $class = $this.data().toggle, $target = $this.data("target") || $this.attr("href"), $class && ($tmp = $class.split(":")[1]) && ($classes = $tmp.split(",")), $target && ($targets = $target.split(",")), $targets && $targets.length && $.each($targets, function(index, value) {
      "#" != $targets[index] && $($targets[index]).toggleClass($classes[index])
    }), $this.toggleClass("active")
  })
}), + function($) {
  "use strict";
  var Shift = function(element) {
    this.$element = $(element), this.$prev = this.$element.prev(), !this.$prev.length && (this.$parent = this.$element.parent())
  };
  Shift.prototype = {
    constructor: Shift,
    init: function() {
      var $el = this.$element,
        method = $el.data().toggle.split(":")[1],
        $target = $el.data("target");
      $el.hasClass("in") || $el[method]($target).addClass("in")
    },
    reset: function() {
      this.$parent && this.$parent.prepend(this.$element), !this.$parent && this.$element.insertAfter(this.$prev), this.$element.removeClass("in")
    }
  }, $.fn.shift = function(option) {
    return this.each(function() {
      var $this = $(this),
        data = $this.data("shift");
      data || $this.data("shift", data = new Shift(this)), "string" == typeof option && data[option]()
    })
  }, $.fn.shift.Constructor = Shift
}(jQuery), Date.now = Date.now || function() {
  return +new Date
}, Date.prototype.getMonthName = function() {
  var month_names = [Aquarius.translate("January"), Aquarius.translate("February"), Aquarius.translate("March"), Aquarius.translate("April"), Aquarius.translate("May"), Aquarius.translate("June"), Aquarius.translate("July"), Aquarius.translate("August"), Aquarius.translate("September"), Aquarius.translate("October"), Aquarius.translate("November"), Aquarius.translate("December")];
  return month_names[this.getMonth()]
}, Date.prototype.getMonthAbbr = function() {
  var month_abbrs = [Aquarius.translate("Jan"), Aquarius.translate("Feb"), Aquarius.translate("Mar"), Aquarius.translate("Apr"), Aquarius.translate("May_short"), Aquarius.translate("Jun"), Aquarius.translate("Jul"), Aquarius.translate("Aug"), Aquarius.translate("Sep"), Aquarius.translate("Oct"), Aquarius.translate("Nov"), Aquarius.translate("Dec")];
  return month_abbrs[this.getMonth()]
}, Date.prototype.getDayFull = function() {
  var days_full = [Aquarius.translate("Sunday"), Aquarius.translate("Monday"), Aquarius.translate("Tuesday"), Aquarius.translate("Wednesday"), Aquarius.translate("Thursday"), Aquarius.translate("Friday"), Aquarius.translate("Saturday"), Aquarius.translate("Sunday")];
  return days_full[this.getDay()]
}, Date.prototype.getDayAbbr = function() {
  var days_abbr = [Aquarius.translate("Sun"), Aquarius.translate("Mon"), Aquarius.translate("Tue"), Aquarius.translate("Wed"), Aquarius.translate("Thu"), Aquarius.translate("Fri"), Aquarius.translate("Sat"), Aquarius.translate("Sun")];
  return days_abbr[this.getDay()]
}, Date.prototype.getDayOfYear = function() {
  var onejan = new Date(this.getFullYear(), 0, 1);
  return Math.ceil((this - onejan) / 864e5)
}, Date.prototype.getDaySuffix = function() {
  var d = this.getDate(),
    sfx = ["th", "st", "nd", "rd"],
    val = d % 100;
  return sfx[(val - 20) % 10] || sfx[val] || sfx[0]
}, Date.prototype.getWeekOfYear = function() {
  var onejan = new Date(this.getFullYear(), 0, 1);
  return Math.ceil(((this - onejan) / 864e5 + onejan.getDay() + 1) / 7)
}, Date.prototype.isLeapYear = function() {
  var yr = this.getFullYear();
  if (parseInt(yr) % 4 == 0) {
    if (parseInt(yr) % 100 == 0) {
      if (parseInt(yr) % 400 != 0) return !1;
      if (parseInt(yr) % 400 == 0) return !0
    }
    if (parseInt(yr) % 100 != 0) return !0
  }
  return parseInt(yr) % 4 != 0 ? !1 : void 0
}, Date.prototype.getMonthDayCount = function() {
  var month_day_counts = [31, this.isLeapYear() ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  return month_day_counts[this.getMonth()]
}, Date.prototype.format = function(dateFormat) {
  dateFormat = dateFormat.split("");
  for (var date = this.getDate(), month = this.getMonth(), hours = this.getHours(), minutes = this.getMinutes(), seconds = this.getSeconds(), date_props = {
      d: 10 > date ? "0" + date : date,
      D: this.getDayAbbr(),
      j: this.getDate(),
      l: this.getDayFull(),
      S: this.getDaySuffix(),
      w: this.getDay(),
      z: this.getDayOfYear(),
      W: this.getWeekOfYear(),
      F: this.getMonthName(),
      m: 9 > month ? "0" + (month + 1) : month + 1,
      M: this.getMonthAbbr(),
      n: month + 1,
      t: this.getMonthDayCount(),
      L: this.isLeapYear() ? "1" : "0",
      Y: this.getFullYear(),
      y: this.getFullYear() + "".substring(2, 4),
      a: hours > 12 ? "pm" : "am",
      A: hours > 12 ? "PM" : "AM",
      g: hours % 12 > 0 ? hours % 12 : 12,
      G: hours > 0 ? hours : "12",
      h: hours % 12 > 0 ? hours % 12 : 12,
      H: 10 > hours ? "0" + hours : hours,
      i: 10 > minutes ? "0" + minutes : minutes,
      s: 10 > seconds ? "0" + seconds : seconds
    }, date_string = "", i = 0; i < dateFormat.length; i++) {
    var f = dateFormat[i];
    date_string += f.match(/[a-zA-Z]/g) ? date_props[f] ? date_props[f] : "" : f
  }
  return date_string
}, $(document).ready(function() {
  $.fn.datepicker && $.fn.datepicker.dates && ($.fn.datepicker.dates.aq = {
    days: [Aquarius.translate("Sunday"), Aquarius.translate("Monday"), Aquarius.translate("Tuesday"), Aquarius.translate("Wednesday"), Aquarius.translate("Thursday"), Aquarius.translate("Friday"), Aquarius.translate("Saturday"), Aquarius.translate("Sunday")],
    daysShort: [Aquarius.translate("Sun"), Aquarius.translate("Mon"), Aquarius.translate("Tue"), Aquarius.translate("Wed"), Aquarius.translate("Thu"), Aquarius.translate("Fri"), Aquarius.translate("Sat"), Aquarius.translate("Sun")],
    daysMin: [Aquarius.translate("Su"), Aquarius.translate("Mo"), Aquarius.translate("Tu"), Aquarius.translate("We"), Aquarius.translate("Th"), Aquarius.translate("Fr"), Aquarius.translate("Sa"), Aquarius.translate("Su")],
    months: [Aquarius.translate("January"), Aquarius.translate("February"), Aquarius.translate("March"), Aquarius.translate("April"), Aquarius.translate("May"), Aquarius.translate("June"), Aquarius.translate("July"), Aquarius.translate("August"), Aquarius.translate("September"), Aquarius.translate("October"), Aquarius.translate("November"), Aquarius.translate("December")],
    monthsShort: [Aquarius.translate("Jan"), Aquarius.translate("Feb"), Aquarius.translate("Mar"), Aquarius.translate("Apr"), Aquarius.translate("May_short"), Aquarius.translate("Jun"), Aquarius.translate("Jul"), Aquarius.translate("Aug"), Aquarius.translate("Sep"), Aquarius.translate("Oct"), Aquarius.translate("Nov"), Aquarius.translate("Dec")],
    today: Aquarius.translate("Today"),
    clear: Aquarius.translate("Clear")
  }, $.fn.datepicker.DPGlobal.formatDate = function(date, format, language) {
    return date.format(format)
  })
}), $(document).ready(function() {
  $.fn.dropdown && ($.fn.dropdown.Constructor.prototype.change = function(e) {
    e.preventDefault();
    var $select, $menu, $label, $item = $(e.target),
      $checked = !1;
    !$item.is("a") && ($item = $item.closest("a")), $menu = $item.closest(".dropdown-menu"), $label = $menu.parent().find(".dropdown-label"), $labelHolder = $label.text(), $select = $item.find("input"), $checked = $select.is(":checked"), $select.is(":disabled") || "radio" == $select.attr("type") && $checked || ("radio" == $select.attr("type") && $menu.find("li").removeClass("active"), $item.parent().removeClass("active"), !$checked && $item.parent().addClass("active"), $select.prop("checked", !$select.prop("checked")), $items = $menu.find("li > a > input:checked"), $items.length ? ($text = [], $items.each(function() {
      var $str = $(this).parent().text();
      $str && $text.push($.trim($str))
    }), $text = $text.length < 4 ? $text.join(", ") : $text.length + " selected", $label.html($text)) : $label.html($label.data("placeholder")))
  }, $(this).on("click.dropdown-menu", ".dropdown-select > li > a", $.fn.dropdown.Constructor.prototype.change), $(this).on("click.bs.dropdown.data-api", ".dropdown .on, .dropup .on", function(e) {
    e.stopPropagation()
  }))
}), $(document).ready(function() {
  $(this).on("click", '[data-toggle="fullscreen"]', function(e) {
    screenfull.enabled && screenfull.request()
  })
}), $(document).ready(function() {
  var $window = $(window),
    mobile = function(option) {
      return "reset" == option ? ($('[data-toggle^="shift"]').shift("reset"), !0) : (scrollToTop(), $('[data-toggle^="shift"]').shift("init"), !0)
    },
    fixVbox = function() {
      $(".ie11 .vbox").each(function() {
        $(this).height($(this).parent().height())
      })
    };
  fixVbox(), $window.width() < 768 && mobile();
  var $resize;
  $window.resize(function() {
    clearTimeout($resize), $resize = setTimeout(function() {
      $window.width() < 767 && mobile(), $window.width() >= 768 && mobile("reset") && fixVbox()
    }, 500)
  })
}), $(document).ready(function() {
  $(this).on("click", ".nav-primary a", function(e) {
    var $active, $this = $(e.target);
    $this.is("a") || ($this = $this.closest("a")), $(".nav-vertical").length || ($active = $this.parent().siblings(".active"), $active && $active.find("> a").toggleClass("active") && $active.toggleClass("active").find("> ul:visible").slideUp(200), $this.hasClass("active") ? $this.next().slideUp(200) : $this.next().slideDown(200), $this.toggleClass("active").parent().toggleClass("active"), $this.next().is("ul") && e.preventDefault())
  })
}), $(document).ready(function() {
  $(this).on("click", ".panel-toggle", function(e) {
    e && e.preventDefault();
    var $target, $this = $(e.target),
      $class = "collapse";
    $this.is("a") || ($this = $this.closest("a")), $target = $this.closest(".panel"), $target.find(".panel-body").toggleClass($class), $this.toggleClass("active")
  })
}), $(document).ready(function() {
  $("#content").on("load:after", function() {
    $("input[placeholder], textarea[placeholder]").placeholder()
  })
}), $(document).ready(function() {
  $("#content").on("load:after", function() {
    $('[data-toggle="popover"]').popover()
  }), $(this).on("click", ".popover-title .close", function(e) {
    var $target = $(e.target),
      $popover = $target.closest(".popover").prev();
    $popover && $popover.popover("hide")
  })
});
var scrollToTop = function() {
  !location.hash && setTimeout(function() {
    pageYOffset || window.scrollTo(0, 0)
  }, 1e3)
};
$(document).ready(function() {
  $("#content").on("load:after", function() {
    $("[data-toggle=tooltip]").tooltip()
  })
}), String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1)
}, String.prototype.hashCode = function() {
  function hashFnv32a(str, asString, seed) {
    var i, l, hval = void 0 === seed ? 2166136261 : seed;
    for (i = 0, l = str.length; l > i; i++) hval ^= str.charCodeAt(i), hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
    return asString ? ("0000000" + (hval >>> 0).toString(16)).substr(-8) : hval >>> 0
  }
  return hashFnv32a(this, !0)
};
