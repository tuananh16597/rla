$(document).on('click', '.search-button', function(e) {
  e.preventDefault();
  $('.search-overlay').toggle(function() {
    $(this).css('display', 'block');
  })
})
$(document).on('click', '.close-search-modal', function() {
  $('.search-overlay').css('display', 'none');
})
$(document).on('click', '.close-cookie-consent', function() {
  $('.cookie-consent-bar').css('display', 'none');
})
$(window).scroll(function (event) {
  var height = $(window).scrollTop();
  if ($(window).width() > 768) {
    if(height > $('.carousel-inner').height() - 100) {
      $('.main-header.header-fixed .header-content').css('z-index', '6');
      $('.main-header.header-fixed .header-content').css('display', 'block');
      $('.ul-fixed').css('top', '25%');
    } else {
      $('.main-header.header-fixed .header-content').css('z-index', '2');
      $('.main-header.header-fixed .header-content').css('display', 'none');
    }
  }
});
$(window).on('load', function() {
  $('#preloader').fadeOut(300);
})
$(document).ready(function() {
  var windowsize = $(window).width();

  $(window).resize(function() {
    windowsize = $(window).width();
    if (windowsize < 768) {
      $('.main-header.header-fixed .header-content').removeClass('header-noheight');
    }
  });

  var current_wheel = 2;

  $('#first-carousel').carousel({
    interval: 3000,
    pause: false
  });
  $('#first-carousel').carousel('cycle');

  $('#second-carousel').carousel({
    interval: 5000,
    pause: false
  });
  $('#second-carousel').carousel('cycle');


  $('.service-wheel-slice').on('click', function() {
    $('.service-wheel-slice.active').removeClass('active');
    $(this).addClass('active');
    $('.service-wheel-background-fader').css('transform', 'rotate(' + $(this).data('deg') + 'deg)');
    $('.service-wheel-content.active').removeClass('active');
    $('.wheel-tab-' + $(this).data('index')).addClass('active');
    current_wheel = $(this).data('index');
  })
  setInterval(function() {
    $('.service-wheel-slice[data-index=' + (current_wheel + 1) + ']').trigger('click');
    if (current_wheel == 4) {
      current_wheel = 0;
    }
  }, 3000);


  $('#third-carousel').slick({
    autoplay: true,
    autoplaySpeed: 3000,
    cssEase: 'linear',
    initialSlide: 1,
    infinite: false,
    centerMode: true,
    centerPadding: '0',
    slidesToShow: 3,
    dots: false,
    prevArrow: '<div class="carousel-control left transition-xs">\n' + '        <i class="fa fa-2x fa-chevron-left"></i>\n' + '    </div>',
    nextArrow: '<div class="carousel-control right transition-xs">\n' + '        <i class="fa fa-2x fa-chevron-right"></i>\n' + '    </div>',
    responsive: [{
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 3
      }
    }, {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }]
  });
})
$('.open-contact').on('click', function() {
  $(this).toggleClass('show-class');
  $('.contact-form').toggleClass('show-class');
})
$('.close-form').on('click', function() {
  $('.contact-form').toggleClass('show-class');
  $('.open-contact').toggleClass('show-class');
})
$('.navigation-toggle').on('click', function() {
  $('.mobile-menu').toggleClass('open');
})
$('li').off('mouseover');
$('.has-1-levels').on('click', function(e) {
  e.preventDefault();
})
